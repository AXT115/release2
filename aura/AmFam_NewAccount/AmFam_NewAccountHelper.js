({
    getExistingRecordTypeId : function(component, helper) {
        var action = component.get("c.getExistingRecordType");

        var actionParams = {
            "recordId" : component.get('v.recordId')
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                component.set('v.recordTypeId',resp);
                helper.initRecordTypes(component, helper);
            }
        });

        $A.enqueueAction(action);
    },

    initRecordTypes : function(component, helper) {
        var action = component.get("c.initRecordTypes");
        var recordTypeId;
        if (component.get('v.recordTypeId') != null) {
            recordTypeId = component.get('v.recordTypeId');
        } else {
            recordTypeId = component.get("v.pageReference").state.recordTypeId;
            component.set("v.recordTypeId",recordTypeId);
        }

        var actionParams = {
            "objectName" : 'Account'
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                for (var key in resp) {
                    switch(key) {
                        /*case 'Individual':
                            if (recordTypeId == resp[key]) {
                                component.set("v.isPersonAccount",true);
                                break;
                            }*/
                        case 'IndustriesBusiness':
                            if (recordTypeId == resp[key]) {
                                component.set("v.isBusinessAccount",true);
                                break;
                            }
                        case 'PersonAccount':
                            if (recordTypeId == resp[key]) {
                                component.set("v.isPersonAccount",true);
                                break;
                            }
                    }
                }
                component.set("v.isLoading",false);
            }
        });

        $A.enqueueAction(action);
    },
    
    initPrepopulateAgencyForPA : function(component, helper) {
        var action = component.get("c.prepopulateAgencyForPersonAccount");
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        action.setParams({ "userId" : userId });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                component.set("v.AgencyId", resp); 
            }
        });

        $A.enqueueAction(action);
        
    },

    sendAccountToCDH : function(component, accId) {
        var action = component.get("c.sendPersonAccountToCDH");
        var copyFlag = component.find('addressCheckbox').get('v.checked');
        var selectedHousehold = component.get("v.selectedHousehold");
        var selectedResidentialAddress = null;
        var selectedMailingAddress = null;
        if(component.get("v.selectedResidentialAddress") && component.get("v.selectedResidentialAddress").length >0){
            selectedResidentialAddress = JSON.stringify(component.get("v.selectedResidentialAddress"));
        }
        if(component.get("v.selectedMailingAddress") && component.get("v.selectedMailingAddress").length >0){
            selectedMailingAddress = JSON.stringify(component.get("v.selectedMailingAddress"));
        }
        var actionParams = {
            "code1Complete":component.get("v.code1ResultValidation"),
            "accId" : accId,
            "acc" : component.get('v.account'),
            "copyResToMailing" : copyFlag,
            "selectedHousehold" : selectedHousehold,
            "applyHouseholdMatch" : component.get("v.applyHouseholdMatch"),
            "applyPartyMatch" : component.get("v.applyPartyMatch"),
            "selectedResidentialAddress":selectedResidentialAddress,
            "selectedMailingAddress":selectedMailingAddress
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // If successful callout, redirect
                if (resp.acc.CDHID__c != null) {
                    this.redirectToRecord(component, resp.acc.Id);
                } else {
                    // Otherwise report back any CDH responses to the component
                    component.set("v.account",resp.acc);
                    component.set("v.errors",resp.errors);
                    component.set("v.code1Results",resp.code1);
                    if(JSON.stringify(resp.errors).includes('D-112') || JSON.stringify(resp.errors).includes('D-113')){
                        component.set("v.validAddress", false);
                    }else{
                        component.set("v.validAddress", true);
                        if(resp.code1 != null){
                            component.set("v.code1ResultValidation", true);
                            component.set("v.residentialAddressList",resp.selectedResidentialAddress.Code1Wrapper.AddressesReturnedFromCode1);
                            if(resp.selectedResidentialAddress.Code1Wrapper.AddressesReturnedFromCode1.length==1 && JSON.stringify(resp.selectedResidentialAddress).includes('OVERRIDE')){
                                component.set("v.onlyOverrideResidential", true);
                            }
                            if(!copyFlag){
                            	component.set("v.mailingAddressList",resp.selectedMailingAddress.Code1Wrapper.AddressesReturnedFromCode1);
                                if(resp.selectedMailingAddress.Code1Wrapper.AddressesReturnedFromCode1.length==1 && JSON.stringify(resp.selectedMailingAddress).includes('OVERRIDE')){
                                	component.set("v.onlyOverrideMailing", true);
                                }
                            }  
                        }
                    }
                    component.set("v.partyMatches",resp.partyMatch);
                    component.set("v.householdMatches",resp.householdMatch);
                    
                    if ((resp.code1 && component.get("v.validAddress")) || resp.partyMatch || resp.householdMatch) {
                        $A.util.addClass(component.find('accountForm'),'slds-hide');
                    }
                    if (resp.exactMatches) {
                        component.set("v.exactMatch",true);
                    }
                    component.set("v.isLoading",false);
                    this.scrollToTop();
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: "+errors[0].message);
                        this.handleShowNotice(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.isLoading",false);
                this.scrollToTop();
            }
        });

        $A.enqueueAction(action);
    },

    createPersonAccountRecord : function(component) {
        console.log('inside createPersonAccount');
        var action = component.get("c.createPersonAccount");
        var account = component.get('v.account');
        var actionParams = {
            "acc" : account
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                this.sendAccountToCDH(component, resp.Id);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: "+errors[0].message);
                        this.handleShowNotice(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.isLoading",false);
                this.scrollToTop();
            }
        });

        $A.enqueueAction(action);
    },

    editPersonAccount : function(component, event, account) {
        var action = component.get("c.sendPersonAccountEditToCDH");
        account.Id = component.get("v.recordId");
        var actionParams = {
            "acc" : account
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // If Party Version was incremented, callout was successful, so redirect
                if (resp.errors) {
                    // Otherwise report back any CDH responses to the component
                    component.set("v.account",resp.acc);
                    component.set("v.errors",resp.errors);
                    component.set("v.code1Results",resp.code1);
                    component.set("v.partyMatches",resp.partyMatch);
                    if (resp.code1 || resp.partyMatch || resp.householdMatch) {
                        $A.util.addClass(component.find('accountForm'),'slds-hide');
                    }
                    if (resp.exactMatches) {
                        component.set("v.exactMatch",true);
                    }
                    component.set("v.isLoading",false);
                    this.scrollToTop();
                } else {
                    this.redirectToRecord(component, component.get("v.recordId"));
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: "+errors[0].message);
                        this.handleShowNotice(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.isLoading",false);
                this.scrollToTop();
            }
        });

        $A.enqueueAction(action);
    },

    initDataTableColumns : function(component, helper) {
        component.set('v.partyMatchColumns', [
            {label: 'First Name', fieldName: 'fName', type: 'text'},
            {label: 'Middle Name', fieldName: 'mName', type: 'text'},
            {label: 'Last Name', fieldName: 'lName', type: 'text'},
            {label: 'Age', fieldName: 'age', type: 'text'},
            {label: 'Primary Mailing State', fieldName: 'primaryState', type: 'text'},
            {label: 'SSN', fieldName: 'SSN', type: 'text'},
            {label: 'FEIN', fieldName: 'FEIN', type: 'text'},
            {label: 'Driver\'s License', fieldName: 'driversLicense', type: 'text'},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Match', name: 'select_match', title: 'Click to select this match'}}
        ]);

        component.set('v.partyMatchBusinessColumns', [
            {label: 'Name', fieldName: 'fName', type: 'text'},
            {label: 'Address', fieldName: 'address', type: 'text'},
            {label: 'Form Of Business', fieldName: 'formOfBusiness', type: 'text'},
            {label: 'Taxpayer ID', fieldName: 'taxpayerID', type: 'text'},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Match', name: 'select_business_match', title: 'Click to select this match'}}
        ]);

         component.set('v.addressMatchColumns',[
            {label: 'Address Type', fieldName: 'Code1ReturnIndicator', type: 'text', wrapText: true, iconName: 'standard:marketing_actions'}, 
            {label: 'Street', fieldName: 'addressLine1', type: 'text', iconName: 'standard:visits'},
            //{label: 'Address Line 2', fieldName: 'addressLine2', type: 'text'},
            {label: 'City', fieldName: 'city', type: 'text', iconName: 'standard:service_territory'},
            {label: 'State', fieldName: 'stateCode', type: 'text', iconName: 'standard:location_permit'},
            {label: 'Zip', fieldName: 'zip5Code', type: 'text', iconName: 'standard:location'},
            {label: 'Confirmed Delivery Point?', fieldName: 'deliveryPointValidationMatchIndicator', type: 'text', wrapText: true, iconName: 'standard:home'},
            {label: 'Commercial Mail Receiving Agency?', fieldName: 'deliveryPointValidationCommercialMailreceivingCode', type: 'text', wrapText: true, iconName: 'standard:store'}
            //{label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Address', name: 'select_address', title: 'Click to select this address', iconName: 'standard:address', iconPosition: 'left' }}
        ]);


        component.set('v.householdMatchColumns',[
            {label: 'Household Name', fieldName: 'householdName', type: 'text'},
            {label: 'Household Greeting Name', fieldName: 'householdGreetingName', type: 'text'},
            {label: 'Household Members', fieldName: 'additionalHouseholdMembers', type: 'text', wrapText: true},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Household', name: 'select_household_match', title: 'Click to select this household'}}
        ]);
    },

    select_match : function(component, row) {
        var action = component.get("c.selectPersonPartyMatch");
        
        var actionParams = {
            "selectedRow" : JSON.stringify(row),
            "acc" : component.get("v.account")
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // Redirect to existing Account
                this.redirectToRecord(component, resp);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: "+errors[0].message);
                        this.handleShowNotice(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.isLoading",false);
                this.scrollToTop();
            }
        });

        $A.enqueueAction(action);
    },
    
    select_address: function(component, residentialRecord, mailingRecord) {
        
        console.log('inside createPersonAccount after address select');
        
        var action = component.get("c.createPersonAccount");
        var account = component.get('v.account');
        
        var actionParams = {
            "acc" : account,
            "selectedResidentialRow":residentialRecord,
            "selectedMailingRow":mailingRecord
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                this.sendAccountToCDH(component, resp.Id);
            }
        });

        $A.enqueueAction(action);
    },
    
    select_business_address: function(component, residentialRecord, mailingRecord) {
        
        console.log('inside create Business Account after address select');
        
        var action = component.get("c.createBusinessAccount");
        var account = component.get('v.account');
        
        var actionParams = {
            "acc" : account,
            "selectedResidentialRow":residentialRecord,
            "selectedMailingRow":mailingRecord
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                this.sendBusinessAccountToCDH(component, resp.Id);
            }
        });

        $A.enqueueAction(action);
    },
       

    select_business_match : function(component, row) {
        var action = component.get("c.selectBusinessPartyMatch");
        
        var actionParams = {
            "selectedRow" : row,
            "acc" : component.get("v.account")
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // Redirect to existing Account
                this.redirectToRecord(component, resp);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: "+errors[0].message);
                        this.handleShowNotice(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.isLoading",false);
                this.scrollToTop();
            }
        });

        $A.enqueueAction(action);
    },

    select_household_match : function(component, row) {
        var selectedHousehold = JSON.stringify(row);
        component.set('v.selectedHousehold', selectedHousehold);
        this.createPersonAccountRecord(component);
    },

    sendBusinessAccountToCDH : function(component, accId) {

        var selectedResidentialAddress = null;
        var selectedMailingAddress = null;
        if(component.get("v.selectedResidentialAddress") && component.get("v.selectedResidentialAddress").length >0){
            selectedResidentialAddress = JSON.stringify(component.get("v.selectedResidentialAddress"));
        }
        if(component.get("v.selectedMailingAddress") && component.get("v.selectedMailingAddress").length >0){
            selectedMailingAddress = JSON.stringify(component.get("v.selectedMailingAddress"));
        }
        var action = component.get("c.sendBusinessAccountToCDH");
        console.log(component.find('addressCheckbox').get('v.checked'));
        var actionParams = {
            "code1Complete":component.get("v.code1ResultValidation"),
            "accId" : accId,
            "acc" : component.get('v.account'),
            "applyPartyMatch" : component.get("v.applyPartyMatch"),
            "copyLocationToMailing" : component.find('addressCheckbox').get('v.checked'),
            "selectedResidentialAddress":selectedResidentialAddress,
            "selectedMailingAddress":selectedMailingAddress
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // If successful, redirect to new business account
                if (resp.acc.CDHID__c != null) {
                    this.redirectToRecord(component, resp.acc.Id);
                } else {
                    component.set("v.account",resp.acc);
                    component.set("v.errors",resp.errors);
                    component.set("v.code1Results",resp.code1);
                    if(JSON.stringify(resp.errors).includes('D-112') || JSON.stringify(resp.errors).includes('D-113')){
                        component.set("v.validAddress", false);
                    }else{
                        component.set("v.validAddress", true);
                        if(resp.code1 != null){
                            component.set("v.code1ResultValidation", true);
                            component.set("v.residentialAddressList",resp.selectedResidentialAddress.Code1Wrapper.AddressesReturnedFromCode1);
                            if(resp.selectedResidentialAddress.Code1Wrapper.AddressesReturnedFromCode1.length==1 && JSON.stringify(resp.selectedResidentialAddress).includes('OVERRIDE')){
                                component.set("v.onlyOverrideResidential", true);
                            }
                                if(!component.find('addressCheckbox').get('v.checked')){
                                	component.set("v.mailingAddressList",resp.selectedMailingAddress.Code1Wrapper.AddressesReturnedFromCode1);
                                    if(resp.selectedMailingAddress.Code1Wrapper.AddressesReturnedFromCode1.length==1 && JSON.stringify(resp.selectedMailingAddress).includes('OVERRIDE')){
                                		component.set("v.onlyOverrideMailing", true);
                                    }
                                }  
                        }
                    }
                    component.set("v.partyMatches",resp.partyMatch);
                    component.set("v.householdMatches",resp.householdMatch);
                    if ((resp.code1 && component.get("v.validAddress")) || resp.partyMatch || resp.householdMatch) {
                        $A.util.addClass(component.find('accountForm'),'slds-hide');
                    }
                    if (resp.exactMatches) {
                        component.set("v.exactMatch",true);
                    }
                    component.set("v.isLoading",false);
                    scrollToTop();
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: "+errors[0].message);
                        this.handleShowNotice(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.isLoading",false);
                this.scrollToTop();
            }
        });

        $A.enqueueAction(action);
    },

    createBusinessAccountRecord : function(component) {
        console.log('inside createBusinessAccountRecord');

        var action = component.get("c.createBusinessAccount");
        var account = component.get('v.account');
        var actionParams = {
            "acc" : account
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                this.sendBusinessAccountToCDH(component, resp.Id);
            }
        });

        $A.enqueueAction(action);
    },

    editBusinessAccount : function(component, event, account) {
        var action = component.get("c.sendBusinessAccountEditToCDH");
        account.Id = component.get("v.recordId");
        var actionParams = {
            "acc" : account
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // If successful, redirect to new business account
                if (resp.errors) {
                    component.set("v.account",resp.acc);
                    component.set("v.errors",resp.errors);
                    component.set("v.code1Results",resp.code1);
                    component.set("v.partyMatches",resp.partyMatch);
                    component.set("v.householdMatches",resp.householdMatch);
                    if (resp.code1 || resp.partyMatch || resp.householdMatch) {
                        $A.util.addClass(component.find('accountForm'),'slds-hide');
                    }
                    if (resp.exactMatches) {
                        component.set("v.exactMatch",true);
                    }
                    component.set("v.isLoading",false);
                    scrollToTop();
                } else {
                    this.redirectToRecord(component, resp.acc.Id);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: "+errors[0].message);
                        this.handleShowNotice(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.isLoading",false);
                this.scrollToTop();
            }
        });

        $A.enqueueAction(action);
    },

    redirectToRecord : function(component, recordId) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            pageReference: {
                "type": "standard__recordPage",
                "attributes": {
                    "recordId": recordId,
                    "objectApiName": "Account",
                    "actionName": "view"
                }
            },
            focus: true
        }).then(function(newTabId){
            workspaceAPI.getEnclosingTabId()
            .then(function(enclosingTabId){
                workspaceAPI.closeTab({tabId : enclosingTabId});
                workspaceAPI.refreshTab({tabId : newTabId, includeAllSubtabs: true});
                workspaceAPI.focusTab(newTabId);
            });
        });
    },

    scrollToTop : function() {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    },

    initYearOptions : function(component, helper) {
        var yearOptions = [];
        let currentYear = new Date().getFullYear();
        let earliestYear = 1912;
        yearOptions.push({ id: null, label: '-- None --'})
        while (currentYear >= earliestYear) {
            let dateOption = {};
            dateOption.id = currentYear;
            dateOption.label = currentYear;
            yearOptions.push(dateOption);
            currentYear -= 1;
        }
        component.set("v.yearOptions",yearOptions);
    },

    initGenderOptions : function(component, helper) {
        var genderOptions = [];
        genderOptions.push({ id: null, label: '-- None --'});
        genderOptions.push({ id: 'M', label: 'Male'});
        genderOptions.push({ id: 'F', label: 'Female'});
        var residentState = component.find('residentState');
        console.log(residentState);
        if (residentState) {
            if (residentState.get('v.value') == 'OR' || residentState.get('v.value') == 'CA') {
                genderOptions.push({ id: 'NB', label: 'Nonbinary(not exclusively male or female)'});
            }
        }
        component.set("v.genderOptions",genderOptions);
    },

    handleShowNotice : function(component, message) {
        component.find("notifLib").showNotice({
            "variant": "error",
            "header": "Something has gone wrong!",
            "message": message,
            closeCallback: function(){}
        });
    },

    setResidentStateFromBillingState : function(component, event, helper) {
        let billingComponent = component.find('resAddr');
        if (billingComponent) {
            let billingAddress = billingComponent.get('v.value');
            if (billingAddress.BillingStateCode) {
                let resState = component.find('residentState');
                if (resState) {
                    if (resState.get('v.value') != billingAddress.BillingStateCode) {
                        resState.set('v.value',billingAddress.BillingStateCode);
                    }
                }
            }
        }
    },

    validateForm : function(component, event, helper, isBusinessAccount, isEdit) {
        var fields = event.getParam('fields');
        var fieldvalue = '';
        var message = [];
        var specialCharactersAllowed = new RegExp('^[A-Za-z0-9-@#$+=!;:\'./?,& ]*[A-Za-z0-9]+[A-Za-z0-9-@#$+=!;:\'./?,& ]*$');
        var specialCharactersAllowedHyphen = new RegExp('^[A-Za-z-0-9 ]*[A-Za-z]+[A-Za-z-0-9 ]*$');
        var specialCharactersAllowedHyphenApostrophe = new RegExp('^[A-Za-z-\' ]*[A-Za-z]+[A-Za-z-\' ]*$');
        var specialCharactersAllowedHyphenCommaAmpersand = new RegExp('^[A-Za-z-,& ]*[A-Za-z]+[A-Za-z-,& ]*$');
        var onlyHyphens = new RegExp('^[- ]*$');
        var regex9Numbers = /[0-9]{9}/;
        var testDate = new Date('1900-01-01');
        var today = new Date();

        console.log(JSON.parse(JSON.stringify(fields)));

        if (isBusinessAccount) {  // Business Account Validations
            //Business Account Name can't have spl chars besides @#$+=!;:\'./?,&
            if(!specialCharactersAllowed.test(fields.Name)){
                message.push('Account Name cannot have any special characters besides - @ # $ + = ! ; : \' . / ? , & and cannot be only these special characters');
            }
            //Account Name should not exceed 100 chars
            if(fields.Name.length > 100){
                message.push('Account Name should be less than 100 characters');
            }
            //SSN validation 
            if(fields.SSN__c){
                if (fields.SSN__c != 'XXXXX' + fields.Last_4_SSN__c) {  // Validate if the SSN field is not the masked value
                    //should not contain all 0's in any section 
                    var regexCheckValid = new RegExp('^(?!000|666)[0-9]{3}(?!00)[0-9]{2}(?!0000)[0-9]{4}$');
                    //check the same number repeating 8 times
                    var regexCheckAll9SameNumber = /^(\d)\1{8}$/g; 
                    //check repeating 123
                    var regexRepeatingSec = /^(123)\1\1+$/g;
                    
                    const fourthDigit = ["7", "8", "9"];
                    
                    //if SSN/ITIN starts with 9, should have 7,8,9 in the fourth position
                    if(fields.SSN__c.startsWith('9') && !fourthDigit.includes(fields.SSN__c.charAt(3))){
                        message.push('SSN: '+fields.SSN__c+' is not a valid entry');
                    }
                    if(fields.SSN__c.length != 9 || !regex9Numbers.test(fields.SSN__c)){
                        message.push('SSN: Must be 9 numbers');
                    }else if(!regexCheckValid.test(fields.SSN__c) || regexCheckAll9SameNumber.test(fields.SSN__c) || regexRepeatingSec.test(fields.SSN__c)){
                        message.push('SSN: '+fields.SSN__c+' is not a valid entry');
                    }
                }
            }
            if (fields.Representative__c) {
                if(!specialCharactersAllowed.test(fields.Representative__c)) {
                    message.push('Representative has at least 1 special character that is not allowed in this field.');
                }
            }
            if (fields.Representative_Position__c) {
                if(!specialCharactersAllowed.test(fields.Representative_Position__c)) {
                    message.push('Representative Position has at least 1 special character that is not allowed in this field.');
                }
            }
            if (fields.Form_of_Business__c == 'JNTVNTR') {
                if (fields.Joint_Venture_Comment__c && !specialCharactersAllowed.test(fields.Joint_Venture_Comment__c)) {
                    message.push('Joint Venture Comment: Must have at least 1 letter or number.');
                }
            }
            if (fields.Form_of_Business__c == 'OTHER') {
                if (!specialCharactersAllowed.test(fields.Other_Comment__c)) {
                    message.push('Other Comment: Has at least 1 special character that is not allowed in this field.');
                }
            }
            if (fields.Dissolved_Date__c) {
                if (new Date(fields.Dissolved_Date__c) < testDate) {
                    message.push('Dissolved Date: Cannot be before 1900.');
                }
            }
            if (fields.AccountSource == 'Other') {
                if (fields.Source__c) {
                    if (!specialCharactersAllowed.test(fields.Source__c)) {
                        message.push('Source: Has at least 1 special character that is not allowed in this field.');
                    }
                } else {
                    message.push('Source: Must have at least 1 letter.');
                }
            }
        } else { // Person Account Validations
            // First Name Validations
            if (!specialCharactersAllowedHyphenApostrophe.test(fields.FirstName)) {
                message.push('First Name: Can have only letters and the characters - \'');
            }
            if (fields.FirstName.toLowerCase().includes(' and ') || fields.FirstName.toLowerCase().includes(' or ')) {
                message.push('First Name: '+ fields.FirstName + ' is not a valid entry.');
            }
            if (fields.FirstName.toLowerCase().includes(' estate') ||
                fields.FirstName.toLowerCase().includes(' estates') ||
                fields.FirstName.toLowerCase().includes(' trust'))
            {
                message.push('First Name: Trusts and Estates must be added as organizations.');
            }
            if(fields.FirstName.length > 30){
                message.push('First Name should be less than 30 characters');
            }
            // Middle Name Validations
            if (fields.MiddleName) {
                if (!specialCharactersAllowedHyphenApostrophe.test(fields.MiddleName)) {
                    message.push('Middle Name: Can have only letters and the characters - \'');
                }
                if (fields.MiddleName.toLowerCase().includes(' and ') ||
                    fields.MiddleName.toLowerCase().includes(' or ') ||
                    fields.MiddleName.toLowerCase().includes(' nmi') ||
                    fields.MiddleName.toLowerCase().includes(' nmi ') ||
                    fields.MiddleName.toLowerCase().includes('nmi ') ||
                    fields.MiddleName.toLowerCase() == 'nmi' ||
                    fields.MiddleName.toLowerCase().includes(' nmn') ||
                    fields.MiddleName.toLowerCase().includes(' nmn ') ||
                    fields.MiddleName.toLowerCase().includes('nmn ') ||
                    fields.MiddleName.toLowerCase() == 'nmn')
                {
                    message.push('Middle Name: '+ fields.MiddleName + ' is not a valid entry.');
                }
                if (fields.MiddleName.toLowerCase().includes(' estate') ||
                    fields.MiddleName.toLowerCase().includes(' estates') ||
                    fields.MiddleName.toLowerCase().includes(' trust'))
                {
                    message.push('Middle Name: Trusts and Estates must be added as organizations.');
                }
                if(fields.MiddleName.length > 30){
                    message.push('Middle Name should be less than 30 characters');
                }
            }
            // Last Name Validations
            if (!specialCharactersAllowedHyphenApostrophe.test(fields.LastName)) {
                message.push('Last Name: Can have only letters and the characters - \'');
            }
            if (fields.LastName.toLowerCase().includes(' and ') ||
                fields.LastName.toLowerCase().includes(' or ') ||
                fields.LastName.toLowerCase().includes(' nln') ||
                fields.LastName.toLowerCase().includes(' nln ') ||
                fields.LastName.toLowerCase().includes('nln ') ||
                fields.LastName.toLowerCase() == 'nln')
            {
                message.push('Last Name: '+ fields.LastName + ' is not a valid entry.');
            }
            if (fields.LastName.toLowerCase().includes(' estate') ||
                fields.LastName.toLowerCase().includes(' estates') ||
                fields.LastName.toLowerCase().includes(' trust'))
            {
                message.push('Last Name: Trusts and Estates must be added as organizations.');
            }
            if(fields.LastName.length > 40){
                message.push('Last Name should be less than 40 characters');
            }
            // Date of Birth Validations
            if (fields.PersonBirthdate) {
                if (new Date(fields.PersonBirthdate) < testDate) {
                    message.push('Birthdate: Cannot be before 1900.');
                }
                if (fields.PersonBirthdate >= new Date()) {
                    message.push('Birthdate: Cannot be a future date.');
                }
                if (fields.Deceased_Date__pc) {
                    if (fields.PersonBirthdate > fields.Deceased_Date__pc) {
                        message.push('Birthdate: Cannot be after Deceased Date.')
                    }
                }
            }
            if (fields.Deceased_Date__pc) {
                if (!fields.Deceased__pc) {
                    message.push('Deceased Date: Not a valid entry. Contact is not deceased.');
                }
                if (new Date(fields.Deceased_Date__pc) < testDate) {
                    message.push('Deceased Date: Cannot be before 1900.');
                }
                if (new Date(fields.Deceased_Date__pc) > new Date()) {
                    message.push('Deceased Date: Cannot be a future date.');
                }
                if (fields.PersonBirthdate) {
                    if (fields.Deceased_Date__pc < fields.PersonBirthdate) {
                        message.push('Deceased Date: Cannot be before Birthdate.')
                    }
                }
            }
            if(JSON.stringify(fields).includes('Occupation_Comment__pc') && fields.Occupation_Comment__pc != null){
                var fieldvalue = fields.Occupation_Comment__pc.toLowerCase(); 
                var re = new RegExp('^[A-Za-z0-9-()/& ]*[A-Za-z0-9]+[A-Za-z0-9-()/& ]*$');
                
                if (fieldvalue === 'unknown' || fieldvalue === 'unk') {
                    message.push('Occupation Comment: Unk or Unknown is not a valid entry');
                } 
                else if(!re.test(fieldvalue)){
                    message.push('Occupation Comment: Must have at least 1 letter or number and the only special characters allowed for this field are -()/&');
                }
            }
            //SSN validation
            if(fields.SSN__pc){
                if (fields.SSN__pc != 'XXXXX' + fields.Last_4_SSN__c) {  // Validate if the SSN field is not the masked value
                    //should not contain all 0's in any section
                    var regexCheckValid = new RegExp('^(?!000|666)[0-9]{3}(?!00)[0-9]{2}(?!0000)[0-9]{4}$');
                    //check the same number repeating 8 times
                    var regexCheckAll9SameNumber = /^(\d)\1{8}$/g;
                    //check repeating 123
                    var regexRepeatingSec = /^(123)\1\1+$/g;
                    // check 9 numbers
                    var regex9Numbers = /[0-9]{9}/;
                    
                    const fourthDigit = ["7", "8", "9"];
                    //if SSN/ITIN starts with 9, should have 7,8,9 in the fourth position
                    if(fields.SSN__pc.startsWith('9') && !fourthDigit.includes(fields.SSN__pc.charAt(3))){
                        message.push('SSN: '+fields.SSN__pc+' is not a valid entry');
                    }
                    if(fields.SSN__pc.length != 9 || !regex9Numbers.test(fields.SSN__pc)){
                        message.push('SSN: Must be 9 numbers');
                    }else if(!regexCheckValid.test(fields.SSN__pc) || regexCheckAll9SameNumber.test(fields.SSN__pc) || regexRepeatingSec.test(fields.SSN__pc)){
                        message.push('SSN: '+fields.SSN__pc+' is not a valid entry');
                    }
                }
            }
            //DL Validation
            if (fields.Drivers_License_Issue_Year__c && !fields.Drivers_License_Issue_Month__c) {
                message.push('Must have both License Issue Month and License Issue Year entered.');
            }
            if (!fields.Drivers_License_Issue_Year__c && fields.Drivers_License_Issue_Month__c) {
                message.push('Must have both License Issue Month and License Issue Year entered.');
            }
            if(!fields.Driver_s_License_Number__c && fields.Driver_s_License_State__c !==  'IT' && fields.Driver_s_License_State__c){
                message.push('Driver\'s License Number is Required');
            }
            if (fields.Drivers_License_Issue_Year__c && fields.Drivers_License_Issue_Month__c) {
                if (fields.PersonBirthdate) {
                    if (new Date(fields.PersonBirthdate).getUTCFullYear() + 12 > Number(fields.Drivers_License_Issue_Year__c) || 
                        (new Date(fields.PersonBirthdate).getUTCFullYear() + 12 == Number(fields.Drivers_License_Issue_Year__c) && 
                            new Date(fields.PersonBirthdate).getUTCMonth() + 1 > Number(fields.Drivers_License_Issue_Month__c))) {
                        message.push('Original License Year must equal DOB year + 12 and Original License Month must be greater than or equal to DOB month.');
                    }
                }
                if (Number(fields.Drivers_License_Issue_Year__c) > today.getUTCFullYear()) {
                    message.push('Drivers License Issue License Date: Cannot be a future date.');
                }
                if (Number(fields.Drivers_License_Issue_Year__c) == today.getUTCFullYear() &&
                        Number(fields.Drivers_License_Issue_Month__c) > today.getUTCMonth() + 1) {
                    message.push('Drivers License Issue License Date: Cannot be a future date.');
                }
                if (fields.Deceased_Date__pc) {
                    if (new Date(fields.Deceased_Date__pc).getUTCFullYear() < Number(fields.Drivers_License_Issue_Year__c) ||
                            (new Date(fields.Deceased_Date__pc).getUTCFullYear() == Number(fields.Drivers_License_Issue_Year__c) &&
                                new Date(fields.Deceased_Date__pc).getUTCMonth() + 1 < Number(fields.Drivers_License_Issue_Month__c))) {
                        message.push('Drivers License Issue Date: Cannot be after Deceased Date.')
                    }
                }
            }
            // Employer and Occupation Validations
            if (fields.FinServ__CurrentEmployer__pc) {
                if (!specialCharactersAllowed.test(fields.FinServ__CurrentEmployer__pc)) {
                    message.push('Current Employer: The only special characters allowed for this field are - @ # $ + = ! ; : \' . / ? , &');
                }
                console.log(fields.FinServ__CurrentEmployer__pc.length);
                if (fields.FinServ__CurrentEmployer__pc.length > 50) {
                    message.push('Current Employer should be less than 50 characters');
                }
            }
            // Gender/Resident State validation
            if (fields.Resident_State__pc != fields.BillingStateCode) {
                message.push('Resident State must match with the Residential State selected');
            }
            if (fields.Professional_Suffix_Comment__pc) {
                if (!specialCharactersAllowedHyphenCommaAmpersand.test(fields.Professional_Suffix_Comment__pc)) {
                    message.push('Professional Suffix: The only special characters allowed in this field are - , &');
                }
                if (fields.Professional_Suffix_Comment__pc.toLowerCase().includes('other')) {
                    message.push('Professional Suffix: ' + fields.Professional_Suffix_Comment__pc + ' is not a valid entry.');
                }
                if (fields.Professional_Suffix_Comment__pc.toLowerCase().includes(' estate') ||
                    fields.Professional_Suffix_Comment__pc.toLowerCase().includes(' estates') ||
                    fields.Professional_Suffix_Comment__pc.toLowerCase().includes(' trust') ||
                    fields.Professional_Suffix_Comment__pc.toLowerCase().includes('estate of')) {
                    message.push('Professional Suffix: Trusts and Estates must be added as organizations.');
                }
            }
            if (fields.Pronunciation__pc) {
                if (!specialCharactersAllowed.test(fields.Pronunciation__pc)) {
                    message.push('Pronunciation: Has at least 1 special character that is not allowed in this field.');
                }
            }
        }
        // Validations for Both Person and Business Account
        if (isEdit) {
            if (fields.Greeting__c) {
                if (!specialCharactersAllowed.test(fields.Greeting__c)) {
                    message.push('Greeting: The only special characters allowed for this field are - @ # $ + = ! ; : \' . / ? , & ');
                }
            } else {
                message.push('Greeting: Must have at least 1 letter or number.');
            }
            if (fields.Mailing_Name__c) {
                if (!specialCharactersAllowed.test(fields.Mailing_Name__c)) {
                    message.push('Mailing Name: The only special characters allowed for this field are - @ # $ + = ! ; : \' . / ? , & ');
                }
            } else {
                message.push('Mailing Name: Must have at least 1 letter or number.');
            }
        }
        //FEIN validation
        if (fields.FEIN__c) {
            if (fields.FEIN__c != 'XXXXX' + fields.Last_4_FEIN__c) {
                if (!regex9Numbers.test(fields.FEIN__c)) {
                    message.push('FEIN: Must be 9 numbers.');
                }
            }
        }
        if (!fields.Primary_Spoken_Language__c) {
            if (fields.Secondary_Spoken_Language__c) {
                message.push('Primary Spoken Language: Is required when Secondary Spoken Language is entered.');
            }
        }
        if (fields.Primary_Spoken_Language_Other__c) {
            if (onlyHyphens.test(fields.Primary_Spoken_Language_Other__c)) {
                message.push('Primary Language Other: Must have at least 1 letter or number.');
            } else if (!specialCharactersAllowedHyphen.test(fields.Primary_Spoken_Language_Other__c)) {
                message.push('Primary Language Other: The only special character allowed in this field is -');
            }
        }
        if (fields.Secondary_Spoken_Language_Other__c) {
            if (onlyHyphens.test(fields.Secondary_Spoken_Language_Other__c)) {
                message.push('Secondary Language Other: Must have at least 1 letter or number.');
            } else if (!specialCharactersAllowedHyphen.test(fields.Secondary_Spoken_Language_Other__c)) {
                message.push('Secondary Language Other: The only special character allowed in this field is -');
            }
        }
        component.set("v.validationErrorMessages", message);
    },

    getEditAccess : function(component, helper) {
        var action = component.get("c.checkEditAccess");

        var actionParams = {
            "accId" : component.get('v.recordId')
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                component.set('v.isEditable',resp);
                if (!resp) {
                    var message = [];
                    message.push('The edit operation is currently not permitted for your user and profile.');
                    component.set("v.validationErrorMessages", message);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: "+errors[0].message);
                        this.handleShowNotice(component, errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                component.set("v.isLoading",false);
                this.scrollToTop();
            }
        });

        $A.enqueueAction(action);
    }
})