({
	startQuote : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getPicklistValues");
        action.setParams({ 
            "recordId" : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
			component.set("v.loaded",false);
            if (state === "SUCCESS") {
                if(JSON.stringify(result)=='{}'){
                	component.set("v.error", 'A product system is not available for the line of business on this opportunity. Please use \'Other quoting links\' to quote');    
                }
                var custs = [];
                for(var key in result){
                    custs.push({value:result[key], key:key});
                }
            	component.set("v.picklistValues", custs);
                component.set("v.defaultValue", custs[0].key);
             }
            else if (state === "ERROR") {
            	component.set("v.error", 'An unexpected error occured, please contact adminstrator!');   
            }
        });
        $A.enqueueAction(action);
    },
        
    redirectToQuotingSystem : function(component, event, helper) {
        
        var selected = component.get("v.defaultValue");
        if(selected != '' && component.get("v.selectedValue") == ''){
            component.set("v.selectedValue", selected);
        }
        console.log( 'select' + component.get("v.selectedValue"));
        
        if(component.get("v.selectedValue") == ''){
            component.set("v.error", 'Please select a product to initiate quote');
        }else{
            var recordId = component.get("v.recordId");
            var action = component.get("c.getPolicyQuoteURL");
            var quoteSystem = component.get("v.selectedValue");
            action.setParams({
                "recordId" : recordId,
                "quoteSystem":quoteSystem
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    
                    if(!result.includes('Error')){
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                          "url": result
                        });
                        urlEvent.fire();
                        
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                          "url": '/'+recordId
                        });
                        urlEvent.fire();
                        $A.get("e.force:closeQuickAction").fire();
                       }else{
                            component.set("v.error", 'An unexpected error occured, please contact adminstrator!');
                        }
                    }  
                     else{
                        component.set("v.error", 'An unexpected error occured, please contact adminstrator!');
                        
                     }
              });
               $A.enqueueAction(action);
            }
     },
    
    cancel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})