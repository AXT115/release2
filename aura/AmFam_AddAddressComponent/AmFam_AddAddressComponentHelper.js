({
    countryOptions: [
        {'label': 'United States', 'value': 'US'},
        {'label': 'Canada', 'value': 'Canada'}
    ],	
	
	getCountryOptions: function() {
        return this.countryOptions;
    },

	getAddressTypes: function(cmp) {
		var action = cmp.get("c.getAddressTypes");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var fieldMap = [];
                for(var key in result){
					console.log('### key: ' + key + ' value: ' + result[key]);
                    fieldMap.push({key: key, value: result[key]});
                }
                cmp.set("v.addressTypes", fieldMap);
            }
        });
        $A.enqueueAction(action);

	},

	saveToServer : function(cmp, event) {
		
		var address = cmp.find("newaddress");
		var street = address.get("v.street");
		var city = address.get("v.city");
		var country = address.get("v.country");
		var state = address.get("v.province");
		var postalCode = address.get("v.postalCode");

		var addressType = cmp.get("v.selectedType");;
		var accountId = cmp.get("v.recordId");

		console.log('### addressType: ' + addressType);

		var action = cmp.get("c.saveAddress");
        action.setParams({
            "accountId": accountId,
            "addressType": addressType,
            "street": street,
            "city": city,
            "postalCode": postalCode,
            "state": state,
            "country": country
        });

		console.log('### submit accountId: ' + accountId);
		console.log('### submit addressType: ' + addressType);
		console.log('### submit street: ' + street);
		console.log('### submit city: ' + city);
		console.log('### submit state: ' + state);
		console.log('### submit postalCode: ' + postalCode);
		console.log('### submit country: ' + country);


        action.setCallback(this, function(response){
            var responseState = response.getState();
			//conslog.log('### call back: ' + state);
            if (responseState == "SUCCESS") {
                //cmp.set("v.cases", response.getReturnValue());
				console.log('### call back SUCCESS');
				cmp.set("v.error", '');
				cmp.set("v.success", "Address record was successfully created");

				var a = cmp.find("newaddress");
				a.set("v.street", "");
				a.set("v.city", "");
				a.set("v.country"), "";
				a.set("v.province", "");
				a.set("v.postalCode", "");
		
            }
			else if (responseState == "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        // log the error passed in to AuraHandledException
                        console.log("Error message: " + errors[0].message);
						cmp.set("v.error", errors[0].message);
						cmp.set("v.success", "");
                    }
                } else {
                    console.log("Unknown error");
                }
            }

        });
        $A.enqueueAction(action);
    }		
	
})