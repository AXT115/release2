({
	doInit : function(component, event, helper) {
		
        component.set("v.isLoading",true);
        var recordId = component.get("v.recordId");
        console.log(recordId);
        var action = component.get("c.refreshLead");
        action.setParams({ 
            "recordId" : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
			if (state === "SUCCESS") {
                component.set("v.message", 'The refresh was successfully completed'); 
             }
            else if (state === "ERROR") {
                component.set("v.message", 'An unexpected error occured, please contact adminstrator!');   
            }
            
            if(component.get("v.message") != ''){
                var title = state === "SUCCESS"?'Success!':'Error!';
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type":title =='Error!'?"error":"success",
                    "title": title,
                    "message": component.get("v.message"),
                    "mode":"dismissible",
                    "duration":"20000"
                 });
                toastEvent.fire();
           }
           
           $A.get('e.force:refreshView').fire();
           $A.get("e.force:closeQuickAction").fire();
           component.set("v.isLoading",false);  
            
        });
        $A.enqueueAction(action);
    }
})