({
    recordUpdated : function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "CHANGED") {
            //console.log('changed record!');
        }
    },
    
    // Invokes the subscribe method on the empApi component
    doInit : function(component, event, helper) {
        // Get the empApi component
        const empApi = component.find('empApi');
        // Get the channel from the input box
        //const channel = component.find('channel').get('v.value');
        // Replay option to get new events
        const replayId = -1;

        // Subscribe to an event
        empApi.subscribe('/topic/LeadCDHErrors', replayId, $A.getCallback(eventReceived => {
            // Process event (this is called each time we receive an event)
            if(eventReceived.data.sobject.Id == component.get('v.recordId')) {
                //setTimeout(function(){ component.find("forceRecord").reloadRecord(); }, 3000);
                var action = component.get("c.getLead");
                action.setParams({ Id : eventReceived.data.sobject.Id });
                
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        // Alert the user with the value returned
                        // from the server
                        if(response.getReturnValue().Errors__c != null && component.get('v.simpleRecord').Errors__c != response.getReturnValue().Errors__c) {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error",
                                "message": response.getReturnValue().Errors__c,
                                "type": "error",
                                "mode": "sticky"
                            });
                            toastEvent.fire();
                            // You would typically fire a event here to trigger
                            // client-side notification that the server-side
                            // action is complete
                        }
                        component.set('v.simpleRecord',response.getReturnValue());
                    }
                    else if (state === "INCOMPLETE") {
                        // do something
                    }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " +
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                });
                $A.enqueueAction(action);
            }
        }))
        .then(subscription => {
            // Confirm that we have subscribed to the event channel.
            // We haven't received an event yet.
            console.log('Subscribed to channel ', '');
            // Save subscription to unsubscribe later
            //component.set('v.subscription', subscription);
        });
    },
})