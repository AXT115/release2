({
	doInit: function(component, event, helper) {

        var action = component.get("c.getSourceSystemURL");

		action.setCallback(this, function(response) {
				var state = response.getState();
                var result = response.getReturnValue();
				if (state === "SUCCESS") {
                    var custs = [];
                    for(var key in result){
                        custs.push({value:result[key], key:key});
                    }
                    component.set("v.commercialClassic", custs[0].key);
                    component.set("v.LifeCornestone", custs[0].value);
			    }
			});
			$A.enqueueAction(action);
	}
})