({
    startQuote : function(component, event, helper) {
        component.set("v.errors",null);
        component.set("v.loaded",false);
        component.set("v.overrideIndicator",false);
        var action = component.get("c.promoteEntrant");
        var addressObj = null;
        if (Object.keys(component.get('v.addressMap')).length !== 0) {
            var index = component.find("mySelect").get("v.value");
            addressObj = JSON.stringify(component.get('v.addressMap')[index]);
        }
        var overrideIndicator = component.get('v.selectedAddress') != 'None';
        action.setParams({ 
            'leadID' : component.get('v.recordId'),
            'forceAddress' : overrideIndicator,
            'addrData' : addressObj
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned
                // from the server
                var retVal = response.getReturnValue();
                component.set("v.leadRec",retVal.leadRecord);
                var urlMap = retVal.urlMap;
                // this is the hack.  need to get the map so
                // that it initializes properly.
                var c_urlMap = component.get("v.urlMap");
                c_urlMap = urlMap;
                component.set("v.urlMap",c_urlMap);
                component.set("v.overrideIndicator",retVal.showOverride);
                
                var addresses = [];
                if (retVal.address != null) {
                    addresses.push({label: 'Please select an address', value: 'None'})
                    for (var index = 0; index < retVal.address.length; index++) { 
                        var addressLabel = '(' + retVal.address[index].Code1ReturnIndicator + ')';
                        if (retVal.address[index].addressLine1 != undefined) {
                            addressLabel += (' ' + retVal.address[index].addressLine1);
                        }
                        if (retVal.address[index].addressLine2 != undefined) {
                            addressLabel += (' ' + retVal.address[index].addressLine2);
                        }
                        if (retVal.address[index].city != undefined) {
                            addressLabel += (' ' + retVal.address[index].city);
                        }
                        if (retVal.address[index].stateCode != undefined) {
                            addressLabel += (', ' + retVal.address[index].stateCode);
                        }
                        if (retVal.address[index].zip5Code != undefined) {
                            addressLabel += (' ' + retVal.address[index].zip5Code);
                        }
                        addresses.push({label: addressLabel, value: index});
                    } 
                    component.set("v.addressMap",retVal.address);
                    component.set("v.address",addresses);
                }
                
                //console.table(retVal.address);
                if (retVal.errors != null) {
                    component.set("v.errors",retVal.errors);
                } else {
                    // Lines of Business has Person Vehicle AND no others selected
                    if (retVal.leadRecord.Lines_of_Business__c != null) {
                        const lobSize = retVal.leadRecord.Lines_of_Business__c.split(';');
                        // Lines of Business includes Personal Vehicle and no others selected
                        if(retVal.leadRecord.Lines_of_Business__c.includes("Personal Vehicle") 
                            && lobSize.length === 1){
                            component.set("v.selectedValue","PersonalAuto");
                        // Lines of Business includes Property and no others selected
                        } else if (retVal.leadRecord.Lines_of_Business__c.includes("Property") 
                                    && lobSize.length === 1){
                            component.set("v.selectedValue","Homeowners");
                        // Lines of Business includes Life and no others selected
                        } else if (retVal.leadRecord.Lines_of_Business__c.includes("Life")
                        && lobSize.length === 1){
                            component.set("v.selectedValue","Life_Cornerstone");
                        }
                    }
                    component.set("v.showDropdown",true);
                    // Refresh page
                    $A.get('e.force:refreshView').fire();
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set('v.loaded',true);
        });
        $A.enqueueAction(action);
    },

    redirectToQuotingSystem : function(component, event, helper) {
        if (!component.find("productSelect").get("v.validity").valueMissing) {
            var l = component.get("v.leadRec");
            var selectedDropdownValue = component.get("v.selectedValue");
            var urlMap = component.get("v.urlMap");
            var url = helper.getURLFor(selectedDropdownValue, urlMap);

            // Close quick action modal
            $A.get("e.force:closeQuickAction").fire();
            // Redirect
            if (selectedDropdownValue === 'Life_Cornerstone'){
                window.open(url+'/ipipeline?cdhid='+l.Contact_CDHID__c+'&initialTab=Client');
            }else{
                window.open(url+l.Contact_CDHID__c+'&productCode='+selectedDropdownValue+'&producerID='+l.Agency__r.Producer_ID__c);
            }
        }
    },
    
})