/**
*   @description  Test class for AssociatedLocationService
*
*   @author Salesforce Services
*   @date 5/28/2021
*   @group Test
*/
@IsTest
public class AmFam_AssociatedLocationServiceTest {
    public AmFam_AssociatedLocationServiceTest() {
 
    }

    static List<Schema.Location> setupLocations()
    {
        List<Schema.Location> newLoc = new List<Schema.Location>();

        Schema.Location l = new Schema.Location();
        l.LocationType = 'R';
        l.Name = 'ZZZZ';
        newLoc.add(l);

        l = new Schema.Location();
        l.LocationType = 'M';
        l.Name = 'YYYY';
        newLoc.add(l);

        l = new Schema.Location();
        l.LocationType = 'R';
        l.Name = 'XXXX';
        newLoc.add(l);

        l = new Schema.Location();
        l.LocationType = 'M';
        l.Name = 'WWWW';
        newLoc.add(l);

        insert newLoc;

        List<Schema.Address> newAddress = new List<Schema.Address>();
        for (Schema.Location loc : newLoc)
        {
            Schema.Address addr = new Schema.Address();
            addr.ParentId = loc.Id; 
            addr.Street = loc.Id+loc.LocationType;
            addr.City = addr.Street+'City';
            addr.State = 'IL';
            addr.PostalCode = '12345';
            addr.AddressType=loc.LocationType;
            addr.LocationType = loc.LocationType;
            addr.GeocodeAccuracy = 'Address';
            addr.Country = 'USA';
            addr.Latitude = 41.5928338;
            addr.Longitude = -87.6054553;
            addr.StateCode = 'IL';
            newAddress.add(addr);
        }
        insert newAddress;
        return newLoc;
    }

    static List<AssociatedLocation> setupAssociatedLocations(Account person1, Account person2, List<Schema.Location> newLoc)
    {
        List<AssociatedLocation> asslocs = new List<AssociatedLocation>();
        AssociatedLocation loc = new AssociatedLocation();
        loc.PersonAccount__c = person1.Id;
        loc.ParentRecordId = loc.PersonAccount__c;
        loc.LocationId = newLoc[0].Id;
        loc.Purpose__c = 'PRM';
        loc.Type = newLoc[0].LocationType;
        asslocs.add(loc);

        loc = new AssociatedLocation();
        loc.PersonAccount__c = person1.Id;
        loc.ParentRecordId = loc.PersonAccount__c;
        loc.LocationId = newLoc[1].Id;
        loc.Purpose__c = 'PRM';
        loc.Type = newLoc[1].LocationType;
        asslocs.add(loc);

        loc = new AssociatedLocation();
        loc.PersonAccount__c = person2.Id;
        loc.ParentRecordId = loc.PersonAccount__c;
        loc.LocationId = newLoc[2].Id;
        loc.Purpose__c = 'PRM';
        loc.Type = newLoc[2].LocationType;
        asslocs.add(loc);

        loc = new AssociatedLocation();
        loc.PersonAccount__c = person2.Id;
        loc.ParentRecordId = loc.PersonAccount__c;
        loc.LocationId = newLoc[3].Id;
        loc.Purpose__c = 'PRM';
        loc.Type = newLoc[3].LocationType;
        asslocs.add(loc);
        return asslocs;

    }

    static testmethod void testAddressMap()
    {
        List<Schema.Location> newLoc = AmFam_AssociatedLocationServiceTest.setupLocations();

        //create few accounts type is irrelevant here, just need something to attach associated to
        List<Account> accounts = AmFam_TestDataFactory.createAccountsWithLeads (2, 0);

        List<AssociatedLocation> assLoc = new List<AssociatedLocation>();
        for (Integer ndx = 0; ndx < 4; ndx++)
        {
            AssociatedLocation al = new AssociatedLocation();
            al.LocationId = newLoc[ndx].Id;
            al.PersonAccount__c = accounts[math.mod(ndx,2)].Id;
            al.ParentRecordId = al.PersonAccount__c;
            al.Type = newLoc[ndx].LocationType;
            assLoc.add(al);
        }

        insert assLoc;

        Map<Id, Map<String,Schema.Address>> result = AmFam_AssociatedLocationService.createAddressMap(assLoc);
        System.assert(result.size() == 4);
    }

    static testmethod void testUpdatePrimaryAddress()
    {
        //setup baseline Locations & Addresses
        List<Schema.Location> newLoc = AmFam_AssociatedLocationServiceTest.setupLocations();

        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');

        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');

        List<AccountContactRelation> rels = new List<AccountContactRelation>();

        List<Contact> contacts = [SELECT Id FROM Contact];

        for (Contact contact : contacts)
        {
            AccountContactRelation rel = new AccountContactRelation();
            rel.AccountId = household.Id;
            rel.ContactId = contact.Id;
            rel.Roles = 'Client';
            rel.IsActive = true;
            rels.add(rel);
        }
        
        insert rels;

        //Associate Locations
        List<AssociatedLocation> asslocs = setupAssociatedLocations(person1, person2, newLoc);
        
        insert asslocs;

        //finally into the test
        Test.startTest();

        //Update AssLoc
        Schema.Location locChange = new Schema.Location();  
        locChange.LocationType = 'M';
        locChange.Name = 'AAAA';
        insert locChange;

        Schema.Address addrChange = new Schema.Address();
        addrChange.ParentId = locChange.Id; 
        addrChange.Street = person1.Id+locChange.LocationType+'CHANGED';
        addrChange.AddressType=locChange.LocationType;
        addrChange.LocationType = locChange.LocationType;
        insert addrChange;

        AssociatedLocation changeAssLoc = asslocs[1]; //person 1 mailing

        changeAssLoc.LocationId = locChange.Id;
        update changeAssLoc;

        Account h = [SELECT ShippingAddress,BillingStreet FROM Account WHERE Id = :household.Id LIMIT 1];
        System.debug('HOUSEHOLD:'+h);
        System.Address addr = h.ShippingAddress;
        System.assert(addr.Street.endsWith('CHANGED'));

        Test.stopTest();

    }

    static testmethod void testUpdatePrimaryAddressNeg()
    {
        //setup baseline Locations & Addresses
        List<Schema.Location> newLoc = AmFam_AssociatedLocationServiceTest.setupLocations();

        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');

        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');

        List<AccountContactRelation> rels = new List<AccountContactRelation>();

        List<Contact> contacts = [SELECT Id FROM Contact];

        for (Contact contact : contacts)
        {
            AccountContactRelation rel = new AccountContactRelation();
            rel.AccountId = household.Id;
            rel.ContactId = contact.Id;
            rel.Roles = 'Client';
            rel.IsActive = true;
            rels.add(rel);
        }
        
        insert rels;

        //Associate Locations
        List<AssociatedLocation> asslocs = setupAssociatedLocations(person1, person2, newLoc);        
        insert asslocs;

        //finally into the test
        Test.startTest();

        Schema.Location locChange = new Schema.Location();  
        locChange.LocationType = 'M';
        locChange.Name = 'BBB';
        insert locChange;

        Schema.Address addrChange = new Schema.Address();
        addrChange.ParentId = locChange.Id; 
        addrChange.Street = person2.Id+locChange.LocationType+'CHANGEDB';
        addrChange.AddressType=locChange.LocationType;
        addrChange.LocationType = locChange.LocationType;
        insert addrChange;

        AssociatedLocation changeAssLoc = asslocs[3]; //person2 residence

        changeAssLoc.LocationId = locChange.Id;
        update changeAssLoc;

        Account h = [SELECT ShippingAddress,BillingAddress FROM Account WHERE Id = :household.Id LIMIT 1];
        Address addr = h.ShippingAddress;
        System.assert(addr?.Street == null || !addr.Street.endsWith('CHANGEDB'));

        Test.stopTest();

    }

}