/**
* @author         salesforce services
* @date           10/23/2020
* @description    handler class for Producer Type Service. Contains method to call out Producer Type Service API.
* @group          API Callout
*
*/
public class AmFam_ProducerTypeServiceHandler {
    /*
    public static AmFam_ProducerTypeService upsertRelatedAccount(String producerId) {
        AmFam_ProducerTypeService response = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/' + producerId + '/booksofbusiness');
        List<Account> accountsToUpsert = new List<Account>();
        if (response?.bookOfBusiness != null) {
            for (AmFam_ProducerTypeService.BookOfBusiness bob : response?.bookOfBusiness) {
                String agencyId = bob.producerCodes[0]?.agencyId;
                if (bob.role == 'AGENT' && agencyId != null) {
                    AmFam_ProducerTypeService agencyResp = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/agencies/'+agencyId);
                    if (agencyResp != null) {
                        if (agencyResp.agency != null) {
                            List<Account> accList = [SELECT Id, Producer_ID__c FROM Account WHERE Producer_ID__c =: producerId LIMIT 1];
                            Account acc = new Account();
                            if (accList.size() == 1) {
                                acc = accList[0];
                            }
                            acc.Name = agencyResp.agency.name;
                            acc.Goes_By_Name__c = agencyResp.agency.name;
                            acc.Producer_ID__c = producerId;
                            if (agencyResp?.agency?.contactDetails?.addresses != null) {
                                for (AmFam_ProducerTypeService.Addresses address : agencyResp?.agency?.contactDetails?.addresses) {
                                    if (address.primaryAddressIndicator) {
                                        acc.ShippingStreet = address.addressLine1;
                                        acc.ShippingCity = address.city;
                                        acc.ShippingState = address.state;
                                        acc.ShippingPostalCode = address.zip5Code;
                                        acc.ShippingCountryCode = address.country;
                                    }
                                }
                            }
                            accountsToUpsert.add(acc);
                        }
                    }
                }
            }
        }
        if (accountsToUpsert.size() > 0) {
            upsert accountsToUpsert;
        }
        return response;
    }
    */
    public static AmFam_ProducerTypeService producerTypeServiceCallOut(String extraParam) {
        API_Header__mdt apiHeader = [SELECT MasterLabel, Header_Value__c FROM API_Header__mdt WHERE MasterLabel = 'AFI-API-Key' LIMIT 1];
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setHeader('AFI-AppName', 'Salesforce');
        request.setHeader('AFI-API-Key', apiHeader.Header_Value__c);
        //String extraParam = '/v2/01/agencies/';
        request.setEndpoint('callout:ProducerType_API' + extraParam);
        HttpResponse response;
        try {
            response = http.send(request);
            system.debug(response.getBody());
            // If the request is successful, parse the JSON response.
            if (response.getStatusCode() == 200) {
                // Deserialize the JSON string into collections of primitive data types.
                AmFam_ProducerTypeService  prodTypeSvc = (AmFam_ProducerTypeService)JSON.deserialize(response.getBody(), AmFam_ProducerTypeService.class);
                return prodTypeSvc;
            }
        } catch (Exception e) {
            //store exception details on Failure Logs with user details and error details
            AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
            failureLog.createAPIFailureLog('AmFam_ProducerTypeServiceHandler', response, request.getEndpoint(), extraParam, e.getMessage());
            System.debug(e.getMessage());
       }
       return null;
    }
}