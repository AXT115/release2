/**
*   @description: the class is a test class for AmFam_CDHQueueLoadValidation.cls
*	@author: Anbu Chinathambi
*   @date: 10/19/2021
*   @group: Test class
*/

@isTest
public class AmFam_CDHQueueLoadValidationTest {
    
    static testMethod void testValidatePhone() {
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User newUser = new User (     alias = 'janes', 
                                    email= 'Jane12456@xyz.com',
                                    emailencodingkey='UTF-8', 
                                    FederationIdentifier= '12456',
                                    firstname= 'Jane', 
                                    lastname= 'Phillips',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', 
                                    profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username= 'JanePtest12456@xyz.com.test');
        
        insert newUser;

        
        Account objAcc = new Account();
        objAcc.FirstName = 'Philly';
        objAcc.LastName = 'Jones';
        objAcc.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();
        insert objAcc;
        
        CDH_Queue_Validation_Setup__c setting = new CDH_Queue_Validation_Setup__c();
        setting.CDH_Queue_Enabled_Date__c = System.now() - 1;
        setting.Validate__c = true;
        setting.User_Id__c = newUser.id;
        insert setting;
        
        objAcc.LastName = 'JonesTw';
        update objAcc;
        
        System.runAs(newUser) {
            ContactPointPhone cpPhone = new ContactPointPhone();
            cpPhone.TelephoneNumber = '2223334444';
            cpPhone.ParentId = objAcc.id;
            cpPhone.UsageType = 'HOME';
            cpPhone.RecordTypeId = AmFam_Utility.getContactPointPhonePersonRecordType();
            
            try{
                insert cpPhone;
            }catch(DmlException e){
                system.debug('Exception' + e.getMessage());  
            }
            system.debug('cpPhone>>>' + cpPhone);
            system.assert(cpPhone?.id == null);
        }
    }
    
    static testMethod void testValidateEmail() {
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User newUser = new User (   alias = 'Jncy', 
                                    email= 'Jancy12456@xyz.com',
                                    emailencodingkey='UTF-8', 
                                    FederationIdentifier= '67456',
                                    firstname= 'Jancy', 
                                    lastname= 'Rode',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', 
                                    profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username= 'JancyPtest12456@xyz.com.test');
        
        insert newUser;
        
        Account objAcc = new Account();
        objAcc.FirstName = 'Dell';
        objAcc.LastName = 'Messi';
        objAcc.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();
        insert objAcc;
        
        CDH_Queue_Validation_Setup__c setting = new CDH_Queue_Validation_Setup__c();
        setting.CDH_Queue_Enabled_Date__c = System.now() - 1;
        setting.Validate__c = true;
        setting.User_Id__c = newUser.Id;
        insert setting;
        
        objAcc.LastName = 'Tina';
        update objAcc;
            
        System.runAs(newUser) {
            
            ContactPointEmail cpEmail = new ContactPointEmail();
            cpEmail.EmailAddress = 'buzz@kill.com';
            cpEmail.ParentId = objAcc.id;
            cpEmail.UsageType = 'HOME';
            
            try{
                insert cpEmail;    
            }catch(DmlException e){
                system.debug('Exception' + e.getMessage());  
            }
            system.assert(cpEmail?.id == null);
        }
    }
}