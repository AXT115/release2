/**
*   @description trigger handler for ContactPointPhone object. Note: this should be the only place
*	where ALL AssociatedLocation related trigger logic should be implemented.
*   @author Salesforce Services
*   @date 06/04/2020
*   @group Trigger
*/

public with sharing class AmFam_ContactPointPhoneTriggerHandler {


    public void OnBeforeInsert (List<ContactPointPhone> newContactPointPhone) {
        //the below method is specially designed to prevent insert/update if the related Account lastmodified date is greater than CDH Queue Enabled Date
        AmFam_CDHQueueLoadValidation.validate(null, newContactPointPhone);
    }

    public void OnAfterInsert (List<ContactPointPhone> newContactPointPhone,Map<ID, ContactPointPhone> newContactPointPhoneMap) {
        AmFam_ContactPointPhoneService.updateHouseholdPrimaryPhone(newContactPointPhone,newContactPointPhoneMap);
    }

    public void OnAfterUpdate (List<ContactPointPhone> newContactPointPhone,
                               List<ContactPointPhone> oldContactPointPhone,
                               Map<ID, ContactPointPhone> newContactPointPhoneMap,
                               Map<ID, ContactPointPhone> oldContactPointPhoneMap) {
                                AmFam_ContactPointPhoneService.updateHouseholdPrimaryPhone(newContactPointPhone,newContactPointPhoneMap);
    }

    public void OnBeforeUpdate(List<ContactPointPhone> newContactPointPhone,
                                List<ContactPointPhone> oldContactPointPhone,
                                Map<ID, ContactPointPhone> newContactPointPhoneMap,
                                Map<ID, ContactPointPhone> oldContactPointPhoneMap) {
                                 //the below method is specially designed to prevent insert/update if the related Account lastmodified date is greater than CDH Queue Enabled Date
                                 AmFam_CDHQueueLoadValidation.validate(null, newContactPointPhone);
                               

    }
}