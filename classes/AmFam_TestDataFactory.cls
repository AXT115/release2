@isTest
public class AmFam_TestDataFactory {
    public static Account createAccountByNameWithLeads (String accName, Integer numLeads) {
        String accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency').getRecordTypeId();

        Account a = new Account(Name=accName,
                                RecordTypeId = accRecordTypeId,
                                Producer_ID__c = '73320');
        insert a;


        List<Lead> leads = new List<Lead>();
        for (Integer i=0; i<numLeads; i++) {
            Lead ld = new Lead();
            ld.Agency__c = a.Id;
            ld.Company = 'Test Lead' + i;
            ld.Email = i + 'testLeadQuote@test.com';
            ld.FirstName = 'Test';
            ld.LastName = 'Lead';
            leads.add(ld);
        }
        // Insert all contacts for all accounts
        insert leads;
        return a;
    }

    public static List<Account> createAccountsWithLeads (Integer numAccts, Integer numLeadsPerAcct) {
        String accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency').getRecordTypeId();
        String leadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();

        List<Account> accts = new List<Account>();
        for(Integer i=0; i<numAccts; i++) {
            Account a = new Account(Name='TestAccount' + i,
                                    RecordTypeId = accRecordTypeId,
                                    Producer_ID__c = '10000' + i);
            accts.add(a);
        }
        insert accts;


        List<Lead> leads = new List<Lead>();
            for (Integer j=0; j<numAccts; j++) {
                Account acct = accts[j];
                // For each account just inserted, add contacts
                for (Integer k=numLeadsPerAcct*j; k<numLeadsPerAcct*(j+1); k++) {
                    Lead ld = new Lead();
                    ld.Agency__c = acct.Id;
                    ld.Company = 'Test Lead' + j + k;
                    ld.Email = j + k + 'testLeadQuote@test.com';
                    ld.FirstName = 'Test';
                    ld.LastName = 'Lead';
                    ld.RecordTypeId = leadRecordTypeId;
                    leads.add(ld);
                }
            }
            // Insert all contacts for all accounts
            insert leads;
            return accts;
    }
    
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }

    public static String generateRandomStringWithoutSpecifiedString(Integer len, String stringToAvoid) {
        String genString = generateRandomString(len);
        if (genString.containsIgnoreCase(stringToAvoid)) {
            genString = generateRandomStringWithoutSpecifiedString(len, stringToAvoid);
        }
        return genString;
    }

    public static Account insertPersonAccount(String firstName, String lastName, String individualType){
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.FirstName = firstName;
        objAcc.LastName = lastName;
        objAcc.FinServ__IndividualType__c = individualType;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        objAcc = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE Id =: objAcc.id];
        return objAcc;
    }
    
        
    public static Account insertAgencyAccount(String accName, String producerId){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_Id__c = producerId;
        // objAcc.FinServ__IndividualId__c = accName;
        // objAcc.CDHID__c = producerId;
        // objAcc.FinServ__SourceSystemId__c = accName;
        // objAcc.Agent_District_Code__c = producerId;
        insert objAcc;
        return objAcc;
    }

    public static Account insertHouseholdAccount(String accName, String producerId){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IndustriesHousehold').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_Id__c = producerId;
        // objAcc.FinServ__IndividualId__c = accName;
        // objAcc.CDHID__c = producerId;
        // objAcc.FinServ__SourceSystemId__c = accName;
        // objAcc.Agent_District_Code__c = producerId;
        insert objAcc;
        return objAcc;
    }

    public static Account insertIndividualAccount(String accName, String producerId){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_Id__c = producerId;
        insert objAcc;
        return objAcc;
    }
    
    public static Account insertAgentAccount(String accName, String producerId){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_Id__c = producerId;
        insert objAcc;

        Contact objContact = new Contact();
        objContact.FirstName = accName;
        objContact.LastName = accName;
        objContact.AccountId = objAcc.Id;
        insert objContact;

        return objAcc;
    }
}