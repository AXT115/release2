public without sharing class AmFam_ProducerAccessService {
    public static void createProducerAccessRecords(Account acc, Lead leadRec, Account existingAccount, String reason) {
        try {
            Id agencyId = acc?.Agency__c!= null?acc.Agency__c:leadRec?.Agency__c;
            Account agency = [SELECT Id, Producer_Id__c FROM Account WHERE Id =: agencyId];
            // Look for existing producer access records for this exact combination
            List<ProducerAccess__c> existingPARecords = [SELECT Id, Reason__c, Producer_Identifier__c, RelatedId__c FROM ProducerAccess__c WHERE Producer_Identifier__c =: agency.Producer_Id__c AND RelatedId__c =: existingAccount.Id ];
            // Only create new producer access records if the system does not currently contain them.  
            // If it does, we do not need to create new access records, just need to redirect to the record.
            List<ProducerAccess__c> updateExistingPARecords = new List<ProducerAccess__c>();
            if (existingPARecords.size() == 0) {
                ProducerAccess__c pa = new ProducerAccess__c();
                pa.Producer_Identifier__c = agency.Producer_Id__c;
                pa.RelatedId__c = existingAccount.Id;
                pa.Reason__c = reason;
                insert pa;
            }else if(existingPARecords != null){
                for(ProducerAccess__c pAccess : existingPARecords){
                    List<String> reasonList = pAccess.Reason__c.split(';');
                    String existingReasons = pAccess.Reason__c;
                    if(!reasonList.contains(reason)){
                    	pAccess.Reason__c = existingReasons+';'+reason;
                        updateExistingPARecords.add(pAccess);
                    }
                }
            }
            if(updateExistingPARecords.size()>0) update updateExistingPARecords;            
        } catch (Exception ex) {
            throw new AuraHandledException('Message: ' + ex.getMessage() + '\nStack Trace: ' + ex.getStackTraceString());
        }
    }
}