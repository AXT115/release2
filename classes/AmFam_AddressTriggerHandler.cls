public without sharing class AmFam_AddressTriggerHandler {

    /*
    public void OnBeforeInsert (List<Address> newRecord) {}
    */

    public void OnAfterInsert (List<Schema.Address> newRecords, Map<Id, Schema.Address> newRecordsMap) {
        AmFam_AddressTriggerService.updateAccountAddress(newRecords, null, newRecordsMap, null);
    }

    public void OnAfterUpdate (List<Schema.Address> newRecords,
                               List<Schema.Address> oldRecords,
                               Map<Id, Schema.Address> newRecordsMap,
                               Map<Id, Schema.Address> oldRecordsMap) {
        
        AmFam_AddressTriggerService.updateAccountAddress(newRecords, oldRecords, newRecordsMap, oldRecordsMap);
    }

    /*
    public void OnBeforeUpdate(List<Account> newRecords,
                                List<Account> oldRecords,
                                Map<ID, Account> newRecordsMap,
                                Map<ID, Account> oldRecordsMap) {

    }
    */

}