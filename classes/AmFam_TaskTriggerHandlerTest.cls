@isTest
private class AmFam_TaskTriggerHandlerTest {
    @TestSetup
    static void bypassProcessBuilderSettings(){
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }

    @isTest static void incrementContactAttemptOnLeadTest() {
        wsPartyManage.lpeCallout = false;
        Account a = new Account(Name = 'Test Agency');
        insert a;
        Lead ld = new Lead();
        ld.Agency__c = a.Id;
        ld.FirstName = 'TestFirst';
        ld.LastName = 'TestLast';
        ld.Phone = '1231231234';
        ld.Email = 'testing@test.com';
        ld.Email_Usage__c = 'HOME';
        ld.LeadSource = 'Amfam.com';
        ld.Company = 'Test';
        ld.Stage__c = 'Working';
        ld.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();
        insert ld;

        Task newEmail = new Task (Subject = 'Email: Testing for Email', WhoId = ld.id, TaskSubtype = 'Email', Status = 'Open');
        insert newEmail;
        Lead updatedLead =  [SELECT Id, Contact_Attempts__c FROM Lead WHERE Id =: ld.id];
        System.assertEquals(1, updatedLead.Contact_Attempts__c);

        Task newEmail1 = new Task (Subject = 'Email: Testing for Email', WhoId = ld.id, Status = 'Open');
        insert newEmail1;
        updatedLead =  [SELECT Id, Contact_Attempts__c FROM Lead WHERE Id =: ld.id];
        System.assertEquals(2, updatedLead.Contact_Attempts__c);

        Task newEmail2 = new Task (Subject = 'Email: Testing for Email', Status = 'Open');
        insert newEmail2;
        updatedLead =  [SELECT Id, Contact_Attempts__c FROM Lead WHERE Id =: ld.id];
        System.assertEquals(2, updatedLead.Contact_Attempts__c);

    }
}