/**
*    @description  Test Code to check AmFam_UserTriggerHandler Class
*    @author Salesforce Services
*    @date 05/21/2020
*    @group @Test
*/

//!!!GMU-7/8/21
//These tests have been disabled and replaced by tests for AmFam_UserService and of individual Queueable classes
//This was necessary due to the introduction of a chain of Queueables to be able to accomplish what was
//needed for the UserTriggerHandler.  Queueables may not be chained in tests which prevents these kind of 
//end to end functional tests from working
//Leaving test here for reference and in case a way to do these kind of tests becomes available


@isTest
private class AmFam_UserTriggerHandlerTest {
/*
    @TestSetup
    static void bypassProcessBuilderSettings(){
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    @isTest static void userInsertTest() {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        wsPartyManage.lpeCallout = false;
        Account acc = AmFam_TestDataFactory.createAccountByNameWithLeads('John Herrera Agency, LLC', 2);
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345'
                                );
        Test.startTest();
        insert agentUser;
        Test.stopTest();

        List<Account> accts = [SELECT Id,RecordType.Name,Name,Producer_ID__c FROM Account];

        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId,AccountId,RowCause  FROM AccountShare];
        List<Id> ids = new List<Id>();
        for (AccountShare share : accountShares)
        {
            ids.add(share.UserOrGroupId);
        }
        List<LeadShare> leadShares = [SELECT Id, UserOrGroupId FROM LeadShare
                                        WHERE Lead.Agency__c =: acc.Id AND UserOrGroupId =:agentUser.Id];
        System.assertEquals(1, accountShares.size());
    }
    @isTest static void userInsertTest2() {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        wsPartyManage.lpeCallout = false;
        Account acc = AmFam_TestDataFactory.createAccountByNameWithLeads('John Herrera Agency, LLC', 2);
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345'
                                );
        insert agentUser;

        Test.startTest();
        System.enqueueJob(new AmFam_UserUpdateFromCalloutQueueable(new List<Id>(newUserMap.keySet())));
        AmFam_UserService.producerUpdate(newUserMap.keyset());
        Test.stopTest();

        List<Account> accts = [SELECT Id,RecordType.Name,Name,Producer_ID__c FROM Account];
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId,AccountId,RowCause  FROM AccountShare];
        List<Id> ids = new List<Id>();
        for (AccountShare share : accountShares)
        {
            ids.add(share.UserOrGroupId);
        }
        List<LeadShare> leadShares = [SELECT Id, UserOrGroupId FROM LeadShare
                                        WHERE Lead.Agency__c =: acc.Id AND UserOrGroupId =:agentUser.Id];
        System.assertEquals(1, accountShares.size());
    }


    @isTest static void userUpdateTest() {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        wsPartyManage.lpeCallout = false;
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        Account acc = AmFam_TestDataFactory.createAccountByNameWithLeads('John Herrera Agency, LLC', 2);
        User repUser = new User (alias = 'srep', email='test1246@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test1246@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
        Test.startTest();
        AmFam_UserService.callProducerAPI = false;
        insert repUser;
        AmFam_UserService.callProducerAPI = true;
        update repUser;
        Test.stopTest();
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId FROM AccountShare
                                            WHERE AccountId = :acc.Id AND UserOrGroupId =:repUser.Id];
        List<LeadShare> leadShares = [SELECT Id, UserOrGroupId FROM LeadShare
                                    WHERE Lead.Agency__c =: acc.Id AND UserOrGroupId =:repUser.Id];
        System.assertEquals(1, accountShares.size());
    }

    @isTest static void ssoUserProfileTest() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Sales & Service Operations Rep'];
        User ssoUser = new User (alias = 'srep', email='test12481@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test12481',
                                lastname='TestSSO',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test12481@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
        Test.startTest();
        AmFam_UserService.callProducerAPI = false;
        insert ssoUser;
        AmFam_UserService.callProducerAPI = true;
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        Test.stopTest();
        Group g = [SELECT Id FROM Group WHERE DeveloperName = 'S_SO_Users'];
        List<GroupMember> groupMembers = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: g.Id AND UserOrGroupId =: ssoUser.Id];
        System.assertEquals(groupMembers[0].UserOrGroupId, ssoUser.Id);
    }

    @isTest static void userUpdateAddRemoveLeadAccessTest() {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        wsPartyManage.lpeCallout = false;
        AmFam_UserService.callProducerAPI = false;
        Account acc = AmFam_TestDataFactory.createAccountByNameWithLeads('John Herrera Agency, LLC', 2);
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User repUser = new User (alias = 'srep', email='test124678@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test124678',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test124678@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
        insert repUser;
        Account a = new Account(Name = 'Test Agency', Producer_ID__c = '1000005');
        insert a;
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = 'TestFirst';
        l.LastName = 'TestLast';
        l.Phone = '1231231234';
        l.Email = 'testing@test.com';
        l.LeadSource = 'Amfam.com';
        l.Company = 'Test';
        l.OwnerId = repUser.id;
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();
        insert l;
        AccountShare accShr = new AccountShare();
        accShr.AccountAccessLevel = 'Read';
        accShr.AccountId = a.Id;
        accShr.UserOrGroupId = repUser.Id;
        accShr.OpportunityAccessLevel = 'None';
        insert accShr;
        Test.startTest();
        AmFam_UserService.callProducerAPI = true;
        update repUser;
        Test.stopTest();
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId FROM AccountShare
                                            WHERE AccountId =: acc.Id AND UserOrGroupId =:repUser.Id];
        List<LeadShare> leadShares = [SELECT Id, UserOrGroupId FROM LeadShare
                                    WHERE Lead.Agency__c =: acc.Id AND UserOrGroupId =:repUser.Id];
        System.assertEquals(1, accountShares.size());
    }
*/
}