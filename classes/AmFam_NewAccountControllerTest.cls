@isTest
private class AmFam_NewAccountControllerTest {
    @TestSetup
    static void makeData(){
    }

    @IsTest
    static void testGetExistingRecordType(){
        Account acc = new Account(Name = 'Test', RecordTypeId = AmFam_Utility.getAccountBusinessRecordType());
        insert acc;
        System.assertEquals(AmFam_Utility.getAccountBusinessRecordType(),AmFam_NewAccountController.getExistingRecordType(acc.Id));
    }

    @IsTest
    static void testCheckIfFieldsChanged(){
        Account existingAcc = new Account(FirstName = 'First', LastName = 'Last', RecordTypeId = AmFam_Utility.getPersonAccountRecordType());
        Account acc = new Account(FirstName = 'First', LastName = 'LastTwo');
        Test.startTest();
        System.assert(AmFam_NewAccountController.checkIfFieldsChanged(existingAcc,acc,'CDH_Demographic_Information_PA'));
        Test.stopTest();
        
    }

    @IsTest
    static void testInitRecordTypes(){
        Map<String, Id> retVal = AmFam_NewAccountController.initRecordTypes('Account');
        System.assert(!retVal.isEmpty());
    }

    @IsTest
    static void testSendPersonAccountToCDH(){
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User u = new User (alias = 'srep', email='test1246@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test1246@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert u;
        AmFam_UserService.callProducerAPI = true;

        Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('CONTACT'));
        system.runAs(u) {
            Account agency = new Account();
            agency.Name = 'Test Agency';
            agency.Producer_ID__c = '12345';
            agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            insert agency;
            Account acc = new Account();
            acc.Agency__c = agency.Id;
            acc.Partner__c = 'AFI';
            acc.Salutation = 'MR';
            acc.FirstName = 'Testing';
            acc.MiddleName = 'A';
            acc.LastName = 'AccountForInsert';
            acc.Family_Suffix__pc = 'JR';
            acc.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();

            acc.BillingStreet = '1940 Maryland Avenue';
            acc.BillingCity = 'Tampa';
            acc.BillingStateCode = 'FL';
            acc.BillingCountryCode = 'US';
            acc.BillingPostalCode = '33602';
    
            acc.ShippingStreet = '1945 Maryland Avenue';
            acc.ShippingCity = 'Tampa';
            acc.ShippingStateCode = 'FL';
            acc.ShippingCountryCode = 'US';
            acc.ShippingPostalCode = '33602';
    
            insert acc;
            Test.startTest();
            AmFam_NewAccountController.AccountWrapper accWrapper = AmFam_NewAccountController.sendPersonAccountToCDH(false, acc.Id, acc, true, null, null, null, null, null);
            Test.stopTest();


            List<Schema.Address> addressList = [SELECT Street, City, PostalCode FROM Address];

            //A single address should be created from the CDH Response
            System.assertEquals(1, addressList.size());


            System.assertEquals('Testing',accWrapper.acc.Greeting__c);
        }
    }
}