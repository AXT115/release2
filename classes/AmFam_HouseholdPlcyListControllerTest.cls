@IsTest
public with sharing class AmFam_HouseholdPlcyListControllerTest {
/*
    @IsTest
    public static void testGetHouseholdPolicies()
    {
        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');

        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');

        List<FinServ__ReciprocalRole__c> roles = new List<FinServ__ReciprocalRole__c>();

        List<AccountContactRelation> rels = new List<AccountContactRelation>();

        List<Contact> contacts = [SELECT Id FROM Contact];

        for (Contact contact : contacts)
        {
            AccountContactRelation rel = new AccountContactRelation();
            rel.AccountId = household.Id;
            rel.ContactId = contact.Id;
            rel.Roles = 'Client';
            rel.IsActive = true;
            rels.add(rel);
        }
        
        insert rels;

        List<InsurancePolicy> newPols = new List<InsurancePolicy>();
        InsurancePolicy pol1 = new InsurancePolicy();
        pol1.Name = '1234';
        pol1.PolicyName = 'Policy1';
        pol1.NameInsuredId = person1.Id;
        pol1.PolicyType = 'Life';
        newPols.add(pol1);

        InsurancePolicy pol2 = new InsurancePolicy();
        pol2.Name = '5678';
        pol2.PolicyName = 'Policy1';
        pol2.NameInsuredId = person2.Id;
        pol2.PolicyType = 'Auto';
        newPols.add(pol2);

        insert newPols;

        List<InsurancePolicyParticipant> ipps = new List<InsurancePolicyParticipant>();
        InsurancePolicyParticipant ipp = new InsurancePolicyParticipant();
        ipp.InsurancePolicyId = pol1.Id;
        ipp.PrimaryParticipantAccountId = person1.Id;
        ipp.Role = 'Owner';
        ipps.add(ipp);

        ipp = new InsurancePolicyParticipant();
        ipp.InsurancePolicyId = pol2.Id;
        ipp.PrimaryParticipantAccountId = person2.Id;
        ipp.Role = 'Owner';
        ipps.add(ipp);

        insert ipps;


        List<AmFam_HouseholdPolicyListController.ResponseRecord> householdPols = AmFam_HouseholdPolicyListController.getHouseholdPolicies(household.Id);
        System.assertEquals(2,householdPols.size());
        
    }
*/
    @IsTest
    public static void testGetPolicies()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');

        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');

        List<FinServ__ReciprocalRole__c> roles = new List<FinServ__ReciprocalRole__c>();

        List<AccountContactRelation> rels = new List<AccountContactRelation>();

        List<Contact> contacts = [SELECT Id FROM Contact];

        for (Contact contact : contacts)
        {
            AccountContactRelation rel = new AccountContactRelation();
            rel.AccountId = household.Id;
            rel.ContactId = contact.Id;
            rel.Roles = 'Client';
            rel.IsActive = true;
            rels.add(rel);
        }
        
        insert rels;

        Account agencyBobAccount = AmFam_TestDataFactory.insertAgencyAccount('Agency Account','73320');
        Account agencyOutAccount = AmFam_TestDataFactory.insertAgencyAccount('Agency Account','99999');

        //need 2 producers, one in bob, one out bob
        //need 2 agency accounts one with prod id of 73320
        Producer prod1 = new Producer();
        prod1.Producer_ID__c = '73320';
        prod1.Name = '73320';
        prod1.InternalUserId = UserInfo.getUserId();
        prod1.Type = 'Agency';
        insert prod1;

        Producer prod2 = new Producer();
        prod2.Producer_ID__c = '99999';
        prod2.Name = '99999';
        prod2.InternalUserId = UserInfo.getUserId();
        prod2.Type = 'Agency';
        insert prod2;

        List<InsurancePolicy> newPols = new List<InsurancePolicy>();
        InsurancePolicy pol1 = new InsurancePolicy();
        pol1.Name = '1234';
        pol1.PolicyName = 'Policy1';
        pol1.NameInsuredId = person1.Id;
        pol1.PolicyType = 'Life';
        pol1.ProducerId = prod1.Id;
        newPols.add(pol1);

        InsurancePolicy pol2 = new InsurancePolicy();
        pol2.Name = '5678';
        pol2.PolicyName = 'Policy2';
        pol2.NameInsuredId = person2.Id;
        pol2.PolicyType = 'Auto';
        pol2.ProducerId = prod2.Id;
        newPols.add(pol2);

        InsurancePolicy pol3 = new InsurancePolicy();
        pol3.Name = '1010';
        pol3.PolicyName = 'Policy3';
        pol3.NameInsuredId = person2.Id;
        pol3.PolicyType = 'Auto';
        pol3.ProducerId = prod2.Id;
        newPols.add(pol3);
        insert newPols;

        List<InsurancePolicyParticipant> ipps = new List<InsurancePolicyParticipant>();
        InsurancePolicyParticipant ipp = new InsurancePolicyParticipant();
        ipp.InsurancePolicyId = pol1.Id;
        ipp.PrimaryParticipantAccountId = person1.Id;
        ipp.Role = 'Owner';
        ipps.add(ipp);

        ipp = new InsurancePolicyParticipant();
        ipp.InsurancePolicyId = pol2.Id;
        ipp.PrimaryParticipantAccountId = person2.Id;
        ipp.Role = 'Owner';
        ipps.add(ipp);

        ipp = new InsurancePolicyParticipant();
        ipp.InsurancePolicyId = pol3.Id;
        ipp.PrimaryParticipantAccountId = person2.Id;
        ipp.Role = 'Owner';
        ipps.add(ipp);

        insert ipps;

        Test.startTest();
        Map<String, List<AmFam_HouseholdPolicyListController.ResponseRecord>> householdPols = AmFam_HouseholdPolicyListController.getPolicies(household.Id);
        Test.stopTest();
        List<AmFam_HouseholdPolicyListController.ResponseRecord> resultPols = householdPols.get('policies');
        System.assertEquals(1,resultPols.size());

        resultPols = householdPols.get('additional');
        System.assertEquals(2,resultPols.size());

    }
}