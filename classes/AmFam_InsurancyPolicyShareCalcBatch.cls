public with sharing class AmFam_InsurancyPolicyShareCalcBatch implements database.batchable<sobject>, Database.AllowsCallouts, Database.Stateful
{    
    //add flag field to InsurancePolicy
    //update update trigger handler to not process records that have the flag set
    //reset flags on records processed by this batch

    public AmFam_InsurancyPolicyShareCalcBatch() 
    {
    }
    
    public Database.QueryLocator start(database.batchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id,ProducerId,Batch_Processed__c FROM InsurancePolicy WHERE Batch_Processed__c = false]);
    }

    public void execute(Database.Batchablecontext bd, List<InsurancePolicy> scope) 
    {
        AmFam_InsurancePolicyService.updateInsurancePolicyShares(scope);

        for (InsurancePolicy policy : scope) 
        {
            policy.Batch_Processed__c = true;
        }

        if (!scope.isEmpty())
        {
            update scope;
        }
    }

    public void finish(database.batchableContext bc) 
    {
    }
}