@IsTest
public with sharing class AmFam_CreateUserAccountsQueueableTest {

    @IsTest
    private static void testCreateAccountsForUser()
    {
        Id individualRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Id agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();  

        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = AmFam_UserUpdateFromCalloutQueueableTest.createTestUser();
        u.Producer_ID__c = '73320';
        insert u;

        //List<User> userList = new List<User>{u};
        Test.startTest();
        
        List<Id> userIds = new List<Id>();
        userIds.add(u.Id);
        
        System.enqueueJob(new AmFam_CreateUserAccountsQueueable(userIds));

        Test.stopTest();

        List<Account> accts = [SELECT Id,Producer_ID__c,Name,RecordType.Name FROM Account];
        System.debug('ACCOUNTS:'+accts);

        List<Contact> contacts = [SELECT Id,Name,AccountId,User_Producer_ID__c FROM Contact];
        System.debug('CONTACTS:'+contacts);

        List<Account> indAcct = [SELECT Id,Producer_ID__c,Agent_District_Code__c FROM Account WHERE RecordTypeId = :individualRecordTypeId];
        System.debug('INDACCT:'+indAcct);
        String rawProdId = indAcct[0].Producer_ID__c.removeStart('P-');
        List<Account> agencyAcct = [SELECT Id,Producer_ID__c,Agent_District_Code__c FROM Account WHERE RecordTypeId = :agencyRecordTypeId AND Producer_ID__c = :rawProdId];
        List<Contact> contact = [SELECT Id, AccountId FROM Contact WHERE AccountId = :indAcct[0].Id LIMIT 1];

        System.assert(indAcct.size() == 1);
        System.assert(agencyAcct.size() == 1);
        System.assert(contact.size() == 1);

        List<AccountContactRelation> acr = [SELECT Id FROM AccountContactRelation WHERE ContactId = :contact[0].Id AND 
                                                        AccountId = :indAcct[0].Id];
        System.debug('ACR:'+acr);
        System.assert(acr.size() == 1);

        Account acct = indAcct[0];
        System.assert(acct.Agent_District_Code__c != null);

        List<Id> acctIds = new List<Id>();
        acctIds.add(indAcct[0].Id);
        acctIds.add(agencyAcct[0].Id);

        List<String> prodIds = new List<String>{u.Producer_ID__c,'P-'+u.Producer_ID__c};

        System.debug('ACCTIDS:'+acctIds);
        System.debug('PRODIDS:'+prodIds);
        List<ProducerAccess__c> prodAccess = [SELECT Id,Producer_Identifier__c,RelatedId__c FROM ProducerAccess__c WHERE Producer_Identifier__c IN :prodIds AND RelatedId__c IN :acctIds];
        System.debug('TEST PRODACCESS:'+prodAccess);

        System.assert(prodAccess.size() == 2);
    }
}