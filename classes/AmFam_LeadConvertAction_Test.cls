@istest
global without sharing class AmFam_LeadConvertAction_Test {

    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }



    static testMethod void testConvertPersonLeadNoMatch() {
        User admin = insertUser('System Administrator', 'uadmin', 'Admin', 'Admin');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');


        System.runAs(admin) {
            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');

            Producer producer1 = insertProducer(agency, agent1, '12345');
            ProducerAccess__c pa = insertProducerAccess(agency, '12345');
    
            Lead newLead = insertPersonLead(agency, 'Pepe', 'Trueno');
    

            Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('convertPersonNoMatch'));

            Test.startTest();

            AmFam_UserService.callProducerUpdateAPI = false;

            AmFam_LeadConvertQuickActionController.getConvertOptionsForLead(newLead.Id, 'Pepe Trueno');

            AmFam_LeadConvertQuickActionController.convertLead(newLead.Id, 'Pepe', 'Trueno', null, true, null, null, null, true, true, null);

            Test.stopTest();


            List<Account> accountList = [SELECT Id, Name, OwnerId, Owner.Name, IsPersonAccount FROM Account];
            
            //Validate the results
            //Person account and household from convert should all have the user set to user1
            for (Account acc : accountList) {
                if (acc.Id != agency.Id) {
                    System.assertEquals(agent1.Id, acc.OwnerId);
                }
                if (acc.IsPersonAccount) {
                    List<AssociatedLocation> assocLocs = [SELECT Id, Purpose__c, Type, LocationId, Location.Name FROM AssociatedLocation ORDER BY Location.Name];
                    system.assertEquals(6, assocLocs.size());

                    for (AssociatedLocation assLoc : assocLocs) {
                        System.debug('### AL: ' + assLoc.Id + ' - Location ' + assLoc.LocationId + ' ' + assLoc.Location.Name);
                    }


                    List<Schema.Address> addressList = [SELECT Id FROM Address];
                    List<Schema.Location> locationList = [SELECT Id FROM Location];

                    system.assertEquals(3, addressList.size());
                    system.assertEquals(3, locationList.size());


                }
            }


        }
    }

    static testMethod void testConvertPersonLeadWithMatch() {

        User admin = insertUser('System Administrator', 'uadmin', 'Admin', 'Admin');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');


        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account personAccount = insertPersonAccount( 'firstName', 'lastName', agency.Id);

            Producer producer1 = insertProducer(agency, agent1, '12345');
            ProducerAccess__c pa1 = insertProducerAccess(agency, '12345');

            ProducerAccess__c pa2 = insertProducerAccess(personAccount, '12345');


            Lead newLead = insertPersonLead(agency, 'Pepe', 'Trueno');

            Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('convertPersonWithMatch'));

            Test.startTest();

            AmFam_UserService.callProducerUpdateAPI = false;

            AmFam_LeadConvertQuickActionController.convertLead(newLead.Id, 'Pepe', 'Trueno', null, true, null, null, null, true, true, null);

            AmFam_LeadConvertQuickActionController.convertLead(newLead.Id, 'Pepe', 'Trueno', '98765', true, null, null, null, true, true, null);

            Test.stopTest();


            List<Account> accountList = [SELECT Id, Name, OwnerId, Owner.Name FROM Account];
            
            //Validate the results
            //Person account and household from convert should all have the user set to user1
            for (Account acc : accountList) {
                
                if (acc.Id != agency.Id) {
                    System.assertEquals(agent1.Id, acc.OwnerId);
                }
            }



        }
    }





    

    static testMethod void testConvertBusinessLeadNoMatch() {

        User admin = insertUser('System Administrator', 'uadmin', 'Admin', 'Admin');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Lead newLead = insertBusinessLead(agency, 'Pepe', 'Trueno', 'Acme');

            Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('convertBusinessNoMatch'));

            Test.startTest();


            AmFam_UserService.callProducerUpdateAPI = false;

            AmFam_LeadConvertQuickActionController.convertLead(newLead.Id, 'Pepe', 'Trueno', null, true, null, null, null, true, true, null);

            List<Account> accountList = [SELECT Id, Name FROM Account];

            Test.stopTest();

        }
    }



    static testMethod void testConvertWithError() {

        User admin = insertUser('System Administrator', 'uadmin', 'Admin', 'Admin');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Lead newPersonLead = insertPersonLead(agency, 'Road', 'Runner');
            Lead newBusinessLead = insertBusinessLead(agency, 'The', 'Coyote', 'Acme');

            Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('convertWithError'));

            Test.startTest();

            AmFam_UserService.callProducerUpdateAPI = false;


            AmFam_LeadConvertQuickActionController.constructContact(newPersonLead, null, null, null);
            AmFam_LeadConvertQuickActionController.constructContact(newBusinessLead, null, null, null);

            AmFam_LeadConvertQuickActionController.getPeresonIdentityInfo(newPersonLead);

            AmFam_LeadConvertQuickActionController.LeadConvertResponseWrapper lcwWithError = new AmFam_LeadConvertQuickActionController.LeadConvertResponseWrapper(new List<String>());
            lcwWithError.addErrorMessage('error message');
            
            AmFam_LeadConvertQuickActionController.LeadConvertResponseWrapper lcwWithError2 = new AmFam_LeadConvertQuickActionController.LeadConvertResponseWrapper(agency.Id);
            
            AmFam_LeadConvertQuickActionController.LeadConverWrapper lcw = new AmFam_LeadConvertQuickActionController.LeadConverWrapper(newPersonLead, agency);


            try {
                AmFam_LeadConvertQuickActionController.getConvertOptionsForLead('00Q8A000005NrFUUA1', 'leadName');
            
            }
            catch (Exception ex) {
                System.debug('### catch exception' + ex.getMessage());
            }

            AmFam_LeadConvertQuickActionController.convertLead(newBusinessLead.Id, 'Pepe', 'Trueno', null, true, null, null, null, true, true, null);

            List<Account> accountList = [SELECT Id, Name FROM Account];

            Test.stopTest();

        }
    }

    static testMethod void testupdateLead() {
        
    	User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');
        
        System.runAs(agent1) {

            wsPartyManage.lpeCallout = false;
            
            Account agency = insertAgencyAccount('Acme');
            Lead newPersonLead = insertPersonLead(agency, 'Road', 'Runner');
            String selectedRow = '[{"addressLine1":"322 Jessica Ave","censusBlockGroup":"2","censusTractNumber":"940004","city":"Bartlesville","Code1ReturnIndicator":"OVERRIDE","countryIdentifier":"USA","deliveryPointValidationCommercialMailreceivingCode":"Unconfirmed","deliveryPointValidationMatchIndicator":"Unable to confirm delivery point","fipsCountyNumber":"113","fipsStateNumber":"40","geoMatchLevelCode":"6","latitudeNumber":"+36.744800","longitudeNumber":"-96.004200","shortCity":"Bartlesville","stateCode":"OK","zip5Code":"74003"},{"addressLine1":"67 Utah Ave","censusBlockGroup":"3","censusTractNumber":"000600","city":"Bartlesville","Code1ReturnIndicator":"OVERRIDE","countryIdentifier":"USA","deliveryPointValidationCommercialMailreceivingCode":"Unconfirmed","deliveryPointValidationMatchIndicator":"Unable to confirm delivery point","fipsCountyNumber":"147","fipsStateNumber":"40","geoMatchLevelCode":"6","latitudeNumber":"+36.730700","longitudeNumber":"-95.919700","shortCity":"Bartlesville","stateCode":"OK","zip5Code":"74006"},{"addressLine1":"1107 S Keeler Ave","carrierRoute":"C004","censusBlockGroup":"1","censusBlockMD":"011","censusTractNumber":"000800","city":"Bartlesville","Code1ReturnIndicator":"STANDARD","countryIdentifier":"USA","deliveryPointValidationCommercialMailreceivingCode":"No","deliveryPointValidationMatchIndicator":"Yes","fipsCountyNumber":"147","fipsStateNumber":"40","geoMatchLevelCode":"3","latitudeNumber":"+36.742174","longitudeNumber":"-95.980961","shortCity":"Bartlesville","stateCode":"OK","zip2Code":"07","zip4Code":"4755","zip5Code":"74003"}]';
            Test.startTest();
            AmFam_UserService.callProducerUpdateAPI = false;
            Lead leadRec = AmFam_LeadConvertQuickActionController.updateLeadRecord(newPersonLead, selectedRow, newPersonLead.id);
            Test.stopTest();
            system.assert(leadRec.StateCode == 'OK');
          }
    }

    static testMethod void testConvertCode1AddressValidation() {

        User admin = insertUser('System Administrator', 'uadmin', 'Admin', 'Admin');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Thanos');
            Lead newLead = insertPersonLead(agency, 'Joe', 'Tremo');

            Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('convertCODE1AddressValidation'));

            Test.startTest();
            AmFam_UserService.callProducerUpdateAPI = false;
            AmFam_LeadConvertQuickActionController.convertLead(newLead.Id, 'Pepe', 'Trueno', null, false, null, null, null, true, true, null);
            Test.stopTest();

        }
    }


    private static Lead insertPersonLead(Account agency, String firstName, String lastName) {

        Lead l = new Lead();
        l.Agency__c = agency.Id;
        l.FirstName = firstName;
        l.LastName = lastName;
        l.MobilePhone = '1231231234';
        l.Street = '1 Main Street';
        l.City = 'Anytown';
        l.StateCode = 'WI';
        l.CountryCode = 'US';
        l.PostalCode = '12345';
        l.Residence_Business__c = 'PRM';
        l.Mailing__c = 'PRM';

        l.Email = 'testing@test.com';
        l.Email_Usage__c = 'HOME';
        l.Marital_Status__c = 'M';
        l.Gender__c = 'M';
        l.Date_Of_Birth__c = Date.today();
        l.LeadSource = 'Amfam.com';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        l.Party_Level__c = 'Entrant';
        l.Disposition_Reason__c = 'New';
        
        l.Phone = '6141231234';
        l.Phone_Usage__c = 'WORK';
        l.Additional_Phone_1__c = '6141231235';
        l.Additional_Phone_1_Usage__c = 'HOME';
        l.Additional_Phone_2__c = '6141231236';
        l.Additional_Phone_2_Usage__c = 'WORK';
        l.Additional_Phone_3__c = '6141231237';
        l.Additional_Phone_3_Usage__c = 'HOME';
        
        l.Email = 'sample123p@test.com.test';
        l.Email_Usage__c = 'HOME';
        l.Additional_Email_1__c = 'sample124p@test.com.test';
        l.Additional_Email_1_Usage__c = 'HOME';
        l.Additional_Email_2__c = 'sample125p@test.com.test';
        l.Additional_Email_2_Usage__c = 'WORK';
        
        l.Residence_Business_2__c = 'SEC';
        l.Mailing_2__c = 'OTHER';
        l.Additional_Address_Line_1__c = '2p Main Street';
        l.Additional_Address_Line_2__c = 'Apple-2';
        l.City_1__c = 'Anytown';
        l.State_Province_1__c = 'WI';
        l.Zip_Postal_Code_1__c = '12346';
        l.Country_1__c = 'US';

        l.Residence_Business_3__c = 'SEC';
        l.Mailing_3__c = 'OTHER';
        l.Additional_Address_Line_3__c = '3p Main Street';
        l.Additional_Address_Line_4__c = 'Apple-3';
        l.City_2__c = 'Anytown';
        l.State_Province_2__c = 'WI';
        l.Zip_Postal_Code_2__c = '12347';
        l.Country_2__c = 'US';
        insert l;

        return l;
    }

    private static Lead insertBusinessLead(Account agency, String firstName, String lastName, String company) {

        Lead l = new Lead();
        l.Agency__c = agency.Id;
        l.FirstName = firstName;
        l.LastName = lastName;
        l.Company = company;
        l.MobilePhone = '5141231234';
        l.Residence_Business__c	= 'PRM';
        l.Mailing__c = 'PRM';
        l.Street = '2 Main Street';
        l.City = 'Anytown';
        l.StateCode = 'WI';
        l.CountryCode = 'US';
        l.PostalCode = '12345';
        l.Email_Usage__c = 'HOME';
        l.Marital_Status__c = 'M';
        l.Gender__c = 'M';
        l.Date_Of_Birth__c = Date.today();
        l.LeadSource = 'Amfam.com';
        l.Company = 'Test';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        l.Party_Level__c = 'Entrant';
        l.Disposition_Reason__c = 'New';
        l.Phone = '5141231234';
        l.Phone_Usage__c = 'WORK';
        l.Additional_Phone_1__c = '5141231235';
        l.Additional_Phone_1_Usage__c = 'BIZ';
        l.Additional_Phone_2__c = '5141231236';
        l.Additional_Phone_2_Usage__c = 'WORK';
        l.Additional_Phone_3__c = '5141231237';
        l.Additional_Phone_3_Usage__c = 'BIZ';
        
        l.Email = 'sample123@test.com.test';
        l.Email_Usage__c = 'BIZ';
        l.Additional_Email_1__c = 'sample124@test.com.test';
        l.Additional_Email_1_Usage__c = 'BIZ';
        l.Additional_Email_2__c = 'sample125@test.com.test';
        l.Additional_Email_2_Usage__c = 'WORK';
        
        
        l.Residence_Business_2__c = 'SEC';
        l.Mailing_2__c = 'OTHER';
        l.Additional_Address_Line_1__c = '33 Steve Street';
        l.Additional_Address_Line_2__c = 'Apple-2';
        l.City_1__c = 'Anytown';
        l.State_Province_1__c = 'WI';
        l.Zip_Postal_Code_1__c = '12346';
        l.Country_1__c = 'US';

        l.Residence_Business_3__c = 'SEC';
        l.Mailing_3__c = 'OTHER';
        l.Additional_Address_Line_3__c = '3 Jennings Street';
        l.Additional_Address_Line_4__c = 'Apple-3';
        l.City_2__c = 'Anytown';
        l.State_Province_2__c = 'WI';
        l.Zip_Postal_Code_2__c = '12347';
        l.Country_2__c = 'US';
        insert l;

        return l;


    }


    private static Account insertAgencyAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_ID__c = '12345';

        insert objAcc;
        return objAcc;
    }

    private static Account insertPersonAccount(String firstName, String lastName, String agencyId){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.FirstName = firstName;
        objAcc.LastName = lastName;
        objAcc.RecordTypeId = devRecordTypeId;
        //objAcc.Producer_ID__c = '12345';
        objAcc.CDHID__c = '98765';
        if (String.isNotEmpty(agencyId) ) {
            objAcc.Agency__c = agencyId;
        }

        insert objAcc;
        return objAcc;
    }


    private static User insertUser(String profileName, String aliasName, String fisrtName, String lastName) {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name =: profileName];
        User agent = new User (     alias = aliasName, 
                                    email= aliasName + '12456@xyz.com',
                                    emailencodingkey='UTF-8', 
                                    FederationIdentifier= aliasName + '12456',
                                    firstname=fisrtName, 
                                    lastname=aliasName,
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', 
                                    profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username= aliasName + 'test12456@xyz.com.test',
                                    AmFam_User_ID__c= aliasName + '1234',
                                    Producer_Id__c = '12345'
                                );
        
        insert agent;

        return agent;

    }



    
    private static Producer insertProducer(Account acc, User internalUser, String identifier) {

        Producer producer = new Producer();
        producer.AccountId = acc.Id;
        producer.InternalUserId = internalUser.Id;
        producer.Name = identifier;

        insert producer;

        return producer;

    }

    private static ProducerAccess__c insertProducerAccess(Account acc, String identifier) {
        ProducerAccess__c pa = new ProducerAccess__c();
        pa.Producer_Identifier__c = identifier;
        pa.RelatedId__c = acc.Id;
        pa.Reason__c = 'ENTRY';

        insert pa;

        return pa;
    }


       
    private static AccountContactRelation insertAccountContactRelation(String accountId, String contactId) {
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = accountId;
        acr.ContactId = contactId;
        insert acr;
        return acr;
    }

    static testMethod void testConvertPhoneBusiness() {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        insert agent;
        System.runAs(agent) {
            Account agency = insertAgencyAccount('Acme');
            Lead newLead = insertBusinessLead(agency, 'Pepe', 'Trueno', 'Acme');
            Test.startTest();
            List<wsPartyManageParty.PartyPhoneType> partyPhoneList = AmFam_LeadConvertQuickActionController.getPartyPhone(newLead);
            List<ContactPointPhone> phoneList = AmFam_LeadConvertQuickActionController.convertPhone(agency.Id, partyPhoneList);
            system.assertEquals(4, phoneList.size(), 'Phone list count should match');
            system.assertEquals('5141231234', phoneList[0].TelephoneNumber, 'Phone number should match');
            Test.stopTest();
        }
    }

    static testMethod void testConvertEmailBusiness() {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        insert agent;
        System.runAs(agent) {
            Account agency = insertAgencyAccount('Acme');
            Lead newLead = insertBusinessLead(agency, 'Pepe', 'Trueno', 'Acme');
            Test.startTest();
            List<wsPartyManageParty.PartyEmailType> partyEmailList = AmFam_LeadConvertQuickActionController.getPartyEmail(newLead);
            List<ContactPointEmail> emailList = AmFam_LeadConvertQuickActionController.convertEmail(agency.Id, partyEmailList);
            system.assertEquals(3, emailList.size(), 'Email list count should match');
            system.assertEquals('sample123@test.com.test', emailList[0].EmailAddress, 'Email Address should match');
            Test.stopTest();
        }
    }
    
    static testMethod void testConvertAddressBusiness() {
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                               emailencodingkey='UTF-8', FederationIdentifier='test12456',
                               firstname='User', lastname='Agent',
                               languagelocalekey='en_US',
                               localesidkey='en_US', profileid = p.Id,
                               timezonesidkey='America/Chicago',
                               username='test12456@xyz.com.test',
                               AmFam_User_ID__c='test12456'
                              );
        insert agent;
        System.runAs(agent) {
            Account agency = insertAgencyAccount('Acme');
            Lead newLead = insertBusinessLead(agency, 'Pepe', 'Trueno', 'Acme');
            Test.startTest();
            List<wsPartyManageParty.PartyAddressType> partyAddresses = AmFam_LeadConvertQuickActionController.getPartyAddress(newLead, null, null, null);
            AmFam_AccountService.createsAddressRecord(agency.Id, partyAddresses);
            List<AssociatedLocation> addressLocationList = [SELECT Id FROM AssociatedLocation];
            system.assertEquals(6, addressLocationList.size(), 'Address list count should match');
            //system.assertEquals('B', addressLocationList[0].Type, 'Address type should match');
            List<Schema.Address> addressList = [SELECT Id FROM Address];
            system.assertEquals(3,addressList.size());
            Test.stopTest();
        }
    }
    
    static testMethod void testConvertPhonePerson() {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        insert agent;
        System.runAs(agent) {
            Account agency = insertAgencyAccount('Acme');
            //Account personAccount = insertPersonAccount( 'firstName', 'lastName',agency.Id);
            Lead newLead = insertPersonLead(agency, 'Pepe', 'Trueno');
            Test.startTest();
            List<wsPartyManageParty.PartyPhoneType> partyPhoneList = AmFam_LeadConvertQuickActionController.getPartyPhone(newLead);
            List<ContactPointPhone> phoneList = AmFam_LeadConvertQuickActionController.convertPhone(agency.Id, partyPhoneList);
            system.assertEquals(4, phoneList.size(), 'Phone list count should match');
            system.assertEquals('6141231234', phoneList[0].TelephoneNumber, 'Phone number should match');
            Test.stopTest();
        }
    }

    static testMethod void testConvertEmailPerson() {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        insert agent;
        System.runAs(agent) {
            Account agency = insertAgencyAccount('Acme');
            //Account personAccount = insertPersonAccount( 'firstName', 'lastName',agency.Id);
            Lead newLead = insertPersonLead(agency, 'Pepe', 'Trueno');
            Test.startTest();
            List<wsPartyManageParty.PartyEmailType> partyEmailList = AmFam_LeadConvertQuickActionController.getPartyEmail(newLead);
            List<ContactPointEmail> emailList = AmFam_LeadConvertQuickActionController.convertEmail(agency.Id, partyEmailList);
            system.assertEquals(3, emailList.size(), 'Email list count should match');
            system.assertEquals('sample123p@test.com.test', emailList[0].EmailAddress, 'Email Address should match');
            Test.stopTest();
        }
    }
    
    static testMethod void testConvertAddressPerson() {
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                               emailencodingkey='UTF-8', FederationIdentifier='test12456',
                               firstname='User', lastname='Agent',
                               languagelocalekey='en_US',
                               localesidkey='en_US', profileid = p.Id,
                               timezonesidkey='America/Chicago',
                               username='test12456@xyz.com.test',
                               AmFam_User_ID__c='test12456'
                              );
        insert agent;
        System.runAs(agent) {
            Account agency = insertAgencyAccount('Acme');
            //Account personAccount = insertPersonAccount( 'firstName', 'lastName',agency.Id);
            Lead newLead = insertPersonLead(agency, 'Pepe', 'Trueno');
            Test.startTest();
            List<wsPartyManageParty.PartyAddressType> partyAddresses = AmFam_LeadConvertQuickActionController.getPartyAddress(newLead, null, null, null);
            AmFam_AccountService.createsAddressRecord(agency.Id, partyAddresses);
            List<AssociatedLocation> addressLocationList = [SELECT Id FROM AssociatedLocation];
            system.assertEquals(6, addressLocationList.size(), 'Address list count should match');
            List<Schema.Address> addressList = [SELECT Id FROM Address];
            system.assertEquals(3,addressList.size());
            Test.stopTest();
        }
    }




}