public class AmFam_ParticipantTriggerHandler {
    /*
    public void OnBeforeInsert (List<Participant__c> newRecords) {

    }
    */
    public void OnAfterInsert (List<Participant__c> newRecords) {

        AmFam_ParticipantService.grantSameAccessFromAccount(newRecords);
    }

    public void OnAfterDelete(List<Participant__c> oldRecords,
                                Map<ID, Participant__c> oldRecordsMap) {
                                    
        AmFam_ParticipantService.removeAssociationFromAccountShares (oldRecords);
    
    }


    /*
    public void OnAfterUpdate (List<Participant__c> newRecords,
                               List<Participant__c> oldRecords,
                               Map<ID, Participant__c> newRecordsMap,
                               Map<ID, Participant__c> oldRecordsMap) {

        
    }
    public void OnBeforeUpdate(List<Participant__c> newRecords,
                                List<Participant__c> oldUser,
                                Map<ID, Participant__c> newRecordsMap,
                                Map<ID, Participant__c> oldRecordsMap) {

    }
    */


    

}