/**
*   @description trigger handler for AssociatedLocation object. Note: this should be the only place
*	where ALL AssociatedLocation related trigger logic should be implemented.
*   @author Salesforce Services
*   @date 05/13/2020
*   @group Trigger
*/

public class AmFam_AssociatedLocationTriggerHandler {


    public void OnBeforeInsert (List<AssociatedLocation> newAssociatedLocation) {

    }

    public void OnAfterInsert (List<AssociatedLocation> newAssociatedLocation,Map<ID, AssociatedLocation> newAssociatedLocationMap) {
        AmFam_AssociatedLocationService.updatePrimaryAddress(newAssociatedLocation,newAssociatedLocationMap);
    }

    public void OnAfterUpdate (List<AssociatedLocation> newAssociatedLocation,
                               List<AssociatedLocation> oldAssociatedLocation,
                               Map<ID, AssociatedLocation> newAssociatedLocationMap,
                               Map<ID, AssociatedLocation> oldAssociatedLocationMap) {
        AmFam_AssociatedLocationService.updatePrimaryAddress(newAssociatedLocation,newAssociatedLocationMap);
    }

    public void OnBeforeUpdate(List<AssociatedLocation> newAssociatedLocation,
                                List<AssociatedLocation> oldAssociatedLocation,
                                Map<ID, AssociatedLocation> newAssociatedLocationMap,
                                Map<ID, AssociatedLocation> oldAssociatedLocationMap) {

    }


}