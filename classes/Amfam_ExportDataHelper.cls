public without sharing class Amfam_ExportDataHelper {

    public static List<String> producerIdList = new List<String>{'1248','136240','1699','1947','21284','21800','22279','22442','22475','22814','23201','23226','23498','23992',
'24466','24633','25008','29443','29473','3839','4113','4223','425211','4294','4345','4390','4897',
'502011','502489','502680','502946','587','59891','6707','68603','77772','85555','8562','52481'};
    

    public static void exportAllProductionCSVForApex() {
        exportListCSVForApex(producerIdList);
    }

    public static void exportListCSVForApex(List<String> producers){
    
        Account[] accounts = [SELECT ID, Goes_by_Name__c,Producer_ID__c  FROM Account WHERE Producer_ID__c in: producers];

        for (Account account : accounts) {
	        Amfam_ExportDataHelper.createFilesForAgency(account.id);
        }

    }

    @future
    public static void createFilesForAgency(String accountId) {
            Amfam_ExportDataHelper.createCSVForApex(accountId);
            Amfam_ExportDataHelper.createTaskCSV(accountId);        
    }
    
    
    public static void createTaskCSV(String accountId) {
       Account account = [SELECT ID, Goes_by_Name__c,Producer_ID__c  FROM Account WHERE id =: accountId];
       String csvHeaders= 'Created Date, First Name,Last Name,Status,Subject,Comments,Priority,Due Date,Phone,Email\n';
       String csvData = csvHeaders;
       String fileName = account.Producer_ID__c + '_' + account.Goes_by_Name__c + '_Tasks.csv'; 

        Lead[] leads = [SELECT id, Name, FirstName, LastName, MobilePhone, Phone, Phone_Usage__c, Email, Email_Usage__c
        FROM Lead WHERE agency__c =: account.ID];

        Map<Id, Lead> leadsMap = new Map<Id, Lead>(leads);

        Task [] taskItems = [SELECT Subject, CreatedDate, Description, Status, WhoId, Priority, ActivityDate FROM Task
        where whoId =: leadsMap.keyset() AND TaskSubtype =: 'Task'];

        for (Task task : taskItems) {

            String record = '';
            Lead taskLead = leadsMap.get(task.whoId);
            record += addColumnData(task.CreatedDate.format('MM-dd-yyyy')) + ',';
            record += addColumnData(taskLead.FirstName) + ',';
            record += addColumnData(taskLead.LastName) + ',';
            record += addColumnData(task.Status) + ',';
            record += addColumnData(task.Subject) + ',';
            record += addColumnData(task.Description) + ',';
            record += addColumnData(task.Priority) + ',';
            record += addColumnDataDate(task.ActivityDate) + ',';


            String phone = '';

            if (lead.phone != null)
            {
                phone = taskLead.phone;
            }
            record += addColumnData(phone) + ',';

            record += addColumnData(taskLead.email);

            record += '\n';
            csvData += record;
        }

      
        ContentVersion file = new ContentVersion(     title = fileName,     versionData = Blob.valueOf(csvData ),      pathOnClient = '/' + fileName    );      
        insert file;

    }

    
    public static void createCSVForApex (String accountId) 
   {
       Account account = [SELECT ID, Goes_by_Name__c,Producer_ID__c  FROM Account WHERE id =: accountId];
       String csvHeaders= 'First Name,Last Name,Company,Position,Date of Birth,Gender,Marital Status,Phone Type,Phone,Email Type,Email,Country,Address 1,Address 2,City,State,Zip,Line of Business,Stage,Disposition Reason,Type,ID,Source,Score,Priority,Next Contact Date,Last Contact Date,DL Number,DL State,DL Date,Validated Mobile,Phone Validated,Home Email Validated,Office Email Validated,Do Not Contact Email,Do Not Contact Mobile,Do Not Contact Phone, Email Source,Mobile Preferred,Phone Preferred,Email Preferred,Phone Preferred Time,Phone Source,Home Range High,Home Range Low,Length of Residence,Mobile Preferred Time,Mobile Source,New Car Buyer,Owner/Renter,Owns Classic Car,Owns Motorcycle,Owns RV,Owns Truck,Prior Carrier Name,Prior Carrier Policy Effective Date,Prior Carrier Policy Expiration Date,Purchased Car,Purchased Truck,Vehicle Count,Chatter \n';
       String csvData = csvHeaders;
       String fileName = account.Producer_ID__c + '_' + account.Goes_by_Name__c + '_Leads.csv'; 
       
        
        Lead[] leads = [SELECT id, Name, FirstName, LastName, Company, Date_of_Birth__c, Gender__c, Marital_Status__c, MobilePhone, Phone, Phone_Usage__c, Email, Email_Usage__c,
        Street, City, StateCode, PostalCode, Lines_of_Business__c, Stage__c, Disposition_Reason__c, Party_Level__c, Contact_CDHID__c, LeadSource, Lead_Score__c,
        Priority__c,Next_Contact_Date__c, Last_Contact_Date__c, Drivers_License_Number__c, Driver_s_License_State__c, Drivers_License_Issue_Month__c, Drivers_License_Issue_Year__c,
        Mobile_Phone_Validated__c,Phone_Validated__c,Home_Email_Validated__c,Office_Email_Validated__c,ll_Do_Not_Contact_Email__c,ll_Do_Not_Contact_Mobile__c,ll_Do_Not_Contact_Phone__c, ll_Email_Source__c,
        ll_Home_Range_High__c,ll_Home_Range_Low__c,ll_Is_Mobile_Preferred__c,ll_Is_Phone_Preferred__c,ll_Is_Preferred_Email__c,ll_Length_of_Residence__c,ll_Mobile_Preferred_Time__c,ll_Mobile_Source__c,
        ll_New_Car_Buyer__c,ll_Owner_Renter__c,ll_Owns_Classic_Car__c,ll_Owns_Motorcycle__c,ll_Owns_RV__c,ll_Owns_Truck__c,ll_Phone_Preferred_Time__c,ll_Phone_Source__c,ll_Prior_Carrier_Name__c,ll_Prior_Carrier_Policy_Effective_Date__c,
        ll_Prior_Carrier_Policy_Expiration_Date__c,ll_Purchased_Car__c,ll_Purchased_Truck__c,ll_Vehicle_Count__c
        FROM Lead WHERE agency__c =: account.ID AND Duplicate_Lead__c != true ORDER BY Party_Level__c ASC];
        
        Map<Id, Lead> leadsMap = new Map<Id, Lead>(leads);


        FeedItem [] masterFeedItems = [SELECT BestCommentId,Body,CommentCount,CreatedBy.Name,CreatedDate,HasContent,HasFeedEntity,HasLink,HasVerifiedComment,
        Id,InsertedById,IsClosed,IsDeleted,IsRichText,LastModifiedDate,LikeCount,LinkUrl,ParentId,RelatedRecordId,Status,SystemModstamp,Title,Type FROM FeedItem
        where ParentId =: leadsMap.keyset() AND IsRichText =: true];

        for (Lead lead : leads) {
            String record = '';
            record += addColumnData(lead.FirstName) + ',';
            record += addColumnData(lead.LastName) + ',';
            record += addColumnData(lead.Company) + ',';
            record += addColumnData('') + ','; // Position is always blank


            if(lead.Date_of_Birth__c != null) {
                DateTime birthDate = Datetime.newInstance(lead.Date_of_Birth__c.year(), lead.Date_of_Birth__c.month(),lead.Date_of_Birth__c.day());
                record += addColumnData(birthDate.format('MM-dd-yyyy')) + ',';
            } else {
                record += addColumnData('') + ',';
            }
            
            String gender = '';
            if (lead.gender__c == 'M') {
               gender = 'Male'; 
            } else if (lead.gender__c == 'F') {
                gender = 'Female';
            }
            record += addColumnData(gender) + ',';
            
            String martialStatus = '';
            if (lead.Marital_Status__c == 'M') {
                martialStatus = 'Married';
            }
            record += addColumnData(martialStatus) + ',';

            String phoneType = '';
            String phone = '';
            if (lead.phone != null)
            {
                phone = lead.phone;
                phoneType = lead.Phone_Usage__c;
            }
            record += addColumnData(phoneType) + ',';
            record += addColumnData(phone) + ',';

            String emailType = '';
            if (lead.Email_Usage__c == 'HOME') {
                emailType = 'Home';
            } else if (lead.Email_Usage__c == 'WORK') {
                emailType = 'Work';
            }
            record += addColumnData(emailType) + ','; 
            record += addColumnData(lead.email) + ',';
            record += addColumnData('') + ','; // country should be blank
            
            String addressLine1 = '';
            String addressLine2 = '';
            
            if(lead.Street != null) {
                String[] addressLines = lead.Street.split('(\r?\n)+');
                addressLine1 = addressLines[0].trim();

                if (addressLines.size() > 1){
                    addressLine2 = addressLines[1].trim();
                }
            }

                        
            record += addColumnData(addressLine1) + ',';
            record += addColumnData(addressLine2) + ',';
            record += addColumnData(lead.City) + ',';
            record += addColumnData(lead.StateCode) + ',';
            record += addColumnData(lead.PostalCode) + ',';
            
            String lineOfBusiness = '';
            if (lead.Lines_of_Business__c != null) {
                        String[] lobList = lead.Lines_of_Business__c.split(';');
                        
                        if(lead.Lines_of_Business__c.contains('Personal Vehicle')) {
                            lineOfBusiness = 'Personal Vehicle';
                        } else if (lead.Lines_of_Business__c.contains('Property')) {
                            lineOfBusiness = 'Property';  
                        } else if (lead.Lines_of_Business__c.contains('Life')) {
                            lineOfBusiness = 'Life';
                        } else {
                            lineOfBusiness = lobList[0];
                        }
            }

            record += addColumnData(lineOfBusiness)+ ',';       

            record += addColumnData(lead.Stage__c)+ ',';
            record += addColumnData(lead.Disposition_Reason__c)+ ',';

            String partyType = 'Lead';
            if(lead.Party_Level__c == 'Contact') {
                partyType = 'Contact';
            }

            record += addColumnData(partyType) + ',';
            record += addColumnData(lead.Contact_CDHID__c) + ',';

            record += addColumnData(lead.LeadSource) + ',';


            String leadScore = '';

            if(lead.Lead_Score__c != null ) {
                leadScore =  String.valueOf(lead.Lead_Score__c);
            }

            record += addColumnData(leadScore) + ',';
            

            record += addColumnData(lead.Priority__c) + ',';

            if(lead.Next_Contact_Date__c != null) {
                DateTime nextContactDate = Datetime.newInstance(lead.Next_Contact_Date__c.year(), lead.Next_Contact_Date__c.month(),lead.Next_Contact_Date__c.day());
                record += addColumnData(nextContactDate.format('MM-dd-yyyy')) + ',';
            } else {
                record += addColumnData('') + ',';
            }
            
            if(lead.Last_Contact_Date__c != null) {
                DateTime lastContactDate = Datetime.newInstance(lead.Last_Contact_Date__c.year(), lead.Last_Contact_Date__c.month(),lead.Last_Contact_Date__c.day());
                record += addColumnData(lastContactDate.format('MM-dd-yyyy')) + ',';
            } else {
                record += addColumnData('') + ',';
            }

            record += addColumnData(lead.Drivers_License_Number__c) + ',';
            record += addColumnData(lead.Driver_s_License_State__c) + ',';
            record += addColumnData(lead.Drivers_License_Issue_Month__c) + ',';
            record += addColumnData(lead.Drivers_License_Issue_Year__c) + ',';

            //replacing Drivers_Original_License_Date__c field with Drivers_License_Issue_Month__c and Drivers_License_Issue_Year__c, the below code is no longer needed
            /*if(lead.Drivers_Original_License_Date__c  != null) {
                DateTime originalLicenseDate = Datetime.newInstance(lead.Drivers_Original_License_Date__c .year(), lead.Drivers_Original_License_Date__c .month(),lead.Drivers_Original_License_Date__c .day());
                record += addColumnData(originalLicenseDate.format('MM-dd-yyyy')) + ',';
            } else {
                record += addColumnData('') + ',';
            }*/
            


            
            record += addColumnDataBoolean(lead.Mobile_Phone_Validated__c) + ',';
            record += addColumnDataBoolean(lead.Phone_Validated__c) + ',';
            record += addColumnDataBoolean(lead.Home_Email_Validated__c) + ',';
            record += addColumnDataBoolean(lead.Office_Email_Validated__c) + ',';
            
            record += addColumnData(lead.ll_Do_Not_Contact_Email__c) + ',';
            record += addColumnData(lead.ll_Do_Not_Contact_Mobile__c) + ',';
            record += addColumnData(lead.ll_Do_Not_Contact_Phone__c) + ',';
            record += addColumnData(lead.ll_Email_Source__c) + ',';

            record += addColumnDataBoolean(lead.ll_Is_Mobile_Preferred__c) + ',';
            record += addColumnDataBoolean(lead.ll_Is_Phone_Preferred__c) + ',';
            record += addColumnDataBoolean(lead.ll_Is_Preferred_Email__c) + ',';
            
            record += addColumnData(lead.ll_Phone_Preferred_Time__c) + ',';
            record += addColumnData(lead.ll_Phone_Source__c) + ',';

            
            record += addColumnDataDecimal(lead.ll_Home_Range_High__c) + ',';
            record += addColumnDataDecimal(lead.ll_Home_Range_Low__c) + ',';
            record += addColumnDataDecimal(lead.ll_Length_of_Residence__c) + ',';
            record += addColumnData(lead.ll_Mobile_Preferred_Time__c) + ',';
            record += addColumnData(lead.ll_Mobile_Source__c) + ',';


            record += addColumnDataBoolean(lead.ll_New_Car_Buyer__c) + ',';
            record += addColumnData(lead.ll_Owner_Renter__c) + ',';
            record += addColumnDataBoolean(lead.ll_Owns_Classic_Car__c) + ',';
            record += addColumnDataBoolean(lead.ll_Owns_Motorcycle__c) + ',';
            record += addColumnDataBoolean(lead.ll_Owns_RV__c) + ',';
            record += addColumnDataBoolean(lead.ll_Owns_Truck__c) + ',';

            record += addColumnData(lead.ll_Prior_Carrier_Name__c) + ',';
             

            record += addColumnDataDate(lead.ll_Prior_Carrier_Policy_Effective_Date__c) + ','; 
            record += addColumnDataDate(lead.ll_Prior_Carrier_Policy_Expiration_Date__c) + ',';
             
            record += addColumnDataBoolean(lead.ll_Purchased_Car__c) + ',';
            record += addColumnDataBoolean(lead.ll_Purchased_Truck__c) + ',';
            record += addColumnDataDecimal(lead.ll_Vehicle_Count__c) + ',';
       
            // Chatter
          
           List<FeedItem> leadFeedItems = new List<FeedItem>();
           for(FeedItem feedItem : masterFeedItems) {
               if(feedItem.ParentId == lead.id){
                   leadFeedItems.add(feedItem);
               }
           }

           String chatterData = ApexActServiceHelper.formatChatterBody(leadFeedItems);
            record += addColumnData(chatterData);
            

            record += '\n';
            csvData += record;
        }
    
        
        ContentVersion file = new ContentVersion(     title = fileName,     versionData = Blob.valueOf(csvData ),      pathOnClient = '/' + fileName    );      
        insert file;

    }

    public static String addColumnDataBoolean(Boolean data) {
       String returnValue ='';
       if(data != null) {
            if(data) {
                returnValue = 'Yes';
            } else {
                returnValue = 'No';
            } 
       }
       return returnValue;
   }

   
    public static String addColumnDataDecimal(Decimal data) {
       String returnValue ='';
       if(data != null) {
            
            returnValue = String.valueOf(data).escapeCsv();
       }
       return returnValue;
   }

   public static String addColumnDataDate(Date data) {
       String returnValue ='';
        if(data != null) {
                DateTime dateTimeData = Datetime.newInstance(data.year(), data.month(),data.day());
                returnValue = addColumnData(dateTimeData.format('MM-dd-yyyy'));
        }
       return returnValue;
   }

   public static String addColumnData(String data) {
       String returnValue ='';
       if(data != null) {
            returnValue = data.escapeCsv();
       }
       return returnValue;
   }

    


}