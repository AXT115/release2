global class AmFam_CleanupSchedulable implements Schedulable {
    global void execute(SchedulableContext sc) {
        AmFam_CleanupRecordBatchClass batch = new AmFam_CleanupRecordBatchClass(); 
        database.executebatch(batch, 100);
    }
}