/**
*   @description: trigger handler for Opportunity object. Note: this should be the only place
*	where ALL Opportunity related trigger logic should be implemented.
*   @author: Anbu Chinnathambi
*   @date: 06/23/2021
*   @group: Trigger
*/

public without sharing class AmFam_OpportunityTriggerHandler {
    

    public void OnBeforeInsert (List<Opportunity> newRecord) {

    	AmFam_OpportunityTriggerService.extendPCIFYToOpportunity();
    }

    /*
    public void OnAfterInsert (List<Opportunity> newRecords) {


    }
    
    public void OnAfterUpdate (List<Opportunity> newRecords,
                               List<Opportunity> oldRecords,
                               Map<ID, Opportunity> newRecordsMap,
                               Map<ID, Opportunity> oldRecordsMap) {
    }*/

    
    public void OnBeforeUpdate(List<Opportunity> newRecords,
                                List<Opportunity> oldRecords,
                                Map<ID, Opportunity> newRecordsMap,
                                Map<ID, Opportunity> oldRecordsMap) {
                                    
    	AmFam_OpportunityTriggerService.extendPCIFYToOpportunity();
    }
    

}