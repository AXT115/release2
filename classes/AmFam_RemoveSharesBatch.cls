global without sharing class AmFam_RemoveSharesBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    public final String query;
    public final Set<String> producerIdentifiersSet;
    public final Map<String, Set<String>> producersIndexedByUserId;
    public final List<String> producerIds;
    public final String singleProducerId;
    public final Set<String> accountIds;
    public final List<AccountShare> accountShareList;

    public final Map<String, Set<String>> accountsIndexedByProducerId;
    public Set<String> householdIds;
    public Set<String> relatedAccountIds;
    public Map<String,ProducerAccess__c> householdRProducerAccess;
    public Map<String,ProducerAccess__c> householdRelatedProducerAccess;
    public Map<String, Set<String>> relatedAccountIndexedByHousehold;

    
    
    public AmFam_RemoveSharesBatch(Map<String, Set<String>> producersToUserMap, List<String> producerIdList, Map<String, Set<String>> accountsByProducer, Set<String> accSet, List<AccountShare> accShareList, Map<String, Set<String>> accountsIndexedByProducerId, Set<String> prodIdSet) {

        this.producersIndexedByUserId = producersToUserMap;

        if (producerIdList != null && producerIdList.size() > 0) {
            this.singleProducerId = producerIdList.remove(0);

            this.query = 'SELECT Id, RelatedId__c, Producer_Identifier__c FROM ProducerAccess__c WHERE Producer_Identifier__c =: singleProducerId';
        }

        this.producerIds = producerIdList;
        this.accountIds = accSet;
        this.accountShareList = accShareList;
        this.producerIdentifiersSet = prodIdSet;

        this.householdIds = new Set<String>();
        this.relatedAccountIds = new Set<String>();
        this.householdRProducerAccess = new Map<String,ProducerAccess__c>();
        this.householdRelatedProducerAccess = new Map<String,ProducerAccess__c>();
        this.relatedAccountIndexedByHousehold = new Map<String, Set<String>>();    

        this.accountsIndexedByProducerId = (accountsIndexedByProducerId == null) ? new Map<String, Set<String>>() : accountsIndexedByProducerId;

    }


    public Database.QueryLocator start(Database.BatchableContext BC){

        String singleProducerId = this.singleProducerId;


        Id householdRTId = AmFam_Utility.getAccountHouseholdRecordType();        
        
        //Get Household records related to the granted access accounts
        // ---- To get the households, we need to retrieve AccountContactRelation where the Account's RecordType is Household and the Contact's Account is on the removed access list from the current trigger
        List<AccountContactRelation> acrList = [SELECT Id, AccountId, ContactId FROM AccountContactRelation WHERE Account.RecordTypeId =: householdRTId AND Contact.AccountId IN: this.accountIds];
        for (AccountContactRelation acr : acrList) {
            this.householdIds.add(acr.AccountId);
        }


        //Get AccountContactRelation records relatd to the same households but no on the original accounts losing access on the trigger
        List<AccountContactRelation> acrListNotToDelete = [SELECT Id, AccountId, ContactId, Contact.AccountId FROM AccountContactRelation WHERE AccountId IN: householdIds AND Contact.AccountId NOT IN: this.accountIds];

        Set<String> relatedAccountIds = new Set<String>();
        for (AccountContactRelation acr : acrListNotToDelete) {
            String householdId = acr.AccountId;
            String relatedId = acr.Contact.AccountId;

            Set<String> relatedToHousehold = relatedAccountIndexedByHousehold.get(householdId);
            if (relatedToHousehold == null) {
                relatedToHousehold = new Set<String>();
            }

            relatedToHousehold.add(relatedId);
            this.relatedAccountIndexedByHousehold.put(householdId, relatedToHousehold);

            this.relatedAccountIds.add(relatedId);
        }
        
        return Database.getQueryLocator(this.query);
    }
 

    public void execute(Database.BatchableContext BC, List<SObject> scope){

        System.debug('### execute - scope: ' + scope);
        
        for (SObject pa :  scope) {
            String accountId = (String) pa.get('RelatedId__c');
            String producerIdentifier = (String) pa.get('Producer_Identifier__c');
            String paId = (String) pa.get('Id');

            Set<String> accountIds = this.accountsIndexedByProducerId.get(producerIdentifier);

            if (accountIds == null) {
                accountIds = new Set<String>();
            }

            accountIds.add(accountId);
            this.accountsIndexedByProducerId.put(producerIdentifier, accountIds);

            //Build the PR list for Households
            if (this.householdIds.contains(accountId) && this.producerIdentifiersSet.contains(producerIdentifier)) {
                this.householdRProducerAccess.put(paId, (ProducerAccess__c) pa);
            }
            
            //Build the PR list for Households
            if (this.relatedAccountIds.contains(accountId)) {
                this.householdRelatedProducerAccess.put(paId, (ProducerAccess__c) pa);
            }
            
        }
    }



    public void finish(Database.BatchableContext BC){

        System.debug('### finish');


        //Debug Query: SELECT Account.Name,Id,RowCause,UserOrGroup.Name, Account.RecordType.Name FROM AccountShare WHERE RowCause = 'Manual' 

        if (this.producerIds != null && this.producerIds.size() > 0) {

            AmFam_RemoveSharesBatch rsb = new AmFam_RemoveSharesBatch(this.producersIndexedByUserId, this.producerIds, this.accountsIndexedByProducerId, this.accountIds, this.accountShareList, this.accountsIndexedByProducerId, this.producerIdentifiersSet);
            
            rsb.householdIds = this.householdIds;
            rsb.relatedAccountIds = this.relatedAccountIds;
            rsb.householdRProducerAccess = this.householdRProducerAccess;
            rsb.relatedAccountIndexedByHousehold = this.relatedAccountIndexedByHousehold;        

            database.executebatch(rsb);
            //Call next batch
        }
        else {
            //All queries are over now, so go to process the delete
            AmFam_RemoveSharesBatch.processSharesDelete(this.producersIndexedByUserId, this.accountsIndexedByProducerId, this.accountIds, this.accountShareList, this.relatedAccountIds, this.householdIds, this.householdRProducerAccess, this.householdRelatedProducerAccess, this.relatedAccountIndexedByHousehold);
        }

    }

    public static void processSharesDelete(Map<String, Set<String>> producersToUserMap, Map<String, Set<String>> accountsIndexedByProducerId, Set<String> accountIds, List<AccountShare> accountShareList,
                                            Set<String> relatedAccountIds, Set<String> householdIds, Map<String,ProducerAccess__c> householdRProducerAccess,Map<String,ProducerAccess__c> householdRelatedProducerAccess, Map<String, Set<String>> relatedAccountIndexedByHousehold) {


        System.debug('### process shares delete');

        Map<String, Map<String, Integer>> mapTotalAccountByUser = new Map<String, Map<String, Integer>>();

        for (String userId : producersToUserMap.keySet()) {

            Set<String> producers = producersToUserMap.get(userId);

            Map<String, Integer> accountTotals = mapTotalAccountByUser.get(userId);

            if (accountTotals == null) {
                accountTotals = new Map<String, Integer>();
            }

            for (String producerName : producers) {
                Set<String> accounts = accountsIndexedByProducerId.get(producerName);
                if (accounts != null) {
                    for (String accountId : accounts) {
                        Integer total = accountTotals.get(accountId);

                        if (total == null) {
                            total = 0;
                        }

                        total++;

                        accountTotals.put(accountId, total);
                    }
                }

            }

            mapTotalAccountByUser.put(userId, accountTotals);

        }

        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId, AccountId, RowCause FROM AccountShare WHERE UserOrGroupId IN: producersToUserMap.keySet() AND AccountId IN: accountIds];

        Map<String, String> existingShares = new Map<String, String>();
        Map<String, AccountShare> ownerShares = new Map<String, AccountShare>();

        for (AccountShare accs : accountShares ) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            if (accs.RowCause == 'Manual') {
                existingShares.put(key, accs.Id);
            }
            else if (accs.RowCause == 'Owner') {
                ownerShares.put(key, accs);
            }
        }

        Set<String> alreadyDeleted = new Set<String>();
        List<AccountShare> accountShareListToDelete = new List<AccountShare>();
        for (AccountShare accs : accountShareList) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;

            Map<String, Integer> totalForUser = mapTotalAccountByUser.get(accs.UserOrGroupId);

            if (totalForUser == null) {
                totalForUser = new Map<String, Integer>();
            }

            Integer totalForAccount = totalForUser.get(accs.AccountId);
            if (totalForAccount == null) {
                totalForAccount = 0;
            }

            if (existingShares.containsKey(key) && !alreadyDeleted.contains(key)) {

                if (totalForAccount < 1) {
                    alreadyDeleted.add(key);
                    accs.Id = existingShares.get(key);
                    accountShareListToDelete.add(accs);
                    }
                else {
                    totalForAccount--;
                    totalForUser.put(accs.AccountId, totalForAccount);
                    mapTotalAccountByUser.put(accs.UserOrGroupId, totalForUser);
                }

            }
        }

        if (accountShareListToDelete.size() > 0) {
            System.debug('### accountSharesToDelete: ' + accountShareListToDelete);
            delete accountShareListToDelete;

            //Process opportunities to move them into the Agency Account if the agent losses access to the customer account
            processOpportunities(accountIds, producersToUserMap.keySet(), alreadyDeleted);            



            //Call the InteractionSummaryService to extend the access removal from Interaction Summaries
            AmFam_InteractionSummaryService.removeAccessFromAccount(accountShareListToDelete);


        }


        //Extend to Household
        processHouseholds(accountIds, relatedAccountIds, householdIds, householdRProducerAccess, householdRelatedProducerAccess, relatedAccountIndexedByHousehold);


        //Process Change Ownership
        processChangeOwnership(ownerShares);


    }

    public static void processHouseholds(Set<String> accountids, Set<String> relatedAccountIds, Set<String> householdIds, Map<String,ProducerAccess__c> householdRProducerAccess, Map<String,ProducerAccess__c> householdRelatedProducerAccess, Map<String, Set<String>> relatedAccountIndexedByHousehold) {

        //******************************** */
        //HOUSEHOLD PRODUCERACCESS DELETE

        //Build a Map of Accounts related to Households (indexd by Household)
        Map<String, Set<String>> relatedProducerIdentifier = new Map<String, Set<String>>();
        if (householdRelatedProducerAccess != null) {
            for (ProducerAccess__c pa : householdRelatedProducerAccess.values()) {
                String accId = pa.RelatedId__c;
                String identifier = pa.Producer_Identifier__c;

                Set<String> tempList = relatedProducerIdentifier.get(accId);
                if (tempList == null) {
                    tempList = new Set<String>();
                }
                tempList.add(identifier);

                relatedProducerIdentifier.put(accId, tempList);

            }
        }

        List<ProducerAccess__c> householdProducerAccessToDelete = new List<ProducerAccess__c>();


        //Process and generate the list or ProducerRecords to delete
        //      Household Producer Records will be deleted if their ProducerIdentifiers are not granting access to an Account related to the same Household
        if (householdRProducerAccess != null) {

            for (ProducerAccess__c pa : householdRProducerAccess.values()) {
                String householdId = pa.RelatedId__c;
                String identifier = pa.Producer_Identifier__c;

                //get household related account ids
                Set<String> relatedAccountIdsList = relatedAccountIndexedByHousehold.get(householdId);

                Boolean otherPathFound = false;


                if (relatedAccountIdsList != null && relatedAccountIdsList.size() > 0) {

                    //get related accounts producer identifiers
                    for (String accId : relatedAccountIdsList) {
                        Set<String> relatedAccountIdentifiers = relatedProducerIdentifier.get(accId);

                        if (relatedAccountIdentifiers != null && relatedAccountIdentifiers.contains(identifier)) {
                            otherPathFound = true;
                            break;
                        }

                    }
                }

                if (otherPathFound == false) {
                    householdProducerAccessToDelete.add(pa);
                }
    
            }
            
        }

        if (householdProducerAccessToDelete.size() > 0) {
            delete householdProducerAccessToDelete;
        }


    }



    public static void processChangeOwnership(Map<String, AccountShare> ownerShares) {

        //****************************** */
        //Process change of Ownership
                
        
        String corporateUserId = getCorporateAccountUser();
        Map<String, Account> accountsToUpdate = new Map<String, Account>();

        Set<String> ownedAccountids = new Set<String>();
        for (AccountShare accs : ownerShares.values()) {
            ownedAccountids.add(accs.AccountId);

            Account acc = new Account(Id=accs.AccountId, OwnerId=corporateUserId);
            accountsToUpdate.put(acc.Id, acc);
            
        }


        //Before processing ownerchip update, a backup of existing manual shares are required because SF remove some manual shares when account owner changes
        //Which shares are deleted and which remain will depend on who created those shares originally
        //Deleted shares need to be restored after the ownershipt is updated
        List<AccountShare> assShareBackupBefore = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel, OpportunityAccessLevel 
                                                    FROM AccountShare
                                                    WHERE AccountId IN: ownedAccountids AND RowCause = 'Manual' AND UserOrGroup.IsActive = true];



        if (accountsToUpdate.size() > 0) {
            update accountsToUpdate.values();
        }

        //Retrieve the account shares remaining after the ownership update to not restore those that were not deleted
        List<AccountShare> assShareBackupAfter = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel, OpportunityAccessLevel 
                                                    FROM AccountShare
                                                    WHERE AccountId IN: ownedAccountids AND RowCause = 'Manual' AND UserOrGroup.IsActive = true];

        Set<String> sharesStillThere = new Set<String>();
        for (AccountShare accShare : assShareBackupAfter) {
            String key = accShare.AccountId + '-' + accShare.UserOrGroupId;
        }

        List<AccountShare> sharesListToRestore = new List<AccountShare>();

        for (AccountShare accShare : assShareBackupBefore) {
            String key = accShare.AccountId + '-' + accShare.UserOrGroupId;

            //If the AccountId-User combination is not there, then it was deleted on Account Ownership change and need to be restored
            if (!sharesStillThere.contains(key)) {
                AccountShare restoreShare = new AccountShare();
                restoreShare.AccountId = accShare.AccountId;
                restoreShare.UserOrGroupId = accShare.UserOrGroupId;
                restoreShare.OpportunityAccessLevel = accShare.OpportunityAccessLevel;
                restoreShare.AccountAccessLevel = accShare.AccountAccessLevel;
                restoreShare.RowCause = 'Manual';

                sharesListToRestore.add(restoreShare);
            }
        }

        if (sharesListToRestore.size() > 0) {
            insert sharesListToRestore;
        }


    }


    private static void processOpportunities(Set<String> accountIds, Set<String> userIds, Set<String> deletedShareKeys) {

        System.debug('### processOpportunities - accountIds: ' + accountIds);
        System.debug('### processOpportunities - userIds: ' + userIds);
        System.debug('### processOpportunities - deletedShareKeys: ' + deletedShareKeys);
 

        //DeletedShareKeys will be the combination of "AccountId-UserOrGroupId" values of the share
        //An Opportunity child of the AccountId and owned by UserOrGroupId should be moved under the agency account
    
        List<Opportunity> oppList = [SELECT Id, AccountId, Account.Name, Account.OwnerId, Original_Account_Id__c, Original_Account_Name__c, Account.Agency__c, OwnerId FROM Opportunity WHERE AccountId IN: accountIds AND OwnerId IN: userIds];
        System.debug('### processOpportunities - oppList: ' + oppList);
        System.debug('### processOpportunities  ' );
        System.debug('### processOpportunities  ' );

        List<Opportunity> oppListToUpdate = new List<Opportunity>();

        for (Opportunity opp : oppList) {
            
            String key = opp.AccountId + '-' + opp.OwnerId;

            System.debug('### processOpportunities - key: ' + key);
            
            if (deletedShareKeys.contains(key)) {
                System.debug('### yes... it exists: ' + key);
                opp.Original_Account_Id__c = opp.AccountId;
                opp.Original_Account_Name__c = opp.Account.Name;
                opp.AccountId = opp.Account.Agency__c;

                oppListToUpdate.add(opp);
            }
        }

        System.debug('### processOpportunities - oppListToUpdate: ' + oppListToUpdate);

        if (oppListToUpdate.size() > 0) {
            update oppListToUpdate;
        }



    }


    private static String getCorporateAccountUser() {
        List<AmFam_Ids__mdt> amfamIds = [SELECT AFMIC_Corporate_UserId__c FROM AmFam_Ids__mdt WHERE DeveloperName = 'AFMIC_Corporate' LIMIT 1];

       
        if (amfamIds.size() > 0) {
            String returnId = amfamIds[0].AFMIC_Corporate_UserId__c;
            if (String.isNotEmpty(returnId)) {
                return returnId;
            }
        }

        return null;

    }    


}