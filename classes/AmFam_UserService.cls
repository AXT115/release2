public class AmFam_UserService {
    private class PermissionMapping
    {
        //maps profile name to permission bundle id
        Map<String,Id> profileMappings = new Map<String,Id>(); 

        //maps permission bundle id to permission group ids
        Map<Id,List<String>> bundleMappings = new Map<Id,List<Id>>(); 
    }

    public class SDLProducerResult
    {
        public List<Producer> newProducers = new List<Producer>();
        public List<Producer> deleteProducers = new List<Producer>();
        public List<ProducerAccess__c> newProducerAccess = new List<ProducerAccess__c>();
        public List<ProducerAccess__c> deleteProducerAccess = new List<ProducerAccess__c>();
    }

    @TestVisible private static Boolean callProducerAPI = true;
    @TestVisible private static Boolean callProducerUpdateAPI = true;
    
    public static void userAccessToAccountLead(List<User> userRecs) {
        //Written only for SSO logins and not bulkified
        for (User userRec : userRecs) {
            //Add S&SO user to the public group.
            User u = [SELECT Id, AmFam_User_ID__c, Profile.Name FROM User WHERE Id =: userRec.Id LIMIT 1];
            if (u.Profile.Name == 'Sales & Service Operations Rep') {
                Group g = [SELECT Id FROM Group WHERE DeveloperName = 'S_SO_Users'];
                List<GroupMember> groupMembers = [SELECT Id FROM GroupMember WHERE GroupId =: g.Id AND UserOrGroupId =: u.Id LIMIT 1];
                if (groupMembers.size() != 1) {
                    GroupMember gm= new GroupMember();
                    gm.GroupId=g.id;
                    gm.UserOrGroupId = u.Id;
                    insert gm;
                }
            }
            if(!System.isBatch() && u.AmFam_User_ID__c != null && callProducerAPI && !Test.isRunningTest()){
                database.executebatch(new AmFam_ManageSharingAccessBatch(u.AmFam_User_ID__c));
            }
        }
    }

    public static void updatePermissionSetGroupAssignmentsForUser(List<User> users)
    {
        PermissionMapping permMap = AmFam_UserService.loadPermissionsMetadata();

        List<Id> userIds = new List<Id>();
        Set<Id> profileIds = new Set<Id>();
        for (User u : users)
        {
            userIds.add(u.Id);
            if (u.ProfileId != null)
            {
                profileIds.add(u.ProfileId);
            }
        }

        List<Profile> profs = [SELECT Id,Name FROM Profile WHERE Id IN :profileIds];
        Map<Id,String> profileNameMap = new Map<Id,String>();
        for (Profile prof : profs)
        {
            profileNameMap.put(prof.Id,prof.Name);
        }

        List<PermissionSetAssignment> allExistingPSA = [SELECT Id,AssigneeId,PermissionSetGroupId FROM PermissionSetAssignment WHERE AssigneeId IN :userIds AND PermissionSetGroupId != NULL];

        List<PermissionSetAssignment> deleteAssignments = new List<PermissionSetAssignment>();
        List<PermissionSetAssignment> addAssignments = new List<PermissionSetAssignment>();

        Map<Id,Map<Id,PermissionSetAssignment>> usersPermSetMap = new Map<Id,Map<Id,PermissionSetAssignment>>();
        for (PermissionSetAssignment psa : allExistingPSA)
        {
            if (!usersPermSetMap.containsKey(psa.AssigneeId))
            {
                usersPermSetMap.put(psa.AssigneeId,new Map<Id,PermissionSetAssignment>());
            }
            Map<Id,PermissionSetAssignment> userPermSetMap = usersPermSetMap.get(psa.AssigneeId);
            userPermSetMap.put(psa.PermissionSetGroupId,psa);
        }

        for (User u : users)
        {
            if (u.ProfileId != null)
            {
                String profName = profileNameMap.get(u.ProfileId);
                Map<Id,PermissionSetAssignment> existingPermSetMap = usersPermSetMap.get(u.Id);

                if (permMap.profileMappings.containsKey(profName))
                {
                    Id permBundleId = permMap.profileMappings.get(profileNameMap.get(u.ProfileId));
    
                    if (permBundleId != null)
                    {
                        List<Id> permSetGroupIds = permMap.bundleMappings.get(permBundleId);

                        for (Id groupId : permSetGroupIds)
                        {   
                            if (existingPermSetMap != null && existingPermSetMap.containsKey(groupId))
                            {
                                existingPermSetMap.remove(groupId);
                            }
                            else
                            {
                                //!!!Note API 52.0+ introduces IsActive on PermissionSetAssignment and seems like would want to update
                                //IsActive on existing assignments         
                                PermissionSetAssignment assign = new PermissionSetAssignment();
                                assign.AssigneeId = u.Id;
                                assign.PermissionSetGroupId = groupId;
                                addAssignments.add(assign);
                            }
                        }
                    }
                }

                //anything that remains is unreferenced and should be deleted
                if (existingPermSetMap != null)
                {
                    deleteAssignments.addAll(existingPermSetMap.values());
                }
            }
        }

        if (!deleteAssignments.isEmpty())
        {   
            delete deleteAssignments;
        }

        if (!addAssignments.isEmpty())
        {   
            insert addAssignments;
        }
    }

    private static PermissionMapping loadPermissionsMetadata()
    {       
        PermissionMapping permMap = new PermissionMapping();

        //get the profile <-> perm bundle mappings && create dict keyed on profile name to get bundle id
        List<AmFam_Profile_Perm_Bundle_Mapping__mdt> mappings = [SELECT AmFam_Permission_Bundle__c,Profile_Name__c FROM AmFam_Profile_Perm_Bundle_Mapping__mdt];
        
        for (AmFam_Profile_Perm_Bundle_Mapping__mdt mapping : mappings)
        {
            permMap.profileMappings.put(mapping.Profile_Name__c, mapping.AmFam_Permission_Bundle__c);
        }

        //get the perm bundle members & collect all reference perm group names 
        List<AmFam_Perm_Bundle_Member__mdt> members = [SELECT AmFam_Permission_Bundle__c,Id,Permission_Set_Group_API_Name__c FROM AmFam_Perm_Bundle_Member__mdt];
        
        Set<String> groupNames = new Set<String>();
        for (AmFam_Perm_Bundle_Member__mdt member : members)
        {
            groupNames.add(member.Permission_Set_Group_API_Name__c);
        } 

        //get ids for perm groups & create map keyed on group name for id
        List<PermissionSetGroup> permGroups = [SELECT Id, DeveloperName FROM PermissionSetGroup WHERE DeveloperName in :groupNames];

        Map<String,Id> groupMap = new Map<String,Id>();
        for (PermissionSetGroup permGroup : permGroups)
        {
            groupMap.put(permGroup.DeveloperName,permGroup.Id);
        }

        //create dict keyed on bundle id containing array of all perm groups in each bundle
        for (AmFam_Perm_Bundle_Member__mdt member : members)
        {
            if (permMap.bundleMappings.containsKey(member.AmFam_Permission_Bundle__c))
            {
                //append
                Id groupId = groupMap.get(member.Permission_Set_Group_API_Name__c);
                if (groupId == null)
                {
                    System.debug('No PermissionSetGroup with name '+member.Permission_Set_Group_API_Name__c+' is defined');
                }
                else
                {
                    permMap.bundleMappings.get(member.AmFam_Permission_Bundle__c).add(groupId);
                }
            }
            else
            {
                //create entry
                Id groupId = groupMap.get(member.Permission_Set_Group_API_Name__c);
                if (groupId == null)
                {
                    System.debug('No PermissionSetGroup with name '+member.Permission_Set_Group_API_Name__c+' is defined');
                }
                else
                {
                    List<Id> groups = new List<Id>{groupMap.get(member.Permission_Set_Group_API_Name__c)};
                    permMap.bundleMappings.put(member.AmFam_Permission_Bundle__c,groups);
                }
            }
        }
        return permMap;
    }

    private static Map<String,String> loadProfileProducerTypeMapping()
    {
        Map<String,String> mapping = new Map<String,String>{'Agent CSR' =>	'CSR', 
                                                            'AmFam Agent' => 'Agency', 
                                                            'Field Operations' => 'FO', 
                                                            'Sales District Leader' => 'SDL',
                                                            'Sales & Service Operations Rep' => 'SSO'};

       return mapping; 
    }

    //Was a future now called from AmFam_ProducerUpdateQueueable
    public static void producerUpdate(List<Id> userIds)
    {
        String ssoProducerId = '52481';

        Verbose_Access_Logging__mdt logrec = [SELECT Verbose_logging_on__c FROM Verbose_Access_Logging__mdt LIMIT 1];
        Boolean verboseLogging = logrec.Verbose_logging_on__c;

        if (!callProducerUpdateAPI) {
            return;
        }
        Id individualRecordTypeId = AmFam_Utility.getAccountIndividualRecordType();
        Id agencyRecordTypeId = AmFam_Utility.getAccountAgencyRecordType();

        Profile ssoProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Sales & Service Operations Rep'];
        Profile sdlProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Sales District Leader'];
        Profile agentProfile = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];

        Map<String,String> profileProducerTypeMap = AmFam_UserService.loadProfileProducerTypeMapping();

        List<User> users = [SELECT Id,AmFam_User_ID__c,ContactId,ProfileId,Profile.Name,Producer_ID__c,Name FROM User WHERE Id IN :userIds];
        List<String> userProdIds = new List<String>();

        for (User u : users) {
            userProdIds.add(u.Producer_ID__c);
        }

        if (verboseLogging) {
            for (User u : users) {
                System.debug('#-# producerUpdate-user '+u.Name+':'+u.Id+' pid:'+u.Producer_ID__c);
            }
        }

        List<Account> userIndivAccounts = [SELECT Id, Producer_ID__c FROM Account WHERE Producer_ID__c in :userProdIds
                                                    AND RecordTypeId = :individualRecordTypeId];
        Map<String,Account> indivAccountMap = new Map<String,Account>();
        for (Account acct : userIndivAccounts) {
            indivAccountMap.put(acct.Producer_ID__c,acct);
        }

        List<Producer> additions = new List<Producer>();
        List<Producer> updates = new List<Producer>();
        List<Producer> deletions = new List<Producer>();
        List<ProducerAccess__c> additionsProducerAccess = new List<ProducerAccess__c>();
        List<ProducerAccess__c> deletionsProducerAccess = new List<ProducerAccess__c>();
        
        List<Producer> allExistingProducers = [SELECT Id,AccountId,Name,ContactId,InternalUserId,Type FROM Producer WHERE InternalUserId IN :userIds];
        Map<Id,Map<String,Producer>> existingProducersMap = new Map<Id,Map<String,Producer>>();

        for (Producer p : allExistingProducers) {
            if (!existingProducersMap.containsKey(p.InternalUserId)) {
                existingProducersMap.put(p.InternalUserId,new Map<String,Producer>());
            }

            Map<String,Producer> prodMap = existingProducersMap.get(p.InternalUserId);
            prodMap.put(p.Name,p);
        }

        if (verboseLogging) {
            //dump existing map
            for (Id uid : existingProducersMap.keySet()) {
                System.debug('#-# exmap - uid:'+uid);
                Map<String,Producer> prods = existingProducersMap.get(uid);
                for (String pid : prods.keySet())
                {
                    System.debug('#-# exmap -- '+pid+':'+prods.get(pid));
                }                
            }
        }

        for (User u : users) {
            if (verboseLogging) {
                System.debug('#-# processing User:'+u.Id);
            }
            String endpoint = '/alternatekeys?requestIdentifierType=USER_ID&responseIdentifierType=PRODUCER_ID&sourceSystemIdentifier=AMFAM&requestIdentifierValue='+u.AmFam_User_ID__c;

            AmFam_ProducerTypeService prodTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut(endpoint);

            if (prodTypeSvc?.alternateKey?.keyId != null) {
                // Get the Producer Info from the Producer Service
                prodTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/' + prodTypeSvc.alternateKey.keyId + '/booksofbusiness');
            }
        
            Map<String,Producer> existingMap = existingProducersMap.get(u.Id);

            //save aside producer ids to query for agencies
            Set<String> prodIds = new Set<String>();
            Set<String> allBoBProdIds  = new Set<String>();

            Map<String,Producer> stagedProducer = new Map<String,Producer>();
            List<Producer> newProducer = new List<Producer>();
    
            if (prodTypeSvc?.bookOfBusiness != null) {
                for (AmFam_ProducerTypeService.BookOfBusiness book : prodTypeSvc.bookOfBusiness) {
                    if (book.producerCodes == null) {
                        if (u.ProfileId == ssoProfile.Id) {
                            if (existingMap != null && existingMap.containsKey(ssoProducerId)) {
                                if (verboseLogging) {
                                    System.debug('#-# SSO - remove existing:'+ssoProducerId);
                                }
                                existingMap.remove(ssoProducerId);
                            } else {
                                if (verboseLogging) {
                                    System.debug('#-# SSO - add staged prod:'+ssoProducerId);
                                }

                                Producer producer = new Producer();
                                producer.name = ssoProducerId;
                                producer.InternalUserId = u.Id;
                                producer.ContactId = u.ContactId;
                                producer.Type = profileProducerTypeMap.get(u.Profile.Name);
                                prodIds.add(ssoProducerId);
                                //account is set below
                                stagedProducer.put(ssoProducerId,producer);
                            }
                        }
                    } else {
                        for (AmFam_ProducerTypeService.ProducerCodes prodCode : book.producerCodes) {
                            allBoBProdIds.add(prodCode.producerCode);

                            if (existingMap != null && existingMap.containsKey(prodCode.producerCode)) {
                                Producer currProd = existingMap.get(prodCode.producerCode);
                                if (currProd.Type != profileProducerTypeMap.get(u.Profile.Name)) {
                                    currProd.Type = profileProducerTypeMap.get(u.Profile.Name);
                                    updates.add(currProd);
                                }

                                if (verboseLogging) {
                                    System.debug('#-# remove existing:'+prodCode.producerCode);
                                }
                                //already exists so bypass it
                                existingMap.remove(prodCode.producerCode);
                            } else {
                                if (verboseLogging) {
                                    System.debug('#-# add staged prod:'+prodCode.producerCode);
                                }
                                Producer producer = new Producer();
                                producer.name = prodCode.producerCode;
                                producer.InternalUserId = u.Id;
                                producer.ContactId = u.ContactId;
                                producer.Type = profileProducerTypeMap.get(u.Profile.Name);
                                prodIds.add(prodCode.producerCode);
                                //account is set below
                                stagedProducer.put(prodCode.producerCode,producer);
                            }
                        }
                    }
                }
            } else {
                if (prodTypeSvc.status.code == 404 && prodTypeSvc.status.reason == 'Not Found') {
                    String prodId = (u.Producer_ID__c == null) ? 'P-'+u.Id : u.Producer_ID__c;

                    if (existingMap.containsKey(prodId)){
                        if (verboseLogging) {
                            System.debug('#-# noBoB-remove existing:'+prodId);
                        }
                        existingMap.remove(prodId);
                    } else {
                        if (verboseLogging) {
                            System.debug('#-# noBoB-add staged prod:'+prodId);
                        }
                        Producer producer = new Producer();
                        producer.name = prodId;
                        producer.InternalUserId = u.Id;
                        producer.ContactId = u.ContactId;
                        producer.Type = profileProducerTypeMap.get(u.Profile.Name);
                        prodIds.add(prodId);
                        //account is set below
                        //P-<>  will go unmatched in staged
                        newProducer.add(producer); 
                    }
                }
            }

            if (verboseLogging) {
                System.debug('#-# prodIds:'+prodIds);
                System.debug('#-# staged producer keyset:'+stagedProducer.keySet());
            }
            
            //get referenced agency records
            List<Account> agencyAccounts = [SELECT Id,Producer_Id__c FROM Account WHERE recordTypeId=:agencyRecordTypeId AND Producer_ID__c in :prodIds];

            if (verboseLogging) {
                for (Account a : agencyAccounts)
                {
                    System.debug('#-# agency accout:'+a.Id+' pid:'+a.Producer_Id__c);
                }
            }

            for (Account agency : agencyAccounts) {
                if (stagedProducer.containsKey(agency.Producer_Id__c)) {
                    if (verboseLogging) {
                        System.debug('#-# staged contains:'+agency.Producer_Id__c);
                    }
                    Producer matched = stagedProducer.get(agency.Producer_Id__c);
                    matched.AccountId = agency.Id;
                    newProducer.add(matched);
                } else {
                    if (verboseLogging) {
                        System.debug('#-# no staged producer for Agency:'+agency+' pid:'+agency.Producer_Id__c);
                    }
                }
            }

            additions.addAll(newProducer);
            if (existingMap != null) {
                deletions.addAll(existingMap.values());
            }

            //3150 #1
            if (u.ProfileId == sdlProfile.Id) {
                if (verboseLogging) {
                    System.debug('#-# SDL profile processing');
                }

                SDLProducerResult result = sdlProducerUpdate(u, indivAccountMap.get(u.Producer_ID__c), allBoBProdIds);  
                additions.addAll(result.newProducers);
                deletions.addAll(result.deleteProducers);   
                additionsProducerAccess.addAll(result.newProducerAccess);
                deletionsProducerAccess.addAll(result.deleteProducerAccess);          
            }

        }

        if (!additions.isEmpty()) {
            if (verboseLogging) {
                for (Producer p : additions) {
                    System.debug('#-# inserting producer pid:'+p.Name+' u:'+p.InternalUserId);
                }
            }
            insert additions;
        }

        if (!updates.isEmpty()) {
            if (verboseLogging) {
                for (Producer p : updates) {
                    System.debug('#-# update producer pid:'+p.Name+' u:'+p.InternalUserId);
                }
            }
            update updates;
        }

        //any entries still in existingMap are not longer referenced and should be deleted
        if (!deletions.isEmpty()) {
            if (verboseLogging) {
                for (Producer p : deletions) {
                    System.debug('#-# delete producer pid:'+p.Name+' u:'+p.InternalUserId);
                }
            }
            delete deletions;
        }

        if (!additionsProducerAccess.isEmpty()) {
            insert additionsProducerAccess;
        }

        if (!deletionsProducerAccess.isEmpty()) {
            delete deletionsProducerAccess;
        }
    }

    public static SDLProducerResult sdlProducerUpdate(User u, Account agentIndivAccount, Set<String> producerIds) {
        SDLProducerResult result = new SDLProducerResult();
        Id individualRecordTypeId = AmFam_Utility.getAccountIndividualRecordType();

        //generate P- versions of sdl producerIds
        List<String> pdashProdIds = new List<String>();
        for (String pid : producerIds) {
            pdashProdIds.add('P-'+pid);
        }

        //get existing producer recs for user with type SDL & prod id like P-
        List<Producer> existingProducers = [SELECT Id,Name FROM Producer WHERE Type = 'SDL' AND InternalUserId = :u.Id 
                                                                    AND Name LIKE 'P-%'];

        Map<String,Producer> existingMap = new Map<String,Producer>();
        for  (Producer p : existingProducers) {
            existingMap.put(p.Name,p);
        }

        List<ProducerAccess__c> existingProducerAccess = [SELECT Id,Producer_Identifier__c FROM ProducerAccess__c 
                                                                 WHERE RelatedId__c = :agentIndivAccount.Id 
                                                                     AND Producer_Identifier__c LIKE 'P-%'];

        Map<String,ProducerAccess__c> existingAccessMap = new Map<String,ProducerAccess__c>();
        for  (ProducerAccess__c pa : existingProducerAccess) {
            existingAccessMap.put(pa.Producer_Identifier__c,pa);
        }   

        //select Indiv Accts with prodid in pdash
        List<Account> accounts = [SELECT Id,Producer_ID__c FROM Account WHERE RecordTypeId = :individualRecordTypeId
                                                            AND Producer_ID__c IN :pdashProdIds];

        for (Account acct : accounts) {
            if (existingMap.containsKey(acct.Producer_ID__c)) {
                existingMap.remove(acct.Producer_ID__c);
                existingAccessMap.remove(acct.Producer_ID__c);
            } else {
                Producer producer = new Producer();
                producer.name = acct.Producer_ID__c;
                producer.InternalUserId = u.Id;
                producer.AccountId = acct.Id;
                producer.Type = 'SDL';
                result.newProducers.add(producer); 

                ProducerAccess__c prodAccess = new ProducerAccess__c();
                prodAccess.RelatedId__c = agentIndivAccount.Id;
                prodAccess.Producer_Identifier__c = acct.Producer_ID__c;
                prodAccess.Reason__c = 'ENTRY';
                result.newProducerAccess.add(prodAccess);
            }
        }
        
        //anything left is unreferenced and should be cleared
        result.deleteProducers.addAll(existingMap.values());
        result.deleteProducerAccess.addAll(existingAccessMap.values());

        return result;
    }
    
    @future(callout=true)
    public static void updateUserAgencyAccount(String producerId) {
        List<Account> accountsToUpdate = new List<Account>();

        AmFam_ProducerTypeService response = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/' + producerId + '/booksofbusiness');
        if (response?.bookOfBusiness != null) {
            for (AmFam_ProducerTypeService.BookOfBusiness bob : response?.bookOfBusiness) {
                String agencyId = bob.producerCodes[0]?.agencyId;
                if (bob.role == 'AGENT' && agencyId != null) {
                    Account agencyAccount = [SELECT Id,Producer_Id__c FROM Account where Producer_Id__c = :producerId LIMIT 1];

                    if (agencyAccount != null) {
                        AmFam_ProducerTypeService agencyResp = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/agencies/'+agencyId);
                        if (agencyResp != null) {
                            AmFam_UserService.updateAgencyAccountFromAgencyResponse(agencyAccount, agencyResp);
                            accountsToUpdate.add(agencyAccount);
                        }
                    }
                }
            }
        }

        if (!accountsToUpdate.isEmpty()) {
            update accountsToUpdate;
        }
    }

    public static void updateAgencyAccountFromAgencyResponse(Account agencyAccount, AmFam_ProducerTypeService agencyResp) {
        if (agencyResp.agency != null) {
            agencyAccount.Name = agencyResp.agency.name;
            if (agencyResp?.agency?.contactDetails?.addresses != null) {
                for (AmFam_ProducerTypeService.Addresses address : agencyResp?.agency?.contactDetails?.addresses) {
                    if (address.primaryAddressIndicator) {
                        agencyAccount.ShippingStreet = address.addressLine1;
                        agencyAccount.ShippingCity = address.city;
                        agencyAccount.ShippingState = address.state;
                        agencyAccount.ShippingPostalCode = address.zip5Code;
                        agencyAccount.ShippingCountryCode = address.country;
                    }
                }
            }
        }
    }
}