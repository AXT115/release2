public class AmFam_InteractionSummaryTriggerHandler {
    
    /*
    public void OnBeforeInsert (List<InteractionSummary> newRecords) {

    }
    */
    public void OnAfterInsert (List<InteractionSummary> newRecords) {

        AmFam_InteractionSummaryService.grantSameAccessFromAccount(newRecords);
    }

      
    public void OnAfterUpdate (List<InteractionSummary> newRecords,
                               List<InteractionSummary> oldRecords,
                               Map<ID, InteractionSummary> newRecordsMap,
                               Map<ID, InteractionSummary> oldRecordsMap) {

        
        AmFam_InteractionSummaryService.grantSameAccessFromAccount(newRecords, oldRecordsMap, true);
    }

    /*
    public void OnBeforeUpdate(List<InteractionSummary> newRecords,
                                List<InteractionSummary> oldUser,
                                Map<ID, InteractionSummary> newRecordsMap,
                                Map<ID, InteractionSummary> oldRecordsMap) {

    }
    */


}