/**
*   @description: the apex class is the server side controller for aura component AmFam_ViewInsurancePolicyDetails
*	The component launches the dynamic URL to source system from SF UI based on the source system field value in Insurance Policy object.
*   @author: Anbu Chinnathambi
*   @date: 07/06/2021
*   @group: Apex Class
*/

public class AmFam_ViewPolicyDetailsController {
    
    @AuraEnabled       
    public static String getSourceSystemURL(String policyId, string billingAccount){  
        InsurancePolicy policy = [select id, Name, SourceSystem, RecordType.Name, Billing_Account__c from InsurancePolicy where id=:policyId Limit 1];
        //if billing Account is not blank, then its billing type And both Application/Quote record type uses the same urls, so setting type = quote if its application
        String type = String.isBlank(billingAccount) ? policy.RecordType.Name =='Application'?'Quote':policy.RecordType.Name : 'Billing' ;
        string sourceSystem = policy.SourceSystem == 'APLU'?'AutoPlus':policy.SourceSystem;//APLU uses policy viewer system
        String org;
        String policynumber = String.isBlank(billingAccount) ? policy.Name : policy.Billing_Account__c; 
        //formatting policynumber
        if(sourceSystem.contains('PolicyCenter') && !sourceSystem.contains('-') && type == 'Policy' && String.isBlank(billingAccount)){
            policynumber = policynumber.substring(0, 5) + '-' + policynumber.substring(5, 10) + '-' + policynumber.substring(10, 12);
         }
        system.debug('policynumber' + policynumber);
        
        //checking whether its Production, QA or INT
        if(URL.getSalesforceBaseUrl().getHost().contains('--')){
             org = URL.getSalesforceBaseUrl().getHost().substringAfter('--')?.contains('qa')? 'QA':'INT';
        }else{
             org = 'Production';
        }
        
        Map<String, String> orgURLMap = new Map<String, String>();
        Source_System__mdt source = [SELECT DeveloperName, MasterLabel, INT_URL__c, Type__c, Production_URL__c,QA_URL__c FROM Source_System__mdt where MasterLabel=: sourceSystem AND Type__c =:type Limit 1];
        orgURLMap.put('Production', source.Production_URL__c+policynumber);
        orgURLMap.put('QA',source.QA_URL__c+policynumber);
        orgURLMap.put('INT',source.INT_URL__c+policynumber);
        system.debug('returnURL' + orgURLMap.get(org));
        
        String returnURL  = orgURLMap.get(org);
        
        if((sourceSystem == 'AutoPlus' || sourceSystem == 'PropertyPlus') && !String.isBlank(billingAccount)){
            returnURL = returnURL+'&billingSystem=PersonalLinesCBS&';
        }
        
        return returnURL;
    }
}