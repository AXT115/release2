public class Amfam_LeadRefreshHelper {
    /*
	* Refreshes the leads in a future method if the lastModifiedBy is User Integration. This user should be LeadListener.
	* @ New lead records
	*/
    public static void refreshLeadsFromTrigger(List<Lead> leadRecs) {
        
        Profile currentUserProfile = [Select Name from Profile where Id =: userinfo.getProfileid()];
        if (currentUserProfile.Name == 'Integration Profile') {
          
            List<Lead> leads = [Select Id, CDHID__c, Contact_CDHID__c, LastModifiedBy.Name, Agency__r.Is_Corporate_Account__c FROM Lead WHERE Id In : leadRecs];
            List<String> leadIds = new List<String>();
            for (Lead lead : leads) {
                if (!System.isFuture() && !System.isBatch() && !System.isQueueable()) {
                    if (lead.CDHID__c != null || lead.Contact_CDHID__c != null) {
                        leadIds.add(lead.Id);
                    }
                }
            }
            refreshLeadsFuture(leadIds);
        }
    }
    
    /*
     * Refreshes all of the leads in the list in a future method 
     * This will make one callout for each lead record
     */
    @future(callout = true)
    public static void refreshLeadsFuture(List<String> leadIds) {
    	List<Lead> leadsToUpdate = new List<Lead>();
        List<Lead> leads =  [Select id, CDHID__c, Party_Version__c, Party_Level__c, Contact_CDHID__c, Additional_Phone_1__c, Additional_Phone_2__c, Additional_Phone_3__c, Phone, Email,
                        Additional_Email_1__c, Additional_Email_2__c
                        FROM Lead where id In : leadIds];
        for(Lead lead : leads){
            try {
             Lead updatelead = wsPartySearchHelper.refreshLead(lead);
             leadsToUpdate.add(updateLead);
            } catch(Exception e) {
               System.debug('Caught Exception in refreshLead ' + e.getMessage());
            }
        }
        if(!leads.isEmpty()) {
        	update leads;
        }
        
    }

}