//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)
//Methods Included: searchParty, retrieveParty, retrievePartyList, searchAcrossPartners, ping
// Primary Port Class Name: PartySearchServiceSOAP  
public class wsPartySearchServiceCallout {
    private static final String s_ns0 = 'http://service.amfam.com/partysearchservice';
    private static final String s_ns1 = 'http://schema.amfam.com/ping';
    public class PartySearchServiceSOAP {
        public String endpoint_x = 'callout:Party_Search_Service';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private transient String[] ns_map_type_info = new String[]{'http://schema.amfam.com/ping','wsPartySearchPing','http://schema.amfam.com/party','wsPartySearchParty','http://service.amfam.com/partysearchservice','wsPartySearchService','http://service.amfam.com/partysearchservice/v7','wsPartySearchServiceCallout'};
        public wsPartySearchPing.PingResultType ping(wsPartySearchPing.PingInputType PingInput) {
            wsPartySearchPing.ping_element request_x = new wsPartySearchPing.ping_element();
            wsPartySearchPing.pingResponse_element response_x;
            request_x.PingInput = PingInput;
            // A map of key-value pairs that represent the response that the external service sends after receiving the request. In each pair, the key is a response identifier. The value is the response object, which is an instance of a type that is created as part of the auto-generated stub class.
            Map<String, wsPartySearchPing.pingResponse_element> response_map_x = new Map<String, wsPartySearchPing.pingResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this, // stub - An instance of the Apex class that is auto-generated from a WSDL (the stub class).
                request_x, // request - The request to the external service. The request is an instance of a type that is created as part of the auto-generated stub class.
                response_map_x, // response
                new String[]{endpoint_x, //  the URL of the external web service
                'http://service.amfam.com/partysearchservice/v7/ping', // The SOAP action.
                s_ns1, // The request namespace.
                'ping', // The request name.
                s_ns1, // The response namespace.
                'pingResponse', //  The response name.
                'wsPartySearchPing.pingResponse_element'} // The response type.
            );
            response_x = response_map_x.get('response_x');
            return response_x.PingResult;
        }
        public wsPartySearchService.RetrieveResponseType retrieveParty(wsPartySearchService.RetrieveRequestType RetrieveRequest) {
            wsPartySearchService.RetrievePartyType request_x = new wsPartySearchService.RetrievePartyType();
            wsPartySearchService.RetrievePartyResponseType response_x;
            request_x.RetrieveRequest = RetrieveRequest;
            // A map of key-value pairs that represent the response that the external service sends after receiving the request. In each pair, the key is a response identifier. The value is the response object, which is an instance of a type that is created as part of the auto-generated stub class.
            Map<String, wsPartySearchService.RetrievePartyResponseType> response_map_x = new Map<String, wsPartySearchService.RetrievePartyResponseType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this, // stub - An instance of the Apex class that is auto-generated from a WSDL (the stub class).
                request_x, // request - The request to the external service. The request is an instance of a type that is created as part of the auto-generated stub class.
                response_map_x, // response
                new String[]{endpoint_x, //  the URL of the external web service
                'http://service.amfam.com/partysearchservice/v7/retrieveParty', // The SOAP action.
                s_ns0, // The request namespace.
                'retrieveParty', // The request name.
                s_ns0, // The response namespace.
                'retrievePartyResponse', //  The response name.
                'wsPartySearchService.RetrievePartyResponseType'} // The response type.
            );
            response_x = response_map_x.get('response_x');
            return response_x.RetrieveResponse;
        }
        public wsPartySearchService.MultiplePartiesRetrieveResponseType retrievePartyList(wsPartySearchService.MultiplePartiesRetrieveRequestType RetrievePartyListRequest) {
            wsPartySearchService.RetrievePartyListType request_x = new wsPartySearchService.RetrievePartyListType();
            wsPartySearchService.RetrievePartyListResponseType response_x;
            request_x.RetrievePartyListRequest = RetrievePartyListRequest;
            // A map of key-value pairs that represent the response that the external service sends after receiving the request. In each pair, the key is a response identifier. The value is the response object, which is an instance of a type that is created as part of the auto-generated stub class.
            Map<String, wsPartySearchService.RetrievePartyListResponseType> response_map_x = new Map<String, wsPartySearchService.RetrievePartyListResponseType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this, // stub - An instance of the Apex class that is auto-generated from a WSDL (the stub class).
                request_x, // request - The request to the external service. The request is an instance of a type that is created as part of the auto-generated stub class.
                response_map_x, // response
                new String[]{endpoint_x, //  the URL of the external web service
                'http://service.amfam.com/partysearchservice/v7/retrievePartyList', // The SOAP action.
                s_ns0, // The request namespace.
                'retrievePartyList', // The request name.
                s_ns0, // The response namespace.
                'retrievePartyListResponse', //  The response name.
                'wsPartySearchService.RetrievePartyListResponseType'} // The response type.
            );
            response_x = response_map_x.get('response_x');
            return response_x.RetrievePartyListResponse;
        }
        public wsPartySearchService.SearchAcrossPartnersResultType searchAcrossPartners(wsPartySearchService.SearchAcrossPartnersRequestType searchAcrossPartnersRequest) {
            wsPartySearchService.SearchAcrossPartnersType request_x = new wsPartySearchService.SearchAcrossPartnersType();
            wsPartySearchService.SearchAcrossPartnersResponseType response_x;
            request_x.searchAcrossPartnersRequest = searchAcrossPartnersRequest;
            // A map of key-value pairs that represent the response that the external service sends after receiving the request. In each pair, the key is a response identifier. The value is the response object, which is an instance of a type that is created as part of the auto-generated stub class.
            Map<String, wsPartySearchService.SearchAcrossPartnersResponseType> response_map_x = new Map<String, wsPartySearchService.SearchAcrossPartnersResponseType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this, // stub - An instance of the Apex class that is auto-generated from a WSDL (the stub class).
                request_x, // request - The request to the external service. The request is an instance of a type that is created as part of the auto-generated stub class.
                response_map_x, // response
                new String[]{endpoint_x, //  the URL of the external web service
                'http://service.amfam.com/partysearchservice/v7/searchAcrossPartners', // The SOAP action.
                s_ns0, // The request namespace.
                'searchAcrossPartners', // The request name.
                s_ns0, // The response namespace.
                'searchAcrossPartnersResponse', //  The response name.
                'wsPartySearchService.SearchAcrossPartnersResponseType'} // The response type.
            );
            response_x = response_map_x.get('response_x');
            return response_x.SearchAcrossPartnersResponse;
        }
        public wsPartySearchService.PartySearchResponseType searchParty(wsPartySearchService.PartySearchRequestType PartySearchRequest) {
            wsPartySearchService.SearchPartyType request_x = new wsPartySearchService.SearchPartyType();
            wsPartySearchService.SearchPartyResponseType response_x;
            request_x.PartySearchRequest = PartySearchRequest;
            // A map of key-value pairs that represent the response that the external service sends after receiving the request. In each pair, the key is a response identifier. The value is the response object, which is an instance of a type that is created as part of the auto-generated stub class.
            Map<String, wsPartySearchService.SearchPartyResponseType> response_map_x = new Map<String, wsPartySearchService.SearchPartyResponseType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this, // stub - An instance of the Apex class that is auto-generated from a WSDL (the stub class).
                request_x, // request - The request to the external service. The request is an instance of a type that is created as part of the auto-generated stub class.
                response_map_x, // response
                new String[]{endpoint_x, //  the URL of the external web service
                'http://service.amfam.com/partysearchservice/v7/searchParty', // The SOAP action.
                s_ns0, // The request namespace.
                'searchParty', // The request name.
                s_ns0, // The response namespace.
                'searchPartyResponse', //  The response name.
                'wsPartySearchService.SearchPartyResponseType'} // The response type.
            );
            response_x = response_map_x.get('response_x');
            return response_x.PartySearchResponse;
        }
        
        private DOM.XmlNode populateDoc(DOM.Document doc){
            String env = 'http://schemas.xmlsoap.org/soap/envelope/';
            String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
            String xsd = 'http://www.w3.org/2001/XMLSchema';
            
            DOM.XmlNode envelope = doc.createRootElement('Envelope', env, 'env');
            envelope.setNamespace('xsd', xsd);
            envelope.setNamespace('xsi', xsi);
            DOM.XmlNode header = envelope.addChildElement('env:Header', null, null);
            AddHeader(header);
            //System.debug(doc.toXmlString());
            DOM.XmlNode body = envelope.addChildElement('env:Body', null, null);
            return body;
        }

        private void AddHeader(DOM.XmlNode header){
            DOM.XmlNode appHeader = header.addChildElement('AppAuthzHeader',null,null);
            DOM.XmlNode userIdNode = appHeader.addChildElement('userId', null, null);
            userIdNode.addTextNode('SFCSRVT');
      
        }

        public wsPartySearchPing.PingResultType ping_Http(wsPartySearchPing.PingInputType PingInput) {
            DOM.Document doc = new DOM.Document();
            DOM.XmlNode body = populateDoc(doc);
            DOM.XmlNode methodNode = body.addChildElement('ping', 'http://service.amfam.com/partysearchservice/v7', '');            
            wsPartySearchPing.ping_element request_x = new wsPartySearchPing.ping_element(PingInput);
            request_x.populateXmlNode(methodNode);
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint_x);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'text/xml; charset=UTF-8');
            req.setHeader('SOAPAction', 'http://service.amfam.com/partysearchservice/v7/ping');
            //System.debug(doc.toXmlString());
            req.setBodyDocument(doc);
            //System.debug(req.getBody());
            Http http = new Http();
            HTTPResponse res = http.send(req);
            //System.debug(res.getBody());
            Dom.Document responseDoc = res.getBodyDocument();
            Dom.XmlNode rootNode = responseDoc.getRootElement();
            Dom.XmlNode bodyNode = rootNode.getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/');
            Dom.XmlNode pingResponseNode = bodyNode.getChildElement('pingResponse', 'http://schema.amfam.com/ping');
                        
            wsPartySearchPing.pingResponse_element response_x = new wsPartySearchPing.pingResponse_element(pingResponseNode);
            return response_x.PingResult;
        }
 
        public wsPartySearchService.RetrieveResponseType retrieveParty_Http(wsPartySearchService.RetrieveRequestType RetrieveRequest) {
            DOM.Document doc = new DOM.Document();
            DOM.XmlNode body = populateDoc(doc);
            DOM.XmlNode methodNode = body.addChildElement('retrieveParty', 'http://service.amfam.com/partysearchservice', '');           
            wsPartySearchService.RetrievePartyType request_x = new wsPartySearchService.RetrievePartyType(RetrieveRequest);
            request_x.populateXmlNode(methodNode);
            
            HttpRequest req = new HttpRequest();
            API_Header__mdt apiHeader = [SELECT MasterLabel, Header_Value__c FROM API_Header__mdt WHERE MasterLabel = 'AFI-API-Key'];
            req.setEndpoint(endpoint_x);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'text/xml; charset=UTF-8');
            req.setHeader('SOAPAction', 'http://service.amfam.com/partysearchservice/v7/retrieveParty');
            req.setHeader('AFI-AppName', 'Salesforce');
            req.setHeader('AFI-API-Key', apiHeader.Header_Value__c);
            System.debug(doc.toXmlString());
            req.setBodyDocument(doc);
            //System.debug(req.getBody());
            Http http = new Http();
            HTTPResponse res = http.send(req);
            System.debug(res.getBody());
            Dom.Document responseDoc = res.getBodyDocument();
            Dom.XmlNode rootNode = responseDoc.getRootElement();
            Dom.XmlNode bodyNode = rootNode.getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/');
            Dom.XmlNode retrievePartyResponseNode = bodyNode.getChildElement('retrievePartyResponse', 'http://service.amfam.com/partysearchservice');
                        
            wsPartySearchService.RetrievePartyResponseType response_x = new wsPartySearchService.RetrievePartyResponseType(retrievePartyResponseNode);
            return response_x.RetrieveResponse;
        }
 
        public wsPartySearchService.MultiplePartiesRetrieveResponseType retrievePartyList_Http(wsPartySearchService.MultiplePartiesRetrieveRequestType RetrievePartyListRequest) {
            DOM.Document doc = new DOM.Document();
            DOM.XmlNode body = populateDoc(doc);
            DOM.XmlNode methodNode = body.addChildElement('retrievePartyList', 'http://service.amfam.com/partysearchservice/v7', '');           
            wsPartySearchService.RetrievePartyListType request_x = new wsPartySearchService.RetrievePartyListType(RetrievePartyListRequest);
            request_x.populateXmlNode(methodNode);
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint_x);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'text/xml; charset=UTF-8');
            req.setHeader('SOAPAction', 'http://service.amfam.com/partysearchservice/v7/retrievePartyList');
            //System.debug(doc.toXmlString());
            req.setBodyDocument(doc);
            //System.debug(req.getBody());
            Http http = new Http();
            HTTPResponse res = http.send(req);
            //System.debug(res.getBody());
            Dom.Document responseDoc = res.getBodyDocument();
            Dom.XmlNode rootNode = responseDoc.getRootElement();
            Dom.XmlNode bodyNode = rootNode.getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/');
            Dom.XmlNode retrievePartyListResponseNode = bodyNode.getChildElement('retrievePartyListResponse', 'http://service.amfam.com/partysearchservice');
                        
            wsPartySearchService.RetrievePartyListResponseType response_x = new wsPartySearchService.RetrievePartyListResponseType(retrievePartyListResponseNode);
            return response_x.RetrievePartyListResponse;
        }
 
        public wsPartySearchService.SearchAcrossPartnersResultType searchAcrossPartners_Http(wsPartySearchService.SearchAcrossPartnersRequestType searchAcrossPartnersRequest) {
            DOM.Document doc = new DOM.Document();
            DOM.XmlNode body = populateDoc(doc);
            DOM.XmlNode methodNode = body.addChildElement('searchAcrossPartners', 'http://service.amfam.com/partysearchservice/v7', '');            
            wsPartySearchService.SearchAcrossPartnersType request_x = new wsPartySearchService.SearchAcrossPartnersType(searchAcrossPartnersRequest);
            request_x.populateXmlNode(methodNode);
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint_x);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'text/xml; charset=UTF-8');
            req.setHeader('SOAPAction', 'http://service.amfam.com/partysearchservice/v7/searchAcrossPartners');
            //System.debug(doc.toXmlString());
            req.setBodyDocument(doc);
            //System.debug(req.getBody());
            Http http = new Http();
            HTTPResponse res = http.send(req);
            //System.debug(res.getBody());
            Dom.Document responseDoc = res.getBodyDocument();
            Dom.XmlNode rootNode = responseDoc.getRootElement();
            Dom.XmlNode bodyNode = rootNode.getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/');
            Dom.XmlNode searchAcrossPartnersResponseNode = bodyNode.getChildElement('searchAcrossPartnersResponse', 'http://service.amfam.com/partysearchservice');
                        
            wsPartySearchService.SearchAcrossPartnersResponseType response_x = new wsPartySearchService.SearchAcrossPartnersResponseType(searchAcrossPartnersResponseNode);
            return response_x.SearchAcrossPartnersResponse;
        }
 
        public wsPartySearchService.PartySearchResponseType searchParty_Http(wsPartySearchService.PartySearchRequestType PartySearchRequest) {
            DOM.Document doc = new DOM.Document();
            DOM.XmlNode body = populateDoc(doc);
            DOM.XmlNode methodNode = body.addChildElement('searchParty', 'http://service.amfam.com/partysearchservice/v7', '');         
            wsPartySearchService.SearchPartyType request_x = new wsPartySearchService.SearchPartyType(PartySearchRequest);
            request_x.populateXmlNode(methodNode);
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint_x);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'text/xml; charset=UTF-8');
            req.setHeader('SOAPAction', 'http://service.amfam.com/partysearchservice/v7/searchParty');
            //System.debug(doc.toXmlString());
            req.setBodyDocument(doc);
            //System.debug(req.getBody());
            Http http = new Http();
            HTTPResponse res = http.send(req);
            //System.debug(res.getBody());
            Dom.Document responseDoc = res.getBodyDocument();
            Dom.XmlNode rootNode = responseDoc.getRootElement();
            Dom.XmlNode bodyNode = rootNode.getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/');
            Dom.XmlNode searchPartyResponseNode = bodyNode.getChildElement('searchPartyResponse', 'http://service.amfam.com/partysearchservice');
                        
            wsPartySearchService.SearchPartyResponseType response_x = new wsPartySearchService.SearchPartyResponseType(searchPartyResponseNode);
            return response_x.PartySearchResponse;
        }
    }
}