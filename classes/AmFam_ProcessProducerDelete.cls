public without sharing class AmFam_ProcessProducerDelete implements Database.Batchable<sObject>, Database.Stateful{

    public final Map<String, Set<String>> mapProducerIndexedByUser;
    public final Map<String, List<Producer>> producerMap;
    public final List<String> producerIdList;
    public final List<AccountShare> accountShareList;
    public final Set<String> accountIds;
    public final String singleProducerId;
    public final String query;


    public Map<String, Set<String>> mapAccountsIndexedByProducer;
    public Map<String, Map<String, Integer>> mapTotalAccountByUser = new Map<String, Map<String, Integer>>();    


    public AmFam_ProcessProducerDelete(Map<String, Set<String>> userMap, List<String> prIdList, Map<String, List<Producer>> prMap, Map<String, Set<String>> accountsByProducer, Map<String, Map<String, Integer>> totalAccountsByUser, Set<String> accIds) {


        this.producerMap = prMap;
        this.mapProducerIndexedByUser = userMap;
        this.mapAccountsIndexedByProducer = (accountsByProducer != null) ? accountsByProducer : new Map<String, Set<String>>();
        this.mapTotalAccountByUser = (totalAccountsByUser != null) ? totalAccountsByUser : new Map<String, Map<String, Integer>>();
        this.accountIds = (accIds != null) ? accIds : new Set<String>();
        this.accountShareList = new List<AccountShare>();


        if (prIdList != null && prIdList.size() > 0) {
            this.singleProducerId = prIdList.remove(0);
            this.producerIdList = prIdList;

            this.query = 'SELECT Id, RelatedId__c, Producer_Identifier__c FROM ProducerAccess__c WHERE Producer_Identifier__c =: singleProducerId';
        }
    
        System.debug('### AmFam_ProcessProducerDelete - Init - this.producerMap: ' + this.producerMap);        
        System.debug('### AmFam_ProcessProducerDelete - Init - this.mapProducerIndexedByUser: ' + this.mapProducerIndexedByUser);        
        System.debug('### AmFam_ProcessProducerDelete - Init - this.mapAccountsIndexedByProducer: ' + this.mapAccountsIndexedByProducer);        
        System.debug('### AmFam_ProcessProducerDelete - Init - this.mapTotalAccountByUser: ' + this.mapTotalAccountByUser);        
        System.debug('### AmFam_ProcessProducerDelete - Init - this.accountIds: ' + this.accountIds);        
        System.debug('### AmFam_ProcessProducerDelete - Init - this.accountShareList: ' + this.accountShareList);        

        System.debug('### AmFam_ProcessProducerDelete - Init - this.query: ' + this.query);        

    }



    public Database.QueryLocator start(Database.BatchableContext BC){

        String singleProducerId = this.singleProducerId;


        return Database.getQueryLocator(this.query);
    }

    public void execute(Database.BatchableContext BC, List<ProducerAccess__c> scope){

        System.debug('### AmFam_ProcessProducerDelete - execute - scope: ' + scope);        


        //Get the list of accounts related by Producer Id
        for (ProducerAccess__c pa : scope) {

            String producerName = pa.Producer_Identifier__c;
            Set<String> listOfAccounts = this.mapAccountsIndexedByProducer.get(producerName);
            if (listOfAccounts == null) {
                listOfAccounts = new Set<String>();
            }

            listOfAccounts.add(pa.RelatedId__c);
            this.mapAccountsIndexedByProducer.put(producerName, listOfAccounts);

            this.accountIds.add(pa.RelatedId__c);


            

            List<Producer> producerList = this.producerMap.get(producerName);

            if (producerList != null) {
                for (Producer pr : producerList) {
                    AccountShare accShare = new AccountShare();
                    accShare.AccountId = pa.RelatedId__c;
                    accShare.UserOrGroupId = pr.InternalUserId;

                    this.accountShareList.add(accShare);
                }
            }
            

        }


        System.debug('### AmFam_ProcessProducerDelete - execute - this.accountShareList: ' + this.accountShareList); 

    }




    public void finish(Database.BatchableContext BC){

        if (this.producerIdList != null && this.producerIdList.size() > 0) {

            AmFam_ProcessProducerDelete rsb = new AmFam_ProcessProducerDelete(this.mapProducerIndexedByUser, this.producerIdList, this.producerMap, this.mapAccountsIndexedByProducer, this.mapTotalAccountByUser, this.accountIds);
            database.executebatch(rsb);
            //Call next batch
        }
        else {
            

            //Calculate the number of access paths a user has to each account
            Map<String, Map<String, Integer>> totalAccountsByUser = AmFam_ProcessProducerDelete.getTotalAccountsByUser(this.mapProducerIndexedByUser, this.mapAccountsIndexedByProducer);

            //All queries are over now, so go to process the delete
            AmFam_ProcessProducerDelete.processSharesDelete(this.mapProducerIndexedByUser.keySet(), this.accountIds, totalAccountsByUser, this.accountShareList );
        }

    }

    public static Map<String, Map<String, Integer>> getTotalAccountsByUser(Map<String,Set<String>> mapProducerIndexedByUser, Map<String, Set<String>> mapAccountsIndexedByProducer) {

        Map<String, Map<String, Integer>> mapTotalAccountByUser = new Map<String, Map<String, Integer>>();

                
        for (String userId : mapProducerIndexedByUser.keySet()) {

            Set<String> producers = mapProducerIndexedByUser.get(userId);

            Map<String, Integer> accountTotals = mapTotalAccountByUser.get(userId);
            if (accountTotals == null) {
                accountTotals = new Map<String, Integer>();
            }

            for (String producerName : producers) {
                Set<String> accounts = mapAccountsIndexedByProducer.get(producerName);
                if (accounts != null) {
                    for (String accountId : accounts) {
                        Integer total = accountTotals.get(accountId);
                        if (total == null) {
                            total = 0;
                        }

                        total++;

                        accountTotals.put(accountId, total);
                    }
                }

            }

            mapTotalAccountByUser.put(userId, accountTotals);

        }

        return mapTotalAccountByUser;
    }

    public static void processSharesDelete(Set<String> userIdSet, Set<String> accountIdSet, Map<String, Map<String, Integer>> totalAccountByUser, List<AccountShare> accountShareList) {

        Map<Id,AccountShare> accountShareListToDeleteMap = new Map<Id,AccountShare>();

        //Remove existing AccountShares
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId, AccountId FROM AccountShare 
                    WHERE UserOrGroup.IsActive = true AND RowCause = 'Manual' AND UserOrGroupId IN: userIdSet AND AccountId IN: accountIdSet ];


        Map<String, String> existingShares = new Map<String, String>();

        for (AccountShare accs : accountShares ) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            existingShares.put(key, accs.Id);
        }

        Set<String> deletedShareKeys = new Set<String>();

        for (AccountShare accs : accountShareList) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;

            Map<String, Integer> totalForUser = totalAccountByUser.get(accs.UserOrGroupId);

            if (totalForUser == null) {
                totalForUser = new Map<String, Integer>();
            }

            Integer totalForAccount = totalForUser.get(accs.AccountId);
            if (totalForAccount == null) {
                totalForAccount = 0;
            }

            if (existingShares.containsKey(key)  ) {

                if (totalForAccount < 2) {
                    accs.Id = existingShares.get(key);
                    accountShareListToDeleteMap.put(accs.Id,accs);
                    deletedShareKeys.add(key);
                }
                else {
                    totalForAccount--;
                    totalForUser.put(accs.AccountId, totalForAccount);
                    totalAccountByUser.put(accs.UserOrGroupId, totalForUser);
                }
            }
        }

        if (accountShareListToDeleteMap.size() > 0) {
            delete accountShareListToDeleteMap.values();

            //Process opportunities to move them into the Agency Account if the agent losses access to the customer account
            processOpportunities(accountIdSet, userIdSet, deletedShareKeys);            


            //Cal the InteractionSummaryService to extend the access removal from Interaction Summaries
            AmFam_InteractionSummaryService.removeAccessFromAccount(accountShareListToDeleteMap.values());
        }

    }

    private static void processOpportunities(Set<String> accountIds, Set<String> userIds, Set<String> deletedShareKeys) {

        //DeletedShareKeys will be the combination of "AccountId-UserOrGroupId" values of the share
        //An Opportunity child of the AccountId and owned by UserOrGroupId should be moved under the agency account
    
        List<Opportunity> oppList = [SELECT Id, Agency__c, Agency__r.OwnerId, AccountId, Account.Name, Original_Account_Id__c, Original_Account_Name__c, Account.Agency__c, OwnerId FROM Opportunity WHERE AccountId IN: accountIds AND OwnerId IN: userIds];

        Map<String, Opportunity> oppListToUpdate = new Map<String, Opportunity>();

        for (Opportunity opp : oppList) {
            
            String agencyId = opp.Agency__c;
            String key = opp.AccountId + '-' + opp.OwnerId;

            String agencyKey = agencyId + '-' + opp.OwnerId;

            if (deletedShareKeys.contains(key)) {

                if (agencyId != null) {
                    opp.Original_Account_Id__c = opp.AccountId;
                    opp.Original_Account_Name__c = opp.Account.Name;
                    opp.AccountId = agencyId;

                    if (deletedShareKeys.contains(agencyKey) && opp.Agency__r.OwnerId != opp.OwnerId) {
                        opp.OwnerId = opp.Agency__r.OwnerId;
                    }

                    oppListToUpdate.put(opp.Id, opp);
                }
                

            }
        }

        if (oppListToUpdate.size() > 0) {
            update oppListToUpdate.values();
        }
    }    



}