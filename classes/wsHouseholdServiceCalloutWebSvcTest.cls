@isTest
public class wsHouseholdServiceCalloutWebSvcTest {
    
    @isTest 
    public static void coverGeneratedCodeTypes() {
        Test.setMock(WebServiceMock.class, new wsHouseholdServiceCalloutMockImpl());
        wsHouseholdServiceCallout parentObject = new wsHouseholdServiceCallout();
        DOM.Document doc = new DOM.Document();
        DOM.XmlNode mockNode = doc.createRootElement('Envelope', 'http://schemas.xmlsoap.org/soap/envelope/', 'env');
        new wsHouseholdService.EditHouseholdRequestType(mockNode);				
        new wsHouseholdService.EditHouseholdResponseType(mockNode);				
        new wsHouseholdService.EditHouseholdResultType(mockNode);				
        new wsHouseholdService.GetHouseholdByMemberIdRequestType(mockNode);				
        new wsHouseholdService.GetHouseholdByMemberIdResponseType(mockNode);				
        new wsHouseholdService.GetHouseholdByMemberIdResultType(mockNode);				
        new wsHouseholdService.MergedAwayHouseholdType(mockNode);				
        new wsHouseholdService.MergeHouseholdRequestType(mockNode);				
        new wsHouseholdService.MergeHouseholdResponseType(mockNode);				
        new wsHouseholdService.MergeHouseholdResultType(mockNode);				
        new wsHouseholdService.RetrieveHouseholdRequestType(mockNode);				
        new wsHouseholdService.RetrieveHouseholdResponseType(mockNode);				
        new wsHouseholdService.RetrieveResponseType(mockNode);				
        new wsHouseholdService.SplitHouseholdRequestType(mockNode);				
        new wsHouseholdService.SplitHouseholdResponseType(mockNode);				
        new wsHouseholdService.SplitHouseholdResultType(mockNode);				
        new wsHouseholdService.SurvivingHouseholdType(mockNode);				
        new wsHouseholdServiceParty.AddressType(mockNode);				
        new wsHouseholdServiceParty.AffinityGroupType(mockNode);				
        new wsHouseholdServiceParty.ChangeRequestDetailsType(mockNode);				
        new wsHouseholdServiceParty.Code1ResultType(mockNode);				
        new wsHouseholdServiceParty.Code1WrapperType(mockNode);				
        new wsHouseholdServiceParty.CodeType(mockNode);				
        new wsHouseholdServiceParty.ContactType(mockNode);				
        new wsHouseholdServiceParty.DriversLicenseType(mockNode);				
        new wsHouseholdServiceParty.EmailType(mockNode);				
        new wsHouseholdServiceParty.EmploymentOccupationType(mockNode);				
        new wsHouseholdServiceParty.EntrantAdditionalInfoType(mockNode);				
        new wsHouseholdServiceParty.EntrantDemographicInfoType(mockNode);				
        new wsHouseholdServiceParty.EntrantIdentityInfoType(mockNode);				
        new wsHouseholdServiceParty.EntrantType(mockNode);				
        new wsHouseholdServiceParty.ErrorsType(mockNode);				
        new wsHouseholdServiceParty.ErrorType(mockNode);				
        new wsHouseholdServiceParty.FormOfBusinessAndDescriptionType(mockNode);				
        new wsHouseholdServiceParty.HouseholdAddressType(mockNode);				
        new wsHouseholdServiceParty.HouseholdMemberType(mockNode);				
        new wsHouseholdServiceParty.HouseholdType(mockNode);				
        new wsHouseholdServiceParty.LifeEventDateType(mockNode);				
        new wsHouseholdServiceParty.MilitaryDeploymentType(mockNode);				
        new wsHouseholdServiceParty.OccupationCodeAndCategoryType(mockNode);				
        new wsHouseholdServiceParty.OrganizationAdditionalInfoType(mockNode);				
        new wsHouseholdServiceParty.OrganizationDemographicInfoType(mockNode);				
        new wsHouseholdServiceParty.OrganizationIdentityInfoType(mockNode);				
        new wsHouseholdServiceParty.OrganizationLifeEventInfoType(mockNode);				
        new wsHouseholdServiceParty.OrganizationNameType(mockNode);				
        new wsHouseholdServiceParty.OrganizationType(mockNode);				
        new wsHouseholdServiceParty.OriginalLicenseDateType(mockNode);				
        new wsHouseholdServiceParty.PartyAdditionalInfoType(mockNode);				
        new wsHouseholdServiceParty.PartyAddressPurposeAndUsageType(mockNode);				
        new wsHouseholdServiceParty.PartyAddressType(mockNode);				
        new wsHouseholdServiceParty.PartyEmailType(mockNode);				
        new wsHouseholdServiceParty.PartyEmailUsageAndDescriptionType(mockNode);				
        new wsHouseholdServiceParty.PartyFaultType(mockNode);				
        new wsHouseholdServiceParty.PartyPhoneType(mockNode);				
        new wsHouseholdServiceParty.PartyPhoneUsageAndDescriptionType(mockNode);				
        new wsHouseholdServiceParty.PartyRelationshipType(mockNode);				
        new wsHouseholdServiceParty.PartySocialMediaType(mockNode);				
        new wsHouseholdServiceParty.PartyType(mockNode);				
        new wsHouseholdServiceParty.PersonAdditionalInfoType(mockNode);				
        new wsHouseholdServiceParty.PersonDemographicInfoType(mockNode);				
        new wsHouseholdServiceParty.PersonIdentityInfoType(mockNode);				
        new wsHouseholdServiceParty.PersonLifeEventInfoType(mockNode);				
        new wsHouseholdServiceParty.PersonNameType(mockNode);				
        new wsHouseholdServiceParty.PersonType(mockNode);				
        new wsHouseholdServiceParty.PhoneNumberType(mockNode);				
        new wsHouseholdServiceParty.PhoneType(mockNode);				
        new wsHouseholdServiceParty.PrefixAndDescriptionType(mockNode);				
        new wsHouseholdServiceParty.PrincipalRelationshipType(mockNode);				
        new wsHouseholdServiceParty.PrivacyOptOutType(mockNode);				
        new wsHouseholdServiceParty.ProfessionalDesignationAndDescriptionType(mockNode);				
        new wsHouseholdServiceParty.RelatedPartyType(mockNode);				
        new wsHouseholdServiceParty.SourceSystemInformationType(mockNode);				
        new wsHouseholdServiceParty.ThirdPartyInterestInfoType(mockNode);				
        new wsHouseholdServiceParty.TinNumberAndType(mockNode);				
        new wsHouseholdServicePing.PingInputType(mockNode);				
        new wsHouseholdServicePing.pingResponse_element(mockNode);				
        new wsHouseholdServicePing.PingResultType(mockNode);				
        new wsHouseholdServicePing.VerificationType(mockNode);				
        wsHouseholdService.EditHouseholdRequestType Obj1 = new wsHouseholdService.EditHouseholdRequestType();
        Obj1.populateXmlNode(mockNode);
        new wsHouseholdService.EditHouseholdResponseType();
        wsHouseholdService.EditHouseholdResultType Obj3 = new wsHouseholdService.EditHouseholdResultType();
        Obj3.populateXmlNode(mockNode);
        wsHouseholdService.EditHouseholdType Obj4 = new wsHouseholdService.EditHouseholdType();
        Obj4.populateXmlNode(mockNode);
        wsHouseholdService.GetHouseholdByMemberIdRequestType Obj5 = new wsHouseholdService.GetHouseholdByMemberIdRequestType();
        Obj5.populateXmlNode(mockNode);
        new wsHouseholdService.GetHouseholdByMemberIdResponseType();
        wsHouseholdService.GetHouseholdByMemberIdResultType Obj7 = new wsHouseholdService.GetHouseholdByMemberIdResultType();
        Obj7.populateXmlNode(mockNode);
        wsHouseholdService.GetHouseholdByMemberIdType Obj8 = new wsHouseholdService.GetHouseholdByMemberIdType();
        Obj8.populateXmlNode(mockNode);
        wsHouseholdService.MergedAwayHouseholdType Obj9 = new wsHouseholdService.MergedAwayHouseholdType();
        Obj9.populateXmlNode(mockNode);
        wsHouseholdService.MergeHouseholdRequestType Obj10 = new wsHouseholdService.MergeHouseholdRequestType();
        Obj10.populateXmlNode(mockNode);
        new wsHouseholdService.MergeHouseholdResponseType();
        wsHouseholdService.MergeHouseholdResultType Obj12 = new wsHouseholdService.MergeHouseholdResultType();
        Obj12.populateXmlNode(mockNode);
        wsHouseholdService.MergeHouseholdType Obj13 = new wsHouseholdService.MergeHouseholdType();
        Obj13.populateXmlNode(mockNode);
        wsHouseholdService.RetrieveHouseholdRequestType Obj14 = new wsHouseholdService.RetrieveHouseholdRequestType();
        Obj14.populateXmlNode(mockNode);
        new wsHouseholdService.RetrieveHouseholdResponseType();
        wsHouseholdService.RetrieveHouseholdType Obj16 = new wsHouseholdService.RetrieveHouseholdType();
        Obj16.populateXmlNode(mockNode);
        wsHouseholdService.RetrieveResponseType Obj17 = new wsHouseholdService.RetrieveResponseType();
        Obj17.populateXmlNode(mockNode);
        wsHouseholdService.SplitHouseholdRequestType Obj18 = new wsHouseholdService.SplitHouseholdRequestType();
        Obj18.populateXmlNode(mockNode);
        new wsHouseholdService.SplitHouseholdResponseType();
        wsHouseholdService.SplitHouseholdResultType Obj20 = new wsHouseholdService.SplitHouseholdResultType();
        Obj20.populateXmlNode(mockNode);
        wsHouseholdService.SplitHouseholdType Obj21 = new wsHouseholdService.SplitHouseholdType();
        Obj21.populateXmlNode(mockNode);
        wsHouseholdService.SurvivingHouseholdType Obj22 = new wsHouseholdService.SurvivingHouseholdType();
        Obj22.populateXmlNode(mockNode);
        wsHouseholdServiceParty.AddressType Obj23 = new wsHouseholdServiceParty.AddressType();
        Obj23.populateXmlNode(mockNode);
        wsHouseholdServiceParty.ParseDateTime('2014-11-05T13:15:30Z');
        wsHouseholdServiceParty.AffinityGroupType Obj24 = new wsHouseholdServiceParty.AffinityGroupType();
        Obj24.populateXmlNode(mockNode);
        wsHouseholdServiceParty.ChangeRequestDetailsType Obj25 = new wsHouseholdServiceParty.ChangeRequestDetailsType();
        Obj25.populateXmlNode(mockNode);
        wsHouseholdServiceParty.Code1ResultType Obj26 = new wsHouseholdServiceParty.Code1ResultType();
        Obj26.populateXmlNode(mockNode);
        wsHouseholdServiceParty.Code1WrapperType Obj27 = new wsHouseholdServiceParty.Code1WrapperType();
        Obj27.populateXmlNode(mockNode);
        wsHouseholdServiceParty.CodeType Obj28 = new wsHouseholdServiceParty.CodeType();
        Obj28.populateXmlNode(mockNode);
        wsHouseholdServiceParty.ContactType Obj29 = new wsHouseholdServiceParty.ContactType();
        Obj29.populateXmlNode(mockNode);
        wsHouseholdServiceParty.DriversLicenseType Obj30 = new wsHouseholdServiceParty.DriversLicenseType();
        Obj30.populateXmlNode(mockNode);
        wsHouseholdServiceParty.EmailType Obj31 = new wsHouseholdServiceParty.EmailType();
        Obj31.populateXmlNode(mockNode);
        wsHouseholdServiceParty.EmploymentOccupationType Obj32 = new wsHouseholdServiceParty.EmploymentOccupationType();
        Obj32.populateXmlNode(mockNode);
        wsHouseholdServiceParty.EntrantAdditionalInfoType Obj33 = new wsHouseholdServiceParty.EntrantAdditionalInfoType();
        Obj33.populateXmlNode(mockNode);
        wsHouseholdServiceParty.EntrantDemographicInfoType Obj34 = new wsHouseholdServiceParty.EntrantDemographicInfoType();
        Obj34.populateXmlNode(mockNode);
        wsHouseholdServiceParty.EntrantIdentityInfoType Obj35 = new wsHouseholdServiceParty.EntrantIdentityInfoType();
        Obj35.populateXmlNode(mockNode);
        wsHouseholdServiceParty.EntrantType Obj36 = new wsHouseholdServiceParty.EntrantType();
        Obj36.populateXmlNode(mockNode);
        wsHouseholdServiceParty.ErrorsType Obj37 = new wsHouseholdServiceParty.ErrorsType();
        Obj37.populateXmlNode(mockNode);
        wsHouseholdServiceParty.ErrorType Obj38 = new wsHouseholdServiceParty.ErrorType();
        Obj38.populateXmlNode(mockNode);
        wsHouseholdServiceParty.FormOfBusinessAndDescriptionType Obj39 = new wsHouseholdServiceParty.FormOfBusinessAndDescriptionType();
        Obj39.populateXmlNode(mockNode);
        wsHouseholdServiceParty.HouseholdAddressType Obj40 = new wsHouseholdServiceParty.HouseholdAddressType();
        Obj40.populateXmlNode(mockNode);
        wsHouseholdServiceParty.HouseholdMemberType Obj41 = new wsHouseholdServiceParty.HouseholdMemberType();
        Obj41.populateXmlNode(mockNode);
        wsHouseholdServiceParty.HouseholdType Obj42 = new wsHouseholdServiceParty.HouseholdType();
        Obj42.populateXmlNode(mockNode);
        wsHouseholdServiceParty.LifeEventDateType Obj43 = new wsHouseholdServiceParty.LifeEventDateType();
        Obj43.populateXmlNode(mockNode);
        wsHouseholdServiceParty.MilitaryDeploymentType Obj44 = new wsHouseholdServiceParty.MilitaryDeploymentType();
        Obj44.populateXmlNode(mockNode);
        wsHouseholdServiceParty.OccupationCodeAndCategoryType Obj45 = new wsHouseholdServiceParty.OccupationCodeAndCategoryType();
        Obj45.populateXmlNode(mockNode);
        wsHouseholdServiceParty.OrganizationAdditionalInfoType Obj46 = new wsHouseholdServiceParty.OrganizationAdditionalInfoType();
        Obj46.populateXmlNode(mockNode);
        wsHouseholdServiceParty.OrganizationDemographicInfoType Obj47 = new wsHouseholdServiceParty.OrganizationDemographicInfoType();
        Obj47.populateXmlNode(mockNode);
        wsHouseholdServiceParty.OrganizationIdentityInfoType Obj48 = new wsHouseholdServiceParty.OrganizationIdentityInfoType();
        Obj48.populateXmlNode(mockNode);
        wsHouseholdServiceParty.OrganizationLifeEventInfoType Obj49 = new wsHouseholdServiceParty.OrganizationLifeEventInfoType();
        Obj49.populateXmlNode(mockNode);
        wsHouseholdServiceParty.OrganizationNameType Obj50 = new wsHouseholdServiceParty.OrganizationNameType();
        Obj50.populateXmlNode(mockNode);
        wsHouseholdServiceParty.OrganizationType Obj51 = new wsHouseholdServiceParty.OrganizationType();
        Obj51.populateXmlNode(mockNode);
        wsHouseholdServiceParty.OriginalLicenseDateType Obj52 = new wsHouseholdServiceParty.OriginalLicenseDateType();
        Obj52.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyAdditionalInfoType Obj53 = new wsHouseholdServiceParty.PartyAdditionalInfoType();
        Obj53.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyAddressPurposeAndUsageType Obj54 = new wsHouseholdServiceParty.PartyAddressPurposeAndUsageType();
        Obj54.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyAddressType Obj55 = new wsHouseholdServiceParty.PartyAddressType();
        Obj55.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyEmailType Obj56 = new wsHouseholdServiceParty.PartyEmailType();
        Obj56.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyEmailUsageAndDescriptionType Obj57 = new wsHouseholdServiceParty.PartyEmailUsageAndDescriptionType();
        Obj57.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyFaultType Obj58 = new wsHouseholdServiceParty.PartyFaultType();
        Obj58.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyPhoneType Obj59 = new wsHouseholdServiceParty.PartyPhoneType();
        Obj59.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyPhoneUsageAndDescriptionType Obj60 = new wsHouseholdServiceParty.PartyPhoneUsageAndDescriptionType();
        Obj60.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyRelationshipType Obj61 = new wsHouseholdServiceParty.PartyRelationshipType();
        Obj61.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartySocialMediaType Obj62 = new wsHouseholdServiceParty.PartySocialMediaType();
        Obj62.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PartyType Obj63 = new wsHouseholdServiceParty.PartyType();
        Obj63.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PersonAdditionalInfoType Obj64 = new wsHouseholdServiceParty.PersonAdditionalInfoType();
        Obj64.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PersonDemographicInfoType Obj65 = new wsHouseholdServiceParty.PersonDemographicInfoType();
        Obj65.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PersonIdentityInfoType Obj66 = new wsHouseholdServiceParty.PersonIdentityInfoType();
        Obj66.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PersonLifeEventInfoType Obj67 = new wsHouseholdServiceParty.PersonLifeEventInfoType();
        Obj67.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PersonNameType Obj68 = new wsHouseholdServiceParty.PersonNameType();
        Obj68.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PersonType Obj69 = new wsHouseholdServiceParty.PersonType();
        Obj69.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PhoneNumberType Obj70 = new wsHouseholdServiceParty.PhoneNumberType();
        Obj70.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PhoneType Obj71 = new wsHouseholdServiceParty.PhoneType();
        Obj71.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PrefixAndDescriptionType Obj72 = new wsHouseholdServiceParty.PrefixAndDescriptionType();
        Obj72.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PrincipalRelationshipType Obj73 = new wsHouseholdServiceParty.PrincipalRelationshipType();
        Obj73.populateXmlNode(mockNode);
        wsHouseholdServiceParty.PrivacyOptOutType Obj74 = new wsHouseholdServiceParty.PrivacyOptOutType();
        Obj74.populateXmlNode(mockNode);
        wsHouseholdServiceParty.ProfessionalDesignationAndDescriptionType Obj75 = new wsHouseholdServiceParty.ProfessionalDesignationAndDescriptionType();
        Obj75.populateXmlNode(mockNode);
        wsHouseholdServiceParty.RelatedPartyType Obj76 = new wsHouseholdServiceParty.RelatedPartyType();
        Obj76.populateXmlNode(mockNode);
        wsHouseholdServiceParty.SourceSystemInformationType Obj77 = new wsHouseholdServiceParty.SourceSystemInformationType();
        Obj77.populateXmlNode(mockNode);
        wsHouseholdServiceParty.ThirdPartyInterestInfoType Obj78 = new wsHouseholdServiceParty.ThirdPartyInterestInfoType();
        Obj78.populateXmlNode(mockNode);
        wsHouseholdServiceParty.TinNumberAndType Obj79 = new wsHouseholdServiceParty.TinNumberAndType();
        Obj79.populateXmlNode(mockNode);
        wsHouseholdServicePing.ping_element Obj80 = new wsHouseholdServicePing.ping_element();
        Obj80.populateXmlNode(mockNode);
        wsHouseholdServicePing.PingInputType Obj81 = new wsHouseholdServicePing.PingInputType();
        Obj81.populateXmlNode(mockNode);
        new wsHouseholdServicePing.pingResponse_element();
        wsHouseholdServicePing.PingResultType Obj83 = new wsHouseholdServicePing.PingResultType();
        Obj83.populateXmlNode(mockNode);
        wsHouseholdServicePing.VerificationType Obj84 = new wsHouseholdServicePing.VerificationType();
        Obj84.populateXmlNode(mockNode);
    }
    /*@isTest static void coverCodeForeditHousehold(){
        Test.setMock(WebServiceMock.class, new wsHouseholdServiceCalloutMockImpl());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.editHousehold(null));
    }
    @isTest static void coverCodeForgetHouseholdByMemberId(){
        Test.setMock(WebServiceMock.class, new wsHouseholdServiceCalloutMockImpl());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.getHouseholdByMemberId(null));
    }
    @isTest static void coverCodeFormergeHousehold(){
        Test.setMock(WebServiceMock.class, new wsHouseholdServiceCalloutMockImpl());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.mergeHousehold(null));
    }
    @isTest static void coverCodeForping(){
        Test.setMock(WebServiceMock.class, new wsHouseholdServiceCalloutMockImpl());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.ping(null));
    }
    @isTest static void coverCodeForretrieveHousehold(){
        Test.setMock(WebServiceMock.class, new wsHouseholdServiceCalloutMockImpl());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.retrieveHousehold(null));
    }
    @isTest static void coverCodeForsplitHousehold(){
        Test.setMock(WebServiceMock.class, new wsHouseholdServiceCalloutMockImpl());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.splitHousehold(null));
    }
    @isTest static void coverCodeForeditHousehold_Http(){
        Test.setMock(HttpCalloutMock.class, new wsHouseholdServiceCalloutHttpMock());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.editHousehold_Http(null));		
    }
    @isTest static void coverCodeForgetHouseholdByMemberId_Http(){
        Test.setMock(HttpCalloutMock.class, new wsHouseholdServiceCalloutHttpMock());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.getHouseholdByMemberId_Http(null));		
    }
    @isTest static void coverCodeFormergeHousehold_Http(){
        Test.setMock(HttpCalloutMock.class, new wsHouseholdServiceCalloutHttpMock());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.mergeHousehold_Http(null));		
    }
    @isTest static void coverCodeForping_Http(){
        Test.setMock(HttpCalloutMock.class, new wsHouseholdServiceCalloutHttpMock());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.ping_Http(null));		
    }
    @isTest static void coverCodeForretrieveHousehold_Http(){
        Test.setMock(HttpCalloutMock.class, new wsHouseholdServiceCalloutHttpMock());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.retrieveHousehold_Http(null));		
    }
    @isTest static void coverCodeForsplitHousehold_Http(){
        Test.setMock(HttpCalloutMock.class, new wsHouseholdServiceCalloutHttpMock());
        wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        System.assertEquals(null, testObject.splitHousehold_Http(null));		
    }*/
    
    @isTest static void coverCodeForretrieveHousehold(){
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert agent;
        System.runAs(agent) {
            Test.setMock(WebServiceMock.class, new wsHouseholdServiceCalloutMockImpl());
            Test.startTest();
            wsHouseholdServiceCallout.HouseholdServiceSOAP testObject = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
            Test.stopTest();
            System.assertEquals(null, testObject.retrieveHousehold(null));
        }
	}
}