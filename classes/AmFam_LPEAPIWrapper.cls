public class AmFam_LPEAPIWrapper {
    public class Request {
        public String acord;
        public AddParam additionalParameters;
        public Assignment assignment;
        public List<AssociatedCampaign> associatedCampaigns;
        public ContactMechanisms contactMechanisms;
        public List<Household> household;
        public String relationalId;
        public List<LeadProviderDetail> leadProviderDetails;
        public PersonalDetail personalDetail;
        public PriorCarrier priorCarrier;
        public string militaryReturnedDate;
        public Name name;
    }

    public static String createRequest(Lead rec) {
        Request req = new Request();

        req.Name = new Name();
        req.Name.first = rec.FirstName;
        req.Name.last = rec.LastName;
        req.assignment = new Assignment();
        req.assignment.agentProducerId = Integer.valueOf(rec.Agency__r.Producer_ID__c);
        User u = [SELECT Id, Producer_ID__c FROM User WHERE Id =: rec.OwnerId];
        if (u != null) {
            if (u.Producer_ID__c != null) {
                req.assignment.assignedToProducerId = Integer.valueOf(u.Producer_ID__c);
            } else {
                req.assignment.assignedToProducerId = Integer.valueOf(rec.Agency__r.Producer_ID__c);
            }
        } else {
            req.assignment.assignedToProducerId = Integer.valueOf(rec.Agency__r.Producer_ID__c);
        }
        ContactMechanisms cm = new ContactMechanisms();
        if (rec.Street != null) {
            Address addr = new Address();
            String[] addressLines = rec.Street.split('(\r?\n)+');
            addr.line1 = addressLines[0].trim();
            if(addressLines.size() > 1){
                addr.line2 = addressLines[1].trim();
            }
            addr.city = rec.City;
            addr.stateCode = rec.StateCode;
            addr.zip5 = rec.PostalCode;
            addr.purpose = 'RESIDENTIAL';
            addr.type = 'PRIMARY';
            cm.addresses = new List<Address>{addr};
        }
        if (rec.Email != null) {
            Email e = new Email();
            e.email = rec.Email;
            e.type = rec.Email_Usage__c;
            e.preferred = 'PREFERRED';
            cm.emails = new List<Email>{e};
        }
        if (rec.Phone != null) {
            cm.phones = new List<Phone>();
            if (rec.Phone != null) {
                Phone p1 = new Phone();
                // Format phone number to replace all special characters
                p1.number_x = rec.Phone.replaceAll('\\D','');
                p1.type = rec.Phone_Usage__c;
                cm.phones.add(p1);
            }
            /*if (rec.MobilePhone != null) {
                Phone p2 = new Phone();
                // Format phone number to replace all special characters
                p2.number_x = rec.MobilePhone.replaceAll('\\D','');
                p2.type = 'CELL';
                cm.phones.add(p2);
            }*/
        }
        if (rec.Marital_Status__c != null || rec.Gender__c != null || rec.Date_of_Birth__c != null) {
            PersonalDetail pd = new PersonalDetail();
            if (rec.Marital_Status__c != null) {
                switch on rec.Marital_Status__c {
                    when 'M' {
                        pd.maritalStatus = 'MARRIED';
                    }
                    when 'N' {
                        pd.maritalStatus = 'SINGLE';
                    }
                }
            }
            if (rec.Gender__c != null) {
                switch on rec.Gender__c {
                    when 'M' {
                        pd.gender = 'MALE';
                    }
                    when 'F' {
                        pd.gender = 'FEMALE';
                    }
                    when 'NB' {
                        pd.gender = 'NEUTRAL';
                    }
                }
            }
            if (rec.Date_of_Birth__c != null) {
                pd.dateOfBirth = rec.Date_of_Birth__c;
            }
            req.personalDetail = pd;
        }
        req.contactMechanisms = cm;
        LeadProviderDetail lpd = new LeadProviderDetail();
        lpd.leadProvider = 'SALESFORCE';
        lpd.leadProviderID = rec.Lead_ID__c;
        lpd.providedTs = DateTime.now();
        lpd.leadSource = rec.Source__c;
        lpd.leadSourceType = rec.LeadSource;
        lpd.tags = new List<String>{'AGENT_UPLOAD','BULK'};
        req.leadProviderDetails = new List<LeadProviderDetail>{lpd};
        //For LOB
        if (rec.Lines_of_Business__c != null) {
            List<Line_of_Business_Translation__mdt> lobTranslations = [SELECT Label, LPE_Code__c, LPE_Description__c FROM Line_of_Business_Translation__mdt];
            Map<String, List<String>> lobMap = new Map<String, List<String>>();
            for (Line_of_Business_Translation__mdt lobTranslation : lobTranslations) {
                List<String> lst = new List<String>();
                lst.add(lobTranslation.LPE_Code__c);
                lst.add(lobTranslation.LPE_Description__c);
                lobMap.put(lobTranslation.Label, lst);
            }
            List<AssociatedCampaign> acList = new List<AssociatedCampaign>();
            String[] lobs = rec.Lines_of_Business__c.split(';');
            for (String str : lobs) {
                List<String> lobLPE = lobMap.get(str);
                AssociatedCampaign ac = new AssociatedCampaign();
                ac.code = lobLPE[0];
                ac.description = lobLPE[1];
                acList.add(ac);
            }
            req.associatedCampaigns = acList;
        }
        String jsonString = JSON.serialize(req, true);
        jsonString = jsonString.replace('"number_x":', '"number":');
        system.debug(jsonString);
        return jsonString;
    }

    public class AddParam {
        public String emailSubject;
        public String emailDescription;
    }

    public class Assignment {
        public Integer agentProducerId;
        public Integer assignedToProducerId;
    }

    public class Associates {
        public ContactMechanisms contactMechanisms;
        public String relationalId;
        public Name name;
        public PersonalDetail personalDetail;
    }

    public class ContactMechanisms {
        public List<Address> addresses;
        public List<Email> emails;
        public List<Phone> phones;
    }

    public class Name {
        public String first;
        public String last;
        public String middle;
        public String suffix;
    }

    public class PersonalDetail {
        public String ownerRenter;
        public Date dateOfBirth;
        public String maritalStatus;
        public String gender;
        public String currentResidenceDate;
    }

    public class Address {
        public String line1;
        public String line2;
        public String city;
        public String stateCode;
        public String zip5;
        public String zip4;
        public String purpose;
        public String type;
    }

    public class Email {
        public String email;
        public String type;
        public String preferred;
    }

    public class Phone {
        public String number_x; // Number is a reserved keyword in Apex
        public String type;
        public String preferred;
        public String preferredTime;
    }
    public class AssociatedCampaign {
        public String code;
        public String description;
    }

    public class Household {
        public String type;
        public String relationalId;
    }

    public class LeadProviderDetail {
        public String leadProvider;
        public String leadProviderID;
        public DateTime providedTs;
        public String leadSource;
        public String leadSourceType;
        public List<String> tags;
    }

    public class PriorCarrier {
        public String name;
        public Date effectiveDate;
        public Date expirationDate;
    }
}