public with sharing class AmFam_ContactPointPhoneService {
    public static void updateHouseholdPrimaryPhone(List<ContactPointPhone> phones,Map<Id,ContactPointPhone> phonesMap)
    {
        Id householdAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IndustriesHousehold').getRecordTypeId();        

        //cull ones marked primary && account (need to use Id prefix 001)
        System.debug('PHONES'+phones);
        Map<Id,ContactPointPhone> acctPhoneMap = new Map<Id,ContactPointPhone>();
        for (ContactPointPhone phone : phones)
        {
            String parentIdString = phone.ParentId;
            if (phone.IsPrimary == true && parentIdString.startsWith('001'))
            {
                //there could be more than one for a parent, but going with a last in approach
                acctPhoneMap.put(phone.ParentId,phone);
            }
        }

        //get the Accounts reference by the Map
        List<Account> accounts = [SELECT Id,Phone FROM Account WHERE Id in :acctPhoneMap.keyset() AND
                                                        Primary_Contact__pc = true];
        Set<Id> acctIds = new Set<Id>();
        for (Account acct : accounts)
        {
            acctIds.add(acct.Id);
        }                                                        
        
        List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId in :acctIds];                                                
        Set<Id> contactIds = new Set<Id>();
        for (Contact contact : contacts)
        {
            contactIds.add(contact.Id);
        }                                                        
        //now need to get ACR where Contact is the account.contact && account record type = Household
        //that will give me the HH account ids
        List<AccountContactRelation> relations = [SELECT Id,AccountId,ContactId,Contact.AccountId 
                                                    FROM AccountContactRelation
                                                    WHERE Account.RecordTypeId = :householdAccountRTId AND
                                                        ContactId in :contactIds];                                                    

        //
        List<Account> updatedAccts = new List<Account>();
        for (AccountContactRelation rel : relations)
        {
            //not sure if supposed to do anything with AreaCode
            Account updatedAcct = new Account();
            updatedAcct.Id = rel.AccountId;
            updatedAcct.Phone = acctPhoneMap.get(rel.Contact.AccountId).TelephoneNumber;
            updatedAccts.add(updatedAcct);
        }
        if (!updatedAccts.isEmpty())
        {
            update updatedAccts;
        }
    }

    // This method is used to map the CDH response for Party Phones to ContactPointPhone records.
    public static List<ContactPointPhone> mapCDHResponse(String accountId, List<wsPartyManageParty.PartyPhoneType> responsePhoneList) {
        List<ContactPointPhone> cppList = new List<ContactPointPhone>();
        for (wsPartyManageParty.PartyPhoneType p : responsePhoneList) {
            for (wsPartyManageParty.PartyPhoneUsageAndDescriptionType usage : p.PartyPhoneUsageAndDescription) {
                ContactPointPhone cpp = new ContactPointPhone();
                cpp.ParentId = accountId;
                cpp.TelephoneNumber = p.Phone.PhoneNumber.AreaCode + p.Phone.PhoneNumber.number_x;
                cpp.BestTimeToContactTimezone = p.Phone.TimeZone;
                cpp.IsPrimary = p.TrustManagedPrimaryIndicator == 'Y' ? true : false;
                cpp.ExtensionNumber = p.Phone.extension;
                cpp.Special_Instruction__c = p.specialInstructions;
                cpp.UsageType = usage.PartyPhoneUsage;
                cpp.Usage_Type_Comment__c = usage.otherDescription;
                cppList.add(cpp);
            }
        }
        return cppList;
    }
}