@isTest
private class AmFam_OpportunityTriggerTest {
    
   @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    
    //Method to test extendPCIFYToOpportunity method on AmFam_OpportunityService class
    static testMethod void testPCIFYOnOpportunity(){
        
        Id devRecordTypeId = AmFam_Utility.getAccountAgencyRecordType();        
        Account objAcc = new Account();
        objAcc.Name = 'Test Agency Account';
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.ShippingStreet = '123 Main Street';
        objAcc.ShippingCity = 'San Francisco';
        objAcc.BillingStreet = '123 Main Street';
        objAcc.BillingCity = 'San Francisco';
        
        insert objAcc;
        
        Opportunity opp = new Opportunity(Name = 'Test PCIFY', CloseDate =system.today()+30 , AccountId =objAcc.id, StageName = 'New', Description = 'Test' );
        insert opp;
        
        test.startTest();
        opp.Description = '4917610000000000';
        update opp;
        test.stopTest();
        
        Opportunity oppy = [Select id, Description from Opportunity where Id =: opp.id LIMIT 1];
        System.assert(oppy.Description.contains('*'));
        
    }
}