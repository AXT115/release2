/* Class Name   : AmFam_JitSSOUserHandler
* Project / CR  : AmFam E1P CRM
* Description   : Just-in-time provisioning of single sign-on users
* Created By    : Salesforce Services
* Created On    : 05/14/2020

* Modification Log:
* ------------------------------------------------------------------------------------------------------------------
* Date             Developer           Description
* ------------------------------------------------------------------------------------------------------------------
* 05/14/2020       Nitin Gupta         Just-in-time provisioning of single sign-on users
*/


//This class provides logic for inbound just-in-time provisioning of single sign-on users.
global class AmFam_JitSSOUserHandler implements Auth.SamlJitHandler {
	private class JitException extends Exception{}

	private void handleUser(boolean create, User u, Map<String, String> attributes,
	    String federationIdentifier, boolean isStandard) 
    {        
        if(create && attributes.containsKey('emailAddress')) {
            u.Email = attributes.get('emailAddress');
            //Build username by appending org name to email address
            String orgName;
            List<Org_Name_SSO__mdt> orgNames = [Select DeveloperName FROM Org_Name_SSO__mdt limit 1];
            if(orgNames.size()>0) {
            	orgName = orgNames[0].DeveloperName;
            } else {
               orgName = 'OrgName';
            }
            u.Username = attributes.get('emailAddress') + '.' + orgName;
        }
        if(create) {
            if(attributes.containsKey('UniqueUserIdentifier')) 
            {
                u.FederationIdentifier = attributes.get('UniqueUserIdentifier');
            }
            
            if(attributes.containsKey('afiexternaluserid')) 
            {
                if (u.FederationIdentifier == null)
                {
                    u.FederationIdentifier = attributes.get('afiexternaluserid');
                }
                else 
                {
                    u.AFI_External_User_ID__c = attributes.get('afiexternaluserid');
                }
            } else {
                u.FederationIdentifier = federationIdentifier;
            }
        }
        if(attributes.containsKey('SalesforceProfile')) {

            String profileName = attributes.get('SalesforceProfile');
            List<Profile> profiles = [SELECT Id FROM Profile WHERE Name=:profileName limit 1]; //Get profile id based on Name recieved from AD
            List<UserRole> roles = [SELECT Id, Name FROM UserRole];


            if(profiles.size()>0){

                u.ProfileId = profiles[0].Id;
                //Assign Roles based on profile (If Profile = agent, then role = Agent. If Profile = CSR, then role = CSR.)
                for (UserRole r : roles) {
                    if ((r.Name == 'CSR' && profileName == 'Agent CSR' ) ||
                        (r.Name == 'Agent' && profileName == 'AmFam Agent') ||
                        (r.Name == 'Field Operations' && profileName == 'Field Operations') ||
                        (r.Name == 'Sales & Service Operations Rep' && profileName == 'Sales & Service Operations Rep') ||
                        (r.Name == 'Sales District Leader' && profileName == 'Sales District Leader'))
                    {
                        u.UserRoleId = r.Id;
                    }
                }
            }
        }
        
        if (attributes.containsKey('legalLastName')) {
       		u.LastName = attributes.get('legalLastName');
     	}
        if (attributes.containsKey('legalFirstName')) {
       		u.FirstName = attributes.get('legalFirstName');
        }
        if (attributes.containsKey('samaccountname')) {
            u.AmFam_User_ID__c = attributes.get('samaccountname');
        }

        String uid = UserInfo.getUserId();
        User currentUser =
            [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
        if(attributes.containsKey('LocaleSidKey')) {
            u.LocaleSidKey = attributes.get('LocaleSidKey');
        } else if(create) {
            u.LocaleSidKey = currentUser.LocaleSidKey;
        }
        if(attributes.containsKey('LanguageLocaleKey')) {
            u.LanguageLocaleKey = attributes.get('LanguageLocaleKey');
        } else if(create) {
            u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
        }
        if(attributes.containsKey('Alias')) {
            u.Alias = attributes.get('Alias');
        } else if(create) {
            String alias = '';
            if(u.FirstName == null) {
                alias = u.LastName;
            } else {
                alias = u.FirstName.charAt(0) + u.LastName;
            }
            if(alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            u.Alias = alias;
        }
        if(attributes.containsKey('TimeZoneSidKey')) {
            u.TimeZoneSidKey = attributes.get('TimeZoneSidKey');
        } else if(create) {
            u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
        }
        if(attributes.containsKey('EmailEncodingKey')) {
            u.EmailEncodingKey = attributes.get('EmailEncodingKey');
        } else if(create) {
            u.EmailEncodingKey = currentUser.EmailEncodingKey;
        }

		if(!create) {
            update(u);
        }
	}

	private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
		String federationIdentifier, Map<String, String> attributes, String assertion) {
		if(communityId != null || portalId != null) {
			handleUser(create, u, attributes, federationIdentifier, false);
		} else {
			handleUser(create, u, attributes, federationIdentifier, true);
		}
	}
	// CreateUser method will be called when user is logging in for the first time using SSO
	global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
		String federationIdentifier, Map<String, String> attributes, String assertion) {
		User u = new User();
		handleJit(true, u, samlSsoProviderId, communityId, portalId,
            federationIdentifier, attributes, assertion);
		return u;
	}
	// UpdateUser method will be called when user already exists in Salesforce
	global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
		String federationIdentifier, Map<String, String> attributes, String assertion) {
		User u = [SELECT Id, FirstName, ContactId FROM User WHERE Id=:userId];
		handleJit(false, u, samlSsoProviderId, communityId, portalId,
			federationIdentifier, attributes, assertion);
	}
}