/*
* @author         salesforce services
* @date           05/18/2020
* @description    test class for AmFam_ProducerService class
*/

@IsTest
public class AmFam_ProducerServiceTest {

	public Static String jsonNoProd = '{'+
	'    \"status\": {'+
	'        \"code\": 200,'+
	'        \"reason\": \"SUCCESS\",'+
	'        \"transactionId\": \"00000171bdd4069f-245ac6\"'+
	'    },'+
	'    \"producer\":{'+
	'  \"typeOfAgentCode\":\"CSR - Licensed\",\"jobTitle\":\"Customer Service Representative\",\"storefrontIndicator\":false, \"producerId\":\"10142\",\"fullName\":\"Test CQP512\",\"firstName\":\"Test\",\"middleName\":\"CSR\",\"lastName\":\"CQP512\",\"nickName\":\"\",\"activeStatusIndicator\":true,\"contactDetail\" : '+
	' {\"emails\" : [\"TCQP512@amfam.net\"],'+
	'  \"phones\" : [{\"areaCode\":\"608\",\"typeOfUsageCode\":\"VOICE\",\"phoneNumber\":\"6120047\",\"typeOfPhoneCode\":\"OFFICE\"}] }'+
	'}'+
	'}';

	public Static String json = '{'+
	'    \"status\": {'+
	'        \"code\": 200,'+
	'        \"reason\": \"SUCCESS\",'+
	'        \"transactionId\": \"00000171bdd4069f-16335d\"'+
	'    },'+
	'    \"producer\":{'+
	'  \"typeOfAgentCode\":\"CSR - Licensed\",\"jobTitle\":\"Customer Service Representative\",\"storefrontIndicator\":false, \"producerId\":\"10142\",\"fullName\":\"Test CQP512\",\"firstName\":\"Test\",\"middleName\":\"CSR\",\"lastName\":\"CQP512\",\"nickName\":\"\",\"activeStatusIndicator\":true,\"contactDetail\" : '+
	' {\"emails\" : [\"TCQP512@amfam.net\"],'+
	'  \"phones\" : [{\"areaCode\":\"608\",\"typeOfUsageCode\":\"VOICE\",\"phoneNumber\":\"6120047\",\"typeOfPhoneCode\":\"OFFICE\"}] }, '+
	'        '+
	'        \"contactDetails\":[{ \"README\" : \"THIS STRUCTURE IS DEPRECATED\",'+
	'   \"README2\" : \"USE contactDetail STRUCTURE INSTEAD\",'+
	'   \"email\" : \"TCQP512@amfam.net\",'+
	'  \"effectiveDate\" : \"1900-01-01\",'+
	'  \"endEffectiveDate\" : \"2300-01-01\"'+
	'  }],\"alternateId\":{\"globalNickname\":\"tCQP512\",\"externalAccountId\":\"0008000273\"},\"producerDetail\":{},'+
	'  \"managers\":[{'+
	'    \"agency\": {'+
	'        \"incorporatedIndicator\": false'+
	'},'+
	'    \"manager\":{'+
	'    \"typeOfAgentCode\": \"Agent\",'+
	'    \"jobTitle\": \"Agent\",'+
	'    \"producerId\": \"10047\",'+
	'    \"firstName\": \"Test\",'+
	'    \"middleName\": \"Agent\",'+
	'    \"lastName\": \"AQP506\",'+
	'    \"nickName\": \"\",'+
	'    \"fullName\": \"Test Agent AQP506\",'+
	'    \"storefrontIndicator\": false'+
	'    }'+
	'}],'+
	'    \"staff\" : '+
	'	['+
	'	{ \"Producer\": '+
	'		{\"producerId\" : \"10111\", '+
	'		 \"producerPartyId\" : \"10111\"'+
	'		},'+
	'		\"firstName\" : \"Test\",'+
	'		\"middleName\" : \"Agent\",'+
	'		\"lastName\" : \"AQP570\",'+
	'		\"role\" : \"Agent\", '+
	'		\"effectiveDate\" : \"\",'+
	'		\"endEffectiveDate\" : \"\"'+
	'	 }, '+
	'	 { \"Producer\": {\"producerId\" : \"10112\", '+
	'  \"producerPartyId\" : \"10112\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP571\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10113\", '+
	'  \"producerPartyId\" : \"10113\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP572\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10114\", '+
	'  \"producerPartyId\" : \"10114\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP573\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10115\", '+
	'  \"producerPartyId\" : \"10115\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP574\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10116\", '+
	'  \"producerPartyId\" : \"10116\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP575\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10117\", '+
	'  \"producerPartyId\" : \"10117\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP576\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10118\", '+
	'  \"producerPartyId\" : \"10118\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP577\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10119\", '+
	'  \"producerPartyId\" : \"10119\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP578\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10120\", '+
	'  \"producerPartyId\" : \"10120\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP579\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10332\", '+
	'  \"producerPartyId\" : \"10332\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP597\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }, '+
	'  { \"Producer\": {\"producerId\" : \"10368\", '+
	'  \"producerPartyId\" : \"10368\"},'+
	'  \"firstName\" : \"Test\",'+
	'  \"middleName\" : \"Agent\",'+
	'  \"lastName\" : \"AQP606\",'+
	'  \"role\" : \"Agent\", '+
	'  \"effectiveDate\" : \"\",'+
	'  \"endEffectiveDate\" : \"\"'+
	'  }],'+
	'  \"booksOfBusiness\":[{'+
	'	\"typeOfAgentCode\": \"Agency\",'+
	'     \"primaryProducerId\": \"100001\",'+
	'     \"agency\":{'+
	'	\"name\": \"Test AgencyInc AQP506 Agency, Inc.\",'+
	'     \"districtCode\": \"790\",'+
	'     \"incorporatedIndicator\": true,'+
	'     \"agencyPartyId\": \"10706\"'+
	'}'+
	'}],'+
	'  \"offices\" : ['+
	'        {'+
	'        \"typeOfOfficeCode\" : \"WORK_ADDRESS\",'+
	'        \"address\" : {'+
	'         \"line1\" : \"7210 MADISON AVE STE M\",'+
	'         \"line2\" :\"\",'+
	'          \"city\" :\"INDIANAPOLIS\",'+
	'          \"state\" : \"IN\",'+
	'          \"zip5\": \"46227\",'+
	'          \"zip4\" : \"5227\" },'+
	'          \"phones\" : ['+
	'          {\"typeOfPhoneCode\" : \"OFFICE\",\"typeOfUsageCode\" : \"VOICE\",\"areaCode\" : \"608\", \"phoneNumber\" : \"6120047\"}'+
	'          ]          '+
	'        }'+
	'    ]'+
	'},'+
	''+
	'    \"READ ME\":\"ANY OBJECT STRUCTURE FOLLOWING THIS IS DEPRECATED - DO NOT USE\",'+
	'    \"producerId\": \"10142\",'+
	'    \"producerPartyId\": \"10142\",'+
	'    \"credentials\": \"\",'+
	'    \"name\": {'+
	'        \"firstName\": \"Test\",'+
	'        \"middleName\": \"CSR\",'+
	'        \"lastName\": \"CQP512\",'+
	'        \"nickName\": \"\"'+
	'    },'+
	'    \"effectiveDate\": \"2003-10-07\",'+
	'    \"endEffectiveDate\": \"2300-01-01\",'+
	'    \"contactDetails\": {'+
	'        \"locations\": ['+
	'            { \"locationType\" : \"WORK_ADDRESS\",'+
	'  \"locationUsage\" : \"WORK_ADDRESS\", '+
	'  \"locationId\" : \"\",'+
	'  \"latitude\": \"\",'+
	'  \"longitude\": \"\",'+
	'  \"address\" : { \"addressLine1\" : \"7210 MADISON AVE STE M\", '+
	'                \"addressLine2\" : \"\", '+
	'                \"city\" : \"INDIANAPOLIS\", '+
	'                \"state\" : \"IN\", '+
	'                \"zipFirstFive\" : \"46227\", '+
	'                \"zipLastFour\" : \"5227\"'+
	'              },'+
	'  \"effectiveDate\" : \"2003-10-07\",'+
	'  \"endEffectiveDate\" : \"2300-01-01\"'+
	'  }, { \"locationType\" : \"WORK_ADDRESS\",'+
	'  \"locationUsage\" : \"PRIMARY\", '+
	'  \"locationId\" : \"\",'+
	'  \"latitude\": \"\",'+
	'  \"longitude\": \"\",'+
	'  \"address\" : { \"addressLine1\" : \"7210 MADISON AVE STE M\", '+
	'                \"addressLine2\" : \"\", '+
	'                \"city\" : \"INDIANAPOLIS\", '+
	'                \"state\" : \"IN\", '+
	'                \"zipFirstFive\" : \"46227\", '+
	'                \"zipLastFour\" : \"5227\"'+
	'              },'+
	'  \"effectiveDate\" : \"2003-10-07\",'+
	'  \"endEffectiveDate\" : \"2300-01-01\"'+
	'  }'+
	'        ],'+
	'        \"phones\": ['+
	'            { \"phoneType\" : \"WORK_ADDRESS\",'+
	'  \"phoneUsage\" : \"WORK_ADDRESS\", '+
	'  \"phoneNumber\" : \"6086120047\",'+
	'  \"effectiveDate\" : \"2003-10-07\",'+
	'  \"endEffectiveDate\" : \"2300-01-01\"'+
	'  }, { \"phoneType\" : \"WORK_ADDRESS\",'+
	'  \"phoneUsage\" : \"PRIMARY\", '+
	'  \"phoneNumber\" : \"6086120047\",'+
	'  \"effectiveDate\" : \"2003-10-07\",'+
	'  \"endEffectiveDate\" : \"2300-01-01\"'+
	'  }'+
	'        ],'+
	'        \"emails\": ['+
	'            { \"README\" : \"THIS STRUCTURE IS DEPRECATED\",'+
	'   \"README2\" : \"USE contactDetail STRUCTURE INSTEAD\",'+
	'   \"email\" : \"TCQP512@amfam.net\",'+
	'  \"effectiveDate\" : \"1900-01-01\",'+
	'  \"endEffectiveDate\" : \"2300-01-01\"'+
	'  }'+
	'        ],'+
	'        \"socialMedia\": []'+
	'    },'+
	'    \"staff\": ['+
	'        '+
	'    ],'+
	'    \"managers\": ['+
	'        { \"producerId\" : \"10047\",'+
	'  \"producerPartyId\" : \"10047\",\"role\" : \"Agent\",'+
	'  \"agency\" : \"Test Agent AQP506\", '+
	'  \"effectiveDate\" : \"2002-07-05\",'+
	'  \"endEffectiveDate\" : \"2300-01-01\"'+
	'  }'+
	'    ],'+
	'    \"bookOfBusiness\": ['+
	'        { \"role\" : \"CSR - Licensed\",'+
	'  \"agency\" : \"\", '+
	'  \"effectiveDate\" : \"2002-10-08\",'+
	'  \"endEffectiveDate\" : \"2300-01-01\"'+
	'  }'+
	'    ]'+
	'}';
	// This test method should give 100% coverage
	static testMethod void testParse() {

		AmFam_ProducerService r = AmFam_ProducerService.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Status objStatus = new AmFam_ProducerService.Status(System.JSON.createParser(json));
		System.assert(objStatus != null);
		System.assert(objStatus.code == null);
		System.assert(objStatus.reason == null);
		System.assert(objStatus.transactionId == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Agency objAgency = new AmFam_ProducerService.Agency(System.JSON.createParser(json));
		System.assert(objAgency != null);
		System.assert(objAgency.incorporatedIndicator == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Agency_Z objAgency_Z = new AmFam_ProducerService.Agency_Z(System.JSON.createParser(json));
		System.assert(objAgency_Z != null);
		System.assert(objAgency_Z.name == null);
		System.assert(objAgency_Z.districtCode == null);
		System.assert(objAgency_Z.incorporatedIndicator == null);
		System.assert(objAgency_Z.agencyPartyId == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Address objAddress = new AmFam_ProducerService.Address(System.JSON.createParser(json));
		System.assert(objAddress != null);
		System.assert(objAddress.line1 == null);
		System.assert(objAddress.line2 == null);
		System.assert(objAddress.city == null);
		System.assert(objAddress.state == null);
		System.assert(objAddress.zip5 == null);
		System.assert(objAddress.zip4 == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Producer_Z objProducer_Z = new AmFam_ProducerService.Producer_Z(System.JSON.createParser(json));
		System.assert(objProducer_Z != null);
		System.assert(objProducer_Z.typeOfAgentCode == null);
		System.assert(objProducer_Z.jobTitle == null);
		System.assert(objProducer_Z.storefrontIndicator == null);
		System.assert(objProducer_Z.producerId == null);
		System.assert(objProducer_Z.fullName == null);
		System.assert(objProducer_Z.firstName == null);
		System.assert(objProducer_Z.middleName == null);
		System.assert(objProducer_Z.lastName == null);
		System.assert(objProducer_Z.nickName == null);
		System.assert(objProducer_Z.activeStatusIndicator == null);
		System.assert(objProducer_Z.contactDetail == null);
		System.assert(objProducer_Z.contactDetails == null);
		System.assert(objProducer_Z.alternateId == null);
		System.assert(objProducer_Z.producerDetail == null);
		System.assert(objProducer_Z.managers == null);
		System.assert(objProducer_Z.staff == null);
		System.assert(objProducer_Z.booksOfBusiness == null);
		System.assert(objProducer_Z.offices == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.ContactDetails_Z objContactDetails_Z = new AmFam_ProducerService.ContactDetails_Z(System.JSON.createParser(json));
		System.assert(objContactDetails_Z != null);
		System.assert(objContactDetails_Z.locations == null);
		System.assert(objContactDetails_Z.phones == null);
		System.assert(objContactDetails_Z.emails == null);
		System.assert(objContactDetails_Z.socialMedia == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Staff objStaff = new AmFam_ProducerService.Staff(System.JSON.createParser(json));
		System.assert(objStaff != null);
		System.assert(objStaff.Producer == null);
		System.assert(objStaff.firstName == null);
		System.assert(objStaff.middleName == null);
		System.assert(objStaff.lastName == null);
		System.assert(objStaff.role == null);
		System.assert(objStaff.effectiveDate == null);
		System.assert(objStaff.endEffectiveDate == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Locations objLocations = new AmFam_ProducerService.Locations(System.JSON.createParser(json));
		System.assert(objLocations != null);
		System.assert(objLocations.locationType == null);
		System.assert(objLocations.locationUsage == null);
		System.assert(objLocations.locationId == null);
		System.assert(objLocations.latitude == null);
		System.assert(objLocations.longitude == null);
		System.assert(objLocations.address == null);
		System.assert(objLocations.effectiveDate == null);
		System.assert(objLocations.endEffectiveDate == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.BookOfBusiness objBookOfBusiness = new AmFam_ProducerService.BookOfBusiness(System.JSON.createParser(json));
		System.assert(objBookOfBusiness != null);
		System.assert(objBookOfBusiness.role == null);
		System.assert(objBookOfBusiness.agency == null);
		System.assert(objBookOfBusiness.effectiveDate == null);
		System.assert(objBookOfBusiness.endEffectiveDate == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Managers_Z objManagers_Z = new AmFam_ProducerService.Managers_Z(System.JSON.createParser(json));
		System.assert(objManagers_Z != null);
		System.assert(objManagers_Z.producerId == null);
		System.assert(objManagers_Z.producerPartyId == null);
		System.assert(objManagers_Z.role == null);
		System.assert(objManagers_Z.agency == null);
		System.assert(objManagers_Z.effectiveDate == null);
		System.assert(objManagers_Z.endEffectiveDate == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Manager objManager = new AmFam_ProducerService.Manager(System.JSON.createParser(json));
		System.assert(objManager != null);
		System.assert(objManager.typeOfAgentCode == null);
		System.assert(objManager.jobTitle == null);
		System.assert(objManager.producerId == null);
		System.assert(objManager.firstName == null);
		System.assert(objManager.middleName == null);
		System.assert(objManager.lastName == null);
		System.assert(objManager.nickName == null);
		System.assert(objManager.fullName == null);
		System.assert(objManager.storefrontIndicator == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Phones_Z objPhones_Z = new AmFam_ProducerService.Phones_Z(System.JSON.createParser(json));
		System.assert(objPhones_Z != null);
		System.assert(objPhones_Z.phoneType == null);
		System.assert(objPhones_Z.phoneUsage == null);
		System.assert(objPhones_Z.phoneNumber == null);
		System.assert(objPhones_Z.effectiveDate == null);
		System.assert(objPhones_Z.endEffectiveDate == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Name objName = new AmFam_ProducerService.Name(System.JSON.createParser(json));
		System.assert(objName != null);
		System.assert(objName.firstName == null);
		System.assert(objName.middleName == null);
		System.assert(objName.lastName == null);
		System.assert(objName.nickName == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.ContactDetail objContactDetail = new AmFam_ProducerService.ContactDetail(System.JSON.createParser(json));
		System.assert(objContactDetail != null);
		System.assert(objContactDetail.emails == null);
		System.assert(objContactDetail.phones == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService objAmFam_ProducerService = new AmFam_ProducerService(System.JSON.createParser(json));
		System.assert(objAmFam_ProducerService != null);
		System.assert(objAmFam_ProducerService.status == null);
		System.assert(objAmFam_ProducerService.producer == null);
		System.assert(objAmFam_ProducerService.README == null);
		System.assert(objAmFam_ProducerService.producerId == null);
		System.assert(objAmFam_ProducerService.producerPartyId == null);
		System.assert(objAmFam_ProducerService.credentials == null);
		System.assert(objAmFam_ProducerService.name == null);
		System.assert(objAmFam_ProducerService.effectiveDate == null);
		System.assert(objAmFam_ProducerService.endEffectiveDate == null);
		System.assert(objAmFam_ProducerService.contactDetails == null);
		System.assert(objAmFam_ProducerService.staff == null);
		System.assert(objAmFam_ProducerService.managers == null);
		System.assert(objAmFam_ProducerService.bookOfBusiness == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Phones objPhones = new AmFam_ProducerService.Phones(System.JSON.createParser(json));
		System.assert(objPhones != null);
		System.assert(objPhones.areaCode == null);
		System.assert(objPhones.typeOfUsageCode == null);
		System.assert(objPhones.phoneNumber == null);
		System.assert(objPhones.typeOfPhoneCode == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.ContactDetails objContactDetails = new AmFam_ProducerService.ContactDetails(System.JSON.createParser(json));
		System.assert(objContactDetails != null);
		System.assert(objContactDetails.README == null);
		System.assert(objContactDetails.README2 == null);
		System.assert(objContactDetails.email == null);
		System.assert(objContactDetails.effectiveDate == null);
		System.assert(objContactDetails.endEffectiveDate == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Producer objProducer = new AmFam_ProducerService.Producer(System.JSON.createParser(json));
		System.assert(objProducer != null);
		System.assert(objProducer.producerId == null);
		System.assert(objProducer.producerPartyId == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.BooksOfBusiness objBooksOfBusiness = new AmFam_ProducerService.BooksOfBusiness(System.JSON.createParser(json));
		System.assert(objBooksOfBusiness != null);
		System.assert(objBooksOfBusiness.typeOfAgentCode == null);
		System.assert(objBooksOfBusiness.primaryProducerId == null);
		System.assert(objBooksOfBusiness.agency == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Managers objManagers = new AmFam_ProducerService.Managers(System.JSON.createParser(json));
		System.assert(objManagers != null);
		System.assert(objManagers.agency == null);
		System.assert(objManagers.manager == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.AlternateId objAlternateId = new AmFam_ProducerService.AlternateId(System.JSON.createParser(json));
		System.assert(objAlternateId != null);
		System.assert(objAlternateId.globalNickname == null);
		System.assert(objAlternateId.externalAccountId == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.ProducerDetail objProducerDetail = new AmFam_ProducerService.ProducerDetail(System.JSON.createParser(json));
		System.assert(objProducerDetail != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Address_Z objAddress_Z = new AmFam_ProducerService.Address_Z(System.JSON.createParser(json));
		System.assert(objAddress_Z != null);
		System.assert(objAddress_Z.addressLine1 == null);
		System.assert(objAddress_Z.addressLine2 == null);
		System.assert(objAddress_Z.city == null);
		System.assert(objAddress_Z.state == null);
		System.assert(objAddress_Z.zipFirstFive == null);
		System.assert(objAddress_Z.zipLastFour == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		AmFam_ProducerService.Offices objOffices = new AmFam_ProducerService.Offices(System.JSON.createParser(json));
		System.assert(objOffices != null);
		System.assert(objOffices.typeOfOfficeCode == null);
		System.assert(objOffices.address == null);
		System.assert(objOffices.phones == null);
	}
}