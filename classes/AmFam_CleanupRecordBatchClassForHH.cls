global class AmFam_CleanupRecordBatchClassForHH implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
     
    global Database.QueryLocator start(Database.BatchableContext bc){
        string type = 'Household';
        string soqlQuery = 'SELECT Name, Identifier__c, Exception__c, Processed__c, Level_Of_Details__c, Type__c From CleanupRecord__c where Processed__c = false AND Type__c =: type';//AND LastModifiedDate = LAST_N_DAYS:1
        return Database.getQueryLocator(soqlQuery); 
    }
     
    global void execute(Database.BatchableContext bc, List<CleanupRecord__c> scope){

        //collection of CDHIDs
        List<string> cdhIdList = new List<string>();
        //Map of existing account and its CDHID
        Map<string, Account> cdhIdAccountMap = new  Map<string, Account>();
        //collection of existing HouseHold Ids
        List<Id> houseHoldAccountIdList = new List<Id>();
        //collection of account records to be inserted/updated
        set<Account> setOfAccountsForUpsert = new set<Account>();
        //CDH response map
        Map<String, wsHouseholdService.RetrieveResponseType> cdhIdRetrieveResponseMapHH = new Map<String, wsHouseholdService.RetrieveResponseType>();
        //list of clean up records for updated
        List<CleanupRecord__c> listOfCleanUpRecForUpdate = new List<CleanupRecord__c>();
        //to avoid duplicate records 
        List<CleanupRecord__c> records = new List<CleanupRecord__c>();
        //HouseHold RecordType Id
        Id householdRT = AmFam_Utility.getAccountHouseholdRecordType();
        
        
        for(CleanupRecord__c crr : scope){
            if(crr?.Identifier__c != null && !cdhIdList.contains(crr?.Identifier__c)){
                cdhIdList.add(crr?.Identifier__c); 
                records.add(crr);
            }
        }
        
        
        for(Account acc : [select id, CDHID__c, RecordTypeId, PersonContactId from Account where CDHID__c =:cdhIdList]){
        	if(acc?.CDHID__c != null && acc.RecordTypeId == householdRT){
            	cdhIdAccountMap.put(acc.CDHID__c, acc);
            	houseHoldAccountIdList.add(acc.id);    
            }
        }
        
        //collection of CDH IDs of both processing HouseHold and its HouseHold members
        List<string> relatedPartyCDHIdList = cdhIdList;
        
        //retrieve response for each clean up record and process account details to be updated
        for (CleanupRecord__c cr : records){
            try{
                Account accountRec = cdhIdAccountMap.get(cr?.Identifier__c); 
                if(string.isNotBlank(cr?.Identifier__c)){
                    //retrieve 
                   if(cr.Type__c == 'Household'){
                            wsHouseholdService.RetrieveResponseType householdSearchResponse = AmFam_CleanUpRecordBatchHelperForHH.retrieveHouseHold(cr?.Identifier__c);
                            Map<Account, List<string>> returnMapHH = AmFam_CleanUpRecordBatchHelperForHH.processHouseHoldAccountDetails(accountRec, householdSearchResponse);
                            setOfAccountsForUpsert.addAll(returnMapHH.keySet());
                            for(List<string> st : returnMapHH.values()){
                                if(st != null){relatedPartyCDHIdList.addAll(st);}    
                            }
                            cr.Processed__c= true;                             
                            cdhIdRetrieveResponseMapHH.put(cr?.Identifier__c, householdSearchResponse);       
                        }
                    }
                    listOfCleanUpRecForUpdate.add(cr);
               }
            catch(Exception e){
                System.debug('Error-' + e.getMessage());   
            }
        }
        List<Id> accountIdSuccess = new List<Id>();
        List<Account> listOfAccountsForUpsert = new List<Account>();
        listOfAccountsForUpsert.addAll(setOfAccountsForUpsert);
        if(listOfAccountsForUpsert.size()>0){
        	Database.UpsertResult[] results = Database.upsert(listOfAccountsForUpsert, Account.Id);
            for(Integer i=0;i<results.size();i++){
                if (results.get(i).isSuccess()){
                    accountIdSuccess.add(results.get(i).getId());
                }else if (!results.get(i).isSuccess()){
                    Database.Error error = results.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    listOfAccountsForUpsert.get(i);
                    system.debug('Failed ID'+listOfAccountsForUpsert.get(i).Id);
                 }
            }
        }
        
        Map<string, Account> cdhIdAllAccountMap = new  Map<string, Account>();
        List<Id> accountIdList = new List<Id>();
        Map<Id, String> personContactIdAccCDHIDMap = new Map<Id, String>();
        Id personAccountRTId = AmFam_Utility.getPersonAccountRecordType();
        for(Account acc : [select id, CDHID__c, RecordTypeId, RecordType.Name, PersonContactId from Account where CDHID__c =:relatedPartyCDHIdList]){
            if(acc.RecordTypeId == householdRT){
                cdhIdAllAccountMap.put(acc.CDHID__c, acc);
                accountIdList.add(acc.id);
                if(acc.RecordTypeId == personAccountRTId){personContactIdAccCDHIDMap.put(acc.PersonContactId, acc.CDHID__c);}    
            }
         }
        
        //collection of all existing Account Contact Relation records related to given CDHIDs
        Map<Id, List<AccountContactRelation>> hhIDAccountContactRelMap = new  Map<Id, List<AccountContactRelation>>(); 
        for(AccountContactRelation accConRel : [select id, AccountId, ContactId  from AccountContactRelation where AccountId =: houseHoldAccountIdList]){
            if(hhIDAccountContactRelMap.containsKey(accConRel.AccountId)){
            	hhIDAccountContactRelMap.get(accConRel.AccountId).add(accConRel);
            }else{
                hhIDAccountContactRelMap.put(accConRel.AccountId, new List<AccountContactRelation>{accConRel});    
            }	    
        }
       
      List<AccountContactRelation> listoFAccountContactRelToInsert = new List<AccountContactRelation>();
      List<AccountContactRelation> listoFAccountContactRelToDel = new List<AccountContactRelation>();
        
      for(CleanupRecord__c crR : records ){
            string cdhId = crR?.Identifier__c;
            Account rec = cdhIdAllAccountMap.get(cdhId);
            if(rec != null && string.isNotBlank(cdhId)){
                wsHouseholdService.RetrieveResponseType resp = cdhIdRetrieveResponseMapHH.get(cdhId);
            	Map<String, List<AccountContactRelation>> returnedRelationshipMap = AmFam_CleanUpRecordBatchHelperForHH.createRelationshipRecords(resp, rec, hhIDAccountContactRelMap, cdhIdAllAccountMap, personContactIdAccCDHIDMap); 
                if(returnedRelationshipMap != null){
                    if(returnedRelationshipMap.get('insert') != null && returnedRelationshipMap.get('insert').size()>0) listoFAccountContactRelToInsert.addAll(returnedRelationshipMap.get('insert'));  
                    if(returnedRelationshipMap.get('delete') != null && returnedRelationshipMap.get('delete').size()>0) listoFAccountContactRelToDel.addAll(returnedRelationshipMap.get('delete'));   
                }
            }
         }
        
      try{
          Database.SaveResult[] saveInsertAccountContactRel = Database.insert(listoFAccountContactRelToInsert, false);
          if(listoFAccountContactRelToDel.size()>0)  delete listoFAccountContactRelToDel;
          update listOfCleanUpRecForUpdate;
        }catch(DmlException e){
            
        } 
           
        
    }
     
    global void finish(Database.BatchableContext bc){
         
    }
}