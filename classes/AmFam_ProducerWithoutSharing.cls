/**********************************************************
     * Purpose     : E1PCRMPT-1740 Agency field on Person Account is prepopulated if CSR has only linked to one agency account
     * Created by  : Anbu Chinnathambi 6/2/2021
     * Modified by :
     * ********************************************************/
public without sharing class AmFam_ProducerWithoutSharing {
    
    @AuraEnabled
    public static String populateAgencyForPersonAccount(String userId){
        List<Producer> producerList = [Select Id,InternalUserId,AccountId from Producer where InternalUserId =: userId ];
        String accountId= producerList.size()==1?producerList[0].AccountId:null;
        system.debug('accountId' + accountId);
        return accountId;
    }
}