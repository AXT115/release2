//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)

@isTest
global class ApexActServiceHttpMock implements HttpCalloutMock {
	global HTTPResponse respond(HTTPRequest req){
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		DOM.Document responseDoc = new DOM.Document();
		DOM.XmlNode envelope = responseDoc.createRootElement('Envelope', 'http://schemas.xmlsoap.org/soap/envelope/', 's');
		DOM.XmlNode bodyNode = envelope.addChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/', null);
        
		DOM.Document reqDoc = req.getBodyDocument();
		DOM.XmlNode rootNode = reqDoc.getRootElement();
		DOM.XmlNode reqBodyNode = rootNode.getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/');
		DOM.XmlNode[] methodNode = reqBodyNode.getChildElements();
		if(methodNode[0].getName() == 'createAct'){
			DOM.XmlNode ResponseNode = bodyNode.addChildElement('createActResponse', 'http://schema.amfam.com/xsd/message/apexact', null);
		}
		else if(methodNode[0].getName() == 'retrieveCategories'){
			DOM.XmlNode ResponseNode = bodyNode.addChildElement('retrieveCategoriesResponse', 'http://schema.amfam.com/xsd/message/apexact', null);
		}
		res.setBody(responseDoc.toXmlString());
		res.setStatusCode(200);
		return res;
	}
}