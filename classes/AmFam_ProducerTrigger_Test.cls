@istest
public with sharing class AmFam_ProducerTrigger_Test {


    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }



    
    static testMethod public void producerInsert_test() {

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account personAccount = insertPersonAccount('Pepe', 'Trueno', agency.Id);
            Account householdAcc = insertHouseholdAccount('Pepe', 'Trueno');

            Test.startTest();

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            ProducerAccess__c pa = insertProducerAccess(personAccount, 'pro12345');
            Producer producer2 = insertProducer(agency, agent2, 'pro12345');

            List<AccountShare> personShares = [SELECT Id FROM AccountShare WHERE AccountId =: personAccount.Id AND RowCause = 'Manual'];
            
            //At this point, PersonAccount should have 2 manual shares created (one foe earch agent)
            System.assertEquals(2, personShares.size());

            List<AccountShare> householdShares1 = [SELECT Id FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];

            //At this point, Household should have 0 manual shares created 
            System.assertEquals(0, householdShares1.size());

            AccountContactRelation acr1 = insertAccountContactRelation(householdAcc.Id, personAccount.PersonContactId);

            List<AccountShare> householdShares2 = [SELECT Id FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];

            //At this point, Household should have 2 manual shares created (one foe earch agent)
            System.assertEquals(2, householdShares2.size());
            
            Test.stopTest();
        }
    }


    static testMethod public void producerDelete_test() {

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account personAccount = insertPersonAccount('Pepe', 'Trueno', agency.Id);
            Account householdAcc = insertHouseholdAccount('Pepe', 'Trueno');

            Test.startTest();

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            ProducerAccess__c pa = insertProducerAccess(personAccount, 'pro12345');
            Producer producer2 = insertProducer(agency, agent2, 'pro12345');

            List<AccountShare> personShares = [SELECT Id FROM AccountShare WHERE AccountId =: personAccount.Id AND RowCause = 'Manual'];

            //At this point, PersonAccount should have 2 manual shares created (one foe earch agent)
            System.assertEquals(2, personShares.size());

            List<AccountShare> householdShares1 = [SELECT Id FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];

            //At this point, Household should have 0 manual shares created 
            System.assertEquals(0, householdShares1.size());

            AccountContactRelation acr1 = insertAccountContactRelation(householdAcc.Id, personAccount.PersonContactId);

            List<AccountShare> householdShares2 = [SELECT Id FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];

            //At this point, Household should have 2 manual shares created (one foe earch agent)
            System.assertEquals(2, householdShares2.size());



            delete producer1;
            delete producer2;

            List<AccountShare> householdShares3 = [SELECT Id FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];

            //At this point, Household should have 0 manual shares. All should be deleted 
            System.assertEquals(0, householdShares1.size());


            Test.stopTest();    
        
        }
    }


///////
    static testMethod public void producerDeleteWithOpportunity_test() {

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.debug('### agent 1: ' + agent1.Id);
        System.debug('### agent 2: ' + agent2.Id);


        List<AmFam_Ids__mdt> amfamIds = [SELECT AFMIC_Corporate_UserId__c FROM AmFam_Ids__mdt WHERE DeveloperName = 'AFMIC_Corporate' LIMIT 1];
        System.debug('### OppName - CoporateUser: ' + amfamIds);

        wsPartyManage.lpeCallout = false;

        Account agency ;
        Account personAccount;
        Account householdAcc ;

        Producer producer1;
        Producer producer2;
        ProducerAccess__c pa;

        Opportunity opp;

        System.runAs(agent1) {


            agency = insertAgencyAccount('Acme');
            personAccount = insertPersonAccount('Pepe', 'Trueno', agency.Id);
            householdAcc = insertHouseholdAccount('Pepe', 'Trueno');

            producer1 = insertProducer(agency, agent1, 'pro12345');
            producer2 = insertProducer(agency, agent2, 'pro12345');
            pa = insertProducerAccess(personAccount, 'pro12345');

        }

        System.runAs(agent2) {
            Test.startTest();


            opp = new Opportunity();
            opp.AccountId = personAccount.Id;
            opp.Name = 'Test Opp';
            opp.Lines_of_Business__c = 'Property';
            opp.StageName = 'New';
            opp.CloseDate = Date.today().addDays(30);
            opp.OwnerId = agent2.Id;

            insert opp;

        }

        System.runAs(admin) {

            List<AccountShare> personShares = [SELECT Id, RowCause, UserOrGroupId FROM AccountShare WHERE AccountId =: personAccount.Id ];

            List<OpportunityShare> oppShares = [SELECT Opportunity.Name, Id, OpportunityId, RowCause, UserOrGroupId,UserOrGroup.Name, OpportunityAccessLevel, Opportunity.Owner.Name FROM OpportunityShare ];

            delete pa;

            Test.stopTest();    

            Opportunity oppAfter = [SELECT Id, Original_Account_ID__c, Original_Account_Name__c, OwnerId, Owner.Name, Account.Owner.Name , Account.Name FROM Opportunity WHERE Id =: opp.Id LIMIT 1];

            //Opportunity's AccountId should be set to the corporate account and Orignal_Account info should be set to the original account
            System.assertNotEquals(opp.AccountId, oppAfter.AccountId);
            System.assertEquals(opp.AccountId, oppAfter.Original_Account_ID__c);



        }
    }


    static testMethod public void producerAccessUpdate_test() {

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account personAccount = insertPersonAccount('Pepe1', 'Trueno1', agency.Id);
            Account householdAcc = insertHouseholdAccount('Pepe', 'Trueno');


            Test.startTest();

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            ProducerAccess__c pa1 = insertProducerAccess(personAccount, 'pro12345');

            AccountContactRelation acr1 = insertAccountContactRelation(householdAcc.Id, personAccount.PersonContactId);

            AccountShare householdShares1 = [SELECT Id, AccountAccessLevel FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual' LIMIT 1];

            //Access level should be "Edit" 
            System.assertEquals('Edit', householdShares1.AccountAccessLevel);


            //Process the update that should trigger the access level to change from Edit to Read
            pa1.Reason__c = 'LEAD';
            update pa1;

            AccountShare householdShares2 = [SELECT Id, AccountAccessLevel FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual' LIMIT 1];

            //Access level should be changed to "Read"
            System.assertEquals('Read', householdShares2.AccountAccessLevel);


            Test.stopTest();
        }
    }    



    static testMethod public void producerAccessDelete_test() {

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account personAccount = insertPersonAccount('Pepe1', 'Trueno1', agency.Id);
            Account householdAcc = insertHouseholdAccount('Pepe', 'Trueno');


            Test.startTest();

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            ProducerAccess__c pa1 = insertProducerAccess(personAccount, 'pro12345');

            AccountContactRelation acr1 = insertAccountContactRelation(householdAcc.Id, personAccount.PersonContactId);

            List<AccountShare> householdShares1 = [SELECT Id, AccountAccessLevel FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];

            //One Household AccountShare record should be created
            System.assertEquals(1, householdShares1.size());

            delete pa1;


            Test.stopTest();

            List<AccountShare> householdShares2 = [SELECT Id, AccountAccessLevel FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];

            //Household AccountShare record should be deleted
            System.assertEquals(0, householdShares2.size());


        }        

    }


    static testMethod public void producerAccessDeleteV2_test() {

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency1 = insertAgencyAccount('Acme1', '123451');
            Account agency2 = insertAgencyAccount('Acme2', '123452');
            Account personAccount1 = insertPersonAccount('Pepe1', 'Trueno1', agency1.Id);
            Account personAccount2 = insertPersonAccount('Pepe2', 'Trueno2', agency2.Id);
            Account householdAcc = insertHouseholdAccount('Pepe', 'Trueno');

            personAccount1.OwnerId = agent1.Id;
            personAccount2.OwnerId = agent2.Id;
            householdAcc.OwnerId = agent1.Id;
                
            List<Account> accToUpdate = new List<Account>();
            accToUpdate.add(personAccount1);
            accToUpdate.add(personAccount2);
            accToUpdate.add(householdAcc);
            
            update accToUpdate;


            Test.startTest();

            AccountContactRelation acr1 = insertAccountContactRelation(householdAcc.Id, personAccount1.PersonContactId);
            AccountContactRelation acr2 = insertAccountContactRelation(householdAcc.Id, personAccount2.PersonContactId);


            Producer producer1 = insertProducer(agency1, agent1, 'pro123451');
            Producer producer2 = insertProducer(agency2, agent2, 'pro123452');
            ProducerAccess__c pa1 = insertProducerAccess(personAccount1, 'pro123451');
            ProducerAccess__c pa2 = insertProducerAccess(personAccount2, 'pro123452');
            ProducerAccess__c pa21 = insertProducerAccess(householdAcc, 'pro123452');


            List<ProducerAccess__c> paList1 = [SELECT Id, RelatedId__c, RelatedId__r.Name, Producer_Identifier__c FROM ProducerAccess__c];

            
            List<AccountShare> householdShares1 = [SELECT Id, AccountId, Account.Name, AccountAccessLevel, UserOrGroupId, UserOrGroup.Name FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];
            
            //There should be only 1 manual share on the Household
            System.assertEquals(1, householdShares1.size());


            delete pa1;

            List<AccountShare> householdShares2 = [SELECT Id, AccountId, Account.Name, AccountAccessLevel, UserOrGroupId, UserOrGroup.Name FROM AccountShare WHERE AccountId =: householdAcc.Id AND RowCause = 'Manual'];

            //There should be only 1 manual share on the Household
            System.assertEquals(1, householdShares2.size());



            Test.stopTest();
        }        

    }    


    
    private static Producer insertProducer(Account acc, User internalUser, String identifier) {

        Producer producer = new Producer();
        producer.AccountId = acc.Id;
        producer.InternalUserId = internalUser.Id;
        producer.Name = identifier;

        insert producer;

        return producer;

    }

    private static ProducerAccess__c insertProducerAccess(Account acc, String identifier) {
        ProducerAccess__c pa = new ProducerAccess__c();
        pa.Producer_Identifier__c = identifier;
        pa.RelatedId__c = acc.Id;
        pa.Reason__c = 'ENTRY';

        insert pa;

        return pa;
    }


    private static User insertUser(String profileName, String aliasName, String fisrtName, String lastName) {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name =: profileName];
        User agent = new User (     alias = aliasName, 
                                    email= aliasName + '12456@xyz.com',
                                    emailencodingkey='UTF-8', 
                                    FederationIdentifier= aliasName + '12456',
                                    firstname=fisrtName, 
                                    lastname=fisrtName,
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', 
                                    profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username= aliasName + 'test12456@xyz.com.test',
                                    AmFam_User_ID__c= aliasName + '1234'
                                );
        
        insert agent;

        return agent;

    }

    private static Account insertAgencyAccount(String accName, String accIdentifier){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_ID__c = accIdentifier;

        insert objAcc;
        return objAcc;
    }


    private static Account insertAgencyAccount(String accName){

        return insertAgencyAccount(accName, '12345');

    }

    private static Account insertPersonAccount(String firstName, String lastName, String agencyId){

        Id devRecordTypeId = AmFam_Utility.getPersonAccountRecordType();       
        Account objAcc = new Account();
        objAcc.Agency__c = agencyId;
        objAcc.FirstName = firstName;
        objAcc.LastName = lastName;
        objAcc.FinServ__IndividualType__c = 'Group';
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.CDHID__c = lastName + '98765';
        insert objAcc;
        objAcc = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE Id =: objAcc.id];
        return objAcc;

    }


    private static Account insertHouseholdAccount(String firstName, String lastName){

        Id devRecordTypeId = AmFam_Utility.getAccountHouseholdRecordType();
        Account objAcc = new Account();
        objAcc.Name = firstName + ' ' + lastName + 'HH';
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.FinServ__IndividualType__c = 'Group';
        insert objAcc;
        objAcc = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE Id =: objAcc.id];
        return objAcc;

    }
       
       
    private static AccountContactRelation insertAccountContactRelation(String accountId, String contactId) {
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = accountId;
        acr.ContactId = contactId;
        insert acr;
        return acr;
    }



}