public class AmFam_ManageGroupMembershipBatch implements database.batchable<sobject>, Database.AllowsCallouts, Database.Stateful{
    AmFam_AccountLeadSharingModel.ProducerWrapper g_ProducerWrapper;

    public AmFam_ManageGroupMembershipBatch(AmFam_AccountLeadSharingModel.ProducerWrapper producerWrapper) {
        this.g_ProducerWrapper = producerWrapper;
    }
   
    public List<GroupMember> start(database.batchableContext bc){
        List<GroupMember> listOfGroupMembers = new List<GroupMember>();
        System.debug('AmFam_ManageGroupMembershipBatch.start');
        System.debug(this.g_ProducerWrapper.toString());

        listOfGroupMembers = AmFam_ManageGroupMembershipModel.getGroupMemberUpdates(g_ProducerWrapper);

        return listOfGroupMembers;
    }
    
    public void execute(database.batchablecontext bd, List<GroupMember> scope) {
        System.debug('AmFam_ManageGroupMembershipBatch.execute');

        if(scope !=null && scope.size() > 0) {
            Database.SaveResult[] srList = Database.insert(scope, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted Group Member. GroupMember ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.' );                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
        }

    }
    
    public void finish(database.batchableContext bc) {
        System.debug('AmFam_ManageGroupMembershipBatch.finish');
    }
}