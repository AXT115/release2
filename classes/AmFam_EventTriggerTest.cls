@isTest
public class AmFam_EventTriggerTest {
    
     @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    
    //Method to test extendPCIFYToEvent method on AmFam_EventService class
    static testMethod void testPCIFYOnEvent() {
        
        Account a = new Account(Name = 'Test Agency', Producer_ID__c = '144745');
        insert a;
        
        Event e = new Event();
        e.WhatId=a.id;
        e.Description = 'Test';
        e.StartDateTime=system.today();
        e.EndDateTime=system.today()+5;
        insert e;
        
        test.startTest();
        e.Description = '4917610000000000';
        update e;
        test.stopTest();
        
        Event eve = [Select id, whatid, Description from Event where whatId =: a.id LIMIT 1];
        System.assert(eve.Description.contains('*'));
    }

}