/**
*   @description: the apex class is test class for AmFam_ContactContactTrigger trigger and related classes
*	@author: Anbu Chinnathambi
*   @date: 08/10/2021
*   @group: Apex Class
*/
@isTest
private class AmFam_ContactContactRelationTriggerTest {
    
   @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    
    //Method to test deleteDuplicateContactContactRel method on AmFam_ContactContactRelationshipService class
    static testMethod void testdeleteDuplicateCCR(){
        
        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('TestOne','PersonOne','Individual');
        Account person2 = AmFam_TestDataFactory.insertPersonAccount('TestTwo','PersonTwo','Individual');
        
        Id rrContactRTId = Schema.SObjectType.FinServ__ReciprocalRole__c.getRecordTypeInfosByDeveloperName().get('ContactRole').getRecordTypeId();
        FinServ__ReciprocalRole__c rrCon = new FinServ__ReciprocalRole__c();
        rrCon.Name = 'Test ';  
        rrCon.FinServ__InverseRole__c = 'Inverse Role';
        rrCon.RecordTypeId = rrContactRTId;
        insert rrCon;
        
        FinServ__ContactContactRelation__c conConRelation = new FinServ__ContactContactRelation__c();
        conConRelation.FinServ__Contact__c = person1.PersonContactId;
        conConRelation.FinServ__RelatedContact__c = person2.PersonContactId;
        conConRelation.FinServ__Role__c = rrCon.id;
        insert conConRelation;
        
        
        Test.startTest();
        AmFam_checkRecursive.firstCallAmFam_ContactContactRelationshipService = false;
        FinServ__ContactContactRelation__c conConRelation2 = new FinServ__ContactContactRelation__c();
        conConRelation2.FinServ__Contact__c = person2.PersonContactId;
        conConRelation2.FinServ__RelatedContact__c = person1.PersonContactId;
        conConRelation2.FinServ__Role__c = rrCon.id;
        insert conConRelation2;
        Test.stopTest();
        
        
        List<FinServ__ContactContactRelation__c> ccrList = [select id, FinServ__Role__c from FinServ__ContactContactRelation__c Limit 100];
        //total Contact-Contact Relationship would be 4(2 sets), where 2 duplicates should have been deleted then number should be 2.
        system.debug('ccrList' + ccrList);
        System.assert(ccrList.size()==2);
     }
}