public without sharing class AmFam_AddressTriggerService {
    
    public static void updateAccountAddress(List<Schema.Address> newRecords,
                                    List<Schema.Address> oldRecords,
                                    Map<ID, Schema.Address> newRecordsMap,
                                    Map<ID, Schema.Address> oldRecordsMap) {
    	
        Set<Id> newRecordIdSet = newRecordsMap?.keyset();
        if(newRecords.size()>0 && newRecordIdSet != null){
            //invoking future method
            if(!system.isbatch() && !system.isfuture()){
            	updateAccountAddressAsync(newRecordIdSet);    
            }else{
                updateAccountAddressSync(newRecords);
            }                                
            
        }
     
    }
    
    @future
    public static void updateAccountAddressAsync(Set<Id> newRecordIds){
    	List<Schema.Address> newRecords = [select id, ParentId, LocationType, AddressType, Address ,Street, City, State, Country, PostalCode from Address where Id IN:newRecordIds ]; 
        updateAccountAddressSync(newRecords);
    }
    
    public static void updateAccountAddressSync(List<Schema.Address> newRecords){
        
        //****************************** */
        //Address Mapping
        // Mailing 	    = Account.Shipping
        // Residence 	= Account.Billing
        String addressType1 = 'M';
        String addressType2 = 'R';
        String addressType3 = 'B';

        String primaryPurpose = 'PRM';

        //To switch the mapping, it's only required to inverse the addressType1 and addressType2 values
        //****************************** */
        Set<Id> locationSet = new Set<Id>();

        //Find address records to update
        for (Schema.Address address : newRecords) {
            locationSet.add(address.ParentId);
        }
        
        //Retrieve the Associated Locations related to the list of updated addresses and build a Map indexed by Location Id
        List<AssociatedLocation> alList = [SELECT Id, LocationId, ParentRecordId, Purpose__c,Type FROM AssociatedLocation WHERE LocationId IN : locationSet AND Purpose__c = 'PRM'];

        System.debug('### alList: ' + alList);

        Map<Id, List<AssociatedLocation>> associatedLocationsByLocationId = new Map<Id, List<AssociatedLocation>>();

        Set<Id> accountIds = new Set<Id>();
        for (AssociatedLocation al : alList) {
            accountIds.add(al.ParentRecordId);

            List<AssociatedLocation> associatedLocations = associatedLocationsByLocationId.get(al.LocationId);
            if (associatedLocations == null) {
                associatedLocations = new List<AssociatedLocation>();
            }

            associatedLocations.add(al);
            associatedLocationsByLocationId.put(al.LocationId, associatedLocations);
        }

        //Retrieve the list of Accounts related to the Addresses
        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode,
                                                                    BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, IsPersonAccount 
                                                            FROM Account 
                                                            WHERE Id IN: accountIds]);

        System.debug('### accountMap: ' + accountMap);

        Map<Id, Account> accountsToUpdateMap = new Map<Id, Account>();
        Map<Id, Schema.Location> locationsToUpdateMap = new Map<Id, Schema.Location>();
        
        for (Schema.Address address : newRecords) {

            System.debug('### address: ' + address);


            Id locationId = address.ParentId;

            List<AssociatedLocation> associatedLocations = associatedLocationsByLocationId.get(locationId);

            if (associatedLocations != null) {
                for (AssociatedLocation al : associatedLocations) {

                    System.debug('### al: ' + al);

                    Account acc = accountMap.get(al.ParentRecordId);

                    System.debug('### acc: ' + acc);

                    if (acc != null) {
                        system.debug('### locationType = '+address.LocationType);
                        if (al.Type == addressType1 && al.Purpose__c == primaryPurpose && (acc.ShippingStreet != address.Street || acc.ShippingCity != address.City) ) {
                            acc.ShippingStreet = address.Street;
                            acc.ShippingCity = address.City;
                            acc.ShippingState = address.State;
                            acc.ShippingCountry = address.Country;
                            acc.ShippingPostalCode = address.PostalCode;
                            
                            accountsToUpdateMap.put(acc.id, acc);
                        } else if ((al.Type == addressType2 || al.Type == addressType3) && al.Purpose__c == primaryPurpose && (acc.BillingStreet != address.Street || acc.BillingCity != address.City)) {
                            acc.BillingStreet = address.Street;
                            acc.BillingCity = address.City;
                            acc.BillingState = address.State;
                            acc.BillingCountry = address.Country;
                            acc.BillingPostalCode = address.PostalCode;
                            if (acc.IsPersonAccount) {
                                acc.Resident_State__pc = address.State;
                            }
                            accountsToUpdateMap.put(acc.id, acc);
                        }

                        Schema.Location location = new Schema.Location();
                        location.Id = locationId;
                        location.Name = address.Street + ', ' + address.City;
                        locationsToUpdateMap.put(location.Id, location);

                    }

                }
            }
        }

        System.debug('### accountsToUpdateMap: ' + accountsToUpdateMap);


        update accountsToUpdateMap.values();
        update locationsToUpdateMap.values();

    }
}