public without sharing class AmFam_ProducerAccessTriggerHandler {
    public AmFam_ProducerAccessTriggerHandler() {

    }

    public void OnAfterInsert (List<ProducerAccess__c> newRecords) {

        AmFam_ProducerAccessTriggerService.createNewAccountShares(newRecords);
    }


    public void OnAfterUpdate(List<ProducerAccess__c> newRecords,
                              Map<ID, ProducerAccess__c> oldRecordsMap) {

        AmFam_ProducerAccessTriggerService.updateAccountShares(newRecords, oldRecordsMap);

    }

    public void OnAfterDelete(List<ProducerAccess__c> oldRecords,
                              Map<ID, ProducerAccess__c> oldRecordsMap) {

        AmFam_ProducerAccessTriggerService.removeAccountShares(oldRecords, oldRecordsMap);

    }


}