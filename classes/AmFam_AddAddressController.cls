public without sharing class AmFam_AddAddressController {
    

    @AuraEnabled
    public static string saveAddress(String accountId, String addressType, String street, String city, String postalCode, String state, String country) {

        try {
            Schema.Location newLocation = new Schema.Location();
            newLocation.LocationType = addressType;
            newLocation.Name = street + ', ' + city;
            insert newLocation;


            System.debug('### new Location: ' + newLocation);


            Schema.Address newAddress = new Schema.Address();
            newAddress.City = city;
            newAddress.Street = street;
            newAddress.Country = country;
            newAddress.PostalCode = postalCode;
            newAddress.State = state;
            newAddress.ParentId = newLocation.Id;
            newAddress.LocationType = addressType;
            insert newAddress;

            System.debug('### new Address: ' + newAddress);


            AssociatedLocation newAssociated = new AssociatedLocation();
            newAssociated.LocationId = newLocation.Id;
            newAssociated.ParentRecordId = accountId;
            newAssociated.Type = addressType;
            insert newAssociated;
            
            System.debug('### new Associated Location: ' + newAssociated);

            return newAddress.Id;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

        

    }

    @AuraEnabled
    public static Map<String, String>  getAddressTypes(){
        
        try {
            Map<String, String> options = new Map<String, String>();
        
            Schema.DescribeFieldResult fieldResult = Schema.Address.LocationType.getDescribe();
            System.debug('### fieldResult: ' +  fieldResult);
            
            List<Schema.PicklistEntry> pValues = fieldResult.getPicklistValues();
            System.debug('### pValues: ' +  pValues);

            for (Schema.PicklistEntry p: pValues) {
                
                
                options.put(p.getValue(), p.getLabel());

            }
            System.debug('### options: ' +  options);

            return options;
                
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
}