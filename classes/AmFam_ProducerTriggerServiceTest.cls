@IsTest
public with sharing class AmFam_ProducerTriggerServiceTest 
{
    private static List<Producer> setupTest()
    {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        AmFam_UserService.callProducerAPI = false;
        AmFam_UserService.callProducerUpdateAPI = false;
        insert agent;
        
        Account agency = AmFam_TestDataFactory.createAccountByNameWithLeads('TestAgency', 4); //73320
        //String uid = UserInfo.getUserId();

        List<Producer> newProducers = new List<Producer>();

        Producer prod = new Producer();
        prod.InternalUserId = agent.Id;
        prod.Name = '73320';

        newProducers.add(prod);

        return newProducers;
    }

    @IsTest
    private static void testCreateNewLeadShares()
    {
        List<Producer> newProducers = AmFam_ProducerTriggerServiceTest.setupTest();
/*        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        AmFam_UserService.callProducerAPI = false;
        AmFam_UserService.callProducerUpdateAPI = false;
        insert agent;
        
        Account agency = AmFam_TestDataFactory.createAccountByNameWithLeads('TestAgency', 4); //73320
        //String uid = UserInfo.getUserId();

        List<Producer> newProducers = new List<Producer>();

        Producer prod = new Producer();
        prod.InternalUserId = agent.Id;
        prod.Name = '73320';

        newProducers.add(prod);
*/
//        AmFam_ProducerTriggerService.createNewLeadShares(newProducers);

        Test.startTest();
        insert newProducers;
        Test.stopTest();

        List<LeadShare> shares = [SELECT Id,RowCause,LeadAccessLevel FROM LeadShare WHERE RowCause = 'Manual' AND LeadAccessLevel = 'Edit'];
        System.debug('LS:'+shares);
        System.assert(shares.size() == 4);
    }

    @IsTest
    private static void testRemoveLeadShares()
    {
        List<Producer> newProducers = AmFam_ProducerTriggerServiceTest.setupTest();        
        insert newProducers;

        List<Id> newProducerIds = new List<Id>();
        for (Producer prod : newProducers)
        {
            newProducerIds.add(prod.Id);
        }
    
        List<LeadShare> shares = [SELECT Id,RowCause,LeadAccessLevel FROM LeadShare WHERE RowCause = 'Manual' AND LeadAccessLevel = 'Edit'];
    
        Test.startTest();

        List<Producer> theProducers = [SELECT Id,Name FROM Producer WHERE Id IN :newProducerIds];

        delete theProducers;

        Test.stopTest();
    
        shares = [SELECT Id,RowCause,LeadAccessLevel FROM LeadShare WHERE RowCause = 'Manual' AND LeadAccessLevel = 'Edit'];
        System.assert(shares.size() == 0);
    }
}