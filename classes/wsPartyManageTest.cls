@isTest
public class wsPartyManageTest {
    @TestSetup
    static void makeData(){
        Account a = new Account(Name = 'Test Account', Producer_ID__c = '12345');
        insert a;
    }
    
    static testMethod void LPECalloutTest() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new AmFam_LPECalloutMock(true));
        
        Account a = [SELECT Id FROM Account LIMIT 1];
        
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
        l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
        l.Phone = '1231231234';
        l.Street = '123 Main Street\nTest';
        l.City = 'Anytown';
        l.StateCode = 'WI';
        l.CountryCode = 'US';
        l.PostalCode = '12345';
        l.Email = 'testing@test.com';
        l.Email_Usage__c = 'HOME';
        l.Marital_Status__c = 'M';
        l.Gender__c = 'M';
        l.Date_Of_Birth__c = Date.today();
        l.LeadSource = 'Amfam.com';
        l.Company = 'Test';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        
        test.startTest();
        insert l;
        test.stopTest();
        
        l = [SELECT Id, LPE_Status__c FROM Lead WHERE Id =: l.Id];
        system.assertEquals(l.LPE_Status__c,'Processing');
    }
    
    static testMethod void LPECalloutFailTest() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new AmFam_LPECalloutMock(false));
        
        Account a = [SELECT Id FROM Account LIMIT 1];
        
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
        l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
        l.Phone = '1231231234';
        l.Street = '123 Main Street\nTest';
        l.City = 'Anytown';
        l.StateCode = 'WI';
        l.CountryCode = 'US';
        l.PostalCode = '12345';
        l.Email = 'testing@test.com';
        l.Email_Usage__c = 'HOME';
        l.Marital_Status__c = 'M';
        l.Gender__c = 'M';
        l.Date_Of_Birth__c = Date.today();
        l.LeadSource = 'Amfam.com';
        l.Company = 'Test';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        
        test.startTest();
        insert l;
        test.stopTest();
        
        l = [SELECT Id, LPE_Status__c FROM Lead WHERE Id =: l.Id];
        system.assertEquals(l.LPE_Status__c,'Not Processed');
    }

    static testMethod void addEntrantSuccessTest() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User u = new User (alias = 'srep', email='test1246@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test1246@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert u;
        AmFam_UserService.callProducerAPI = true;
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('ENTRANT'));

        System.runAs(u) {
            Id agencyRecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            Account a = new Account(Name = 'Test Account 1', Producer_ID__c = '54321', RecordTypeId = agencyRecordTypeId);
            insert a;
            Lead l = new Lead();
            l.Agency__c = a.Id;
            l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
            l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
            l.Phone = '1231231234';
            l.Street = '123 Main Street\nTest';
            l.City = 'Anytown';
            l.StateCode = 'WI';
            l.CountryCode = 'US';
            l.PostalCode = '12345';
            l.Email = 'testing@test.com';
            l.Email_Usage__c = 'HOME';
            l.Marital_Status__c = 'M';
            l.Gender__c = 'M';
            l.Date_Of_Birth__c = Date.today();
            l.LeadSource = 'Amfam.com';
            l.Company = 'Test';
            l.Lines_of_Business__c = 'Personal Vehicle;Property';
            
            test.startTest();
            wsPartyManage.callAddEntrant = true;
            insert l;
            test.stopTest();
        }
    }

    static testMethod void addEntrantFailureTest() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User u = new User (alias = 'srep', email='test1246@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test1246@xyz.com.test',
                                AmFam_User_ID__c = '54321'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert u;
        AmFam_UserService.callProducerAPI = true;
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('ENTRANT', true));

        System.runAs(u) {
            Id agencyRecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            Account a = new Account(Name = 'TestAccount', Producer_ID__c = '54321', RecordTypeId = agencyRecordTypeId);
            insert a;
        
            Lead l = new Lead();
            l.Agency__c = a.Id;
            l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
            l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
            l.Phone = '1231231234';
            l.Street = '123 Main Street\nTest';
            l.City = 'Anytown';
            l.StateCode = 'WI';
            l.CountryCode = 'US';
            l.PostalCode = '12345';
            l.Email = 'testing@test.com';
            l.Email_Usage__c = 'HOME';
            l.Marital_Status__c = 'M';
            l.Gender__c = 'M';
            l.Date_Of_Birth__c = Date.today();
            l.LeadSource = 'Amfam.com';
            l.Company = 'Test';
            l.Lines_of_Business__c = 'Personal Vehicle;Property';
            
            test.startTest();
            wsPartyManage.callAddEntrant = true;
            insert l;
            test.stopTest();
        }
    }

    static testMethod void editEntrantSuccessTest() {
        wsPartyManage.lpeCallout = false;
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User u = new User (alias = 'srep', email='test1246@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test1246@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert u;
        AmFam_UserService.callProducerAPI = true;
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('editParty'));

        System.runAs(u) {
            Id agencyRecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            Account a = new Account(Name = 'Test Account 1', Producer_ID__c = '54321', RecordTypeId = agencyRecordTypeId);
            insert a;
            Lead l = new Lead();
            l.Agency__c = a.Id;
            l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
            l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
            l.Phone = '1231231234';
            l.Street = '123 Main Street\nTest';
            l.City = 'Anytown';
            l.StateCode = 'WI';
            l.CountryCode = 'US';
            l.PostalCode = '12345';
            l.Email = 'testing@test.com';
            l.Email_Usage__c = 'HOME';
            l.Marital_Status__c = 'M';
            l.Gender__c = 'M';
            l.Date_Of_Birth__c = Date.today();
            l.LeadSource = 'Amfam.com';
            l.Company = 'Test';
            l.Lines_of_Business__c = 'Personal Vehicle;Property';
            insert l;

            test.startTest();
            l.Street = '124 Other Street';
            l.City = 'Nowhere';
            l.StateCode = 'IL';
            l.PostalCode = '60613';
            l.Phone = '1234123321';
            l.Email = 'testing2@test.com';
            l.CDHID__c = '123456';
            l.Party_Level__c = 'Entrant';
            update l;
            test.stopTest();
        }
    }
    
    static testMethod void constructSelectedAddressInfoTest() {
        
        wsPartyManage.lpeCallout = false;
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User u = new User (alias = 'srep', email='test1246@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test1246@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert u;
        
        System.runAs(u) {
            Id agencyRecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            
            Account a = new Account(Name = 'Test Account 1', Producer_ID__c = '54329', RecordTypeId = agencyRecordTypeId);
            insert a;
            
            Lead l = new Lead();
            l.Agency__c = a.Id;
            l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
            l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
            l.Phone = '1231231234';
            l.Street = '123 Main Street\nTest';
            l.City = 'Anytown';
            l.StateCode = 'WI';
            l.CountryCode = 'US';
            l.PostalCode = '12345';
            l.Email = 'testing@test.com';
            l.Email_Usage__c = 'HOME';
            l.Marital_Status__c = 'M';
            l.Gender__c = 'M';
            l.Date_Of_Birth__c = Date.today();
            l.LeadSource = 'Amfam.com';
            l.Company = 'Test';
            l.Lines_of_Business__c = 'Personal Vehicle;Property';
            insert l;
            
            Account acc = new Account();
            acc.Agency__c = a.Id;
            acc.Salutation = 'MR';
            acc.FirstName = 'Testing';
            acc.MiddleName = 'A';
            acc.LastName = 'AccountForPA';
            acc.Family_Suffix__pc = 'JR';
            acc.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();
            acc.BillingCountryCode = 'US';
            acc.BillingCity = 'Bartlesville';
            acc.BillingStateCode = 'OK';
            acc.BillingPostalCode = '74006';
            acc.BillingStreet = '224 Steeper Ave';
            acc.ShippingCountryCode = 'US';
            acc.ShippingCity = 'Bartlesville';
            acc.ShippingStateCode = 'OK';
            acc.ShippingPostalCode = '74003';
            acc.ShippingStreet = '792 Sooner Park Dr';
            
            insert acc;
            
            String selectedResidentialAddress = '[{"addressLine1":"224 Steeper Ave","censusBlockGroup":"3","censusTractNumber":"000600","city":"Bartlesville","Code1ReturnIndicator":"OVERRIDE","countryIdentifier":"USA","deliveryPointValidationCommercialMailreceivingCode":"","deliveryPointValidationMatchIndicator":"Unable to confirm delivery point","fipsCountyNumber":"147","fipsStateNumber":"40","geoMatchLevelCode":"6","latitudeNumber":"+36.730700","longitudeNumber":"-95.919700","shortCity":"Bartlesville","stateCode":"OK","zip5Code":"74006"}]';
            String selectedMailingAddress ='[{"addressLine1":"792 Sooner Park Dr","carrierRoute":"C001","censusBlockGroup":"6","censusBlockMD":"013","censusTractNumber":"000500","city":"Bartlesville","Code1ReturnIndicator":"OVERRIDE","countryIdentifier":"USA","deliveryPointValidationCommercialMailreceivingCode":"Unconfirmed","deliveryPointValidationMatchIndicator":"Not a valid delivery point","fipsCountyNumber":"147","fipsStateNumber":"40","geoMatchLevelCode":"2","latitudeNumber":"+36.743882","longitudeNumber":"-95.907767","shortCity":"Bartlesville","stateCode":"OK","zip2Code":"92","zip4Code":"8957","zip5Code":"74006"}]';

            test.startTest();
            List<wsPartyManageParty.PartyAddressType> getPartyAddr = wsPartyManage.getPartyAddress(l, null, selectedResidentialAddress);
            List<wsPartyManageParty.PartyAddressType> getPartyAddrAccount = wsPartyManage.getPartyAddressAccount(acc, False, selectedResidentialAddress, selectedMailingAddress);
            test.stopTest();
            system.debug('getPartyAddr' + getPartyAddr);
            system.debug('getPartyAddrAccount' + getPartyAddrAccount);
            System.assert(getPartyAddr[0].Address.addressLine1 == '224 Steeper Ave');
            System.assert(getPartyAddrAccount[0].Address.city == 'Bartlesville');
        }
    }
}