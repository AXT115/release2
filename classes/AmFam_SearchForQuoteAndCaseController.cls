/**
*   @description: the apex class is the server side controller for aura component AmFam_SearchForQuoteAndCase
*	The component launches the dynamic URL to source system to search for quote or case based on the user selection
*   @author: Anbu Chinnathambi
*   @date: 08/30/2021
*   @group: Apex Class
*/
public class AmFam_SearchForQuoteAndCaseController {
    
    @AuraEnabled       
    public static Map<String,String> getSourceSystemURL(){
        
        String org;
        //checking whether its Production, QA or INT
        if(URL.getSalesforceBaseUrl().getHost().contains('--')){
             org = URL.getSalesforceBaseUrl().getHost().substringAfter('--')?.contains('qa')? 'QA':'INT';
        }else{
             org = 'Production';
        }
        
        Map<String, String> orgURLMapForCommercial = new Map<String, String>();
        Map<String, String> orgURLMapForLife = new Map<String, String>();
        for(Search_for_a_Quote_Case__mdt source : [SELECT DeveloperName, MasterLabel, INT_URL__c, Source_System__c, Production_URL__c,QA_URL__c FROM Search_for_a_Quote_Case__mdt ]){
            if(source.Source_System__c == 'Life_Cornerstone'){
                orgURLMapForLife.put('Production', source.Production_URL__c);
                orgURLMapForLife.put('QA',source.QA_URL__c);
                orgURLMapForLife.put('INT',source.INT_URL__c);
            }else{
                orgURLMapForCommercial.put('Production', source.Production_URL__c);
                orgURLMapForCommercial.put('QA',source.QA_URL__c);
                orgURLMapForCommercial.put('INT',source.INT_URL__c);
            }
        }
        Map<string, string> returnURL = new Map<string, string>();
        returnURL.put(orgURLMapForCommercial.get(org), orgURLMapForLife.get(org));
        system.debug('returnURL' + returnURL);
        return returnURL;
    }

}