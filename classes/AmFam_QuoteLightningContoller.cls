public without sharing class AmFam_QuoteLightningContoller {
    @AuraEnabled
    public static List<AmFam_PartyServiceAPIWrapper.Products> getRelatedList(Id recordId) {
        Lead ld = [SELECT Id, Agency__r.Producer_ID__c, Contact_CDHID__c, Party_Level__c FROM Lead WHERE Id=: recordId];
        if (ld.Party_Level__c == 'Contact' && ld.Contact_CDHID__c != null) {
            AmFam_PartyServiceCallout ps = new AmFam_PartyServiceCallout();
            AmFam_PartyServiceAPIWrapper partySvc = ps.partyServiceCallOut(ld.Contact_CDHID__c);
            if (partySvc != null && partySvc.products != null) {
                for (AmFam_PartyServiceAPIWrapper.Products prd : partySvc.products) {
                    prd.agencyProducerId = ld.Agency__r.Producer_ID__c;
                    prd.hasAccess = getProductAccess(prd, ld.Agency__r.Producer_ID__c);
                }
            }
            return partySvc != null ? partySvc.products : null;
        }
        return null;
    }
    @AuraEnabled
    public static List<Sales_Portal_Links__mdt> getSalesPortalLinks() {
        List<Sales_Portal_Links__mdt> salesPortalLinks = [SELECT DeveloperName, URL__c FROM Sales_Portal_Links__mdt];
        return salesPortalLinks;
    }

    private static Boolean getProductAccess(AmFam_PartyServiceAPIWrapper.Products product, String producerId) {
                 
        Account servicingAccount;
        Account[] servicingAccounts;
        Boolean hasAccess = false;                    

        if(product.servicingAgentProducerId  != null) {
                servicingAccounts = [SELECT Id, Is_Corporate_Account__c, Producer_ID__c FROM Account WHERE Producer_ID__c=: product.servicingAgentProducerId];
                if(servicingAccounts.size() > 0) {
                    servicingAccount = servicingAccounts[0];
                }
            }
                    
        // if producerIds match
        if (producerId == product.servicingAgentProducerId) {
            hasAccess = true;
        }
        // if PersonalAuto and isCorporate or producerId is null
        else if (product.productCode ==  'PersonalAuto' && ((servicingAccount != null && servicingAccount.Is_Corporate_Account__c == true) || (product.servicingAgentProducerId  == null))) {
                        hasAccess = true;
                }  
        else {
            hasAccess = false;
        }
        return hasAccess;
    }
}