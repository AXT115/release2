public class AmFam_UserAccessToAccountLeadQueueable implements Queueable
{
    @TestVisible private static Boolean callProducerAPI = true;

    private List<Id> userRecIds;

    public AmFam_UserAccessToAccountLeadQueueable(List<Id> userRecIds) 
    {
        this.userRecIds = userRecIds;
    }

    public void execute(QueueableContext context) 
    {
        List<User> userRecs = [SELECT Id, AmFam_User_ID__c, Profile.Name FROM User WHERE Id IN :userRecIds];

        AmFam_UserService.userAccessToAccountLead(userRecs);       
    }
}