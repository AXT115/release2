@isTest
global class AmFam_ProducerServiceHttpCalloutMock implements HttpCalloutMock {
    Boolean isMockResponseSuccessful;
    String jsonType;
    String setBodyResponse;
    public AmFam_ProducerServiceHttpCalloutMock (Boolean isMockResponseSuccessful, String jsonType) {
        this.isMockResponseSuccessful  = isMockResponseSuccessful;
        this.jsonType  = jsonType;
        if (jsonType == 'json'){
            setBodyResponse = AmFam_ProducerServiceTest.json;
        } else if (jsonType == 'jsonNoProd') {
            setBodyResponse = AmFam_ProducerServiceTest.jsonNoProd;
        } else {
            setBodyResponse = 'Exception Response';
        }
    }
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        if (this.isMockResponseSuccessful) {
            response.setHeader('Content-Type', 'application/json');
            response.setBody(setBodyResponse);
            response.setStatusCode(200);
        } else {
            response.setStatusCode(400);
            response.setBody(setBodyResponse);
            response.setStatus('Bad request');
        }
        return response;
    }
}