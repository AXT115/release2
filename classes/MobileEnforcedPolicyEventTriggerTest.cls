@IsTest
public class MobileEnforcedPolicyEventTriggerTest {
    @IsTest
    public static void invokeTrigger() {
        MobileEnforcedPolicyEvent event = new MobileEnforcedPolicyEvent(UserId = UserInfo.getUserId(), 
                                                     DeviceIdentifier = 'dac5bbffc407f470',
                                                     AppVersion = '1.0(1)',
                                                     AppPackageIdentifier  = 'com.salesforce.internal.securesdksampleapp',
                                                     OsName = 'Android',
                                                     OsVersion = '7.1.1',
                                                     DeviceModel = 'Android SDK built for x86',
                                                     PolicyResults = '[{"policy":"mobile.security.device_blacklist","actual":"google Android SDK built for x86","expected":"["google"]","passed":"false","severity":"Warn"}]',
                                                     EnforcedAction = 'Warn');
        
        Test.startTest();
        Database.SaveResult result = Database.insertImmediate(event);
        Test.stopTest();
        
        System.assert(result.isSuccess());
        
        Integer customCount = [Select Count() FROM MobileEnforcedPolicyObject__c WHERE UserId__c = :UserInfo.getUserId()];
        System.assertEquals(1, customCount);
    }
}