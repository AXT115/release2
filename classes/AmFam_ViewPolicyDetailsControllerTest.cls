/**
*   @description: the apex class is a test class for AmFam_ViewPolicyDetailsController
*	@author: Anbu Chinnathambi
*   @date: 07/06/2021
*   @group: Test Class
*/

@isTest
public class AmFam_ViewPolicyDetailsControllerTest {
    
     @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    
    //Method to test getSourceSystemURL method from AmFam_ViewPolicyDetailsController class
    static testMethod void testgetSourceSystemURL() {
        
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];

        Producer prod = new Producer();
        prod.Producer_ID__c = '77770';
        prod.Name = '77770';
        prod.InternalUserId = objUser.Id;
        prod.Type = 'Agency';
        insert prod;
        
        Account a = new Account(Name = 'Test Agency', Producer_ID__c = '77770');
        insert a;
        
        Id recordTypeId = Schema.SObjectType.InsurancePolicy.getRecordTypeInfosByDeveloperName().get('Policy').getRecordTypeId();
        InsurancePolicy policy = new InsurancePolicy();
        policy.Name= '41001-50037-00';
        policy.SourceSystem= 'PolicyCenter';
        policy.NameInsuredId= a.id;
        policy.RecordTypeId = recordTypeId;
        policy.ProducerId = prod.Id;
        insert policy;
        
        test.startTest();
        String returnURL = AmFam_ViewPolicyDetailsController.getSourceSystemURL(policy.id, null);
        test.stopTest();
        
        System.assert(returnURL.endsWith('PolicyNumber='+policy.Name.substring(0, 5) + '-' + policy.Name.substring(5, 10) + '-' + policy.Name.substring(10, 12)));
    }
    
    //Method to test getSourceSystemURL method from AmFam_ViewPolicyDetailsController class
    static testMethod void testgetSourceSystemURLForBillingAccount() {
        
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];
        
        Producer prod = new Producer();
        prod.Producer_ID__c = '144745';
        prod.Name = '144745';
        prod.InternalUserId = objUser.Id;
        prod.Type = 'Agency';
        insert prod;
        
        Account a = new Account(Name = 'Test Agency', Producer_ID__c = '144745');
        insert a;
        
        Id recordTypeId = Schema.SObjectType.InsurancePolicy.getRecordTypeInfosByDeveloperName().get('Policy').getRecordTypeId();
        InsurancePolicy policy = new InsurancePolicy();
        policy.Name= '41001-50037-90';
        policy.SourceSystem= 'AutoPlus';
        policy.NameInsuredId= a.id;
        policy.RecordTypeId = recordTypeId;
        policy.ProducerId = prod.Id;
        policy.Billing_Account__c = '61504136873';
        insert policy;
        
        test.startTest();
        String returnURL = AmFam_ViewPolicyDetailsController.getSourceSystemURL(policy.id, policy.Billing_Account__c);
        test.stopTest();
        
        System.assert(returnURL.endsWith('billingNumberSearchString='+policy.Billing_Account__c+'&billingSystem=PersonalLinesCBS&'));
    }

}