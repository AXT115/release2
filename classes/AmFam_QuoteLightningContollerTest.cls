/**
*    @description  Test Code to check AmFam_QuoteLightningContoller Class
*    @author Salesforce Services
*    @date 06/02/2020
*    @group @Test
*/
@isTest
private class AmFam_QuoteLightningContollerTest {
    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    @isTest static void getRelatedListTest() {
        wsPartyManage.lpeCallout = false;
        Account[] accts = AmFam_TestDataFactory.createAccountsWithLeads(1, 2);
        List<Lead> leads = [SELECT Id, Agency__r.Producer_ID__c, Contact_CDHID__c, Party_Level__c FROM Lead WHERE Agency__c IN :accts];
        for (Lead ld : leads) {
        ld.Contact_CDHID__c = 'Test123';
        ld.Party_Level__c = 'Contact';
        break;
        }
        update leads;
        Test.startTest();
        // This causes a fake response to be sent from the class that implements HttpCalloutMock.
        Test.setMock(HttpCalloutMock.class, new AmFam_PartyServiceHttpCalloutMock(true));
        for (Lead ld : leads) {
        List<AmFam_PartyServiceAPIWrapper.Products> prods = AmFam_QuoteLightningContoller.getRelatedList(ld.id);
        if (ld.Party_Level__c == 'Contact' && ld.Contact_CDHID__c  != null) {
            System.assertNotEquals(null, prods);
            System.assertEquals('QUOTE', prods[0].typeOfProductCode);
            System.assertEquals('POLICY', prods[1].typeOfProductCode);
            System.assertEquals(false, prods[0].hasAccess);
            System.assertEquals(false, prods[1].hasAccess);
        } else {
            System.assertEquals(null, prods);
        }
        }
        Test.stopTest();
    }

  //Test for Null Response
    @isTest static void getRelatedListTestNullResponse() {
        wsPartyManage.lpeCallout = false;
        Account[] accts = AmFam_TestDataFactory.createAccountsWithLeads(1, 1);
        List<Lead> leads = [SELECT Id, Agency__r.Producer_ID__c, Contact_CDHID__c, Party_Level__c FROM Lead WHERE Agency__c IN :accts];
        for (Lead ld : leads) {
        ld.Contact_CDHID__c = 'Test123';
        ld.Party_Level__c = 'Contact';
        }
        update leads;
        Test.startTest();
        List<String> cdhIds = new List<String>();
        // This causes a fake response to be sent from the class that implements HttpCalloutMock.
        Test.setMock(HttpCalloutMock.class, new AmFam_PartyServiceHttpCalloutMock(false));
        for (Lead ld : leads) {
        List<AmFam_PartyServiceAPIWrapper.Products> prods = AmFam_QuoteLightningContoller.getRelatedList(ld.id);
        System.assertEquals(null, prods);
        }
        Test.stopTest();
    }

    //Test for Exception
    @isTest static void getRelatedListTestException() {
        wsPartyManage.lpeCallout = false;
        Account[] accts = AmFam_TestDataFactory.createAccountsWithLeads(1, 1);
        List<Lead> leads = [SELECT Id, Agency__r.Producer_ID__c, Contact_CDHID__c, Party_Level__c FROM Lead WHERE Agency__c IN :accts];
        for (Lead ld : leads) {
        ld.Contact_CDHID__c = 'Test123';
        ld.Party_Level__c = 'Contact';
        }
        update leads;
        Test.startTest();
        List<String> cdhIds = new List<String>();
        // This causes a fake response to be sent from the class that implements HttpCalloutMock.
        Test.setMock(HttpCalloutMock.class, new AmFam_UnauthorizedEndpointResponseMock());
        for (Lead ld : leads) {
            List<AmFam_PartyServiceAPIWrapper.Products> prods = AmFam_QuoteLightningContoller.getRelatedList(ld.id);
            List<API_Failure_Log__c> logs = [SELECT Id, Error_Message__c FROM API_Failure_Log__c];
            System.assertEquals(1, logs.size());
            System.assertEquals('Unauthorized Endpoint', logs[0].Error_Message__c);
        }
        Test.stopTest();
    }

    @isTest static void getSalesPortalLinksTest() {
        List<Sales_Portal_Links__mdt> salesPortalLinks = AmFam_QuoteLightningContoller.getSalesPortalLinks();
        System.assertNotEquals(null, salesPortalLinks);
    }
}