@isTest
global class AmFam_LPECalloutMock implements HttpCalloutMock {
    Boolean  isMockResponseSuccessful;

    public AmFam_LPECalloutMock (Boolean isMockResponseSuccessful) {
        this.isMockResponseSuccessful  = isMockResponseSuccessful;
    }
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        if (isMockResponseSuccessful) {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"leadId":"12345"}');
            res.setStatusCode(200);
        } else {
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(400);
        }
        
        return res;
    }
}