@isTest
private class AmFam_NewLeadControllerTest {
    @TestSetup
    static void makeData(){
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User u = new User (alias = 'srep', email='test1246@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test1246@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert u;
     }
    
    @isTest
    static void testSendLeadToCDHWithOutError(){
        
        User u = [Select id, email,lastname from User where lastname = 'TestRep' LIMIT 1];
        
        wsPartyManage.lpeCallout = false;
        
        Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('ENTRANT'));
        
        system.runAs(u) {
            
            Account agency = new Account();
            agency.Name = 'Test Agency';
            agency.Producer_ID__c = '12345';
            agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            insert agency;
            
            Lead newLead = new Lead();
            newLead.Agency__c = agency.Id;
            newLead.Gender__c = 'M';
            newLead.FirstName = 'Testing';
            newLead.MiddleName = 'A';
            newLead.LastName = 'New Lead';
            newLead.Street = '822 SW Testing Ave';
            newLead.StateCode = 'OK';
            newLead.PostalCode = '74234';
            newLead.City = 'Tulsa';
            newLead.CountryCode = 'US';
            newLead.Phone = '9239239231';
            newLead.Email = 'testnewlead1@test.com';
            insert newLead;
            
            Test.startTest();
            AmFam_NewLeadController.LeadWrapper leadWrapper = AmFam_NewLeadController.sendLeadToCDH(newLead.id, False, True, null);
            Test.stopTest();
            System.assertEquals('demo@fakeemail.com',leadWrapper.l.Email);
        }
    }
    
    @isTest
    static void testSendLeadToCDHWithError(){
        
        User u = [Select id, email,lastname from User where lastname = 'TestRep' LIMIT 1];
        
        wsPartyManage.lpeCallout = false;
        
        Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('ENTRANT', true));
        
        system.runAs(u) {
            
            Account agency = new Account();
            agency.Name = 'Test Agency';
            agency.Producer_ID__c = '12345';
            agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            insert agency;
            
            Lead newLead = new Lead();
            newLead.Agency__c = agency.Id;
            newLead.Gender__c = 'M';
            newLead.FirstName = 'Testing';
            newLead.MiddleName = 'A';
            newLead.LastName = 'New Lead';
            newLead.Street = '822 SW Testing Ave';
            newLead.StateCode = 'OK';
            newLead.PostalCode = '74234';
            newLead.City = 'Tulsa';
            newLead.CDHID__c = '';
            newLead.CountryCode = 'US';
            newLead.Phone = '9239239231';
            newLead.Email = 'testnewlead1@test.com';
            insert newLead;
            
            Test.startTest();
            AmFam_NewLeadController.LeadWrapper leadWrapper = AmFam_NewLeadController.sendLeadToCDH(newLead.id, False, True, null);
            Test.stopTest();
            System.assert(leadWrapper.errors != null);
        }
    }
    
    @isTest
    static void testsendLeadEditToCDHWithOutError(){
        
        User u = [Select id, email,lastname from User where lastname = 'TestRep' LIMIT 1];
        
        wsPartyManage.lpeCallout = false;
        
        Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('ENTRANT'));
        
        system.runAs(u) {
            
            Account agency = new Account();
            agency.Name = 'Test Agency';
            agency.Producer_ID__c = '12345';
            agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            insert agency;
            
            Lead newLead = new Lead();
            newLead.Agency__c = agency.Id;
            newLead.Gender__c = 'M';
            newLead.FirstName = 'Testing';
            newLead.MiddleName = 'A';
            newLead.LastName = 'New Lead';
            newLead.Street = '822 SW Testing Ave';
            newLead.StateCode = 'OK';
            newLead.PostalCode = '74234';
            newLead.City = 'Tulsa';
            newLead.CountryCode = 'US';
            newLead.Phone = '9239239231';
            newLead.Email = 'testnewlead1@test.com';
            insert newLead;
            
            newLead.Email = 'testnewlead2@test.com';
            newLead.Party_Level__c = 'Entrant';
            newLead.CDHID__c = '11030052';
            newLead.Phone = '7779239231';
            newLead.MobilePhone = '7788239231';
            update newLead;
            
            String LeadJson = JSON.serialize(newLead);
            
            Test.startTest();
            AmFam_NewLeadController.LeadWrapper leadWrapper = AmFam_NewLeadController.sendLeadEditToCDH(LeadJson, newLead.id, False, True, null);
            Test.stopTest();
            System.assertEquals('demo@fakeemail.com',leadWrapper.l.Email);
        }
    }
    
    @isTest
    static void testsendLeadEditToCDHWithError(){
        User u = [Select id, email,lastname from User where lastname = 'TestRep' LIMIT 1];
        wsPartyManage.lpeCallout = false;
        
        Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('ENTRANT', true));
        
        system.runAs(u) {
            
            Account agency = new Account();
            agency.Name = 'Test Agency';
            agency.Producer_ID__c = '12345';
            agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            insert agency;
            
            Lead newLead = new Lead();
            newLead.Agency__c = agency.Id;
            newLead.Gender__c = 'M';
            newLead.FirstName = 'Testing';
            newLead.MiddleName = 'A';
            newLead.LastName = 'New Lead';
            newLead.Street = '822 SW Testing Ave';
            newLead.StateCode = 'OK';
            newLead.PostalCode = '74234';
            newLead.City = 'Tulsa';
            newLead.CountryCode = 'US';
            newLead.Phone = '9239239231';
            newLead.Email = 'testnewlead1@test.com';
            insert newLead;
            
            newLead.Email = 'testnewlead2@test.com';
            newLead.Party_Level__c = 'Entrant';
            //newLead.CDHID__c = '11030052';
            update newLead;
            
            String LeadJson = JSON.serialize(newLead);
            
            Test.startTest();
            AmFam_NewLeadController.LeadWrapper leadWrapper = AmFam_NewLeadController.sendLeadEditToCDH(LeadJson, newLead.id, False, True, null);
            Test.stopTest();
            System.assert(leadWrapper.partyMatch != null);
        }
    }
    
    
    @isTest
    static void testselectAccountPartyMatch(){
        
        User u = [Select id, email,lastname from User where lastname = 'TestRep' LIMIT 1];
        wsPartyManage.lpeCallout = false;
        system.runAs(u) {
            
            Account agency = new Account();
            agency.Name = 'Test Agency';
            agency.Producer_ID__c = '12345';
            agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            insert agency;
            
            Account person = new Account();   
            person.FirstName = 'Test';
            person.LastName = 'Person';
            person.CDHID__c = '9844131';
            person.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();
            insert person;
            
            Lead newLead = new Lead();
            newLead.Agency__c = agency.Id;
            newLead.Gender__c = 'M';
            newLead.FirstName = 'Testing';
            newLead.MiddleName = 'A';
            newLead.LastName = 'New Lead';
            newLead.Street = '822 SW Testing Ave';
            newLead.StateCode = 'OK';
            newLead.PostalCode = '74234';
            newLead.City = 'Tulsa';
            newLead.CountryCode = 'US';
            newLead.Phone = '9239239231';
            newLead.Email = 'testnewlead1@test.com';
            
            
            String selectedRow = '{"address":"234 Dhanush Rd Bartlesville, OK 74006","age":30,"cdhID":"9844131","fName":"Testing","isProducerAssociated":true,"lName":"New Lead","partyLevelCode":"CONTACT","partyType":"isPersonAccount","primaryState":"OK"}';

            Test.startTest();
            String returnResponse = AmFam_NewLeadController.selectAccountPartyMatch(selectedRow, newLead, false, 'isPersonAccount');
            Test.stopTest();
            System.assert(returnResponse == newLead.id);
        }
    }
    
    @isTest
    static void testselectLeadPartyMatch(){
        
        User u = [Select id, email,lastname from User where lastname = 'TestRep' LIMIT 1];
        wsPartyManage.lpeCallout = false;
        system.runAs(u) {
            
            Account agency = new Account();
            agency.Name = 'Test Agency';
            agency.Producer_ID__c = '12345';
            agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            insert agency;
            
            Lead newLead = new Lead();
            newLead.Agency__c = agency.Id;
            newLead.Gender__c = 'M';
            newLead.FirstName = 'Testing';
            newLead.MiddleName = 'A';
            newLead.LastName = 'New Lead';
            newLead.Street = '822 SW Testing Ave';
            newLead.StateCode = 'OK';
            newLead.PostalCode = '74234';
            newLead.City = 'Tulsa';
            newLead.CountryCode = 'US';
            newLead.Phone = '9239239231';
            newLead.Email = 'testnewlead1@test.com';
            
            Lead newLead2 = new Lead();
            newLead2.Agency__c = agency.Id;
            newLead2.Gender__c = 'M';
            newLead2.FirstName = 'Testing';
            newLead2.LastName = 'New Lead';
            newLead2.CDHID__c = '9844131';
            newLead2.Phone = '9239299231';
            newLead2.Email = 'testnewlead2@test.com';
            insert newLead2;
            
            String selectedRow = '{"address":"234 Dhanush Rd Bartlesville, OK 74006","age":30,"cdhID":"9844131","fName":"Testing","isProducerAssociated":true,"lName":"New Lead","partyLevelCode":"LEAD","partyType":"isLeadt","primaryState":"OK"}';

            Test.startTest();
            String returnResponse = AmFam_NewLeadController.selectLeadPartyMatch(selectedRow, newLead, false);
            Test.stopTest();
            System.assert(returnResponse == newLead.id);
        }
    }
    
    @isTest
    static void testcreateUpdateLead(){
        
        User u = [Select id, email,lastname from User where lastname = 'TestRep' LIMIT 1];
        
        wsPartyManage.lpeCallout = false;
        system.runAs(u) {
            
            Account agency = new Account();
            agency.Name = 'Test Agency';
            agency.Producer_ID__c = '12345';
            agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
            insert agency;
            
            Lead newLead = new Lead();
            newLead.Agency__c = agency.Id;
            newLead.Gender__c = 'M';
            newLead.FirstName = 'Testing';
            newLead.MiddleName = 'A';
            newLead.LastName = 'New Lead';
            newLead.Street = '822 SW Testing Ave';
            newLead.StateCode = 'OK';
            newLead.PostalCode = '74234';
            newLead.City = 'Tulsa';
            newLead.CountryCode = 'US';
            newLead.Phone = '9239239231';
            newLead.Email = 'testnewlead1@test.com';
            
            Lead newLead2 = new Lead();
            newLead2.Agency__c = agency.Id;
            newLead2.Gender__c = 'M';
            newLead2.FirstName = 'Testss';
            newLead2.LastName = 'Lead';
            newLead2.Phone = '9239889231';
            newLead2.Email = 'testness2@test.com';
            insert newLead2;
            
            String selectedRow = '[{"addressLine1":"34 Se Jennings Dr","censusBlockGroup":"3","censusTractNumber":"000600","city":"Bartlesville","Code1ReturnIndicator":"OVERRIDE","countryIdentifier":"USA","deliveryPointValidationCommercialMailreceivingCode":"Unconfirmed","deliveryPointValidationMatchIndicator":"Unable to confirm delivery point","fipsCountyNumber":"147","fipsStateNumber":"40","geoMatchLevelCode":"6","latitudeNumber":"+36.730700","longitudeNumber":"-95.919700","shortCity":"Bartlesville","stateCode":"OK","zip5Code":"74006"}]';

            Test.startTest();
            AmFam_PageLayoutForm.PageLayout getpagelayoutMedata = AmFam_NewLeadController.getPageLayoutMetadata('Lead-Agent Lead Layout'); 
            Id leadId = AmFam_NewLeadController.createLead(newLead, selectedRow);
            Lead lead = AmFam_NewLeadController.updateLeadRecord(newLead2, selectedRow, newLead2.id);
            Test.stopTest();
            System.assert(getpagelayoutMedata != null);
            System.assert(leadId != null);
            System.assert(lead.city == 'Bartlesville');
        }
    }
    
    
}