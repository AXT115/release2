public with sharing class AmFam_ContactPointEmailService {
    public static void updateHouseholdPrimaryEmail(List<ContactPointEmail> emails,Map<Id,ContactPointEmail> emailsMap)
    {
        System.debug('updateHouseholdPrimaryEmail');
        Id householdAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IndustriesHousehold').getRecordTypeId();        

        //cull ones marked primary && account (need to use Id prefix 001)
        Map<Id,ContactPointEmail> acctEmailMap = new Map<Id,ContactPointEmail>();
        for (ContactPointEmail email : emails)
        {
            String parentIdString = email.ParentId;
            if (email.IsPrimary == true && parentIdString.startsWith('001'))
            {
                //there could be more than one for a parent, but going with a last in approach
                acctEmailMap.put(email.ParentId,email);
            }
        }

        System.debug('ACCTEMAILMAP:'+acctEmailMap);
        //get the Accounts reference by the Map
        List<Account> accounts = [SELECT Id,Household_Email__c FROM Account WHERE Id in :acctEmailMap.keyset() AND
                                                        Primary_Contact__pc = true];
        Set<Id> acctIds = new Set<Id>();
        for (Account acct : accounts)
        {
            acctIds.add(acct.Id);
        }                                                        
        
        List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId in :acctIds];                                                
        Set<Id> contactIds = new Set<Id>();
        for (Contact contact : contacts)
        {
            contactIds.add(contact.Id);
        }   
        
        
        //now need to get ACR where Contact is the account.contact && account record type = Household
        //that will give me the HH account ids
        List<AccountContactRelation> relations = [SELECT Id,AccountId,ContactId,Contact.AccountId 
                                                    FROM AccountContactRelation
                                                    WHERE Account.RecordTypeId = :householdAccountRTId AND
                                                        ContactId in :contactIds];                                                    

        System.debug('ACR:'+relations);

        List<Account> updatedAccts = new List<Account>();
        for (AccountContactRelation rel : relations)
        {
            //not sure if supposed to do anything with AreaCode
            Account updatedAcct = new Account();
            updatedAcct.Id = rel.AccountId;
            updatedAcct.Household_Email__c = acctEmailMap.get(rel.Contact.AccountId).EmailAddress;
            updatedAccts.add(updatedAcct);
        }
        if (!updatedAccts.isEmpty())
        {
            update updatedAccts;
        }
    }

    // This method is used to map the CDH response for Party Emails to ContactPointEmail records.
    public static List<ContactPointEmail> mapCDHResponse(String accountId, List<wsPartyManageParty.PartyEmailType> responseEmailList) {
        List<ContactPointEmail> cpeList = new List<ContactPointEmail>();
        for (wsPartyManageParty.PartyEmailType e : responseEmailList) {
            for (wsPartyManageParty.PartyEmailUsageAndDescriptionType usage : e.PartyEmailUsageAndDescription) {
                ContactPointEmail cpe = new ContactPointEmail();
                cpe.ParentId = accountId;
                cpe.IsPrimary = e.TrustManagedPrimaryIndicator == 'Y' ? true : false;
                cpe.EmailAddress = e.Email.emailAddressText;
                cpe.Special_Instruction__c = e.specialInstructions;
                cpe.UsageType = usage.PartyEmailUsage;
                cpe.Usage_Type_Comment__c = usage.otherDescription;
                cpeList.add(cpe);
            }
        }
        return cpeList;
    }
}