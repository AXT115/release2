@isTest
public class AmFam_CleanupBatchTestClass {
    
    static testmethod void testPartyRetrieveBatch() {
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User u = new User (alias = 'srep', email='test1246@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                    lastname='TestRep',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1246@xyz.com.test',
                                    AmFam_User_ID__c = '12345'
                                    );
        AmFam_UserService.callProducerAPI = false;
        insert u;
        
        CleanupRecord__c rec = new CleanupRecord__c();
        rec.Identifier__c = '9966055';
        rec.Level_Of_Details__c = 'ALL';    
        rec.Type__c = 'Party';
        insert rec; 
        
        System.runAs(u) {
            
        Account accRec = new Account();
        accRec.FirstName = 'Green';
        accRec.LastName = 'Hen';
        accRec.CDHID__c ='9966055';
        accRec.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();
        insert accRec;
        
        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');
        
        Contact con = [select id from Contact Limit 1];
        
        AccountContactRelation rel = new AccountContactRelation();
        rel.AccountId = household.Id;
        rel.ContactId = con.id;
        rel.Roles = 'Client';
        rel.IsActive = true;
        insert rel;
        
        ContactPointEmail cpEmail = new ContactPointEmail();
        cpEmail.EmailAddress = 'testuser@testmail.com';
        cpEmail.ParentId = accRec.Id;
        cpEmail.UsageType = 'HOME';
        cpEmail.IsPrimary = true;
        insert cpEmail;
        
        ContactPointPhone cpPhone = new ContactPointPhone();
        cpPhone.TelephoneNumber = '9873334444';
        cpPhone.ParentId = accRec.Id;
        cpPhone.IsPrimary = true;
        cpPhone.UsageType = 'HOME';
        cpPhone.RecordTypeId = AmFam_Utility.getContactPointPhonePersonRecordType();
        insert cpPhone;
        
        Schema.Location loc = new Schema.Location();  
        loc.LocationType = 'M';
        loc.Name = 'BBB';
        insert loc;

        Schema.Address addr = new Schema.Address();
        addr.ParentId = loc.Id; 
        addr.Street = accRec.Id+loc.LocationType+'CHANGEDB';
        addr.AddressType= loc.LocationType;
        addr.LocationType = loc.LocationType;
        insert addr;
        
        AssociatedLocation al = new AssociatedLocation();
        al.LocationId = loc.Id;
        al.PersonAccount__c = accRec.Id;
        al.ParentRecordId = accRec.Id;
        al.Type = loc.LocationType;
        insert al;
            
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new wsPartySearchServiceCalloutHttpMock()); 
            wsPartySearchServiceCallout.PartySearchServiceSOAP partySearchService = new wsPartySearchServiceCallout.PartySearchServiceSOAP();
            AmFam_CleanupRecordBatchClass batch = new AmFam_CleanupRecordBatchClass();
            Id jobid= Database.executeBatch(batch);
        Test.stopTest();
            
        Account acc = [select id, CDHID__c from Account where CDHID__c = '9966055' Limit 1];
        List<ContactPointPhone> phone = [select id, ParentId from ContactPointPhone where ParentId =: acc.id];
        List<ContactPointEmail> email = [select id, ParentId from ContactPointEmail where ParentId =: acc.id];
        List<AssociatedLocation> associated = [select id, ParentRecordId from AssociatedLocation where ParentRecordId =: acc.id];
        system.assert(jobid != null);
        system.assert(acc != null);
        system.assert(phone.size()>0);
        system.assert(email.size()>0);
        system.assert(associated.size()>0);
        }
    }
    
    
     static testmethod void testPartyRetrieveBatchHH() {
        
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User u = new User (alias = 'srep', email='test1246@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                    lastname='TestRep',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1246@xyz.com.test',
                                    AmFam_User_ID__c = '12345'
                                    );
        AmFam_UserService.callProducerAPI = false;
        insert u;
        
        CleanupRecord__c rec = new CleanupRecord__c();
        rec.Identifier__c = '8502509';
        rec.Level_Of_Details__c = 'ALL';    
        rec.Type__c = 'Household';
        insert rec; 
         
        Test.setMock(HttpCalloutMock.class, new wsHouseholdServiceCalloutHttpMock());
        System.runAs(u) {
            Test.startTest();
            AmFam_CleanupRecordBatchClassForHH batch = new AmFam_CleanupRecordBatchClassForHH();
            Id jobid= Database.executeBatch(batch);
            Test.stopTest();
            Account acc = [select id, CDHID__c from Account where CDHID__c = '8502509' Limit 1];
            system.assert(jobid != null);
            system.assert(acc != null);
            
        }
    }

}