public class AmFam_PromoteAndCreateQuoteController {
    private static Set<String> errorsToIgnore = new Set<String>{'D-111'};
    
    public class promoteWrapper {
        @AuraEnabled public List<String> errors;
        @AuraEnabled public Lead leadRecord;
        @AuraEnabled public Map<String, String> urlMap;
        @AuraEnabled public Boolean showOverride;
        @AuraEnabled public List<wsPartyManageParty.AddressType> address;
        @AuraEnabled public List<matchWrapper> partyMatches;
        
        public promoteWrapper(List<String> err, Lead rec, Map<String, String> urlMap, Boolean oFlag, List<wsPartyManageParty.AddressType> addr, List<matchWrapper> matches) {
            this.errors = err;
            this.leadRecord = rec;
            this.urlMap = urlMap;
            this.showOverride = oFlag;
            this.address = addr;
            this.partyMatches = matches;
        }
    }
    
    public class matchWrapper {
        @AuraEnabled public String name;
        @AuraEnabled public String address;
        @AuraEnabled public String yearOfBirth;
        @AuraEnabled public String partyLevel;
        
        public matchWrapper(String n, String addr, String yob, String pl) {
            this.name = n;
            this.address = addr;
            this.yearOfBirth = yob;
            this.partyLevel = pl;
        }
    }
    
    @AuraEnabled
    public static promoteWrapper promoteEntrant(String leadID, Boolean forceAddress, String addrData, string selectedAddress) {
        system.debug(addrData);
        wsPartyManageParty.AddressType addrType = null;
        if (addrData != null) {
            addrType = (wsPartyManageParty.AddressType) JSON.deserialize(addrData, wsPartyManageParty.AddressType.class);
        }
        Boolean retVal = false;
        List<wsPartyManageParty.AddressType> addrWrapper = null;
        List<matchWrapper> matches = null;
        Lead rec = [SELECT Id, Company, FirstName, MiddleName, LastName, Name, Gender__c, Marital_Status__c, Date_of_Birth__c, Partner__c,
                            Drivers_License_Number__c,Driver_s_License_State__c, Drivers_License_Issue_Month__c, Drivers_License_Issue_Year__c,
                            CountryCode, Street, City, StateCode, PostalCode, Email, Email_Usage__c, Agency__r.Producer_Id__c,
                            Phone, MobilePhone, Phone_Usage__c, CDHID__c, Party_Version__c, Party_Level__c, Contact_CDHID__c, Lines_of_Business__c,
                            Agency__r.Is_Corporate_Account__c, Stage__c, Source_System_Key__c, Residence_Business__c, Mailing__c, Residence_Business_2__c, Residence_Business_3__c, Mailing_2__c, Mailing_3__c
                    FROM Lead
                    WHERE Id =: leadID LIMIT 1];

        List<NamedCredential> namedCredentials = [SELECT DeveloperName, Endpoint FROM NamedCredential WHERE DeveloperName in ('Sales_Portal', 'Life_Cornerstone')];
        Map<String, String> urlMap = new Map<String, String>();
       
        for (NamedCredential namedCredential : namedCredentials){
           urlMap.put(namedCredential.DeveloperName, namedCredential.Endpoint);
        }

        Boolean flag = false;
        if (rec.Contact_CDHID__c != null) { // Already promoted, just redirect
            if (rec.Stage__c != 'Quote') {
                rec.Stage__c = 'Quote';
                update rec;
            }
            return new promoteWrapper(null, rec, urlMap, flag, addrWrapper, matches);
        } else {
            // Lead needs CDH ID, Party Level of Entrant, and a complete address in order to promote
            if (rec.CDHID__c != null && rec.Party_Level__c == 'Entrant' && rec.Street != null && rec.City != null && rec.StateCode != null 
            && rec.PostalCode != null && rec.CountryCode != null) {
                wsPartyManageService.PromotePartyResultType resp = wsPartyManage.promoteEntrant(rec, forceAddress, addrType, null);
                
                Boolean exactMatch = false;
                Boolean contactMatch = false;
                // Check for Party Matches
                if (resp.PartyMatch != null) {
                    //matches = resp.PartyMatch;
                    matches = new List<matchWrapper>();
                    for(wsPartyManageService.PartyMatchType pmType : resp.PartyMatch) {
                        String name = '';
                        String yearOfBirth = '';
                        String address = '';
                        String partyLevel = pmType.Party.PartyLevelCode;
                        if (pmType.Party.PartyLevelCode == 'CONTACT') {
                            wsPartyManageParty.AddressType partyMatchAddress = pmType.Party.Contact.PartyAddress[0].Address;
                            address = partyMatchAddress.addressLine1;
                            if (partyMatchAddress.addressLine2 != null) {
                                address += '\n' + partyMatchAddress.addressLine2;
                            }
                            address += '\n' + partyMatchAddress.city + ', ' + partyMatchAddress.stateCode + ' ' + partyMatchAddress.zip5Code;
                            name = pmType.Party.Contact.Person.PersonDemographicInfo.Name.firstName;
                            if (pmType.Party.Contact.Person.PersonDemographicInfo.Name.middleName != null) {
                                name += ' ' + pmType.Party.Contact.Person.PersonDemographicInfo.Name.middleName;
                            }
                            if (pmType.Party.Contact.Person.PersonDemographicInfo.Name.lastName != null) {
                                name += ' ' + pmType.Party.Contact.Person.PersonDemographicInfo.Name.lastName;
                            }
                            if (pmType.Party.Contact.Person.PersonDemographicInfo.birthYear != null) {
                                yearOfBirth = pmType.Party.Contact.Person.PersonDemographicInfo.birthYear;
                            }
                        } else { // Entrant
                            wsPartyManageParty.AddressType partyMatchAddress = pmType.Party.Entrant.PartyAddress[0].Address;
                            address = partyMatchAddress.addressLine1;
                            if (partyMatchAddress.addressLine2 != null) {
                                address += '\n' + partyMatchAddress.addressLine2;
                            }
                            address += '\n' + partyMatchAddress.city + ', ' + partyMatchAddress.stateCode + ' ' + partyMatchAddress.zip5Code;
                            name = pmType.Party.Entrant.EntrantDemographicInfo.PersonName.firstName;
                            if (pmType.Party.Entrant.EntrantDemographicInfo.PersonName.middleName != null) {
                                name += ' ' + pmType.Party.Entrant.EntrantDemographicInfo.PersonName.middleName;
                            }
                            if (pmType.Party.Entrant.EntrantDemographicInfo.PersonName.lastName != null) {
                                name += ' ' + pmType.Party.Entrant.EntrantDemographicInfo.PersonName.lastName;
                            }
                            if (pmType.Party.Entrant.EntrantDemographicInfo.birthYear != null) {
                                yearOfBirth = pmType.Party.Entrant.EntrantDemographicInfo.birthYear;
                            }
                        }
                        matches.add(new matchWrapper(name, address, yearOfBirth, partyLevel));
                        
                        if(pmType.Match == 'EXACT') {
                            // Exact Match Found
                            exactMatch = true;
                        }
                        if(pmType.Party.PartyLevelCode == 'CONTACT') {
                            // Contact Match Found
                            contactMatch = true;
                        }
                    }
                }
                
                List<String> errorList = new List<String>();
                if(exactMatch || contactMatch) {
                    errorList.add('We found potential duplicates. Please review the information and either make corrections or search for the contact in Apex.');
                } else {
                    if (resp.Errors != null) {
                        for (wsPartyManageParty.ErrorType errType : resp.Errors.Error) {
                            if (!errorsToIgnore.contains(errType.errorCode)) {
                                errorList.add(errType.errorCode + ' on field ' + errType.field + ': ' + errType.errorMessage + ' (' + errType.key + ')');
                            }
                        }
                    }
                }
                
                if (resp.Code1Result != null) {
                    for (wsPartyManageParty.Code1ResultType result : resp.Code1Result) {
                        flag = true;
                        String errorMsg = '';
                        wsPartyManageParty.Code1WrapperType wrapper = result.Code1Wrapper;
                        addrWrapper = wrapper.AddressesReturnedFromCode1;
                        errorMsg = 'We could not validate the address on this record. Please select an option below.';
                        errorList.add(errorMsg);
                    }
                }
                
                if(errorList.size() > 0) {
                    return new promoteWrapper(errorList, rec, null, flag, addrWrapper, matches); // Display Errors
                } else {
                    // We have an override selected, update the data in SF
                    if (addrType != null) {
                        rec.Street = addrType.addressLine1;
                        if (addrType.addressLine2 != null) {
                            rec.Street += '\n' + addrType.addressLine2;
                        }
                        rec.City = addrType.city;
                        rec.StateCode = addrType.stateCode;
                        rec.PostalCode = addrType.zip5Code;
                        rec.CountryCode = addrType.countryIdentifier;
                    }
                    rec.Contact_CDHID__c = resp.PromotedPartyResponse.partyIdentifier;
                    rec.Party_Level__c = (resp.PromotedPartyResponse.PartyLevelCode.toLowerCase()).capitalize();
                    rec.Party_Version__c = resp.PromotedPartyResponse.partyVersion;
                    rec.Stage__c = 'Quote';
                    rec.Errors__c = '';
                    update rec;  // Update lead with promoted return data
                    return new promoteWrapper(null, rec,  urlMap, flag, addrWrapper, matches);  // Redirect
                }
            } else {
                // Lead is not ready to be promoted (needs cdh id)
                return new promoteWrapper(new List<String>{'The lead has missing or incomplete information required to quote. Please review and update the lead and try again.'}, rec, null, flag, addrWrapper, matches); // Display base error
            }
        }
    }
}