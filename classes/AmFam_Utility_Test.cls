@isTest
public without sharing class AmFam_Utility_Test {


    static testMethod void testGetAddressRecord() {

        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];
        Account agencyAccount = insertAgencyAccount('Agency Account');


        Schema.Location newLocation = new Schema.Location();
        newLocation.LocationType = 'M';
        newLocation.Name = 'Test add1';
        insert newLocation;

        Schema.Address add1 = new Schema.Address();
        add1.Street = '1940 Maryland Avenue';
        add1.City = 'Tampa';
        add1.StateCode = 'FL';
        add1.CountryCode = 'US';
        add1.PostalCode = '33602';
        add1.ParentId = newLocation.Id;
        add1.LocationType = 'Location';

        insert add1;



        Test.startTest();
        
        System.runAs(objUser){

            Schema.Address address1 = AmFam_Utility.getAddressRecord( '1940 maryland AVENUE',  'tampa', '33602',  'fl', 'us');
            Schema.Address address2 = AmFam_Utility.getAddressRecord( '1940 maryland AV',  'tampa', '33602',  'fl', 'us');
            Schema.Address address3 = AmFam_Utility.getAddressRecord( '2163 Nelson Street',  'Kasabonika', 'P0V 1Y0',  'on', 'CA');

            System.debug('### address: ' + address1);
            System.debug('### address: ' + address2);
            System.debug('### address: ' + address3);

            System.assertEquals(add1.Id, address1.Id);
            if (address1.Id == add1.Id) {
                System.debug('### same address1: ' + address1.Id );
            }
            else {
                System.debug('### different address1: ' + address1.Id  + '-' + add1.Id);

            }

            System.assertNotEquals(add1.Id, address2.Id);
            if (address2.Id == add1.Id) {
                System.debug('### same address2: ' + address2.Id );
            }
            else {
                System.debug('### different address2: ' + address2.Id  + '-' + add1.Id);

            }



        }

        Test.stopTest();

    }




    private static Account insertAgencyAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        return objAcc;
    }


}