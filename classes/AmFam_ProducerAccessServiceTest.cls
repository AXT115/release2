@istest
public with sharing class AmFam_ProducerAccessServiceTest {

    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }

    static testMethod public void testProducerAccessForExistingAccount() {
        
        AmFam_UserService.callProducerAPI = false;
        
        Account agency = new Account();
        agency.Name = 'Acorn';
        agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
        agency.Producer_ID__c = '989899';
        insert agency;
        
        Account personAccount = new Account();
        personAccount.Agency__c = agency.id;
        personAccount.FirstName = 'Aaron';
        personAccount.LastName = 'Finch';
        personAccount.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();
        insert personAccount;

        wsPartyManage.lpeCallout = false;
        
        Lead l = new Lead();
        l.Agency__c = agency.Id;
        l.FirstName = 'Aaron';
        l.LastName = 'Finch';
        l.Phone = '1231231234';
        l.Phone_Usage__c = 'HOME';
        l.Street = '132 Main Street';
        l.City = 'Anytown';
        l.StateCode = 'WI';
        l.CountryCode = 'US';
        l.PostalCode = '12345';
        l.Residence_Business__c = 'PRM';
        l.Mailing__c = 'PRM';
        l.Marital_Status__c = 'M';
        l.Gender__c = 'M';
        l.LeadSource = 'Amfam.com';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        insert l;

        
        Test.startTest();
        
        AmFam_ProducerAccessService.createProducerAccessRecords(null, l, personAccount, 'EXACTMATCH');
        List<ProducerAccess__c> result = [SELECT Id, Reason__c, Producer_Identifier__c, RelatedId__c FROM ProducerAccess__c where Producer_Identifier__c = '989899' AND RelatedId__c =: personAccount.id ];
        system.assert(result.size()==1);
        system.assert(result[0].Reason__c == 'ENTRY;EXACTMATCH');
        
        Test.stopTest();
     }
    
    
     static testMethod public void testProducerAccessNew() {
        
        AmFam_UserService.callProducerAPI = false;
        
        Account agency = new Account();
        agency.Name = 'Acorn';
        agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
        agency.Producer_ID__c = '989899';
        insert agency;
        
        Account agency2 = new Account();
        agency2.Name = 'Acorn';
        agency2.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
        agency2.Producer_ID__c = '989889';
        insert agency2;
        
        Account personAccount = new Account();
        personAccount.Agency__c = agency.id;
        personAccount.FirstName = 'Aaron';
        personAccount.LastName = 'Finch';
        personAccount.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();
        insert personAccount;

        wsPartyManage.lpeCallout = false;
        
        Lead lead2 = new Lead();
        lead2.Agency__c = agency2.Id;
        lead2.FirstName = 'Reno';
        lead2.LastName = 'Thomas';
        lead2.Phone = '7895631234';
        lead2.Phone_Usage__c = 'HOME';
        lead2.Street = '1532 West Street';
        lead2.City = 'Anytown';
        lead2.StateCode = 'WI';
        lead2.CountryCode = 'US';
        lead2.PostalCode = '12345';
        lead2.Residence_Business__c = 'PRM';
        lead2.Mailing__c = 'PRM';
        lead2.Marital_Status__c = 'M';
        lead2.Gender__c = 'M';
        lead2.LeadSource = 'Amfam.com';
        lead2.Lines_of_Business__c = 'Personal Vehicle;Property';
        insert lead2;
        
        Test.startTest();
        
         AmFam_ProducerAccessService.createProducerAccessRecords(null, lead2, personAccount, 'EXACTMATCH');
        List<ProducerAccess__c> result = [SELECT Id, Reason__c, Producer_Identifier__c, RelatedId__c FROM ProducerAccess__c where Producer_Identifier__c = '989889' ];
        system.assert(result.size()==1);
        system.assert(result[0].Reason__c == 'EXACTMATCH');
        
        Test.stopTest();
     }
}