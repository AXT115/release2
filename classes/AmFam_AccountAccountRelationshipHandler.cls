/**
*   @description: trigger handler for Account-Account Relationship object. Note: this should be the only place
*	where ALL Account-Account Relationship related trigger logic should be implemented.
*   @author: Anbu Chinnathambi
*   @date: 09/12/2021
*   @group: Trigger Handler
*/

public without sharing class AmFam_AccountAccountRelationshipHandler {
    
    /*
    public void OnBeforeInsert (List<FinServ__AccountAccountRelation__c> newRecords) {
    }
    */

    public void OnAfterInsert (List<FinServ__AccountAccountRelation__c> newRecords, Map<ID, FinServ__AccountAccountRelation__c> newRecordsMap) {

       AmFam_AccountAccountRelationshipService.deleteDuplicateAccountAccountRel(newRecords, newRecordsMap);
    }

    /*
    public void OnAfterUpdate (List<FinServ__AccountAccountRelation__c> newRecords,
                               List<FinServ__AccountAccountRelation__c> oldRecords,
                               Map<ID, FinServ__AccountAccountRelation__c> newRecordsMap,
                               Map<ID, FinServ__AccountAccountRelation__c> oldRecordsMap) {

    }


    public void OnBeforeUpdate(List<FinServ__AccountAccountRelation__c> newRecord,
                               List<FinServ__AccountAccountRelation__c> oldRecord,
                               Map<ID, FinServ__AccountAccountRelation__c> newRecordsMap,
                               Map<ID, FinServ__AccountAccountRelation__c> oldRecordsMap) {

    }

    public void OnBeforeDelete(List<FinServ__AccountAccountRelation__c> oldRecords,
    						   Map<ID, FinServ__AccountAccountRelation__c> oldRecordsMap) {

    }
    
    
    public void OnAfterDelete(List<FinServ__AccountAccountRelation__c> oldRecords,
    						   Map<ID, FinServ__AccountAccountRelation__c> oldRecordsMap) {
    }*/



}