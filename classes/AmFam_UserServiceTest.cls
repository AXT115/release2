@IsTest
public with sharing class AmFam_UserServiceTest {
   
    @testSetup static void setup() {
        Account agencyAccount = insertAgencyAccount('Agency Account 1' , '99999');
        Account agentAccount = insertAgentAccount('Agent Account 1');
        Account agencyAccount2 = insertAgencyAccount('Agency Account 2' , '77230');
		Account agentAccount2 = insertAgentAccount('Agent Account 2');
    }

    @IsTest
    public static void testUserAccessToAccountLead()
    {
        AmFam_UserTriggerHandler.disableTriggerHandler = true;

        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        wsPartyManage.lpeCallout = false;

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Sales & Service Operations Rep'];
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345'
                                );
        agentUser.Producer_ID__c = '77230';
        insert agentUser;

        List<User> userRecs = new List<User>{agentUser};
        Test.startTest();
        AmFam_UserService.userAccessToAccountLead(userRecs);
        Test.stopTest();

        //this works but ManageGroupMembershipBatch deletes it
        Group g = [SELECT Id FROM Group WHERE DeveloperName = 'S_SO_Users'];
        List<GroupMember> groupMembers = [SELECT Id FROM GroupMember WHERE GroupId =: g.Id AND UserOrGroupId =: agentUser.Id LIMIT 1];
        System.debug('CONFIRM:'+groupMembers);
        System.assert(groupMembers.size() == 1);
}

    @IsTest
    public static void testUpdatePermissionSetGroupAssignments()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        AmFam_UserTriggerHandler.disableTriggerHandler = true;

        wsPartyManage.lpeCallout = false;
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        Profile p2 = [SELECT Id, Name FROM Profile WHERE Name = 'Agent CSR'];
                                    
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        agentUser.Producer_ID__c = '77230';
        insert agentUser;

        User userClone = agentUser.clone(true);
        userClone.profileid = null;

        List<User> users = new List<User>();
    //    Map<Id,User> userMap = new Map<Id,User>();

        users.add(agentUser);
    //    userMap.put(agentUser.Id,userClone);

        List<PermissionSetAssignment> perms = [Select Id from PermissionSetAssignment];

        Test.startTest();
        AmFam_UserService.updatePermissionSetGroupAssignmentsForUser(users);

        perms = [Select Id,AssigneeId,PermissionSetGroupId,PermissionSetGroup.MasterLabel from PermissionSetAssignment WHERE AssigneeId = :agentUser.Id];
        System.debug(perms);
        System.assert(perms.size() > 0);

        agentUser.profileId = p2.Id;
        AmFam_UserService.updatePermissionSetGroupAssignmentsForUser(users);
        perms = [Select Id,AssigneeId,PermissionSetGroupId,PermissionSetGroup.MasterLabel from PermissionSetAssignment WHERE AssigneeId = :agentUser.Id];
        System.debug(perms);
        System.assert(perms.size() > 0);

        Test.stopTest();

    }

    @IsTest
    public static void testProducerUpdate()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true,'3'));
        AmFam_UserTriggerHandler.disableTriggerHandler = true;

        Account acct = AmFam_TestDataFactory.insertAgencyAccount('Pork N. Beans Agency','00001');
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];

        Contact contact = new Contact(FirstName='Melvin',LastName='Suggs');
        insert contact;


        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        agentUser.Producer_ID__c = '77230';
        insert agentUser;

        Producer producer = new Producer();
        producer.name = '99999';
        producer.InternalUserId = agentUser.Id;
        producer.ContactId = contact.Id;
        insert producer;

        List<Id> users = new List<Id>();
        users.add(agentUser.Id);

        Test.startTest();
        AmFam_UserService.producerUpdate(users);
        Test.stopTest();
        List<Producer> afterProds = [SELECT Id,AccountId,Name,ContactId,Producer_id__c FROM Producer];

        //existing should be deleted & 1 new created
        System.assert(afterProds.size() == 1 && afterProds[0].Name != '99999');
    }

    @IsTest
    public static void testProducerUpdateSSOProfile()
    {
        //returns a BoB but no producers
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true,'888'));
        AmFam_UserTriggerHandler.disableTriggerHandler = true;

        Account acct = AmFam_TestDataFactory.insertAgencyAccount('Pork N. Beans Agency','52481');
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Sales & Service Operations Rep'];

        Contact contact = new Contact(FirstName='Melvin',LastName='Suggs');
        insert contact;


        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        //agentUser.Producer_ID__c = '77230';
        insert agentUser;

        Producer producer = new Producer();
        producer.name = '99999';
        producer.InternalUserId = agentUser.Id;
        producer.ContactId = contact.Id;
        insert producer;

        List<Id> users = new List<Id>();
        users.add(agentUser.Id);

        Test.startTest();
        AmFam_UserService.producerUpdate(users);
        Test.stopTest();
        List<Producer> afterProds = [SELECT Id,AccountId,Name,ContactId,Producer_id__c,Type FROM Producer];

        //existing should be deleted & 1 new created
        System.assert(afterProds.size() == 1 && afterProds[0].Name == '52481' && afterProds[0].Type == 'SSO');
    }

    @IsTest
    public static void testProducerUpdateNonSSOProfile()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true,'999'));
        AmFam_UserTriggerHandler.disableTriggerHandler = true;

        Account acct = AmFam_TestDataFactory.insertAgencyAccount('Pork N. Beans Agency','55555');
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Agent CSR'];

        Contact contact = new Contact(FirstName='Melvin',LastName='Suggs');
        insert contact;


        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        agentUser.Producer_ID__c = '55555';
        insert agentUser;

        Producer producer = new Producer();
        producer.name = '99999';
        producer.InternalUserId = agentUser.Id;
        producer.ContactId = contact.Id;
        insert producer;

        List<Id> users = new List<Id>();
        users.add(agentUser.Id);

        Test.startTest();
        AmFam_UserService.producerUpdate(users);
        Test.stopTest();
        List<Producer> afterProds = [SELECT Id,AccountId,Name,ContactId,Producer_id__c,Type FROM Producer];

        //existing should be deleted & 1 new created
        //System.assert(afterProds.size() == 1 && afterProds[0].Name == '55555' && afterProds[0].Type == 'CSR');
    }

    @IsTest
    public static void testProducerUpdateNoProducer()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true,'999'));
        AmFam_UserTriggerHandler.disableTriggerHandler = true;

        Account acct = AmFam_TestDataFactory.insertAgencyAccount('Pork N. Beans Agency','55555');
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'Agent CSR'];

        Contact contact = new Contact(FirstName='Melvin',LastName='Suggs');
        insert contact;


        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        insert agentUser;

        Producer producer = new Producer();
        producer.name = '99999';
        producer.InternalUserId = agentUser.Id;
        producer.ContactId = contact.Id;
        insert producer;

        List<Id> users = new List<Id>();
        users.add(agentUser.Id);

        Test.startTest();
        AmFam_UserService.producerUpdate(users);
        Test.stopTest();
        List<Producer> afterProds = [SELECT Id,AccountId,Name,ContactId,Producer_id__c,Type FROM Producer];

        //existing should be deleted & 1 new created
        //assert(afterProds.size() == 1 && afterProds[0].Name == 'P-'+agentUser.Id && afterProds[0].Type == 'CSR');
    }

    @IsTest
    public static void testUpdateUserAgencyAccount()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        AmFam_UserTriggerHandler.disableTriggerHandler = true;
        String producerId = '55667';

        Account acct = AmFam_TestDataFactory.insertAgencyAccount('Pork N. Beans Agency',producerId);
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];

        Contact contact = new Contact(FirstName='Melvin',LastName='Suggs');
        insert contact;


        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        agentUser.Producer_ID__c = producerId;
        insert agentUser;

        Test.startTest();
        AmFam_UserService.updateUserAgencyAccount(producerId);
        Test.stopTest();

        Account agAcct = [SELECT Id,Name,Goes_By_Name__c,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountryCode 
                            FROM Account
                            WHERE Id = :acct.Id];
        System.debug(agAcct);
        System.assert(agAcct.ShippingStreet != null);                            

    }

    @IsTest
    public static void testSDLProducerUpdate()
    {
        Id indivRT = AmFam_Utility.getAccountIndividualRecordType();
        Profile sdlProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Sales District Leader'];

        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=sdlProfile.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        agentUser.Producer_ID__c = '99999';
        insert agentUser;

        Account indivAccount = [SELECT Id,Producer_ID__c FROM Account WHERE Producer_ID__c = :agentUser.Producer_ID__c LIMIT 1];
        //create Indiv accounts w/ prodids in what I pass
        Set<String> producerIds = new Set<String>{'11111','22222','33333'};

        List<Account> targetAccounts = new List<Account>();
        for (String prodId : producerIds)
        {
            Account acct = AmFam_TestDataFactory.insertIndividualAccount('Firsty McFirstface', 'P-'+prodId);
        }

        //setup some data to go away
        Account acctx = AmFam_TestDataFactory.insertIndividualAccount('Doomy McDoomface', 'P-88888');

        Producer producer = new Producer();
        producer.name = acctx.Producer_ID__c;
        producer.InternalUserId = agentUser.Id;
        producer.AccountId = acctx.Id;
        producer.Type = 'SDL';
        insert producer; 
        System.debug('XPROD:'+producer);

        ProducerAccess__c prodAccess = new ProducerAccess__c();
        prodAccess.RelatedId__c = indivAccount.Id;
        prodAccess.Producer_Identifier__c = acctx.Producer_ID__c;
        prodAccess.Reason__c = 'ENTRY';
        insert prodAccess;
        System.debug('XPA:'+prodAccess);

        Test.startTest();
        AmFam_UserService.SDLProducerResult result = AmFam_UserService.sdlProducerUpdate(agentUser,indivAccount,producerIds);
        Test.stopTest();

        System.debug(result.newProducers);
        System.debug(result.newProducerAccess);
        System.debug(result.deleteProducers);
        System.debug(result.deleteProducerAccess);

        //check we have expected results
        System.assert(result.newProducers.size() == 3);
        System.assert(result.newProducerAccess.size() == 3);
        System.assert(result.deleteProducers.size() == 1);
        System.assert(result.deleteProducerAccess.size() == 1);

        //now apply them so we know they are right
        insert result.newProducers;
        insert result.newProducerAccess;
        delete result.deleteProducers;
        delete result.deleteProducerAccess;
    }

    private static Account insertAgencyAccount(String accName, String producerId){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_ID__c = producerId;
        insert objAcc;
        return objAcc;
    }

    private static Account insertAgentAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;

        Contact objContact = new Contact();
        objContact.FirstName = accName;
        objContact.LastName = accName;
        objContact.AccountId = objAcc.Id;
        insert objContact;

        return objAcc;
    }

}