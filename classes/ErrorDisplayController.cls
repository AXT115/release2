public class ErrorDisplayController {
    @AuraEnabled
    public static Lead getLead(String Id){
        Lead l = [SELECT Id, Errors__c, CDHID__c FROM Lead WHERE Id =: Id];
        return l;
    }
}