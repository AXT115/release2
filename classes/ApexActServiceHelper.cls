public without sharing class ApexActServiceHelper {

//Calls out to Apex to create note
//@param Contact CDHID, CreateTime, Subject, Comments, CreatedBy TransactionId
// This is meant to isolate the schema. If this needs to be expanded consider creating a separate class to pass paramaters. 
@future(callout=true)
public static void createApexNote(String cdhId, DateTime createTime, String description, String comments, String createdBy, String transactionId) {
    ApexActService.Act apexActService = new  ApexActService.Act();
    schemaAmfamComXsdMessageApexact.CreateActRequestType request = new schemaAmfamComXsdMessageApexact.CreateActRequestType();
    request.Template = new schemaAmfamComXsdMessageApexact.CreateActTemplateType();
    request.Template.Note = new schemaAmfamComXsdMessageApexact.CreateNoteDetailType();
    
    if(description.length() > 255 ){
       description = description.substring(0, 255);
    }
    request.Template.Note.description = description;
    request.Template.Note.startTime = createTime;
    request.Template.Note.Priority = '3';
    request.Template.Note.comments = comments;
    request.Template.Note.createdBy = createdBy;
    schemaAmfamComXsdMessageApexact.EntityParticipantType entityParticipant = new  schemaAmfamComXsdMessageApexact.EntityParticipantType();
    entityParticipant.customerId = cdhId; 
    request.Template.Note.For_x = entityParticipant;
    
    //Audit
    request.Audit = new schemaAmfamComXsdMessageApexact.AuditType();
    request.Audit.sourceSystem = 'Salesforce';
    request.Audit.transactionId = transactionId;

    System.debug(request);
    schemaAmfamComXsdMessageApexact.CreateActResultType response;
        try {
                response = apexActService.createAct_Http(request);
            } catch (Exception e) {
                System.debug(e);
                AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
                API_Failure_Log__c log = failureLog.createGenericAPIFailure('ApexActService', 'ApexActService', transactionId, e.getMessage());
                if (log != null) {
                    insert log;
                }
            }
}

public static void createApexNoteSync(String cdhId, DateTime createTime, String description, String comments, String createdBy, String transactionId) {
    ApexActService.Act apexActService = new  ApexActService.Act();
    schemaAmfamComXsdMessageApexact.CreateActRequestType request = new schemaAmfamComXsdMessageApexact.CreateActRequestType();
    request.Template = new schemaAmfamComXsdMessageApexact.CreateActTemplateType();
    request.Template.Note = new schemaAmfamComXsdMessageApexact.CreateNoteDetailType();
    
    if(description.length() > 255 ){
       description = description.substring(0, 255);
    }
    request.Template.Note.description = description;
    request.Template.Note.startTime = createTime;
    request.Template.Note.Priority = '3';
    request.Template.Note.comments = comments;
    request.Template.Note.createdBy = createdBy;
    schemaAmfamComXsdMessageApexact.EntityParticipantType entityParticipant = new  schemaAmfamComXsdMessageApexact.EntityParticipantType();
    entityParticipant.customerId = cdhId; 
    request.Template.Note.For_x = entityParticipant;
    
    //Audit
    request.Audit = new schemaAmfamComXsdMessageApexact.AuditType();
    request.Audit.sourceSystem = 'Salesforce';
    request.Audit.transactionId = transactionId;

    System.debug(request);
    schemaAmfamComXsdMessageApexact.CreateActResultType response;
    response = apexActService.createAct_Http(request);
}

//Check if the stage has changed and create notes in Apex
//Called from the Lead Trigger
//@params leadRecs, oldLeadsMap
public static void updateApexNotes(List<Lead> leadRecs, Map<Id,Lead> oldLeadsMap) {
    for(Lead lead : leadRecs) {

        Lead oldLead = oldLeadsMap.get(lead.id);

        if (lead.stage__c == 'Completed' && lead.stage__c != oldLead.stage__c) {
            createChatterFeedApexNote(lead, true);
            createTaskApexNotes(lead, true);
        }        
    }
}

// Creates a single note in Apex based on the chatter feed items
// @param Lead record
public static void createChatterFeedApexNote(Lead lead, boolean future) {
    // Read for feed items
    FeedItem [] feedItems = [SELECT BestCommentId,Body,CommentCount,CreatedBy.Name,CreatedDate,HasContent,HasFeedEntity,HasLink,HasVerifiedComment,
    Id,InsertedById,IsClosed,IsDeleted,IsRichText,LastModifiedDate,LikeCount,LinkUrl,ParentId,RelatedRecordId,Status,SystemModstamp,Title,Type FROM FeedItem
    where ParentId =: Lead.id AND IsRichText =: true];

    // Only create a note if there are chatter feed items
    if( feedItems.isEmpty()) {
        return;
    }

    FeedItem firstFeedItem = feedItems[feedItems.size()-1];
    User feedItemCreator = [SELECT AmFam_User_ID__c FROM User WHERE Id =: firstFeedItem.CreatedById];
    String noteChatterBody = formatChatterBody(feedItems);
    if(future)
    { 
         createApexNote(lead.Contact_CDHID__c, firstFeedItem.CreatedDate, removeHTML(firstFeedItem.body), noteChatterBody, feedItemCreator.AmFam_User_ID__c, lead.id);
    } else {
        createApexNoteSync(lead.Contact_CDHID__c, firstFeedItem.CreatedDate, removeHTML(firstFeedItem.body), noteChatterBody, feedItemCreator.AmFam_User_ID__c, lead.id);
    }
}

// Creates a Note in Apex for each Call log on the lead record
//@param Lead
public static void createTaskApexNotes(Lead lead, boolean future)
{
    Task[] tasks = [SELECT AccountId,ActivityDate,CallDisposition,CallDurationInSeconds,CallObject,CallType,CompletedDateTime,CreatedById,CreatedDate,Description,Id,IsArchived,IsClosed,IsDeleted,IsHighPriority,IsRecurrence,
    IsReminderSet,LastModifiedById,LastModifiedDate,OwnerId,PII_Notice__c,Priority,RecurrenceActivityId,RecurrenceDayOfMonth,RecurrenceDayOfWeekMask,RecurrenceEndDateOnly,
    RecurrenceInstance,RecurrenceInterval,RecurrenceMonthOfYear,RecurrenceRegeneratedType,RecurrenceStartDateOnly,RecurrenceTimeZoneSidKey,RecurrenceType,ReminderDateTime,
    Status,Subject,SystemModstamp,TaskSubtype,WhatCount,WhatId,WhoCount,WhoId FROM Task WHERE WhoId =: lead.id ORDER BY CreatedDate];
    

    if(tasks.isEmpty()) {
        return;
    }
    for (Task task : tasks) {

        User taskCreator = [SELECT AmFam_User_ID__c FROM User WHERE Id =: task.CreatedById];
        String taskBody = formatTaskBody(task);
        if(future) {
            createApexNote(lead.Contact_CDHID__c, task.CreatedDate, task.Subject, taskBody, taskCreator.AmFam_User_ID__c, lead.id);
        } else {
            createApexNoteSync(lead.Contact_CDHID__c, task.CreatedDate, task.Subject, taskBody, taskCreator.AmFam_User_ID__c, lead.id);
        }
        
    
    }
}



// Returns a string format of the feedItems
// @Parm ordered list of feedItems
// @ Return formatted string
public static String formatChatterBody(FeedItem [] feedItems) {

    String noteBody = '';

    for(FeedItem feedItem : feedItems) {

        FeedComment [] feedComments = new FeedComment[0];
        if (feedItem.CommentCount > 0) { 
          feedComments = [SELECT CommentBody,CommentType,CreatedBy.Name,CreatedDate,FeedItemId,HasEntityLinks,Id,InsertedById,IsDeleted,IsRichText,IsVerified,ParentId,
          RelatedRecordId,Status,SystemModstamp,ThreadChildrenCount,ThreadLastUpdatedDate,ThreadLevel,ThreadParentId FROM FeedComment
          WHERE FeedItemId =: feedItem.id  ORDER BY CreatedDate];    
        }

        String feedItemBody = '';

        feedItemBody = removeHTML(feedItem.Body) + '\n -' + feedItem.CreatedBy.Name + ' ' + feedItem.CreatedDate;

        // Loop through comments on the feed item
        for (FeedComment feedComment: feedComments) {
            feedItemBody = feedItemBody + '\n\n\t' + removeHTML(feedComment.CommentBody) +  ' \n\t -' +  feedComment.CreatedBy.Name + ' ' + feedComment.CreatedDate;
        }

        noteBody += feedItemBody;
        noteBody += '\n\n-------------------------------------------\n\n';
    }

    return noteBody;

}

//Returns a formatted string based on the Log Call details
//@parm task
//@return a formatted string representation of the Task
// Example:
// Subject: Called Joe Smith
// Comments: Joe is interested in Auto.
// Call Result: Joe said to call him tomorrow.
public static String formatTaskBody(Task task) {

    String taskBody = 'Subject: ' + task.Subject;

    if (task.Description != null) {
        taskBody +=  '\n Comments: ' + task.Description;
    }

    if (task.CallDisposition != null) {
        taskBody += '\n Call Result: ' + task.CallDisposition;
    }
    
    return taskBody;
}

 

// Removes the HTML tags from a string
// Removes escaped apostrophe
// Used for chatter feed that contains html tags.
//@param String value
// @return string
public static String removeHTML(String value) {
    String returnValue = value.replaceAll('<[^>]+>',' ');
    returnValue = returnValue.replaceAll('&#39;','\'');
    return returnValue;
}

}