@isTest
public with sharing class AmFam_PromoteAndCreateQuoteTest {
    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    @isTest static void promoteEntrantTest() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert agent;
        System.runAs(agent) {
            wsPartyManage.lpeCallout = false;
            Account a = new Account(Name = 'Test Agency', Producer_ID__c = '12345');
            insert a;
            Lead l = new Lead();
            l.Agency__c = a.Id;
            l.FirstName = 'FirstName';
            l.LastName = 'LastName';
            l.Phone = '1231231234';
            l.Street = '1 Main Street';
            l.City = 'Anytown';
            l.StateCode = 'WI';
            l.CountryCode = 'US';
            l.PostalCode = '12345';
            l.Email = 'testing@test.com';
            l.Email_Usage__c = 'HOME';
            l.Marital_Status__c = 'M';
            l.Gender__c = 'M';
            l.Date_Of_Birth__c = Date.today();
            l.LeadSource = 'Amfam.com';
            l.Company = 'Test';
            l.Lines_of_Business__c = 'Personal Vehicle;Property';
            l.Party_Level__c = 'Entrant';
            l.Partner__c = 'AFI';
            l.Contact_CDHID__c = 'Test123';
            l.Disposition_Reason__c = 'New';
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();
            insert l;
            Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock());
            Test.startTest();
            system.debug('leadid' + l.Id);
            AmFam_PromoteAndCreateQuoteController.promoteEntrant(l.Id, false, null, null);
            Test.stopTest();
        }
    }

    @isTest static void promoteEntrantTest1() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert agent;
        System.runAs(agent) {
            wsPartyManage.lpeCallout = false;
            Account a = new Account(Name = 'Test Agency', Producer_ID__c = '12345');
            insert a;
            Lead l = new Lead();
            l.Agency__c = a.Id;
            l.FirstName = 'FirstName';
            l.LastName = 'LastName';
            l.Phone = '1231231234';
            l.Street = '1 Main Street';
            l.City = 'Anytown';
            l.StateCode = 'WI';
            l.CountryCode = 'US';
            l.PostalCode = '12345';
            l.Email = 'testing@test.com';
            l.Email_Usage__c = 'HOME';
            l.Marital_Status__c = 'M';
            l.Gender__c = 'M';
            l.Date_Of_Birth__c = Date.today();
            l.LeadSource = 'Amfam.com';
            l.Company = 'Test';
            l.Lines_of_Business__c = 'Personal Vehicle;Property';
            l.Party_Level__c = 'Entrant';
            l.Partner__c = 'AFI';
            l.CDHID__c = 'Test123';
            l.Disposition_Reason__c = 'New';
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();
            insert l;
            Test.setMock(HttpCalloutMock.class, new wsPartyManageServiceCalloutHttpMock('promote'));
            Test.startTest();
            system.debug('leadid' + l.Id);
            AmFam_PromoteAndCreateQuoteController.promoteEntrant(l.Id, false, null, null);
            Test.stopTest();
        }
    }
}