/**
*   @description Class to manage response from Producer Type Service 
*   @author Salesforce Services
*   @date 10/19/2020
*   @group API Services
*/

public class AmFam_ProducerTypeService {
    public List<BookOfBusiness> bookOfBusiness {get;set;}
    public List<Partners> partners {get;set;}
    public Status status {get;set;}
    public Agency agency {get;set;}
    public AlternateKey alternateKey {get;set;}
    public Producer producer {get;set;} 
    
    public class AlternateKey {
        public String keyId {get;set;}
        public String keyType {get;set;}
        
        public AlternateKey(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'keyId') {
                            keyId = parser.getText();
                        } else if (text == 'keyType') {
                            keyType = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'AlternateKey consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class AdditionalDetails {
        public String key {get;set;} 
        public String value {get;set;} 

        public AdditionalDetails(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'key') {
                            key = parser.getText();
                        } else if (text == 'value') {
                            value = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'AdditionalDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Status {
        public String maxMessageLevel {get;set;} 
        public Integer code {get;set;} 
        public String reason {get;set;} 
        public List<Messages> messages {get;set;} 
        public String transactionId {get;set;} 
        public Metrics metrics {get;set;} 
        public List<PartialStatuses> partialStatuses {get;set;} 

        public Status(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'maxMessageLevel') {
                            maxMessageLevel = parser.getText();
                        } else if (text == 'code') {
                            code = parser.getIntegerValue();
                        } else if (text == 'reason') {
                            reason = parser.getText();
                        } else if (text == 'messages') {
                            messages = arrayOfMessages(parser);
                        } else if (text == 'transactionId') {
                            transactionId = parser.getText();
                        } else if (text == 'metrics') {
                            metrics = new Metrics(parser);
                        } else if (text == 'partialStatuses') {
                            partialStatuses = arrayOfPartialStatuses(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Status consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Agency {
        public String agencyId {get;set;} 
        public String name {get;set;} 
        public String district {get;set;} 
        public String agencyWebsite {get;set;} 
        public ContactDetails contactDetails {get;set;} 
        public List<Partners> partners {get;set;} 
        public List<AdditionalDetails> additionalDetails {get;set;} 
        public AgencyStatus agencyStatus {get;set;} 

        public Agency(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'agencyId') {
                            agencyId = parser.getText();
                        } else if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'district') {
                            district = parser.getText();
                        } else if (text == 'agencyWebsite') {
                            agencyWebsite = parser.getText();
                        } else if (text == 'contactDetails') {
                            contactDetails = new ContactDetails(parser);
                        } else if (text == 'partners') {
                            partners = arrayOfPartners(parser);
                        } else if (text == 'additionalDetails') {
                            additionalDetails = arrayOfAdditionalDetails(parser);
                        } else if (text == 'agencyStatus') {
                            agencyStatus = new AgencyStatus(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Agency consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Messages {
        public Integer code {get;set;} 
        public String level {get;set;} 
        public String description {get;set;} 
        public List<Details> details {get;set;} 

        public Messages(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'code') {
                            code = parser.getIntegerValue();
                        } else if (text == 'level') {
                            level = parser.getText();
                        } else if (text == 'description') {
                            description = parser.getText();
                        } else if (text == 'details') {
                            details = arrayOfDetails(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Messages consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class PartialStatuses {
        public String payloadEntityReference {get;set;} 
        public String payloadEntityId {get;set;} 

        public PartialStatuses(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'payloadEntityReference') {
                            payloadEntityReference = parser.getText();
                        } else if (text == 'payloadEntityId') {
                            payloadEntityId = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'PartialStatuses consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class AgencyStatus {
        public String status {get;set;} 
        public String statusReason {get;set;} 
        public String effectiveDate {get;set;} 
        public String endEffectiveDate {get;set;} 

        public AgencyStatus(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'status') {
                            status = parser.getText();
                        } else if (text == 'statusReason') {
                            statusReason = parser.getText();
                        } else if (text == 'effectiveDate') {
                            effectiveDate = parser.getText();
                        } else if (text == 'endEffectiveDate') {
                            endEffectiveDate = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'AgencyStatus consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Addresses {
        public String addressLine1 {get;set;} 
        public String addressLine2 {get;set;} 
        public String city {get;set;} 
        public String state {get;set;} 
        public String zip5Code {get;set;} 
        public String zip4Code {get;set;} 
        public String country {get;set;} 
        public String addressUsage {get;set;} 
        public Boolean primaryAddressIndicator {get;set;} 
        public String locationId {get;set;} 
        public String effectiveDate {get;set;} 
        public String endEffectiveDate {get;set;} 

        public Addresses(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'addressLine1') {
                            addressLine1 = parser.getText();
                        } else if (text == 'addressLine2') {
                            addressLine2 = parser.getText();
                        } else if (text == 'city') {
                            city = parser.getText();
                        } else if (text == 'state') {
                            state = parser.getText();
                        } else if (text == 'zip5Code') {
                            zip5Code = parser.getText();
                        } else if (text == 'zip4Code') {
                            zip4Code = parser.getText();
                        } else if (text == 'country') {
                            country = parser.getText();
                        } else if (text == 'addressUsage') {
                            addressUsage = parser.getText();
                        } else if (text == 'primaryAddressIndicator') {
                            primaryAddressIndicator = parser.getBooleanValue();
                        } else if (text == 'locationId') {
                            locationId = parser.getText();
                        } else if (text == 'effectiveDate') {
                            effectiveDate = parser.getText();
                        } else if (text == 'endEffectiveDate') {
                            endEffectiveDate = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Addresses consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Partners {
        public String partnerId {get;set;} 
        public String partnerName {get;set;} 
        public List<DistributionChannels> distributionChannels {get;set;} 

        public Partners(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'partnerId') {
                            partnerId = parser.getText();
                        } else if (text == 'partnerName') {
                            partnerName = parser.getText();
                        } else if (text == 'distributionChannels') {
                            distributionChannels = arrayOfDistributionChannels(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Partners consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Metrics {
        public String acceptedOn {get;set;} 
        public String returnedOn {get;set;} 

        public Metrics(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'acceptedOn') {
                            acceptedOn = parser.getText();
                        } else if (text == 'returnedOn') {
                            returnedOn = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Metrics consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class DistributionChannels {
        public String distributionChannelId {get;set;} 
        public String distributionChannelName {get;set;} 

        public DistributionChannels(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'distributionChannelId') {
                            distributionChannelId = parser.getText();
                        } else if (text == 'distributionChannelName') {
                            distributionChannelName = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'DistributionChannels consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Details {
        public String field {get;set;} 
        public String description {get;set;} 

        public Details(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'field') {
                            field = parser.getText();
                        } else if (text == 'description') {
                            description = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Details consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Phones {
        public String phoneNumber {get;set;} 
        public String extension {get;set;} 
        public String phoneUsage {get;set;} 
        public String locationId {get;set;} 

        public Phones(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'phoneNumber') {
                            phoneNumber = parser.getText();
                        } else if (text == 'extension') {
                            extension = parser.getText();
                        } else if (text == 'phoneUsage') {
                            phoneUsage = parser.getText();
                        } else if (text == 'locationId') {
                            locationId = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Phones consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class ContactDetails {
        public List<Addresses> addresses {get;set;} 
        public List<Phones> phones {get;set;} 
        public List<Emails> emails {get;set;} 

        public ContactDetails(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'addresses') {
                            addresses = arrayOfAddresses(parser);
                        } else if (text == 'phones') {
                            phones = arrayOfPhones(parser);
                        } else if (text == 'emails') {
                            emails = arrayOfEmails(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'ContactDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Emails {
        public String emailAddress {get;set;} 
        public String emailUsage {get;set;} 
        public String locationId {get;set;} 

        public Emails(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'emailAddress') {
                            emailAddress = parser.getText();
                        } else if (text == 'emailUsage') {
                            emailUsage = parser.getText();
                        } else if (text == 'locationId') {
                            locationId = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Emails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class ProducerCodes {
        public String producerCode {get;set;} 
        public String name {get;set;} 
        public String alternateName {get;set;} 
        public String agencyId {get;set;} 
        public String locationId {get;set;} 
        public String district {get;set;} 
        public List<String> alternateKeys {get;set;} 
        public List<String> restrictedToStates {get;set;} 
        public List<String> restrictedToLinesOfBusiness {get;set;} 
        public List<Disposition> disposition {get;set;} 

        public ProducerCodes(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'producerCode') {
                            producerCode = parser.getText();
                        } else if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'alternateName') {
                            alternateName = parser.getText();
                        } else if (text == 'agencyId') {
                            agencyId = parser.getText();
                        } else if (text == 'locationId') {
                            locationId = parser.getText();
                        } else if (text == 'district') {
                            district = parser.getText();
                        } else if (text == 'alternateKeys') {
                            alternateKeys = arrayOfString(parser);
                        } else if (text == 'restrictedToStates') {
                            restrictedToStates = arrayOfString(parser);
                        } else if (text == 'restrictedToLinesOfBusiness') {
                            restrictedToLinesOfBusiness = arrayOfString(parser);
                        } else if (text == 'disposition') {
                            disposition = arrayOfDisposition(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'ProducerCodes consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Disposition {
        public String key {get;set;} 
        public String value {get;set;} 

        public Disposition(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'key') {
                            key = parser.getText();
                        } else if (text == 'value') {
                            value = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Disposition consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class BookOfBusiness {
        public List<ProducerCodes> producerCodes {get;set;} 
        public String userId {get;set;} 
        public String role {get;set;} 
        public String title {get;set;} 

        public BookOfBusiness(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'producerCodes') {
                            producerCodes = arrayOfProducerCodes(parser);
                        } else if (text == 'userId') {
                            userId = parser.getText();
                        } else if (text == 'role') {
                            role = parser.getText();
                        } else if (text == 'title') {
                            title = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'BookOfBusiness consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Producer {
        public String producerId {get;set;} 
        public DemographicInformation demographicInformation {get;set;} 
        public ContactDetails contactDetails {get;set;} 
        public List<AlternateKeys> alternateKeys {get;set;} 
        public List<Positions> positions {get;set;} 
        public ProducerStatus producerStatus {get;set;} 

        public Producer(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'producerId') {
                            producerId = parser.getText();
                        } else if (text == 'demographicInformation') {
                            demographicInformation = new DemographicInformation(parser);
                        } else if (text == 'contactDetails') {
                            contactDetails = new ContactDetails(parser);
                        } else if (text == 'alternateKeys') {
                            alternateKeys = arrayOfAlternateKeys(parser);
                        } else if (text == 'positions') {
                            positions = arrayOfPositions(parser);
                        } else if (text == 'producerStatus') {
                            producerStatus = new ProducerStatus(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Producer consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Positions {
        public String positionHolderName {get;set;} 
        public String role {get;set;} 
        public String profile {get;set;} 
        public String profileSuffix {get;set;} 
        public List<Partners> partners {get;set;} 
        public String effectiveDate {get;set;} 
        public String endEffectiveDate {get;set;} 

        public Positions(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'positionHolderName') {
                            positionHolderName = parser.getText();
                        } else if (text == 'role') {
                            role = parser.getText();
                        } else if (text == 'profile') {
                            profile = parser.getText();
                        } else if (text == 'profileSuffix') {
                            profileSuffix = parser.getText();
                        } else if (text == 'partners') {
                            partners = arrayOfPartners(parser);
                        } else if (text == 'effectiveDate') {
                            effectiveDate = parser.getText();
                        } else if (text == 'endEffectiveDate') {
                            endEffectiveDate = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Positions consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class ProducerStatus {
        public String status {get;set;} 
        public String statusReason {get;set;} 
        public String effectiveDate {get;set;} 
        public String endEffectiveDate {get;set;} 

        public ProducerStatus(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'status') {
                            status = parser.getText();
                        } else if (text == 'statusReason') {
                            statusReason = parser.getText();
                        } else if (text == 'effectiveDate') {
                            effectiveDate = parser.getText();
                        } else if (text == 'endEffectiveDate') {
                            endEffectiveDate = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'ProducerStatus consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class AlternateKeys {
        public String keyId {get;set;} 
        public String keyType {get;set;} 

        public AlternateKeys(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'keyId') {
                            keyId = parser.getText();
                        } else if (text == 'keyType') {
                            keyType = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'AlternateKeys consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class DemographicInformation {
        public Name name {get;set;} 

        public DemographicInformation(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'name') {
                            name = new Name(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'DemographicInformation consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class Name {
		public String firstName {get;set;} 
		public String lastName {get;set;} 
		public String middleName {get;set;} 
		public String nameSuffix {get;set;} 

		public Name(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'firstName') {
							firstName = parser.getText();
						} else if (text == 'lastName') {
							lastName = parser.getText();
						} else if (text == 'middleName') {
							middleName = parser.getText();
						} else if (text == 'nameSuffix') {
							nameSuffix = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Name consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}

    public AmFam_ProducerTypeService(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'agency') {
                        agency = new Agency(parser);
                    } else if (text == 'bookOfBusiness') {
                        bookOfBusiness = arrayOfBookOfBusiness(parser);
                    } else if (text == 'partners') {
                        partners = arrayOfPartners(parser);
                    } else if (text == 'status') {
                        status = new Status(parser);
                    } else if (text == 'alternateKey') {
                        alternateKey = new AlternateKey(parser);
                    } else if (text == 'producer') {
                        producer = new Producer(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'AmFam_ProducerTypeService consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public static AmFam_ProducerTypeService parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new AmFam_ProducerTypeService(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    
    private static List<Emails> arrayOfEmails(System.JSONParser p) {
        List<Emails> res = new List<Emails>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Emails(p));
        }
        return res;
    }
    
    private static List<Addresses> arrayOfAddresses(System.JSONParser p) {
        List<Addresses> res = new List<Addresses>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Addresses(p));
        }
        return res;
    }

    private static List<PartialStatuses> arrayOfPartialStatuses(System.JSONParser p) {
        List<PartialStatuses> res = new List<PartialStatuses>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new PartialStatuses(p));
        }
        return res;
    }

    private static List<Messages> arrayOfMessages(System.JSONParser p) {
        List<Messages> res = new List<Messages>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Messages(p));
        }
        return res;
    }

    private static List<Phones> arrayOfPhones(System.JSONParser p) {
        List<Phones> res = new List<Phones>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Phones(p));
        }
        return res;
    }

    private static List<Partners> arrayOfPartners(System.JSONParser p) {
        List<Partners> res = new List<Partners>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Partners(p));
        }
        return res;
    }

    private static List<Details> arrayOfDetails(System.JSONParser p) {
        List<Details> res = new List<Details>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Details(p));
        }
        return res;
    }

    private static List<AdditionalDetails> arrayOfAdditionalDetails(System.JSONParser p) {
        List<AdditionalDetails> res = new List<AdditionalDetails>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new AdditionalDetails(p));
        }
        return res;
    }

    private static List<DistributionChannels> arrayOfDistributionChannels(System.JSONParser p) {
        List<DistributionChannels> res = new List<DistributionChannels>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new DistributionChannels(p));
        }
        return res;
    }

    private static List<BookOfBusiness> arrayOfBookOfBusiness(System.JSONParser p) {
        List<BookOfBusiness> res = new List<BookOfBusiness>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new BookOfBusiness(p));
        }
        return res;
    }

    private static List<String> arrayOfString(System.JSONParser p) {
        List<String> res = new List<String>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(p.getText());
        }
        return res;
    }

    private static List<Disposition> arrayOfDisposition(System.JSONParser p) {
        List<Disposition> res = new List<Disposition>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Disposition(p));
        }
        return res;
    }

    private static List<ProducerCodes> arrayOfProducerCodes(System.JSONParser p) {
        List<ProducerCodes> res = new List<ProducerCodes>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new ProducerCodes(p));
        }
        return res;
    }

    private static List<AlternateKeys> arrayOfAlternateKeys(System.JSONParser p) {
        List<AlternateKeys> res = new List<AlternateKeys>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new AlternateKeys(p));
        }
        return res;
    }

    private static List<Positions> arrayOfPositions(System.JSONParser p) {
        List<Positions> res = new List<Positions>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Positions(p));
        }
        return res;
    }
}