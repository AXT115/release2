/**
*    @description  Test Code to check LeadTriggerHandler Class
*    @author Salesforce Services
*    @date 06/07/2020
*    @group @Test
*/
@isTest
private class AmFam_LeadTriggerHandlerTest {

    @TestSetup
    static void bypassProcessBuilderSettings(){
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    @isTest
    static void checkLeadOwnerChangeBelongsToSameAgencyTest() {
        wsPartyManage.lpeCallout = false;
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        Account[] accts = AmFam_TestDataFactory.createAccountsWithLeads(2, 2);
        List<User> users = new List<User>();
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test'
                                );
        User agentUser1 = new User (alias = 'sagent', email='test1246@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1246@xyz.com.test'
                                );
        AmFam_UserService.callProducerAPI = false;
        users.add(agentUser);
        users.add(agentUser1);
        insert users;
        List<Lead> leads = [SELECT Id, OwnerId, Agency__c, Agency__r.Producer_ID__c FROM Lead WHERE Agency__c = :accts];
        //Test for single lead owner change
        Test.startTest();
        try {
            Lead l = Leads[0];
            l.OwnerId = agentUser.Id;
            System.debug('Lead Account ' + l.Agency__r.Producer_ID__c);
            update l;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Leads cannot be assigned to a user who is not associated with the owning agency') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        //Test for multiple lead owner change
        for (Lead l : Leads) {
            l.OwnerId = agentUser.Id;
        }
        try {
            update leads;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Leads cannot be assigned to a user who is not associated with the owning agency') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        Test.stopTest();
    }

    @isTest
    static void setSharingAccessForNewLeadTest() {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345'
                                );
        agentUser.Producer_ID__c = '32123';

       insert agentUser; 

       Producer prod1 = new Producer();
       prod1.Producer_ID__c = '32123';
       prod1.Name = '32123';
       prod1.InternalUserId = agentUser.Id;
       prod1.Type = 'Agency';
       insert prod1;

        Account agency = AmFam_TestDataFactory.insertAgencyAccount('32123 Agency', '32123');
        Account agentAcct = AmFam_TestDataFactory.insertAgentAccount('32123 Agent', 'P-32123');
        Contact contact = [SELECT Id FROM Contact WHERE AccountId = :agentAcct.Id];
        contact.User_Federation_ID__c = 'test1245';
        update contact;
        AccountContactRelation rel = new AccountContactRelation();
        rel.ContactId = contact.Id;
        rel.AccountId = agency.Id;
        insert rel;

        Test.startTest();
        Id agencyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();  

        wsPartyManage.lpeCallout = false;
        Lead ld = new Lead();
        ld.Agency__c = agency.Id;
        ld.Company = 'Test Lead';
        ld.Email = 'testLeadQuote@test.com';
        ld.FirstName = 'Test';
        ld.LastName = 'Lead';
        ld.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();
        insert ld;
        Test.stopTest();

        List<LeadShare> leadShares = [SELECT Id, UserOrGroupId FROM LeadShare
                                        WHERE UserOrGroupId =:agentUser.Id];
                                        //WHERE Lead.Agency__c =: agency.Id AND UserOrGroupId =:agentUser.Id];
        System.assertEquals(1, leadShares.size());
    }

    @isTest
    static void setSharingAccessForNewLeadsTest() {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));
        //Account acc = AmFam_TestDataFactory.createAccountByNameWithLeads('John Herrera Agency, LLC', 0);
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345'
                                );
        agentUser.Producer_ID__c = '32123';

        insert agentUser; //this (because of Queueables), does not create the related contact & account in test

        Producer prod1 = new Producer();
        prod1.Producer_ID__c = '32123';
        prod1.Name = '32123';
        prod1.InternalUserId = agentUser.Id;
        prod1.Type = 'Agency';
        insert prod1;
 
        Account agency = AmFam_TestDataFactory.insertAgencyAccount('32123 Agency', '32123');
        Account agentAcct = AmFam_TestDataFactory.insertAgentAccount('32123 Agent', 'P-32123');
        Contact contact = [SELECT Id FROM Contact WHERE AccountId = :agentAcct.Id];
        contact.User_Federation_ID__c = 'test1245';
        update contact;
        AccountContactRelation rel = new AccountContactRelation();
        rel.ContactId = contact.Id;
        rel.AccountId = agency.Id;
        insert rel;

        Test.startTest();

        User u = [SELECT Id,Producer_ID__c,AmFam_User_ID__c FROM USER WHERE Id = :agentUser.Id];
                        
        List<AccountContactRelation> contacts = [SELECT Id, AccountId, Contact.User_Federation_ID__c
                                                FROM AccountContactRelation];
        Id agencyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();  
                        
        wsPartyManage.lpeCallout = false;
        List<Lead> leads = new List<Lead>();
        for (Integer i=0; i<5; i++) {
            Lead ld = new Lead();
            ld.Agency__c = agency.Id;
            ld.Company = 'Test Lead' + i;
            ld.Email = i + 'testLeadQuote@test.com';
            ld.FirstName = 'Test';
            ld.LastName = 'Lead';
            ld.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();
            leads.add(ld);
        }

        insert leads;
        Test.stopTest();
        List<LeadShare> leadShares = [SELECT Id, UserOrGroupId FROM LeadShare
                                        WHERE Lead.Agency__c =: agency.Id AND UserOrGroupId =:agentUser.Id];
        System.assertEquals(5, leadShares.size());
    }
}