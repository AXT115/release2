/**
*   @description Class to manage response from Producer Service 
*   @author Salesforce Services
*   @date 05/18/2020
*   @group API Services
*/
public class AmFam_ProducerService{

	public Status status {get;set;} 
	public Producer_Z producer {get;set;} 
	public String README {get;set;} 
	public String producerId {get;set;} 
	public String producerPartyId {get;set;} 
	public String credentials {get;set;} 
	public Name name {get;set;} 
	public String effectiveDate {get;set;} 
	public String endEffectiveDate {get;set;} 
	public ContactDetails_Z contactDetails {get;set;} 
	public List<ProducerDetail> staff {get;set;} 
	public List<Managers_Z> managers {get;set;} 
	public List<BookOfBusiness> bookOfBusiness {get;set;} 

	public AmFam_ProducerService(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'status') {
						status = new Status(parser);
					} else if (text == 'producer') {
						producer = new Producer_Z(parser);
					} else if (text == 'READ ME') {
						README = parser.getText();
					} else if (text == 'producerId') {
						producerId = parser.getText();
					} else if (text == 'producerPartyId') {
						producerPartyId = parser.getText();
					} else if (text == 'credentials') {
						credentials = parser.getText();
					} else if (text == 'name') {
						name = new Name(parser);
					} else if (text == 'effectiveDate') {
						effectiveDate = parser.getText();
					} else if (text == 'endEffectiveDate') {
						endEffectiveDate = parser.getText();
					} else if (text == 'contactDetails') {
						contactDetails = new ContactDetails_Z(parser);
					} else if (text == 'staff') {
						staff = arrayOfProducerDetail(parser);
					} else if (text == 'managers') {
						managers = arrayOfManagers_Z(parser);
					} else if (text == 'bookOfBusiness') {
						bookOfBusiness = arrayOfBookOfBusiness(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'AmFam_ProducerServiceconsuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
		
	public static AmFam_ProducerService parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new AmFam_ProducerService(parser);
	}

	public class Status {
		public Integer code {get;set;} 
		public String reason {get;set;} 
		public String transactionId {get;set;} 

		public Status(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'code') {
							code = parser.getIntegerValue();
						} else if (text == 'reason') {
							reason = parser.getText();
						} else if (text == 'transactionId') {
							transactionId = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Status consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Agency {
		public Boolean incorporatedIndicator {get;set;} 

		public Agency(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'incorporatedIndicator') {
							incorporatedIndicator = parser.getBooleanValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Agency consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Agency_Z {
		public String name {get;set;} 
		public String districtCode {get;set;} 
		public Boolean incorporatedIndicator {get;set;} 
		public String agencyPartyId {get;set;} 

		public Agency_Z(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = parser.getText();
						} else if (text == 'districtCode') {
							districtCode = parser.getText();
						} else if (text == 'incorporatedIndicator') {
							incorporatedIndicator = parser.getBooleanValue();
						} else if (text == 'agencyPartyId') {
							agencyPartyId = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Agency_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Address {
		public String line1 {get;set;} 
		public String line2 {get;set;} 
		public String city {get;set;} 
		public String state {get;set;} 
		public String zip5 {get;set;} 
		public String zip4 {get;set;} 

		public Address(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'line1') {
							line1 = parser.getText();
						} else if (text == 'line2') {
							line2 = parser.getText();
						} else if (text == 'city') {
							city = parser.getText();
						} else if (text == 'state') {
							state = parser.getText();
						} else if (text == 'zip5') {
							zip5 = parser.getText();
						} else if (text == 'zip4') {
							zip4 = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Address consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Producer_Z {
		public String typeOfAgentCode {get;set;} 
		public String jobTitle {get;set;} 
		public Boolean storefrontIndicator {get;set;} 
		public String producerId {get;set;} 
		public String fullName {get;set;} 
		public String firstName {get;set;} 
		public String middleName {get;set;} 
		public String lastName {get;set;} 
		public String nickName {get;set;} 
		public Boolean activeStatusIndicator {get;set;} 
		public ContactDetail contactDetail {get;set;} 
		public List<ContactDetails> contactDetails {get;set;} 
		public AlternateId alternateId {get;set;} 
		public ProducerDetail producerDetail {get;set;} 
		public List<Managers> managers {get;set;} 
		public List<Staff> staff {get;set;} 
		public List<BooksOfBusiness> booksOfBusiness {get;set;} 
		public List<Offices> offices {get;set;} 

		public Producer_Z(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'typeOfAgentCode') {
							typeOfAgentCode = parser.getText();
						} else if (text == 'jobTitle') {
							jobTitle = parser.getText();
						} else if (text == 'storefrontIndicator') {
							storefrontIndicator = parser.getBooleanValue();
						} else if (text == 'producerId') {
							producerId = parser.getText();
						} else if (text == 'fullName') {
							fullName = parser.getText();
						} else if (text == 'firstName') {
							firstName = parser.getText();
						} else if (text == 'middleName') {
							middleName = parser.getText();
						} else if (text == 'lastName') {
							lastName = parser.getText();
						} else if (text == 'nickName') {
							nickName = parser.getText();
						} else if (text == 'activeStatusIndicator') {
							activeStatusIndicator = parser.getBooleanValue();
						} else if (text == 'contactDetail') {
							contactDetail = new ContactDetail(parser);
						} else if (text == 'contactDetails') {
							contactDetails = arrayOfContactDetails(parser);
						} else if (text == 'alternateId') {
							alternateId = new AlternateId(parser);
						} else if (text == 'producerDetail') {
							producerDetail = new ProducerDetail(parser);
						} else if (text == 'managers') {
							managers = arrayOfManagers(parser);
						} else if (text == 'staff') {
							staff = arrayOfStaff(parser);
						} else if (text == 'booksOfBusiness') {
							booksOfBusiness = arrayOfBooksOfBusiness(parser);
						} else if (text == 'offices') {
							offices = arrayOfOffices(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Producer_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class ContactDetails_Z {
		public List<Locations> locations {get;set;} 
		public List<Phones_Z> phones {get;set;} 
		public List<ContactDetails> emails {get;set;} 
		public List<ProducerDetail> socialMedia {get;set;} 

		public ContactDetails_Z(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'locations') {
							locations = arrayOfLocations(parser);
						} else if (text == 'phones') {
							phones = arrayOfPhones_Z(parser);
						} else if (text == 'emails') {
							emails = arrayOfContactDetails(parser);
						} else if (text == 'socialMedia') {
							socialMedia = arrayOfProducerDetail(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'ContactDetails_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Staff {
		public Producer Producer {get;set;} 
		public String firstName {get;set;} 
		public String middleName {get;set;} 
		public String lastName {get;set;} 
		public String role {get;set;} 
		public String effectiveDate {get;set;} 
		public String endEffectiveDate {get;set;} 

		public Staff(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'Producer') {
							Producer = new Producer(parser);
						} else if (text == 'firstName') {
							firstName = parser.getText();
						} else if (text == 'middleName') {
							middleName = parser.getText();
						} else if (text == 'lastName') {
							lastName = parser.getText();
						} else if (text == 'role') {
							role = parser.getText();
						} else if (text == 'effectiveDate') {
							effectiveDate = parser.getText();
						} else if (text == 'endEffectiveDate') {
							endEffectiveDate = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Staff consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Locations {
		public String locationType {get;set;} 
		public String locationUsage {get;set;} 
		public String locationId {get;set;} 
		public String latitude {get;set;} 
		public String longitude {get;set;} 
		public Address_Z address {get;set;} 
		public String effectiveDate {get;set;} 
		public String endEffectiveDate {get;set;} 

		public Locations(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'locationType') {
							locationType = parser.getText();
						} else if (text == 'locationUsage') {
							locationUsage = parser.getText();
						} else if (text == 'locationId') {
							locationId = parser.getText();
						} else if (text == 'latitude') {
							latitude = parser.getText();
						} else if (text == 'longitude') {
							longitude = parser.getText();
						} else if (text == 'address') {
							address = new Address_Z(parser);
						} else if (text == 'effectiveDate') {
							effectiveDate = parser.getText();
						} else if (text == 'endEffectiveDate') {
							endEffectiveDate = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Locations consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class BookOfBusiness {
		public String role {get;set;} 
		public String agency {get;set;} 
		public String effectiveDate {get;set;} 
		public String endEffectiveDate {get;set;} 

		public BookOfBusiness(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'role') {
							role = parser.getText();
						} else if (text == 'agency') {
							agency = parser.getText();
						} else if (text == 'effectiveDate') {
							effectiveDate = parser.getText();
						} else if (text == 'endEffectiveDate') {
							endEffectiveDate = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'BookOfBusiness consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Managers_Z {
		public String producerId {get;set;} 
		public String producerPartyId {get;set;} 
		public String role {get;set;} 
		public String agency {get;set;} 
		public String effectiveDate {get;set;} 
		public String endEffectiveDate {get;set;} 

		public Managers_Z(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'producerId') {
							producerId = parser.getText();
						} else if (text == 'producerPartyId') {
							producerPartyId = parser.getText();
						} else if (text == 'role') {
							role = parser.getText();
						} else if (text == 'agency') {
							agency = parser.getText();
						} else if (text == 'effectiveDate') {
							effectiveDate = parser.getText();
						} else if (text == 'endEffectiveDate') {
							endEffectiveDate = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Managers_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Manager {
		public String typeOfAgentCode {get;set;} 
		public String jobTitle {get;set;} 
		public String producerId {get;set;} 
		public String firstName {get;set;} 
		public String middleName {get;set;} 
		public String lastName {get;set;} 
		public String nickName {get;set;} 
		public String fullName {get;set;} 
		public Boolean storefrontIndicator {get;set;} 

		public Manager(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'typeOfAgentCode') {
							typeOfAgentCode = parser.getText();
						} else if (text == 'jobTitle') {
							jobTitle = parser.getText();
						} else if (text == 'producerId') {
							producerId = parser.getText();
						} else if (text == 'firstName') {
							firstName = parser.getText();
						} else if (text == 'middleName') {
							middleName = parser.getText();
						} else if (text == 'lastName') {
							lastName = parser.getText();
						} else if (text == 'nickName') {
							nickName = parser.getText();
						} else if (text == 'fullName') {
							fullName = parser.getText();
						} else if (text == 'storefrontIndicator') {
							storefrontIndicator = parser.getBooleanValue();
						} else {
							System.debug(LoggingLevel.WARN, 'Manager consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Phones_Z {
		public String phoneType {get;set;} 
		public String phoneUsage {get;set;} 
		public String phoneNumber {get;set;} 
		public String effectiveDate {get;set;} 
		public String endEffectiveDate {get;set;} 

		public Phones_Z(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'phoneType') {
							phoneType = parser.getText();
						} else if (text == 'phoneUsage') {
							phoneUsage = parser.getText();
						} else if (text == 'phoneNumber') {
							phoneNumber = parser.getText();
						} else if (text == 'effectiveDate') {
							effectiveDate = parser.getText();
						} else if (text == 'endEffectiveDate') {
							endEffectiveDate = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Phones_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Name {
		public String firstName {get;set;} 
		public String middleName {get;set;} 
		public String lastName {get;set;} 
		public String nickName {get;set;} 

		public Name(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'firstName') {
							firstName = parser.getText();
						} else if (text == 'middleName') {
							middleName = parser.getText();
						} else if (text == 'lastName') {
							lastName = parser.getText();
						} else if (text == 'nickName') {
							nickName = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Name consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class ContactDetail {
		public List<String> emails {get;set;} 
		public List<Phones> phones {get;set;} 

		public ContactDetail(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'emails') {
							emails = arrayOfString(parser);
						} else if (text == 'phones') {
							phones = arrayOfPhones(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'ContactDetail consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}	
	
	public class Phones {
		public String areaCode {get;set;} 
		public String typeOfUsageCode {get;set;} 
		public String phoneNumber {get;set;} 
		public String typeOfPhoneCode {get;set;} 

		public Phones(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'areaCode') {
							areaCode = parser.getText();
						} else if (text == 'typeOfUsageCode') {
							typeOfUsageCode = parser.getText();
						} else if (text == 'phoneNumber') {
							phoneNumber = parser.getText();
						} else if (text == 'typeOfPhoneCode') {
							typeOfPhoneCode = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Phones consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class ContactDetails {
		public String README {get;set;} 
		public String README2 {get;set;} 
		public String email {get;set;} 
		public String effectiveDate {get;set;} 
		public String endEffectiveDate {get;set;} 

		public ContactDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'README') {
							README = parser.getText();
						} else if (text == 'README2') {
							README2 = parser.getText();
						} else if (text == 'email') {
							email = parser.getText();
						} else if (text == 'effectiveDate') {
							effectiveDate = parser.getText();
						} else if (text == 'endEffectiveDate') {
							endEffectiveDate = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'ContactDetails consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Producer {
		public String producerId {get;set;} 
		public String producerPartyId {get;set;} 

		public Producer(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'producerId') {
							producerId = parser.getText();
						} else if (text == 'producerPartyId') {
							producerPartyId = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Producer consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class BooksOfBusiness {
		public String typeOfAgentCode {get;set;} 
		public String primaryProducerId {get;set;} 
		public Agency_Z agency {get;set;} 

		public BooksOfBusiness(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'typeOfAgentCode') {
							typeOfAgentCode = parser.getText();
						} else if (text == 'primaryProducerId') {
							primaryProducerId = parser.getText();
						} else if (text == 'agency') {
							agency = new Agency_Z(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'BooksOfBusiness consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Managers {
		public Agency agency {get;set;} 
		public Manager manager {get;set;} 

		public Managers(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'agency') {
							agency = new Agency(parser);
						} else if (text == 'manager') {
							manager = new Manager(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Managers consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class AlternateId {
		public String globalNickname {get;set;} 
		public String externalAccountId {get;set;} 

		public AlternateId(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'globalNickname') {
							globalNickname = parser.getText();
						} else if (text == 'externalAccountId') {
							externalAccountId = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'AlternateId consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class ProducerDetail {

		public ProducerDetail(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						{
							System.debug(LoggingLevel.WARN, 'ProducerDetail consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Address_Z {
		public String addressLine1 {get;set;} 
		public String addressLine2 {get;set;} 
		public String city {get;set;} 
		public String state {get;set;} 
		public String zipFirstFive {get;set;} 
		public String zipLastFour {get;set;} 

		public Address_Z(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'addressLine1') {
							addressLine1 = parser.getText();
						} else if (text == 'addressLine2') {
							addressLine2 = parser.getText();
						} else if (text == 'city') {
							city = parser.getText();
						} else if (text == 'state') {
							state = parser.getText();
						} else if (text == 'zipFirstFive') {
							zipFirstFive = parser.getText();
						} else if (text == 'zipLastFour') {
							zipLastFour = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Address_Z consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Offices {
		public String typeOfOfficeCode {get;set;} 
		public Address address {get;set;} 
		public List<Phones> phones {get;set;} 

		public Offices(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'typeOfOfficeCode') {
							typeOfOfficeCode = parser.getText();
						} else if (text == 'address') {
							address = new Address(parser);
						} else if (text == 'phones') {
							phones = arrayOfPhones(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Offices consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
		
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	
    private static List<Managers_Z> arrayOfManagers_Z(System.JSONParser p) {
        List<Managers_Z> res = new List<Managers_Z>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Managers_Z(p));
        }
        return res;
    }

    private static List<Phones> arrayOfPhones(System.JSONParser p) {
        List<Phones> res = new List<Phones>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Phones(p));
        }
        return res;
    }

    private static List<ContactDetails> arrayOfContactDetails(System.JSONParser p) {
        List<ContactDetails> res = new List<ContactDetails>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new ContactDetails(p));
        }
        return res;
    }

    private static List<BooksOfBusiness> arrayOfBooksOfBusiness(System.JSONParser p) {
        List<BooksOfBusiness> res = new List<BooksOfBusiness>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new BooksOfBusiness(p));
        }
        return res;
    }

    private static List<Locations> arrayOfLocations(System.JSONParser p) {
        List<Locations> res = new List<Locations>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Locations(p));
        }
        return res;
    }

    private static List<ProducerDetail> arrayOfProducerDetail(System.JSONParser p) {
        List<ProducerDetail> res = new List<ProducerDetail>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new ProducerDetail(p));
        }
        return res;
    }

    private static List<Managers> arrayOfManagers(System.JSONParser p) {
        List<Managers> res = new List<Managers>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Managers(p));
        }
        return res;
    }

    private static List<Offices> arrayOfOffices(System.JSONParser p) {
        List<Offices> res = new List<Offices>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Offices(p));
        }
        return res;
    }

    private static List<String> arrayOfString(System.JSONParser p) {
        List<String> res = new List<String>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(p.getText());
        }
        return res;
    }

    private static List<Staff> arrayOfStaff(System.JSONParser p) {
        List<Staff> res = new List<Staff>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Staff(p));
        }
        return res;
    }

    private static List<BookOfBusiness> arrayOfBookOfBusiness(System.JSONParser p) {
        List<BookOfBusiness> res = new List<BookOfBusiness>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new BookOfBusiness(p));
        }
        return res;
    }

    private static List<Phones_Z> arrayOfPhones_Z(System.JSONParser p) {
        List<Phones_Z> res = new List<Phones_Z>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Phones_Z(p));
        }
        return res;
    }

}