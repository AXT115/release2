@isTest 
public class AmFam_InteractionSummaryParticipant_Test {
    


    @IsTest
    static void testDirectAccountParticipant(){

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('Sales District Leader', 'sagent1', 'User1', 'Agent1');
        //User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account agentAccount1 = insertAgentAccount('Pepe Trueno');

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            ProducerAccess__c pa = insertProducerAccess(agentAccount1, 'pro12345');
            //Producer producer2 = insertProducer(agency, agent2, 'pro12345');


            //Take snapshot before the test
            List<InteractionSummaryShare> shareListBefore = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];



            Test.startTest();

            InteractionSummary interactionSummary1 = insertInteractionSummary('Interaction 1', agentAccount1);

            Test.stopTest();

            //Take snapshot after the test
            List<InteractionSummaryShare> shareListAfter = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];

            if (AmFam_Utility.isTriggerSwitchActive('InteractionSummary_Object') && AmFam_Utility.isTriggerSwitchActive('Participant_Object')) {
                //Compare both snapshots. They shoul be different to confirm a new InteractionSummaryParticipant record was created
                System.assertNotEquals(shareListBefore.size(), shareListAfter.size());
            }


        }

    }



    @IsTest
    static void testDirectAccountParticipant_v2(){

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('Sales District Leader', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('Sales District Leader', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account agentAccount1 = insertAgentAccount('Pepe Trueno');

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            ProducerAccess__c pa = insertProducerAccess(agentAccount1, 'pro12345');

            InteractionSummary interactionSummary1 = insertInteractionSummary('Interaction 1', agentAccount1);

            //Take snapshot before the test
            List<InteractionSummaryShare> shareListBefore = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];

            Test.startTest();

            Producer producer2 = insertProducer(agency, agent2, 'pro12345');


            Test.stopTest();


            //Take snapshot after the test
            List<InteractionSummaryShare> shareListAfter = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];


            if (AmFam_Utility.isTriggerSwitchActive('InteractionSummary_Object') && AmFam_Utility.isTriggerSwitchActive('Participant_Object') && AmFam_Utility.isTriggerSwitchActive('Producer_Object')) {
                //Compare both snapshots. They shoul be different to confirm a new InteractionSummaryParticipantShare record was created
                System.assertNotEquals(shareListBefore.size(), shareListAfter.size());
            }


        }

    }




    @IsTest
    static void testIndirectAccountParticipants_add(){

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('Sales District Leader', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account agentAccount1 = insertAgentAccount('Agent Account 1');
            Account agentAccount2 = insertAgentAccount('Agent Account 2');

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            Producer producer2 = insertProducer(agency, agent2, 'pro12345');
            ProducerAccess__c pa = insertProducerAccess(agentAccount1, 'pro12345');


            //Take snapshot before the test
            List<InteractionSummaryShare> shareListBefore = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];

            InteractionSummary interactionSummary1 = insertInteractionSummary('Interaction 1', null);


            Test.startTest();


            Participant__c p1 = insertParticipant(agentAccount1, interactionSummary1);
            Participant__c p2 = insertParticipant(agentAccount2, interactionSummary1);


            Test.stopTest();

            //Take snapshot after the test
            List<InteractionSummaryShare> shareListAfter = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];

            if (AmFam_Utility.isTriggerSwitchActive('InteractionSummary_Object') && AmFam_Utility.isTriggerSwitchActive('Participant_Object')) {
                //Verify only 1 InteractionSummary was created 
                System.assertEquals( 2, shareListAfter.size());
            }


        }

    }        

    

    @IsTest
    static void testIndirectAccountParticipants_remove(){

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('Sales District Leader', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('AmFam Agent', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account agentAccount1 = insertAgentAccount('Agent Account 1');
            Account agentAccount2 = insertAgentAccount('Agent Account 2');

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            ProducerAccess__c pa = insertProducerAccess(agentAccount1, 'pro12345');
            Producer producer2 = insertProducer(agency, agent2, 'pro12345');


            //Take snapshot before the test
            List<InteractionSummaryShare> shareListBefore = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];

            InteractionSummary interactionSummary1 = insertInteractionSummary('Interaction 1', null);
            Participant__c p1 = insertParticipant(agentAccount1, interactionSummary1);
            Participant__c p2 = insertParticipant(agentAccount2, interactionSummary1);



            Test.startTest();

            delete p1;
            delete p2;


            Test.stopTest();

        }
    }        



    @IsTest
    static void testIndirectAccountParticipants_removeV2(){

        AmFam_UserService.callProducerAPI = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('Sales District Leader', 'sagent1', 'User1', 'Agent1');
        User agent2 = insertUser('Sales District Leader', 'sagent2', 'User2', 'Agent2');

        System.runAs(admin) {

            wsPartyManage.lpeCallout = false;

            Account agency = insertAgencyAccount('Acme');
            Account agentAccount1 = insertAgentAccount('Pepe Trueno');

            Producer producer1 = insertProducer(agency, agent1, 'pro12345');
            ProducerAccess__c pa = insertProducerAccess(agentAccount1, 'pro12345');

            InteractionSummary interactionSummary1 = insertInteractionSummary('Interaction 1', agentAccount1);


            //Take snapshot before the test
            List<InteractionSummaryShare> shareListBefore = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];

            Test.startTest();

            Producer producer2 = insertProducer(agency, agent2, 'pro12345');


            delete producer2;

            Test.stopTest();



            //Take snapshot after the test
            List<InteractionSummaryShare> shareListAfter = [SELECT Id from InteractionSummaryShare WHERE RowCause = 'Manual'];


            if (AmFam_Utility.isTriggerSwitchActive('InteractionSummary_Object') && AmFam_Utility.isTriggerSwitchActive('Participant_Object') && AmFam_Utility.isTriggerSwitchActive('Producer_Object')) {
                //Compare both snapshots. They shoul be equal to confirm a new InteractionSummaryParticipantShare record was deleted
                System.assertEquals(shareListBefore.size(), shareListAfter.size());
            }

 

        }

    }



    private static InteractionSummary insertInteractionSummary(String interactionSummaryName, Account agent) {
        InteractionSummary interactionSummary = new InteractionSummary();
        interactionSummary.Name = interactionSummaryName;
        if (agent != null) {
            interactionSummary.AccountId = agent.Id;
        }

        insert interactionSummary;

        return interactionSummary;

    }
    

    private static Participant__c insertParticipant(Account acc, InteractionSummary interactionSummary) {

        Participant__c p = new Participant__c();
        p.Agent__c = acc.Id;
        p.Interaction_Summary__c = interactionSummary.Id;

        insert p;

        return p;

    }

    private static Producer insertProducer(Account acc, User internalUser, String identifier) {

        Producer producer = new Producer();
        producer.AccountId = acc.Id;
        producer.InternalUserId = internalUser.Id;
        producer.Name = identifier;

        insert producer;

        return producer;

    }

    private static ProducerAccess__c insertProducerAccess(Account acc, String identifier) {
        ProducerAccess__c pa = new ProducerAccess__c();
        pa.Producer_Identifier__c = identifier;
        pa.RelatedId__c = acc.Id;
        pa.Reason__c = 'LEAD';

        insert pa;

        return pa;
    }


    private static User insertUser(String profileName, String aliasName, String fisrtName, String lastName) {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name =: profileName];
        User agent = new User (     alias = aliasName, 
                                    email= aliasName + '12456@xyz.com',
                                    emailencodingkey='UTF-8', 
                                    FederationIdentifier= aliasName + '12456',
                                    firstname=fisrtName, 
                                    lastname=fisrtName,
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', 
                                    profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username= aliasName + 'test12456@xyz.com.test',
                                    AmFam_User_ID__c= aliasName + '1234'
                                );
        
        insert agent;

        return agent;

    }


    private static Account insertAgencyAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_ID__c = '12345';

        insert objAcc;
        return objAcc;
    }

    private static Account insertAgentAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;

        Contact objContact = new Contact();
        objContact.FirstName = accName;
        objContact.LastName = accName;
        objContact.AccountId = objAcc.Id;
        insert objContact;


        return objAcc;
    }


    private static Account insertPersonAccount(String firstName, String lastName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.FirstName = firstName;
        objAcc.LastName = lastName;
        objAcc.RecordTypeId = devRecordTypeId;
        //objAcc.Producer_ID__c = '12345';
        objAcc.CDHID__c = lastName + '98765';

        insert objAcc;
        return objAcc;
    }


}