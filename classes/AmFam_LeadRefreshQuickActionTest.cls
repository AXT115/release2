/**
*    @description  Test Code to check AmFam_LeadRefreshQuickActionController Class
*    @author Salesforce Services
*    @date 09/12/2021
*    @group @Test
*/
@isTest
private class AmFam_LeadRefreshQuickActionTest {

  @TestSetup
  static void bypassProcessBuilderSettings() {
    ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
    settings.Bypass_Process_Builder__c = true;
    upsert settings ByPass_Process_Builder__c.Id;
  }

  @isTest
  static void testRefreshLead() {

    Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
    User u = new User (alias = 'srep', email='test1246@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='test1246',
                                lastname='TestRep',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='test1246@xyz.com.test',
                                AmFam_User_ID__c = '12345'
                                );
    AmFam_UserService.callProducerAPI = false;
    insert u;
      
    Test.setMock(HttpCalloutMock.class, new wsPartySearchServiceCalloutHttpMock());   
      
    system.runAs(u){
        Account agency = new Account();
        agency.Name = 'Test Agency';
        agency.Producer_ID__c = '12345';
        agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
        insert agency;
    
        Lead l = new Lead();
        l.Agency__c = agency.Id;
        l.FirstName = 'First';
        l.LastName = 'Last';
        l.LeadSource = 'Affinity';
        l.Email = 'testingRefreshLead@test.com';
        l.Email_Usage__c = 'HOME';
        l.Lines_of_Business__c = 'Personal Vehicle';
        l.CDHID__c = '543533';
        insert l;
        wsPartyManage.lpeCallout = false;
    
        Test.startTest();
        Lead leadRec = new Lead();
        try{
        	 leadRec = AmFam_LeadRefreshQuickActionController.refreshLead(l.id);
        }
        catch(AuraHandledException e){
             System.assert(e.getMessage() !=null);
        }   
        Test.stopTest();
    }
    
  }
}