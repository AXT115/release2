public class AmFam_CreateUserAccountsQueueable implements Queueable,Database.AllowsCallouts {

    //Comments on approach with regard to future bulkification
    //
    //The intended approach would be to change collectExistingUserInfo to take a list of Ids and return a 
    //list of ExistingUserInfo objects
    //setupUser would be likewise changed to accept that list of ExistingUserInfo objects and process them.
    //Additional changes would be necessary to refactor the collection of the records to be inserted so they could 
    //be processed at the end of processing the ExistingUserInfo objects.
    //
    public class ExistingUserInfo
    {
        public Account individualAccount {get; private set;}
        public Account agencyAccount {get; private set;}
        public Contact contact {get; private set;}
        public AccountContactRelation acr {get; private set;}
        public List<ProducerAccess__c> producerAccess {get; private set;}
        public User u {get; private set;}
        public Id userId {get; private set;}
    }

    @TestVisible
    private static Boolean chainNextQueueable = true;

    private List<Id> userRecIds;
    private static Id individualRecordTypeId;        
    private static Id agencyRecordTypeId;  
    private static Id contactRecordTypeId;

    static {
        individualRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();  
        contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
    }

    public AmFam_CreateUserAccountsQueueable(List<Id> userRecIds)
    {
        this.userRecIds = userRecIds;
        // individualRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        // agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();  
        // contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
    }

    //This is setup for JitSSO only and is not bulkified, only expects a single record
    public void execute(QueueableContext context) 
    {

        if (userRecIds.size() > 0)
        {                                        
            AmFam_CreateUserAccountsQueueable.processUser(userRecIds[0]);
        }

        if (chainNextQueueable && !Test.isRunningTest())
        {
            System.enqueueJob(new AmFam_ProducerUpdateQueueable(userRecIds));
        }
    }

    public static void processUser(Id userId)
    {
        System.debug('PU:'+userId);
        ExistingUserInfo userInfo = AmFam_CreateUserAccountsQueueable.collectExistingUserInfo(userId);
        System.debug('PUINFO:'+userInfo);
        AmFam_CreateUserAccountsQueueable.setupUser(userInfo);                                       
    }

    public static ExistingUserInfo collectExistingUserInfo(Id userId)
    {
        Set<String> producerIds = new Set<String>();
        Set<Id> accountIds = new Set<Id>();

        ExistingUserInfo existingInfo = new ExistingUserInfo();
        existingInfo.userId = userId;

        List<User> userRecs = [SELECT Id, AmFam_User_ID__c, Name, Producer_ID__c, Profile.Name, ProfileId,
                                        FirstName,MiddleName,LastName,Email,FederationIdentifier, Phone, Title
                                        FROM User WHERE Id = :userId];


        if (userRecs.size() > 0)
        {
            existingInfo.u = userRecs[0];

            String acctProducerId = (existingInfo.u.Profile.Name == 'AmFam Agent') ? AmFam_CreateUserAccountsQueueable.agentIndividualAccountProducerId(existingInfo.u.Producer_ID__c): existingInfo.u.Producer_ID__c;
        
            List<Account> a = [SELECT Id,Producer_ID__c FROM Account WHERE RecordTypeId = :AmFam_CreateUserAccountsQueueable.individualRecordTypeId 
                                AND Name = :existingInfo.u.Name
                                AND Producer_ID__c = :acctProducerId
                                LIMIT 1];

            if (a.size() > 0)
            {
                existingInfo.individualAccount = a[0];
                producerIds.add(existingInfo.individualAccount.Producer_ID__c);
                accountIds.add(existingInfo.individualAccount.Id);
            }

            if (existingInfo.u.Profile.Name == 'AmFam Agent')
            {
                a = [SELECT Id,Producer_ID__c FROM Account WHERE RecordTypeId = :agencyRecordTypeId 
                                    AND Producer_ID__c = :existingInfo.u.Producer_ID__c
                                    LIMIT 1];

                if (a.size() > 0)
                {
                    existingInfo.agencyAccount = a[0];
                    producerIds.add(existingInfo.agencyAccount.Producer_ID__c);
                    accountIds.add(existingInfo.agencyAccount.Id);
                }
            }

            List<Contact> c = [SELECT Id FROM Contact WHERE User_Federation_ID__c = :existingInfo.u.FederationIdentifier 
                                LIMIT 1];
            if (c.size() > 0)
            {
                existingInfo.contact = c[0];
            }

            if (existingInfo.agencyAccount != null && existingInfo.contact != null)
            {
                List<AccountContactRelation> acr = [SELECT Id FROM AccountContactRelation WHERE AccountId = :existingInfo.agencyAccount.Id
                                                    AND ContactId = :existingInfo.contact.Id
                                                    LIMIT 1];
                if (acr.size() > 0)
                {
                    existingInfo.acr = acr[0];
                }            
            }

            List<ProducerAccess__c> pa = [SELECT Id,Producer_Identifier__c,RelatedId__c FROM ProducerAccess__c WHERE RelatedId__c IN :accountIds 
                                            AND Producer_Identifier__c IN :producerIds
                                            AND Reason__c = 'ENTRY'];
            existingInfo.producerAccess = pa;
        }

        return existingInfo;
    }

    private static void setupUser(ExistingUserInfo userInfo)
    {
        Profile agentProfile = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent' LIMIT 1];

        List<Account> newAccounts = new List<Account>();
        Map<String,Account> agencyAccountMap = new Map<String,Account>();
        List<Contact> newContacts = new List<Contact>();
        List<AccountContactRelation> newACR = new List<AccountContactRelation>();
        
        Map<String,Account> prodIdAccountMap = new Map<String,Account>();

        Id contactOwnerId = [SELECT Id FROM User WHERE Name = 'Amfam Admin' LIMIT 1][0].Id;

        AmFam_ProducerTypeService agencyResp;

        //get the user recs with the profile name included so we can determine agent/non-agent
        //not sure this is needed yet
        // List<User> userProfileRecs = [SELECT Id, AmFam_User_ID__c, Name, Producer_ID__c, Profile.Name, ProfileId,
        //                                 FirstName,MiddleName,LastName,Email,FederationIdentifier, Phone, Title
        //                                 FROM User WHERE Id IN :userRecIds];

        Map<Id,User> userProfileMap = new Map<Id,User>();  //maps userid->user rec
        Set<String> rawProducerIds = new Set<String>();  //collects all producer ids referenced on users
        Set<String> referencedProducerIds = new Set<String>();  //collects all producer ids referenced on users

        Boolean isAgent = (userInfo.u.Profile.Name == 'AmFam Agent');
        userProfileMap.put(userInfo.u.Id,userInfo.u);
        referencedProducerIds.add( (isAgent) ? agentIndividualAccountProducerId(userInfo.u.Producer_ID__c): userInfo.u.Producer_ID__c);
        rawProducerIds.add(userInfo.u.Producer_ID__c);

        //now get individual accounts for referenced prodIds out of an abundance of caution
        List<Account> accounts = [SELECT Id,Producer_ID__c from Account WHERE Producer_ID__c IN :referencedProducerIds AND RecordTypeId = :individualRecordTypeId];
        Set<String> existingIndProducerIds = new Set<String>();
        for (Account a : accounts)
        {
            existingIndProducerIds.add(a.Producer_ID__c);
        }

        List<Account> agencyAccts = [SELECT Id from Account WHERE Producer_ID__c = :rawProducerIds AND RecordTypeId = :agencyRecordTypeId];
        Set<String> existingAgencyProducerIds = new Set<String>();
        for (Account a : accounts)
        {
            existingAgencyProducerIds.add(a.Producer_ID__c);
        }
       
        // Individual - agent & others
        // Agency - for agent

        String acctProducerId = (isAgent) ? agentIndividualAccountProducerId(userInfo.u.Producer_ID__c): userInfo.u.Producer_ID__c;
        String district;

        if (!existingIndProducerIds.contains(acctProducerId))
        {
            AmFam_ProducerTypeService response = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/' + userInfo.u.Producer_ID__c + '/booksofbusiness');
                
            if (response?.bookOfBusiness != null)
            {
                for (AmFam_ProducerTypeService.BookOfBusiness bob : response?.bookOfBusiness) 
                {
                    if (bob.producerCodes != null)
                    {
                        String agencyId = bob.producerCodes[0]?.agencyId;
                        if (bob.role == 'AGENT' && agencyId != null) 
                        {
                            agencyResp = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/agencies/'+agencyId);
                        }
                        
                        for (AmFam_ProducerTypeService.ProducerCodes currCode : bob.producerCodes)
                        {
                            if (currCode.producerCode == userInfo.u.Producer_ID__c)
                            {
                                district = currCode.district;
                                break;
                            }
                        }
                    }
                }
            }
        }

        //up to here its all prep

        //@create indiv acct
        if (userInfo.individualAccount == null)
        {
            System.debug('Creating Indiv Account');
            Account acct = new Account();
            acct.Producer_ID__c = acctProducerId;
            acct.Name = userInfo.u.Name;
            acct.recordTypeId = individualRecordTypeId;
            if (userInfo.u.ProfileId == agentProfile.Id)
            {
                acct.Agent_District_Code__c = district;
            }
            newAccounts.add(acct);
            userInfo.individualAccount = acct;
            prodIdAccountMap.put(userInfo.u.Producer_ID__c,acct);
        }

        //@create contact
        if (userInfo.contact == null)
        {
            System.debug('Creating Contact');
            Contact con = new Contact();
            con.OwnerId = contactOwnerId;
            con.FirstName = userInfo.u.FirstName;
            con.LastName = userInfo.u.LastName;
            con.Email = userInfo.u.Email;
            con.User_Federation_ID__c = userInfo.u.FederationIdentifier;
            con.MiddleName = userInfo.u.MiddleName;
            con.Phone = userInfo.u.Phone;
            con.Title = userInfo.u.Title;
            con.User_Producer_ID__c = userInfo.u.Producer_ID__c;
            con.recordTypeId = contactRecordTypeId;
            newContacts.add(con);
            userInfo.contact = con;
        }

    if (isAgent)
    {
        if (!existingAgencyProducerIds.contains(userInfo.u.Producer_ID__c) && userInfo.agencyAccount == null)
        {
            //@create agency acct
            System.debug('Creating Agency Account');
            Account acct = new Account();
            acct.Producer_ID__c = userInfo.u.Producer_ID__c;
            acct.Name = userInfo.u.Name + ' Agency'; //???
            acct.recordTypeId = agencyRecordTypeId;
            if (agencyResp != null)
            {
                AmFam_UserService.updateAgencyAccountFromAgencyResponse(acct, agencyResp);
            }
            newAccounts.add(acct);
            userInfo.agencyAccount = acct;
            agencyAccountMap.put(userInfo.u.Producer_ID__c,acct);
        }
    }

    if (!newAccounts.isEmpty())
    {
        insert newAccounts;
    }

    //@ create prod access
    List<ProducerAccess__c> prodAccessList = new List<ProducerAccess__c>();

    Map<String,ProducerAccess__c> exisitngProdAccess = new Map<String,ProducerAccess__c>();
    for (ProducerAccess__c pa : userInfo.producerAccess)
    {
        exisitngProdAccess.put(pa.Producer_Identifier__c,pa);
    }

    List<Account> userAccounts = new List<Account>{userInfo.individualAccount};
    if (isAgent)
    {
        userAccounts.add(userInfo.agencyAccount);
    }

    for (Account acct : userAccounts)
    {
        if (!exisitngProdAccess.containsKey(acct.Producer_ID__c))
        {
            ProducerAccess__c prodAccess = new ProducerAccess__c();
            prodAccess.RelatedId__c = acct.Id;
            prodAccess.Producer_Identifier__c = acct.Producer_ID__c;
            prodAccess.Reason__c = 'ENTRY';
            prodAccessList.add(prodAccess);
            userInfo.producerAccess.add(prodAccess);
        }
        else 
        {
            //it exists and is supposed to so 
            exisitngProdAccess.remove(acct.Producer_ID__c);    
        }
    }

    if (!prodAccessList.isEmpty())
    {
        insert prodAccessList;
    }

    //anything that remains in exisitngProdAccess must no longer be valid
    if (!exisitngProdAccess.isEmpty())
    {
        delete exisitngProdAccess.values();
    }

    if (!newContacts.isEmpty())
    {
        //fill in associated Account Ids
        for (Contact contact : newContacts)
        {
            contact.AccountId = userInfo.individualAccount.Id;
        }
        insert newContacts;
    }

    //@create ACR
    if (userInfo.acr == null && userInfo.agencyAccount != null)
    {
        System.debug('Creating ACR');
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = userInfo.agencyAccount.Id;
        acr.ContactId = userInfo.contact.Id;  
        userInfo.acr = acr; 
        newACR.add(acr); 
    }

    
    if (!newACR.isEmpty())
    {
        insert newACR;
    }
}

    private static String agentIndividualAccountProducerId(string rawProdId)
    {
        return 'P-'+rawProdId;
    }

}