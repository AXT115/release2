@istest
global without sharing class AmFam_ConvertLeadToOpportunityTest {

    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    
    
    static testMethod void testConvertLeadToOpportunity() {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agent = new User (alias = 'saagent', email='test43456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        
        AmFam_UserService.callProducerAPI = false;
        insert agent;

        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = 'Test Account';
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_ID__c = '14535';

        insert objAcc;


        System.runAs(agent) {


            //Pre-grant read access to the account if the user does not have access to it
            AccountShare accountShareCreated = AmFam_Utility.preGrantAccountAccess(objAcc.Id, agent.Id);
            System.debug('### accountShareCreated: ' + accountShareCreated);

                
            Lead l = new Lead();
            l.Agency__c = objAcc.Id;
            l.FirstName = 'Test FirstName';
            l.LastName = 'Test LastName';
            l.Company = 'company';
            l.Phone = '9856458734';
            l.Street = '2 Bvld Street';
            l.City = 'TestTown';
            l.StateCode = 'KS';
            l.CountryCode = 'US';
            l.PostalCode = '13235';
            l.Email = 'test674@test.com';
            l.Email_Usage__c = 'HOME';
            l.Marital_Status__c = 'M';
            l.Gender__c = 'M';
            l.Date_Of_Birth__c = Date.today()-100;
            l.LeadSource = 'Amfam.com';
            l.Company = 'Test';
            l.Lines_of_Business__c = 'Personal Vehicle;Property';
            l.Party_Level__c = 'Entrant';
            l.Disposition_Reason__c = 'New';
            insert l;
    
                
            Test.startTest();
            AmFam_ConvertLeadToOpportunity.convertLeadToOpportunity(l.id, objAcc.Id);
            Test.stopTest();

            List<opportunity> opplist = [select id, Name from Opportunity];
            System.assertEquals(2, opplist.size());
            
        }
    }

}