public class AmFam_InteractionSummaryService {


    //Only creates grant access
    public static void grantSameAccessFromAccount (List<InteractionSummary> newRecords) {
        AmFam_InteractionSummaryService.grantSameAccessFromAccount(newRecords, null, false);
    }

    //Grant Access to Interaction Summaries if the Related Account got new AccuntShare records
    public static void grantSameAccessFromAccount (List<AccountShare> accountShares) {

        Set<String> accountIds = new Set<String>();
        Set<Id> interactionSummaryIds = new Set<Id>();
        Map<Id, Set<Id>> sharesIdbyAccountId = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> interactionSummaryByAccountId = new Map<Id, Set<Id>>();

        List<AccountShare> accountShareWithProfileName = [SELECT Id, AccountId, UserOrGroup.Profile.Name, UserOrGroupId FROM AccountShare WHERE Id IN: accountShares];

        //Get Exclussion Profiles
        List<String> profilesToExclude = new List<String>{'AmFam Agent', 'Agent CSR'};

        
        for (AccountShare accs : accountShareWithProfileName) {

            Id accountId = accs.AccountId;
            Id userId = accs.UserOrGroupId;
            String profileName = (accs.UserOrGroupId != null && accs.UserOrGroup.ProfileId != null) ? accs.UserOrGroup.Profile.Name : null;

            if (!profilesToExclude.contains(profileName)) {

                Set<Id> sharesForAccount = sharesIdbyAccountId.get(accountId);
                if (sharesForAccount == null) {
                    sharesForAccount = new Set<Id>();
                }

                accountIds.add(accountId);
                sharesForAccount.add(userId);
                sharesIdbyAccountId.put(accountId, sharesForAccount);
            }
        }

        //Get all Interaction Summaries directly associated to those accounts 
        List<InteractionSummary> insteractionSummaries = [SELECT Id, AccountId FROM InteractionSummary WHERE AccountId IN: accountIds ];

        if (insteractionSummaries != null && insteractionSummaries.size() > 0) {
            for (InteractionSummary intSum : insteractionSummaries ) {
                Id accountId = intSum.AccountId;


                Set<Id> interactionSummariesForAccount = interactionSummaryByAccountId.get(accountId);
                if (interactionSummariesForAccount == null) {
                    interactionSummariesForAccount = new Set<Id>();
                }

                interactionSummariesForAccount.add(intSum.Id);
                interactionSummaryByAccountId.put(accountId, interactionSummariesForAccount);

                interactionSummaryIds.add(intSum.Id);
            }
        }


        //Get all Interaction Summaries directly associated to those accounts 
        List<Participant__c> participants = [SELECT Id, Agent__c, Interaction_Summary__c FROM Participant__c WHERE Agent__c IN: accountIds];

        if (participants != null && participants.size() > 0) {
            for (Participant__c part : participants ) {
                Id accountId = part.Agent__c;


                Set<Id> interactionSummariesForAccount = interactionSummaryByAccountId.get(accountId);
                if (interactionSummariesForAccount == null) {
                    interactionSummariesForAccount = new Set<Id>();
                }

                interactionSummariesForAccount.add(part.Interaction_Summary__c);
                interactionSummaryByAccountId.put(accountId, interactionSummariesForAccount);

                interactionSummaryIds.add(part.Interaction_Summary__c);
            }

        }


        //Get the list of Keys: InteractionSummaryId-ParticipantId from existing Interaction Summary Participants
        Set<String> interactionSummaryKeys = getInteractionSummaryShareKeys(interactionSummaryIds);

        List<InteractionSummaryShare> interactionSummaryShareToInsert = new List<InteractionSummaryShare>();

        //Process Accunt Shares and replicate to Interaction Summaries
        for (AccountShare accs : accountShareWithProfileName) {
            
            Id accountId = accs.AccountId;
            Id userId = accs.UserOrGroupId;
            String profileName = (accs.UserOrGroupId != null && accs.UserOrGroup.ProfileId != null) ? accs.UserOrGroup.Profile.Name : null;

            if (!profilesToExclude.contains(profileName)) {

                Set<Id> interactionSummariesForAccount = interactionSummaryByAccountId.get(accountId);

                if (interactionSummariesForAccount != null && interactionSummariesForAccount.size() > 0) {
                    for (Id intSumId : interactionSummariesForAccount) {
                        String key = intSumId + '-' + userId;

                        if (!interactionSummaryKeys.contains(key)) {
                            InteractionSummaryShare isp = new InteractionSummaryShare();
                            isp.UserOrGroupId = userId;
                            isp.ParentId = intSumId;
                            isp.RowCause = 'Manual';
                            isp.AccessLevel = 'Edit';

                            interactionSummaryShareToInsert.add(isp);

                            //Add key to existing set to avoid diplicate records
                            interactionSummaryKeys.add(key);
                        }
                        
                    }
                }
            }
        }


        if (interactionSummaryShareToInsert.size() > 0) {
            insert interactionSummaryShareToInsert;
        }

    }



    //Craetes grant access to new related account and remove the access related to old account
    public static void grantSameAccessFromAccount (List<InteractionSummary> newRecords, Map<Id, InteractionSummary> oldRecordsMap, Boolean deleteFirst) {

        Set<String> accountIds = new Set<String>();
        Set<Id> updatedInteractionSummaryIds = new Set<Id>();
        Set<Id> interactionSummaryIds = new Set<Id>();
        Map<Id, Id> accountByInsteractionSumaryId = new Map<Id, Id>();


        //Retrieve the list of Accounts associates to the insteractionsummary and map it by interaction summary ID
        for (InteractionSummary is : newRecords) {
            Id isID = is.Id;
            Id accountId = is.AccountId;

            if (String.isNotEmpty(accountId)) {
                accountByInsteractionSumaryId.put(is.Id, accountId);
                accountIds.add(accountId);
            }
        

            //Retrieve the list of Interaction Summary having the Related account changed
            if (deleteFirst) {
                InteractionSummary oldIS = oldRecordsMap.get(isID);
                Id oldAccountId = oldIS.AccountId;

                if (accountId != oldAccountId) {
                    updatedInteractionSummaryIds.add(isID);
                }
            }

            interactionSummaryIds.add(isID);
        
        }

        //Get Exclussion Profiles
        List<String> profilesToExclude = new List<String>{'AmFam Agent', 'Agent CSR'};
        

        //Retrieve Shared Access from those accounts
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId, AccountId FROM AccountShare WHERE RowCause = 'Manual' AND AccountId IN: accountIds AND UserOrGroup.Profile.Name NOT IN: profilesToExclude];

        system.debug('### accountShares:  ' + accountShares);


        //Create Map of user shares indexed by AccountId
        Map<Id, Set<Id>> sharesByAccount = new Map<Id, Set<Id>>();

        for (AccountShare accSh : accountShares) {
            Id accountId = accSh.Accountid;
            Id userId = accSh.UserOrGroupId;

            Set<Id> sharesForAccount = sharesByAccount.get(accountId);
            if (sharesForAccount == null) {
                sharesForAccount = new Set<Id>();
            }

            sharesForAccount.add(userId);
            sharesByAccount.put(accountId, sharesForAccount);

        }

        
        Set<String> existingRecords = getInteractionSummaryShareKeys(interactionSummaryIds);


        system.debug('### existingRecords:  ' + existingRecords);

        //If the execution comes from an UPDATE, we may need to delete the shares of the old account related to the interaction summaries
        if (deleteFirst) {
            Set<String> recordsToKeep = getInteractionSummaryShareKeysFromParticipants(updatedInteractionSummaryIds);
            AmFam_InteractionSummaryService.removeShareRecords(newRecords, oldRecordsMap, recordsToKeep);
        }


        //Create the InteractionSummaryShares
        List<InteractionSummaryShare> newShareList = new List<InteractionSummaryShare>();

        for (Id isId : accountByInsteractionSumaryId.keySet()) {

            Id accountId = accountByInsteractionSumaryId.get(isId);

            Set<Id> sharesForAccount = sharesByAccount.get(accountId);
            if (sharesForAccount != null && sharesForAccount.size() > 0) {


                for (Id userId : sharesForAccount) {

                        String key = isId + '-' + userId;

                        //Do not insert a record if there is already a combination for the same InteractionSummaryId-ParticipantId
                        if (!existingRecords.contains(key)) {

                            InteractionSummaryShare newShare = new InteractionSummaryShare();
                            newShare.ParentId = isId;
                            newShare.UserOrGroupId = userId;
                            newShare.RowCause = 'Manual';
                            newShare.AccessLevel = 'Edit';

                            newShareList.add(newShare);
                    }
                }
            }

        }


        if (newShareList.size() > 0)
            insert newShareList;

    }


    private static void removeShareRecords(List<InteractionSummary> newRecords, Map<Id, InteractionSummary> oldRecordsMap, Set<String> recordsToKeep) {

        Map<Id, Id> accountByInsteractionSumaryId = new Map<Id, Id>();
        Map<Id, List<AccountParticipant>> accountParticipantByAccountId = new Map<Id, List<AccountParticipant>>();
        Set<Id> interactionSummaryToRemoveShares = new Set<Id>();
        List<InteractionSummaryShare> listToDelete = new List<InteractionSummaryShare>();


        //Retrieve the list of Interaction Summary having the Related account changed
        for (InteractionSummary is : newRecords) {

            Id isID = is.Id;
            Id accountId = is.AccountId;

            InteractionSummary oldIS = oldRecordsMap.get(isID);
            Id oldAccountId = oldIS.AccountId;

            if (String.isNotEmpty(oldAccountId) && accountId != oldAccountId) {
                interactionSummaryToRemoveShares.add(isID);
            }
        }

        //Query all Interaction Summarie Participants for the Interaction Summaries with changed account
        List<InteractionSummaryShare> candidateToDelete = [SELECT Id, ParentId,  UserOrGroupId
                                                                FROM InteractionSummaryShare
                                                                WHERE ParentId IN : interactionSummaryToRemoveShares];


        //Do not remove those records that share the participant with the related Participant__c records
        for (InteractionSummaryShare isp : candidateToDelete) {

            String key = isp.ParentId + '-' + isp.UserOrGroupId;
            if(!recordsToKeep.contains(key)) {
                listToDelete.add(isp);
            }
        }

        
        //Process the delete of Interaction Summary Participant of Interaction Summaries that changes Account
        delete listToDelete;


    }



    //Remove Access to Interaction Summaries if the Related Account loses AccuntShare records
    public static void removeAccessFromAccount (List<AccountShare> accountShares) {

        Set<String> accountIds = new Set<String>();
        Set<Id> interactionSummaryIds = new Set<Id>();
        Map<Id, Set<Id>> sharesIdbyAccountId = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> interactionSummaryByAccountId = new Map<Id, Set<Id>>();

        //Get Exclussion Profiles
        List<String> profilesToExclude = new List<String>{'AmFam Agent', 'Agent CSR'};

        
        for (AccountShare accs : accountShares) {

            Id accountId = accs.AccountId;
            Id userId = accs.UserOrGroupId;
            Set<Id> sharesForAccount = sharesIdbyAccountId.get(accountId);
            if (sharesForAccount == null) {
                sharesForAccount = new Set<Id>();
            }

            accountIds.add(accountId);
            sharesForAccount.add(userId);
            sharesIdbyAccountId.put(accountId, sharesForAccount);
        }

        //Get all Interaction Summaries directly associated to those accounts 
        List<InteractionSummary> insteractionSummaries = [SELECT Id, AccountId FROM InteractionSummary WHERE AccountId IN: accountIds ];

        if (insteractionSummaries != null && insteractionSummaries.size() > 0) {
            for (InteractionSummary intSum : insteractionSummaries ) {
                Id accountId = intSum.AccountId;


                Set<Id> interactionSummariesForAccount = interactionSummaryByAccountId.get(accountId);
                if (interactionSummariesForAccount == null) {
                    interactionSummariesForAccount = new Set<Id>();
                }

                interactionSummariesForAccount.add(intSum.Id);
                interactionSummaryByAccountId.put(accountId, interactionSummariesForAccount);

                interactionSummaryIds.add(intSum.Id);
            }
        }


        //Get all Interaction Summaries directly associated to those accounts 
        List<Participant__c> participants = [SELECT Id, Agent__c, Interaction_Summary__c FROM Participant__c WHERE Agent__c IN: accountIds];

        if (participants != null && participants.size() > 0) {
            for (Participant__c part : participants ) {
                Id accountId = part.Agent__c;


                Set<Id> interactionSummariesForAccount = interactionSummaryByAccountId.get(accountId);
                if (interactionSummariesForAccount == null) {
                    interactionSummariesForAccount = new Set<Id>();
                }

                interactionSummariesForAccount.add(part.Interaction_Summary__c);
                interactionSummaryByAccountId.put(accountId, interactionSummariesForAccount);

                interactionSummaryIds.add(part.Interaction_Summary__c);
            }

        }


        //Get the list of Keys: InteractionSummaryId-ParticipantId from existing Interaction Summary Participants
        Map<String, InteractionSummaryShare> interactinSummaryIdMap = new Map<String, InteractionSummaryShare>();
        List<InteractionSummaryShare> shareList = [SELECT Id, ParentId, UserOrGroupId FROM InteractionSummaryShare WHERE ParentId IN: interactionSummaryIds AND RowCause = 'Manual'];

        for (InteractionSummaryShare iss : shareList) {
            String key = iss.ParentId + '-' + iss.UserOrGroupId;
            interactinSummaryIdMap.put(key, iss);
        }


        List<InteractionSummaryShare> interactionSummaryShareToDelete = new List<InteractionSummaryShare>();

        //Process Accunt Shares and replicate to Interaction Summaries
        for (AccountShare accs : accountShares) {
            
            Id accountId = accs.AccountId;
            Id userId = accs.UserOrGroupId;
            Set<Id> interactionSummariesForAccount = interactionSummaryByAccountId.get(accountId);

            if (interactionSummariesForAccount != null && interactionSummariesForAccount.size() > 0) {
                for (Id intSumId : interactionSummariesForAccount) {
                    String key = intSumId + '-' + userId;

                    if (interactinSummaryIdMap.containsKey(key)) {
                        InteractionSummaryShare isp = interactinSummaryIdMap.get(key);
                        interactionSummaryShareToDelete.add(isp);

                        //Remove key to avoid diplicate records on the delete list
                        interactinSummaryIdMap.remove(key);
                    }
                    
                }
            }
            
        }


        if (interactionSummaryShareToDelete.size() > 0) {
            delete interactionSummaryShareToDelete;
        }

    }


   
    private static Set<String> getInteractionSummaryShareKeysFromParticipants(Set<Id> interactionSumaryIds) {

        Set<String> keys = new Set<String>();


        //Build a Map of interaction Summaries indexed by Account ID
        List<Participant__c> participants = [SELECT Id, Interaction_Summary__c, Agent__c FROM Participant__c WHERE Interaction_Summary__c IN: interactionSumaryIds];

        Set<Id> accountIds = new Set<Id>();
        Map<Id, Set<id>> interactionSummaryByAccount = new Map<Id, Set<Id>>();

        for (Participant__c p : participants) {
            accountIds.add(p.Agent__c);

            Set<Id> interactionSummaries = interactionSummaryByAccount.get(p.Agent__c);
            if (interactionSummaries == null)
                interactionSummaries = new Set<Id>();

            interactionSummaries.add(p.Interaction_Summary__c);
            interactionSummaryByAccount.put(p.Agent__c, interactionSummaries);

        }



        //Retrieve the list of Share Records and build a Set of keys: InteractionSummaryId-UserOrGroupId
        List<AccountShare> shareRecords = [SELECT Id, AccountId, UserOrGroupId FROM AccountShare WHERE RowCause = 'Manual' AND AccountId IN: accountIds];

        for (AccountShare ap : shareRecords) {
            String accountId = ap.AccountId;
            String participantId = ap.UserOrGroupId;

            Set<Id> interactionSummaries = interactionSummaryByAccount.get(accountId);

            if (interactionSummaries != null) {
                for (Id interactionSummaryId : interactionSummaries) {
                    String key = interactionSummaryId + '-' + participantId;
                    keys.add(key);
                }
            }
        }


        return keys;



    }



    
    private static Set<String> getInteractionSummaryShareKeys(Set<Id> interactionSumaryIds) {

        Set<String> keys = new Set<String>();

        //Retrieve all the Interaction Summary Share records related to the INteraction Summaries and build the set of Keys: InteractionSummaryId-ParticipantId
        List<InteractionSummaryShare> isps = [SELECT UserOrGroupId, ParentId FROM InteractionSummaryShare WHERE ParentId IN: interactionSumaryIds];

        for (InteractionSummaryShare isp : isps) {
            String key = isp.ParentId + '-' + isp.UserOrGroupId;
            keys.add(key);
        }

        return keys;
    }   


 

}