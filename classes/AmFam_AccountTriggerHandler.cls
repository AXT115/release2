/**
*   @description trigger handler for Account object. Note: this should be the only place
*	where ALL Account related trigger logic should be implemented.
*   @author Salesforce Services
*   @date 02/08/2020
*   @group Trigger
*/

public without sharing class AmFam_AccountTriggerHandler {
    
    
    public void OnBeforeInsert (List<Account> newRecord) {
        //the code extending PCIFY to Account object
        AmFam_AccountService.extendPCIFYToAccount();
        //Run account validations
        AmFam_AccountService.runAccountTriggerValidations(newRecord);
        //Change account owner to the agency user
        AmFam_AccountService.changeAccountOwner(newRecord); 

    }
    
    public void OnAfterInsert (List<Account> newRecords, Map<Id,Account> newRecordMap) {

        //Creates Address, Location and AssociatedLocation records
        //AmFam_AccountService.createsAddressRecord(newRecords);
        AmFam_AccountService.createProducerAccessForRelatedAgency(newRecords, newRecordMap);
        AmFam_AccountService.createAndAssociateHouseholdAccount(newRecords);
    }

    /*
    public void OnAfterUpdate (List<Account> newRecords,
                               List<Account> oldRecords,
                               Map<ID, Account> newRecordsMap,
                               Map<ID, Account> oldRecordsMap) {
    }*/


    public void OnBeforeUpdate(List<Account> newRecords,
                                List<Account> oldRecords,
                                Map<ID, Account> newRecordsMap,
                                Map<ID, Account> oldRecordsMap) {
                                
        //the code extending PCIFY to Account object
        AmFam_AccountService.extendPCIFYToAccount();
        //Run account validations
        AmFam_AccountService.runAccountTriggerValidations(newRecords);

    }
    
}