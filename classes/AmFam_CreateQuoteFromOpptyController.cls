/**
*   @description: the apex class is the server side controller for aura component AmFam_CreateQuoteFromOpportunity 
*	The component launches the dynamic URL to Policy Quote system from SF UI based on the product the user select on Opportunity.
*   @author: Anbu Chinnathambi
*   @date: 07/30/2021
*   @group: Apex Class
*/

public without sharing class AmFam_CreateQuoteFromOpptyController {
    
    //the method returns ProductCode options based on the Line of Business and returns Oppy LOB's product as a first value, so that it can be set as default value
    @AuraEnabled       
    public static Map<String, String> getPicklistValues(String recordId){
        
        Map<String, String> productCodeMap = new Map<String, String>();
        productCodeMap.put('', '--None--');
        productCodeMap.put('CommercialFusion', 'Commercial (Fusion)');
        productCodeMap.put('CommercialClassic', 'Commercial (Classic)');
        productCodeMap.put('FarmRanchFusion', 'Farm/Ranch (Fusion)');
        productCodeMap.put('PropertyAdvance', 'Property (Advance)');
        productCodeMap.put('Life_Cornerstone', 'Life Cornerstone');
        productCodeMap.put('PersonalAuto', 'Personal Auto');
        
        
        Opportunity opp = [select id, Agency__r.Producer_ID__c, Lines_of_Business__c, Contact_CDHID__c From Opportunity where id=:recordId Limit 1];
        List<String> lobList = opp.Lines_of_Business__c?.split(';'); 
        Map<String, String> picklistProductCodeMap = new Map<String, String>{'Farm/Ranch'=> 'FarmRanchFusion','Life'=> 'Life_Cornerstone','Personal Vehicle'=> 'PersonalAuto','Property'=> 'PropertyAdvance'};
        Map<String, String> returnKeyValueMap = new Map<String, String>();
        if(lobList?.size()==1){
            string lob = lobList[0];
            
            //excluding commerical becuase it has two systems and health,watercraft,Umbrella& annuity 
            if(picklistProductCodeMap.keyset().contains(lob)){
                
                //adding the lob first to have a default value and adding commercial here since it has two systems
                returnKeyValueMap.put(picklistProductCodeMap.get(lob), productCodeMap.get(picklistProductCodeMap.get(lob)));
                returnKeyValueMap.put('CommercialClassic', 'Commercial (Classic)');
                returnKeyValueMap.put('CommercialFusion', 'Commercial (Fusion)');
                
                //adding rest of picklist values here
                Set<String> listOfLOBs = picklistProductCodeMap.keySet();
                listOfLOBs.remove(lob);
                for(string a: listOfLOBs){
                	returnKeyValueMap.put(picklistProductCodeMap.get(a), productCodeMap.get(picklistProductCodeMap.get(a)));
                    returnKeyValueMap.put(picklistProductCodeMap.get(a), productCodeMap.get(picklistProductCodeMap.get(a)));
                }
            }else if(lob == 'Commercial'){
                productCodeMap.remove('');
                returnKeyValueMap = productCodeMap;
            }  
            
         }
         //if Opportunity has more one Line of Business, we will open up all the options
         else{
            returnKeyValueMap = productCodeMap;
         }
         return returnKeyValueMap;
    }
    
    //the method helps to retireve dynamic URL from Custom metadata 
    @AuraEnabled       
    public static String getPolicyQuoteURL(String recordId, string quoteSystem){
        
        Opportunity opp = [select id, Agency__r.Producer_ID__c, Lines_of_Business__c, Contact_CDHID__c, Original_Account_Id__c, AccountId From Opportunity where id=:recordId Limit 1];

        if (String.isNotEmpty(opp.Original_Account_Id__c) && opp.AccountId != opp.Original_Account_Id__c) {

            //Grant read access to the account if the user does not have access to it
            AccountShare accountShareCreated = AmFam_Utility.preGrantAccountAccess(opp.Original_Account_Id__c, UserInfo.getUserId());


            opp.AccountId = opp.Original_Account_Id__c;
            
            update opp;


            //Remove Account share createed to allow the initial parent relationship
            if (accountShareCreated != null) {
                delete accountShareCreated;
            }
        }

        String org;
        system.debug('quoteSystem' + quoteSystem);
        
        //checking whether its Production, QA or INT
        if(URL.getSalesforceBaseUrl().getHost().contains('--')){
             org = URL.getSalesforceBaseUrl().getHost().substringAfter('--')?.contains('qa')? 'QA':'INT';
        }else{
             org = 'Production';
        }
        
        Map<String, String> orgURLMap = new Map<String, String>();
        Policy_Quote_System__mdt quote = [SELECT DeveloperName, MasterLabel, INT_URL__c, QA_URL__c, Production_URL__c, Product_Policy_Code__c FROM Policy_Quote_System__mdt where MasterLabel=: quoteSystem Limit 1];
        
        if(quote.DeveloperName == 'PersonalAuto' ||quote.DeveloperName == 'PropertyAdvance'){
            orgURLMap.put('Production', quote.Production_URL__c+'cdhID='+opp.Contact_CDHID__c+'&productCode='+quote.Product_Policy_Code__c+'&producerID='+opp.Agency__r.Producer_ID__c);
            orgURLMap.put('QA',quote.QA_URL__c+'cdhID='+opp.Contact_CDHID__c+'&productCode='+quote.Product_Policy_Code__c+'&producerID='+opp.Agency__r.Producer_ID__c);
            orgURLMap.put('INT',quote.INT_URL__c+'cdhID='+opp.Contact_CDHID__c+'&productCode='+quote.Product_Policy_Code__c+'&producerID='+opp.Agency__r.Producer_ID__c);   
        }else if(quote.DeveloperName == 'CommercialClassic'){
            orgURLMap.put('Production', quote.Production_URL__c+'cdhId='+opp.Contact_CDHID__c);
            orgURLMap.put('QA',quote.QA_URL__c+'cdhId='+opp.Contact_CDHID__c);
            orgURLMap.put('INT',quote.INT_URL__c+'cdhId='+opp.Contact_CDHID__c); 
        }else if(quote.DeveloperName == 'CommercialFusion'){
            orgURLMap.put('Production', quote.Production_URL__c+'partyID='+opp.Contact_CDHID__c+'&producerID='+opp.Agency__r.Producer_ID__c);
            orgURLMap.put('QA',quote.QA_URL__c+'partyID='+opp.Contact_CDHID__c+'&producerID='+opp.Agency__r.Producer_ID__c);
            orgURLMap.put('INT',quote.INT_URL__c+'partyID='+opp.Contact_CDHID__c+'&producerID='+opp.Agency__r.Producer_ID__c); 
        }else if(quote.DeveloperName == 'FarmRanchFusion'){
            orgURLMap.put('Production', quote.Production_URL__c+'partyID='+opp.Contact_CDHID__c+'&producerID='+opp.Agency__r.Producer_ID__c);
            orgURLMap.put('QA',quote.QA_URL__c+'partyID='+opp.Contact_CDHID__c+'&producerID='+opp.Agency__r.Producer_ID__c);
            orgURLMap.put('INT',quote.INT_URL__c+'partyID='+opp.Contact_CDHID__c+'&producerID='+opp.Agency__r.Producer_ID__c); 
        }else if(quote.DeveloperName == 'Life_Cornerstone'){
            orgURLMap.put('Production', quote.Production_URL__c+'cdhID='+opp.Contact_CDHID__c+'&initialTab=Client');
            orgURLMap.put('QA',quote.QA_URL__c+'cdhID='+opp.Contact_CDHID__c+'&initialTab=Client');
            orgURLMap.put('INT',quote.INT_URL__c+'cdhID='+opp.Contact_CDHID__c+'&initialTab=Client'); 
        }
        
        system.debug('orgURLMap' + orgURLMap);
        system.debug('returnURL' + orgURLMap.get(org));
        
        return orgURLMap.get(org).startsWith('http')?orgURLMap.get(org):'Error';
    }
}