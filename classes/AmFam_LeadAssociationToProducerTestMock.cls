global with sharing class AmFam_LeadAssociationToProducerTestMock implements HttpCalloutMock {
    wsPartyManageServiceCalloutHttpMock wsMock;
    AmFam_ProducerTypeServiceHttpCalloutMock ptsMock;

    public Boolean isError = false;
    public String callType;

    Boolean useAgentJSON = true;
    global String modifier;

    public AmFam_LeadAssociationToProducerTestMock() 
    {
        wsMock = new wsPartyManageServiceCalloutHttpMock();
        ptsMock = new AmFam_ProducerTypeServiceHttpCalloutMock(true);
    }

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        System.debug('MOCK - AmFam_LeadAssociationToProducerTestMock:'+request.toString());
        if (request.toString().contains('ProducerType_API'))
        {            
            System.debug('GOING PTS');
            ptsMock.useAgentJSON = useAgentJSON;
            ptsMock.modifier = modifier;
            return ptsMock.respond(request);
        }
        else 
        {
            System.debug('GOING WS');
            wsMock.isError = isError;
            wsMock.callType = callType;
            return wsMock.respond(request);
         }
    }
}