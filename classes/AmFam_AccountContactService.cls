public without sharing class AmFam_AccountContactService {

    public static void removeProudcerAccessFromHousehold(List<AccountContactRelation> oldRecords) {

        Id householdRTId = AmFam_Utility.getAccountHouseholdRecordType();
        Id personAccountRTId = AmFam_Utility.getPersonAccountRecordType();
        
        List<Id> accountIdList = new List<Id>();
        List<Id> contactIdList = new List<Id>();
        List<Id> contactAccountidList = new List<Id>();


        for (AccountContactRelation acr : oldRecords) {
            accountIdList.add(acr.AccountId);
            contactIdList.add(acr.ContactId);
        }

        Map<Id, Account> householdMap = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE RecordTypeId =: householdRTId AND Id IN: accountIdList]);
        Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Id, Name, AccountId FROM Contact WHERE Id IN: contactIdList AND Account.RecordTypeId =: personAccountRTId]);

        for (Contact contact : contactMap.values()) {
            contactAccountIdList.add(contact.AccountId);
        }

        List<ProducerAccess__c> producerAccessList = [SELECT Id,Name,Producer_Identifier__c,Reason__c,RelatedId__c FROM ProducerAccess__c WHERE RelatedId__c IN: contactAccountIdList OR RelatedId__c IN: accountIdList];
        Map<Id, List<ProducerAccess__c>> producerAccessByAccount = new Map<Id, List<ProducerAccess__c>>();

        Map<String, ProducerAccess__c> exisgingHouseholdProducerAccess = new Map<String, ProducerAccess__c>();

        for(ProducerAccess__c pracc : producerAccessList) {

            //Creates a Map of ProducerAccess records indexed by ContactAccountId
            if (contactAccountidList.contains(pracc.RelatedId__c)) {
                List<ProducerAccess__c> praccForAccount = producerAccessByAccount.get(pracc.RelatedId__c);
                if (praccForAccount == null) {
                    praccForAccount = new List<ProducerAccess__c>();
                }
                praccForAccount.add(pracc);

                producerAccessByAccount.put(pracc.RelatedId__c, praccForAccount);
            }

            //Creates a KEY set with exising ProducerAccess records associated to Househoolds
            else if (accountIdList.contains(pracc.RelatedId__c)) {
                String key = pracc.Producer_Identifier__c + '-' + pracc.RelatedId__c;
                exisgingHouseholdProducerAccess.put(key, pracc);
            }

        }


        List<ProducerAccess__c> producerAccessRecordsToDelete = new List<ProducerAccess__c>();
        Set<String> alreadyIncludedKeys = new Set<String>();

        for (AccountContactRelation acr : oldRecords) {
            Id householdId = acr.AccountId;
            Contact personContact = contactMap.get(acr.ContactId);

            if (personContact != null) {
                Id personAccountId = personContact.AccountId;

                List<ProducerAccess__c> praccForAccount = producerAccessByAccount.get(personAccountId);

                if (praccForAccount != null) {

                    for (ProducerAccess__c pracc : praccForAccount) {

                        String key = pracc.Producer_Identifier__c + '-' + householdId;
                        ProducerAccess__c praccToDelete =  exisgingHouseholdProducerAccess.get(key);

                        //If no producerAccess exists for Household then created new record
                        if (praccToDelete != null && !alreadyIncludedKeys.contains(key)) {
                            producerAccessRecordsToDelete.add(praccToDelete);
                            alreadyIncludedKeys.add(key);
                        }

                    }
                }

            }

        }

        if (producerAccessRecordsToDelete.size() > 0) {
            delete producerAccessRecordsToDelete;
        }
 
    }





    public static void extendProudcerAccessToHousehold(List<AccountContactRelation> newRecords) {
        
        Id householdRTId = AmFam_Utility.getAccountHouseholdRecordType();
        Id personAccountRTId = AmFam_Utility.getPersonAccountRecordType();
        
        List<Id> accountIdList = new List<Id>();
        List<Id> contactIdList = new List<Id>();
        List<Id> contactAccountidList = new List<Id>();


        for (AccountContactRelation acr : newRecords) {
            accountIdList.add(acr.AccountId);
            contactIdList.add(acr.ContactId);
        }

        Map<Id, Account> householdMap = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE RecordTypeId =: householdRTId AND Id IN: accountIdList]);
        Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Id, Name, AccountId FROM Contact WHERE Id IN: contactIdList AND Account.RecordTypeId =: personAccountRTId]);

        for (Contact contact : contactMap.values()) {
            contactAccountIdList.add(contact.AccountId);
        }

        List<ProducerAccess__c> producerAccessList = [SELECT Id,Name,Producer_Identifier__c,Reason__c,RelatedId__c FROM ProducerAccess__c WHERE RelatedId__c IN: contactAccountIdList OR RelatedId__c IN: accountIdList];
        Map<Id, List<ProducerAccess__c>> producerAccessByAccount = new Map<Id, List<ProducerAccess__c>>();

        Set<String> exisgingHouseholdProducerAccess = new Set<String>();

        for(ProducerAccess__c pracc : producerAccessList) {

            //Creates a Map of ProducerAccess records indexed by ContactAccountId
            if (contactAccountidList.contains(pracc.RelatedId__c)) {
                List<ProducerAccess__c> praccForAccount = producerAccessByAccount.get(pracc.RelatedId__c);
                if (praccForAccount == null) {
                    praccForAccount = new List<ProducerAccess__c>();
                }
                praccForAccount.add(pracc);

                producerAccessByAccount.put(pracc.RelatedId__c, praccForAccount);
            }

            //Creates a KEY set with exising ProducerAccess records associated to Househoolds
            else if (accountIdList.contains(pracc.RelatedId__c)) {
                String key = pracc.Producer_Identifier__c + '-' + pracc.RelatedId__c;
                exisgingHouseholdProducerAccess.add(key);
            }

        }


        List<ProducerAccess__c> producerAccessToAdd = new List<ProducerAccess__c>();

        for (AccountContactRelation acr : newRecords) {
            Id householdId = acr.AccountId;
            Contact personContact = contactMap.get(acr.ContactId);

            if (personContact != null) {
                Id personAccountId = personContact.AccountId;

                List<ProducerAccess__c> praccForAccount = producerAccessByAccount.get(personAccountId);

                if (praccForAccount != null) {

                    for (ProducerAccess__c pracc : praccForAccount) {

                        String key = pracc.Producer_Identifier__c + '-' + householdId;

                        //If no producerAccess exists for Household then created new record
                        if (!exisgingHouseholdProducerAccess.contains(key)) {

                            //Add the key to the existing set to avoid duplicates
                            exisgingHouseholdProducerAccess.add(key);

                            ProducerAccess__c newPracc = new ProducerAccess__c();
                            newPracc.Producer_Identifier__c = pracc.Producer_Identifier__c;
                            newPracc.Reason__c = pracc.Reason__c;
                            newPracc.RelatedId__c = householdId;
                            
                            producerAccessToAdd.add(newPracc);
                        }

                    }
                }

            }

        }

        if (producerAccessToAdd.size() > 0) {
            insert producerAccessToAdd;
        }


    }



    
}