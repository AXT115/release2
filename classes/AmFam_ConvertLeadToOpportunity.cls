/**
*   @description: the class handles lead conversion to opportunity, it generates one opportunity for each line of business  
*	@author: Anbu Chinnathambi
*   @date: 06/25/2021
*   @group: Apex Class
*/
public with sharing class AmFam_ConvertLeadToOpportunity {
    
    public static void convertLeadToOpportunity(Id leadId, Id accountId) {
        
        Lead ld = [Select id,Name,FinServ__RelatedAccount__c,Agency__c,Priority__c,Source__c,LeadSource, ll_Prior_Carrier_Name__c,ll_Owner_Renter__c,Next_Contact_Date__c,Last_Contact_Date__c,ll_Length_of_Residence__c, Lines_of_Business__c,FinServ__ReferredByContact__c from Lead where id =: leadId];
        
        if (String.isNotEmpty(ld.Lines_of_Business__c)) {
        
            List<String> lobList = ld.Lines_of_Business__c.split(';');
            system.debug('ld' + ld);
            system.debug('lobList' + lobList);
            Id oppRTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('General').getRecordTypeId();

            List<Opportunity> createOpportunity = new List<Opportunity>();
            for(string lob : lobList){
                Opportunity createOpp = new Opportunity();
                createOpp.Name = ld.Name +'-'+ld.LeadSource+'-'+lob;
                createOpp.AccountId = accountId;
                //createOpp.Related_Account__c = ld.FinServ__RelatedAccount__c;
                createOpp.ll_Prior_Carrier_Name__c	 = ld.ll_Prior_Carrier_Name__c;
                createOpp.ll_Owner_Renter__c = ld.ll_Owner_Renter__c;
                createOpp.Next_Contact_Date__c = ld.Next_Contact_Date__c;
                createOpp.ll_Length_of_Residence__c = ld.ll_Length_of_Residence__c;
                createOpp.Lines_of_Business__c = lob;
                createOpp.Last_Contact_Date__c = ld.Last_Contact_Date__c;
                createOpp.FinServ__ReferredByContact__c =ld.FinServ__ReferredByContact__c;
                createOpp.StageName = 'New';
                createOpp.CloseDate = System.today()+30;
                createOpp.RecordtypeId = oppRTId;
                createOpp.Description = ld.Name +'-'+ld.LeadSource+'-'+lob;
                createOpp.Priority__c =ld.Priority__c;
                createOpp.LeadSource =ld.LeadSource;
                createOpp.Source__c =ld.Source__c;
                createOpp.Agency__c = ld.Agency__c;
                createOpportunity.add(createOpp);
                
            }
            system.debug('createOpportunity' + createOpportunity);
            insert createOpportunity;
        }
    }
}