public class AmFam_ManageGroupMembershipModel {

    public static List<GroupMember> getGroupMemberUpdates(AmFam_AccountLeadSharingModel.ProducerWrapper producerWrapper){
        List<GroupMember> newGroupMemberList = new List<GroupMember>();
        List<GroupMember> groupMembersToDelete = new List<GroupMember>();
        List<GroupMember> groupMembersToAdd = new List<GroupMember>();

        //First Step is to get all existing GroupMember for the User that is being updated
        List<GroupMember> currentGroupMembershipList = [SELECT ID, GroupId, UserOrGroupId from GroupMember where UserOrGroupId = :producerWrapper.salesforceId];
        
        //Second Step is to add back all GroupMembership for incoming producer
        Set<Id> accountIDs = getAccountIdsForProducerId(producerWrapper.bookOfBusinessProducerIds);
        
        //Third Step is to gather Account Participants for all the Accounts
        List<Id> acctPartcipantIds = getAccountParticipantIdsForAccountIds(accountIDs);
        
        //Fourth Step is to get Groups for Account Participants
        List<Group> acctParticipantGroups = getGroupsForAccountParticipantIds(acctPartcipantIds);
        
        //Fifth Step is to create GroupMembers for every BOB Account.  
        for (Group g : acctParticipantGroups){
            GroupMember gm = new GroupMember();
            gm.GroupId = g.Id;
            gm.UserOrGroupId = producerWrapper.salesforceId;
            newGroupMemberList.add(gm);
        }

        //Sixth Step is to look through Current and New GroupMember list to determine which BOB may
        //have been deleted since last time User was updated
        groupMembersToDelete = getGroupMemberListToDelete(currentGroupMembershipList, newGroupMemberList);
        
        //Seventh Step is took look at all new GroupMembers and if exists in current GroupMembers,
        //We would ignore it because GroupMember is already established
        groupMembersToAdd = getGroupMemberListToAdd(currentGroupMembershipList, newGroupMemberList);

        System.debug('GroupMembers Being Deleted');
        for (GroupMember d_gm : groupMembersToDelete){
            System.debug('Deleting GroupmemberShip for: Id: ' + d_gm.Id + ' GroupId: ' + d_gm.GroupId + ' UserOrGroupId: ' + d_gm.UserOrGroupId);
        }
        System.debug('GroupMembers Being Added');
        for (GroupMember a_gm : groupMembersToAdd){
            System.debug('Adding GroupmemberShip for: Id: ' + a_gm.Id + ' GroupId: ' + a_gm.GroupId + ' UserOrGroupId: ' + a_gm.UserOrGroupId);
        }
        delete groupMembersToDelete;

        return groupMembersToAdd;

    }

    public static List<GroupMember> getGroupMemberListToDelete (List<GroupMember> currentGroupMembershipList, List<GroupMember> newGroupMemberList){
        List<GroupMember> groupMembersToDelete = new List<GroupMember>();

        for (GroupMember c_gm : currentGroupMembershipList){
            boolean isCurrentGroupMemberInNewList = false;
            for (GroupMember n_gm : newGroupMemberList){
                if (c_gm.GroupId == n_gm.GroupId && c_gm.UserOrGroupId == n_gm.UserOrGroupId){
                    isCurrentGroupMemberInNewList = true;
                    break;
                }
            }
            // If we don't find the CurrentGroupmember in newGroupMemberList, that means that BOB ProducerID has been removed for the producer coming through
            // So we need to delete the GroupMember.  
            if (!isCurrentGroupMemberInNewList){
                groupMembersToDelete.add(c_gm);
            }
            isCurrentGroupMemberInNewList = false;
        }


        return groupMembersToDelete;
    }

    public static List<GroupMember> getGroupMemberListToAdd (List<GroupMember> currentGroupMembershipList, List<GroupMember> newGroupMemberList){
        List<GroupMember> groupMembersToAdd = new List<GroupMember>();

        for (GroupMember n_gm : newGroupMemberList){
            boolean isInCurrentGroupMemberList = false;
            for (GroupMember c_gm : currentGroupMembershipList){
                if (n_gm.GroupId == c_gm.GroupId && n_gm.UserOrGroupId == c_gm.UserOrGroupId){
                    isInCurrentGroupMemberList = true;
                    break;
                }
            }
            //If we did not find GroupMember in the current list, that means a new BOB Producer Id came through and we need to add them in
            //the group that is related to the new BOB Producer Id
            if (!isInCurrentGroupMemberList){
                groupMembersToAdd.add(n_gm);
            }

            isInCurrentGroupMemberList = false;
        }

        return groupMembersToAdd;
    }

    //This method gathers all Accounts for the list of BookOfBusiness Producer Ids
    public static Set<Id> getAccountIdsForProducerId(Set<String> bookOfBusinessProducerIds){
        System.debug('AmFam_ManageGroupMembershipModel.getAccountIdsForProducerId');
        Set<String> bobIdsNotFound = new Set<String>();
        List<Account> accounts = [SELECT Id, Producer_ID__c, Name FROM Account WHERE Producer_ID__c IN :bookOfBusinessProducerIds];
        Set<Id> accountIDs = new Set<Id>();

        for (Account a : accounts){
            accountIDs.add(a.Id);
            System.debug('Accounts to pull in groups with Id:' + a.Id + ' Account Name: ' + a.Name + ' Account Producer Id: ' + a.Producer_ID__c);
        }

        //Report if BobookOfBusinessProducerIds did not come back in Account Lookup
        for (String bobId : bookOfBusinessProducerIds){
            boolean isBOBInAccount = false;
            for (Account a : accounts){
                if (bobId == a.Producer_ID__c){
                    isBOBInAccount = true;
                    break;
                }
            }

            if(!isBOBInAccount){
                System.debug('BOB ID did not come back in Account List.  Check to see if Account is missing for ProducerId: ' + bobId);
            }

            isBOBInAccount = false;
        }

        return accountIDs;
    }

    //This method gathers all Account Participant IDS
    public static List<Id> getAccountParticipantIdsForAccountIds(Set<Id> accountIDs){
        List<AccountParticipant> accountParticipants = [SELECT Id, ParticipantId, AccountId, Name FROM AccountParticipant WHERE isActive = true AND AccountId IN :accountIDs];
        List<Id> acctPartcipantIds = new List<Id>();

        for(AccountParticipant ap : accountParticipants){
            acctPartcipantIds.add(ap.ParticipantId);
        }

        return acctPartcipantIds;

    }

    // This method looks up all Groups for the Account Participants
    public static List<Group> getGroupsForAccountParticipantIds(List<Id> acctPartcipantIds){
        List<Group> acctParticipantGroups = [SELECT Id,Name,Type FROM Group WHERE Id IN :acctPartcipantIds];

        return acctParticipantGroups;
   }
}