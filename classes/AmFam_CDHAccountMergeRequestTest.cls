/**
*   @description: it's a test class for AmFam_CDHAccountMergeRequest apex class
*   @author: Anbu Chinnathambi
*   @date: 06/25/2021
*   @group: Test Class
*/

@isTest
public class AmFam_CDHAccountMergeRequestTest{
    
        @TestSetup
        static void bypassProcessBuilderSettings() {
            ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
            settings.Bypass_Process_Builder__c = true;
            upsert settings ByPass_Process_Builder__c.Id;
       }
    
    
       static testMethod void testMergePersonAccountSuccess() {
           
        wsPartyManage.lpeCallout = false;
           
        Id devRecordTypeId = AmFam_Utility.getPersonAccountRecordType();
           
        Id hHRecordTypeId =  AmFam_Utility.getAccountHouseholdRecordType();  
           
        Account masterAcc = new Account();
        masterAcc.FirstName = 'Master';
        masterAcc.LastName = ' Account';
        masterAcc.AccountSource = 'Affinity';
        masterAcc.Source__c = 'Test Merge';
        masterAcc.Important_Notes__c = 'Test: Merge Request Received';
        masterAcc.RecordTypeId = devRecordTypeId;
        insert masterAcc;
        
        Account dupAcc = new Account();
        dupAcc.FirstName = 'Dup ';
        dupAcc.LastName = ' Account';
        dupAcc.RecordTypeId = devRecordTypeId;
        insert dupAcc;
        
        Account houseHoldAcc1 = new Account();
        houseHoldAcc1.Name = 'Master Account';
        houseHoldAcc1.RecordTypeId = hHRecordTypeId;
        insert houseHoldAcc1;
        
        Schema.Location l = new Schema.Location();
        l.LocationType = 'Residence';
        l.Name = 'ZZZZ';
        insert l;
           
        Id rrAccountRTId = Schema.SObjectType.FinServ__ReciprocalRole__c.getRecordTypeInfosByDeveloperName().get('AccountRole').getRecordTypeId();
        Id rrContactRTId = Schema.SObjectType.FinServ__ReciprocalRole__c.getRecordTypeInfosByDeveloperName().get('ContactRole').getRecordTypeId();
           
        FinServ__ReciprocalRole__c rrAcc = new FinServ__ReciprocalRole__c();
        rrAcc.Name = 'Test Role Account';  
        rrAcc.FinServ__InverseRole__c = 'Inverse Role';
        rrAcc.RecordTypeId = rrAccountRTId;
        insert rrAcc;
             
        FinServ__ReciprocalRole__c rrCon = new FinServ__ReciprocalRole__c();
        rrCon.Name = 'Test ';  
        rrCon.FinServ__InverseRole__c = 'Inverse Role';
        rrCon.RecordTypeId = rrContactRTId;
        insert rrCon;
           
        FinServ__AccountAccountRelation__c accAccRelation = new FinServ__AccountAccountRelation__c();
        accAccRelation.FinServ__Account__c = houseHoldAcc1.id;
        accAccRelation.FinServ__RelatedAccount__c = dupAcc.id;
        accAccRelation.FinServ__Role__c = rrAcc.id;
        insert accAccRelation;
          
            
        String mas = '"' + masterAcc.id+'"' ; 
        String dup = '"' + dupAcc.id+'"';
        String locat = '"' + l.id+'"';
        String rt = '"' +devRecordTypeId+'"';
        String hh1 = '"' +houseHoldAcc1.id+'"';
        String accRole = '"' +rrAcc.id+'"';
       
       
        String json=		'{'+
		'    "master": {'+
		'        "attributes": {'+
		'            "type": "Account"'+
		'        },'+
		'        "Id": ' + mas +','+
        '        "recordTypeId": ' + rt +
		'        '+
		'    },'+
		'    "duplicates": ['+
		'        {'+
		'            "attributes": {'+
		'                "type": "Account"'+
		'            },'+
		'            "Id": ' + dup +
		'        }'+
		'    ],'+
		'    "survivingParty": ['+
		'        {'+
		'            "attributes": {'+
		'                "type": "ContactPointEmail"'+
		'            },'+
		'			 "parentId": ' + mas +','+
		'            "emailAddress": "manju1@gmail.com",'+
		'            "isPrimary": true'+
		'        },'+
		'        {'+
		'            "attributes": {'+
		'                "type": "ContactPointPhone"'+
		'            },'+
		'			"parentId": ' + mas +','+
		'            "areaCode": "510",'+
		'            "isPrimary": true,'+
		'            "telephoneNumber": "6081023451",'+
		'            "UsageType": "Home"'+
		'        },'+
		'        {'+
		'            "attributes": {'+
		'                "type": "AssociatedLocation"'+
		'            },'+
		'			"personAccount__c": ' + mas +','+
		'            "parentRecordId": ' + mas +','+
		'            "locationId": ' + locat +','+
		'            "usages__c": "Primary"'+
		'        },'+
		'        {'+
		'            "attributes": {'+
		'                "type": "Address"'+
		'            },'+
		'			'+
		'            "parentId": ' + locat +','+
		'            "street": "457 Brookfield Pkwy Apt 106",'+
		'            "city": "Folsom",'+
		'            "postalCode": "95834",'+
		'            "stateCode": "CA",'+
		'            "countryCode": "US",'+
		'            "locationType": "Residence"'+
		'        },'+
        '        {'+
		'            "attributes": {'+
		'                "type": "FinServ__AccountAccountRelation__c"'+
		'            },'+
		'			 "FinServ__RelatedAccount__c": ' + mas +','+
		'            "FinServ__Account__c": ' + hh1 +','+
		'            "FinServ__Role__c": ' + accRole +
		'        }'+
		'    ]'+
		'}';
           
        system.debug('json' + json);
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/MergeAccounts';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response= res;
           
        Test.startTest();
        AmFam_CDHAccountMergeRequest.ResponseWrapper  results = AmFam_CDHAccountMergeRequest.doMerge();
        Test.stopTest();
        system.debug('results>>>' + results);   
        List<ContactPointEmail> email = [select id,parentId from ContactPointEmail where parentId=:masterAcc.id];
        System.assertEquals(results.id , masterAcc.id );
        System.assertEquals(results.success , true);
        System.assert(email.size()>0);
        System.assert(masterAcc.AccountSource == 'Affinity');
     }
    
    static testMethod void testMergePersonAccountFailure() {
           
        wsPartyManage.lpeCallout = false;
        
        Id devRecordTypeId =  AmFam_Utility.getAccountHouseholdRecordType();  
           
        Account masterAcc = new Account();
        masterAcc.Name = 'Master Account';
        masterAcc.RecordTypeId = devRecordTypeId;
        insert masterAcc;
        
        Account dupAcc = new Account();
        dupAcc.Name = 'Dup Account';
        dupAcc.RecordTypeId = devRecordTypeId;
        insert dupAcc;
           
        Schema.Location l = new Schema.Location();
        l.LocationType = 'Residence';
        l.Name = 'ZZZZ';
        insert l;
        
        String mas = '"' + masterAcc.id+'"' ; 
        String dup = '"' + dupAcc.id+'"';
        String locat = '"' + l.id+'"';
        String rt = '"' +devRecordTypeId+'"';
        String json=		'{'+
		'    "master": {'+
		'        "attributes": {'+
		'            "type": "Account"'+
		'        },'+
		'        "Id": ' + mas +','+
        '        "recordTypeId": ' + rt +
		'        '+
		'    },'+
		'    "duplicates": ['+
		'        {'+
		'            "attributes": {'+
		'                "type": "Account"'+
		'            },'+
		'            "Id": ' + dup +
		'        }'+
		'    ],'+
		'    "survivingParty": ['+
		'        {'+
		'            "attributes": {'+
		'                "type": "ContactPointEmail"'+
		'            },'+
		'			 "parentId": ' + mas +','+
		'            "emailAddress": "manju1@gmail.com",'+
		'            "usageType": "OTHER",'+
		'            "isPrimary": true'+
		'        },'+
		'        {'+
		'            "attributes": {'+
		'                "type": "ContactPointPhone"'+
		'            },'+
		'			"parentId": ' + mas +','+
		'            "areaCode": "510",'+
		'            "usageType": "TDD/TTY",'+
		'            "isPrimary": true,'+
		'            "telephoneNumber": "6081021353"'+
		'        },'+
		'        {'+
		'            "attributes": {'+
		'                "type": "AssociatedLocation"'+
		'            },'+
		
		'            "usages__c": "R"'+
		'        },'+
		'        {'+
		'            "attributes": {'+
		'                "type": "Address"'+
		'            },'+
		'            "street": "457 Brookfield Pkwy Apt 106",'+
		'            "city": "Folsom",'+
		'            "postalCode": "95834",'+
		'            "stateCode": "CA",'+
		'            "countryCode": "US",'+
		'            "locationType": "Residence"'+
		'        }'+
		'    ]'+
		'}';
           
           
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/MergeAccounts';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response= res;
           
        Test.startTest();
        AmFam_CDHAccountMergeRequest.ResponseWrapper  results = AmFam_CDHAccountMergeRequest.doMerge();
        Test.stopTest();
        
        System.assertEquals(results.id , null );
        System.assertEquals(results.success , false );
    }
    
    static testMethod void testMergeHouseHoldAccountSuccess() {
           
       wsPartyManage.lpeCallout = false;
        
        Id devRecordTypeId =  AmFam_Utility.getAccountHouseholdRecordType();  
           
        Account masterAcc = new Account();
        masterAcc.Name = 'Master Account';
        masterAcc.PIR_Date__c = system.today() - 2;
        masterAcc.PIR_Outcome__c = 'Offered';
        masterAcc.RecordTypeId = devRecordTypeId;
        insert masterAcc;
        
        Account dupAcc = new Account();
        dupAcc.Name = 'Dup Account';
        dupAcc.RecordTypeId = devRecordTypeId;
        insert dupAcc;
           
        Id personAccountRecordTypeId = AmFam_Utility.getPersonAccountRecordType();
        Account testPA = new Account(FirstName = 'Testing', LastName = 'Account', RecordTypeId = personAccountRecordTypeId);
        insert testPA;
        
        testPA = [SELECT Id, PersonContactId FROM Account WHERE Id =: testPA.Id];
        
        String mas = '"' + masterAcc.id+'"' ; 
        String dup = '"' + dupAcc.id+'"';
        String contactId = '"' +testPA.PersonContactId+'"';
        String rt = '"' +devRecordTypeId+'"';
        
        String json=		'{'+
		'    "master": {'+
		'        "attributes": {'+
		'            "type": "Account"'+
		'        },'+
	    '        "Id": ' + mas +','+
		'		"RecordTypeId": ' + rt +','+
		'		"CDHID__c": "164892",'+
		'        "Party_Version__c": "1",'+
		'		"Name": "Test HouseHold",'+
		'        "Greeting__c": "Stellar"'+
		'        '+
		'    },'+
		'    "duplicates": ['+
		'        {'+
		'            "attributes": {'+
		'                "type": "Account"'+
		'            },'+
		'        "Id": ' + dup +
		'        }'+
		'    ],'+
		'    "survivingParty": null,'+
		'	'+
		'	"survivingHousehold": ['+
		'        {'+
		'            "attributes": {'+
		'                "type": "AccountContactRelation"'+
		'            },'+
		'        "contactId": ' + contactId +','+
		'        "accountId": ' + mas +','+
        '        "roles": "Member"'+
		'        }'+
		'    ]'+
		'}'+
		'';
        
       
           
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/MergeAccounts';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(json);
        RestContext.request = req;
        RestContext.response= res;
           
        Test.startTest();
        AmFam_CDHAccountMergeRequest.ResponseWrapper  results = AmFam_CDHAccountMergeRequest.doMerge();
        Test.stopTest();
        
        AccountContactRelation contactRel = [SELECT Id, Roles, AccountId, ContactId FROM AccountContactRelation WHERE AccountId=:masterAcc.id LIMIT 1];
        System.assert(contactRel!=null);
        System.assert(masterAcc.PIR_Outcome__c == 'Offered');
        System.assertEquals(results.id , masterAcc.Id );
        System.assertEquals(results.success , true );
    }
}