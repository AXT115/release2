//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)
	
public class wsHouseholdServicePing {
	private static final String s_ns0 = 'http://schema.amfam.com/ping';
	public class ping_element {
		public wsHouseholdServicePing.PingInputType PingInput;
		private transient String[] PingInput_type_info = new String[]{'PingInput',s_ns0,'PingInputType','1','1','false'};
		private transient String[] apex_schema_type_info = new String[]{s_ns0,'true','false'};
		private transient String[] field_order_type_info = new String[]{'PingInput'};
		public ping_element(){
		}
		public ping_element(wsHouseholdServicePing.PingInputType PingInput){
			this.PingInput = PingInput;
		}
		public void populateXmlNode(Dom.XmlNode outerNode){
			
			//System.assertEquals('ping', outerNode.getName());
			
			wsHouseholdServicePing.PingInputType PingInputObj = this.PingInput;
			Dom.XmlNode PingInputNode = outerNode.addChildElement('PingInput', 'http://schema.amfam.com/ping', '');
			if(PingInputObj != null){
				PingInputObj.populateXmlNode(PingInputNode);
			}
		}
	}
	public class PingInputType {
		// Restriction enumeration: None, Component
		public String PingLevel;
		private transient String[] PingLevel_type_info = new String[]{'PingLevel',s_ns0,'PingLevelType','1','1','false'};
		private transient String[] apex_schema_type_info = new String[]{s_ns0,'true','false'};
		private transient String[] field_order_type_info = new String[]{'PingLevel'};
		public PingInputType(){
		}
		public PingInputType(DOM.XmlNode responseNode){
			Set<DOM.XmlNode> nodesParsed = new Set<DOM.XmlNode>();
			DOM.XmlNode PingLevelNode = responseNode.getChildElement('PingLevel', 'http://schema.amfam.com/ping');
			this.PingLevel = (PingLevelNode == null) ? null : PingLevelNode.getText();
			nodesParsed.add(PingLevelNode);
			//System.debug(this.PingLevel);			
		}
		public void populateXmlNode(Dom.XmlNode outerNode){
			Dom.XmlNode PingLevelNode = outerNode.addChildElement('PingLevel', 'http://schema.amfam.com/ping', '');
			if(this.PingLevel != null){
				PingLevelNode.addTextNode(this.PingLevel);
			}
		}
	}
	public class pingResponse_element {
		public wsHouseholdServicePing.PingResultType PingResult;
		private transient String[] PingResult_type_info = new String[]{'PingResult',s_ns0,'PingResultType','1','1','false'};
		private transient String[] apex_schema_type_info = new String[]{s_ns0,'true','false'};
		private transient String[] field_order_type_info = new String[]{'PingResult'};
		public pingResponse_element(){
		}
		public pingResponse_element(DOM.XmlNode responseNode){
			Set<DOM.XmlNode> nodesParsed = new Set<DOM.XmlNode>();
			//System.assertEquals('pingResponse', responseNode.getName());
			DOM.XmlNode PingResultNode = responseNode.getChildElement('PingResult', 'http://schema.amfam.com/ping');
			if(PingResultNode == null){
				this.PingResult = null;
			} else{
				wsHouseholdServicePing.PingResultType PingResultObj = new wsHouseholdServicePing.PingResultType(PingResultNode);
				nodesParsed.add(PingResultNode);
				this.PingResult = PingResultObj;
			}
			//System.debug(this.PingResult);			
		}
	}
	public class PingResultType {
		// Restriction enumeration: Success, Failure
		public String Status;
		public String message;
		public wsHouseholdServicePing.VerificationType Verification;
		public String timeTakenForPing;
		private transient String[] Status_type_info = new String[]{'Status',s_ns0,'StatusType','1','1','false'};
		private transient String[] message_type_info = new String[]{'message',s_ns0,'string','1','1','true'};
		private transient String[] Verification_type_info = new String[]{'Verification',s_ns0,'VerificationType','0','10000','false'};
		private transient String[] timeTakenForPing_type_info = new String[]{'timeTakenForPing',s_ns0,'string','1','1','true'};
		private transient String[] apex_schema_type_info = new String[]{s_ns0,'true','false'};
		private transient String[] field_order_type_info = new String[]{'Status','message','Verification','timeTakenForPing'};
		public PingResultType(){
		}
		public PingResultType(DOM.XmlNode responseNode){
			Set<DOM.XmlNode> nodesParsed = new Set<DOM.XmlNode>();
			DOM.XmlNode StatusNode = responseNode.getChildElement('Status', 'http://schema.amfam.com/ping');
			this.Status = (StatusNode == null) ? null : StatusNode.getText();
			nodesParsed.add(StatusNode);
			//System.debug(this.Status);			
			DOM.XmlNode messageNode = responseNode.getChildElement('message', 'http://schema.amfam.com/ping');
			this.message = (messageNode == null || messageNode.getAttributeValue('nil', 'http://www.w3.org/2001/XMLSchema-instance') == 'true') ? null : messageNode.getText();
			nodesParsed.add(messageNode);
			//System.debug(this.message);			
			DOM.XmlNode VerificationNode = responseNode.getChildElement('Verification', 'http://schema.amfam.com/ping');
			if(VerificationNode == null){
				this.Verification = null;
			} else{
				wsHouseholdServicePing.VerificationType VerificationObj = new wsHouseholdServicePing.VerificationType(VerificationNode);
				nodesParsed.add(VerificationNode);
				this.Verification = VerificationObj;
			}
			//System.debug(this.Verification);			
			DOM.XmlNode timeTakenForPingNode = responseNode.getChildElement('timeTakenForPing', 'http://schema.amfam.com/ping');
			this.timeTakenForPing = (timeTakenForPingNode == null || timeTakenForPingNode.getAttributeValue('nil', 'http://www.w3.org/2001/XMLSchema-instance') == 'true') ? null : timeTakenForPingNode.getText();
			nodesParsed.add(timeTakenForPingNode);
			//System.debug(this.timeTakenForPing);			
		}
		public void populateXmlNode(Dom.XmlNode outerNode){
			Dom.XmlNode StatusNode = outerNode.addChildElement('Status', 'http://schema.amfam.com/ping', '');
			if(this.Status != null){
				StatusNode.addTextNode(this.Status);
			}
			Dom.XmlNode messageNode = outerNode.addChildElement('message', 'http://schema.amfam.com/ping', '');
			if(this.message != null){
				messageNode.addTextNode(this.message);
			}
			
			wsHouseholdServicePing.VerificationType VerificationObj = this.Verification;
			Dom.XmlNode VerificationNode = outerNode.addChildElement('Verification', 'http://schema.amfam.com/ping', '');
			if(VerificationObj != null){
				VerificationObj.populateXmlNode(VerificationNode);
			}
			Dom.XmlNode timeTakenForPingNode = outerNode.addChildElement('timeTakenForPing', 'http://schema.amfam.com/ping', '');
			if(this.timeTakenForPing != null){
				timeTakenForPingNode.addTextNode(this.timeTakenForPing);
			}
		}
	}
	public class VerificationType {
		public String componentName;
		// Restriction enumeration: Success, Failure
		public String Status;
		public String message;
		public String timeTakenForComponentVerification;
		private transient String[] componentName_type_info = new String[]{'componentName',s_ns0,'string','1','1','false'};
		private transient String[] Status_type_info = new String[]{'Status',s_ns0,'StatusType','1','1','false'};
		private transient String[] message_type_info = new String[]{'message',s_ns0,'string','1','1','true'};
		private transient String[] timeTakenForComponentVerification_type_info = new String[]{'timeTakenForComponentVerification',s_ns0,'string','1','1','false'};
		private transient String[] apex_schema_type_info = new String[]{s_ns0,'true','false'};
		private transient String[] field_order_type_info = new String[]{'componentName','Status','message','timeTakenForComponentVerification'};
		public VerificationType(){
		}
		public VerificationType(DOM.XmlNode responseNode){
			Set<DOM.XmlNode> nodesParsed = new Set<DOM.XmlNode>();
			DOM.XmlNode componentNameNode = responseNode.getChildElement('componentName', 'http://schema.amfam.com/ping');
			this.componentName = (componentNameNode == null) ? null : componentNameNode.getText();
			nodesParsed.add(componentNameNode);
			//System.debug(this.componentName);			
			DOM.XmlNode StatusNode = responseNode.getChildElement('Status', 'http://schema.amfam.com/ping');
			this.Status = (StatusNode == null) ? null : StatusNode.getText();
			nodesParsed.add(StatusNode);
			//System.debug(this.Status);			
			DOM.XmlNode messageNode = responseNode.getChildElement('message', 'http://schema.amfam.com/ping');
			this.message = (messageNode == null || messageNode.getAttributeValue('nil', 'http://www.w3.org/2001/XMLSchema-instance') == 'true') ? null : messageNode.getText();
			nodesParsed.add(messageNode);
			//System.debug(this.message);			
			DOM.XmlNode timeTakenForComponentVerificationNode = responseNode.getChildElement('timeTakenForComponentVerification', 'http://schema.amfam.com/ping');
			this.timeTakenForComponentVerification = (timeTakenForComponentVerificationNode == null) ? null : timeTakenForComponentVerificationNode.getText();
			nodesParsed.add(timeTakenForComponentVerificationNode);
			//System.debug(this.timeTakenForComponentVerification);			
		}
		public void populateXmlNode(Dom.XmlNode outerNode){
			Dom.XmlNode componentNameNode = outerNode.addChildElement('componentName', 'http://schema.amfam.com/ping', '');
			if(this.componentName != null){
				componentNameNode.addTextNode(this.componentName);
			}
			Dom.XmlNode StatusNode = outerNode.addChildElement('Status', 'http://schema.amfam.com/ping', '');
			if(this.Status != null){
				StatusNode.addTextNode(this.Status);
			}
			Dom.XmlNode messageNode = outerNode.addChildElement('message', 'http://schema.amfam.com/ping', '');
			if(this.message != null){
				messageNode.addTextNode(this.message);
			}
			Dom.XmlNode timeTakenForComponentVerificationNode = outerNode.addChildElement('timeTakenForComponentVerification', 'http://schema.amfam.com/ping', '');
			if(this.timeTakenForComponentVerification != null){
				timeTakenForComponentVerificationNode.addTextNode(this.timeTakenForComponentVerification);
			}
		}
	}
}