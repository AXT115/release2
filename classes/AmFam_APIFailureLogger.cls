public class AmFam_APIFailureLogger {

    /**
    * @param Method Name: createAPIFailureLog
    * @description : Create API Failure Logs for AmFam_ProducerServiceHandler, AmFam_QuoteLightningContoller
    */
    public void createAPIFailureLog (String className, HttpResponse response, String requestURL, String paramId, String errorMessage) {
        try {
            API_Failure_Log__c failureLog = new API_Failure_Log__c();
            failureLog.API_Call__c = className;
            failureLog.Request_URL__c = requestURL;
            if (response != null) {
                failureLog.HTTP_Method__c = 'GET';
                failureLog.Status_Code__c = response.getStatusCode();
                if (response.getBody() != null && !String.isBlank(response.getBody())){
                    failureLog.Response_Body__c = response.getBody();
                } else {
                    failureLog.Response_Body__c = 'No Response Body returned';
                }
            }
            failureLog.Error_Message__c = errorMessage;
            failureLog.API_Param_ID__c = paramId;
            insert failureLog;
        }
        catch (Exception e) {
            system.debug('Failed to create API Failure Log: '+e.getMessage()+': ' +  e.getTypeName() + ' -- ' + e.getCause()+'##'+e.getStackTraceString());
        }
    }

    /**
    * @param Method Name: createAPIFailureLogNoProducerReturned
    * @description : Create API Failure Logs for AmFam_ProducerServiceHandler
    */
    public void createAPIFailureLogNoProducerReturned (String className, String requestURL, String userFedId, String errorMessage) {
        try {
            API_Failure_Log__c failureLog = new API_Failure_Log__c();
            failureLog.API_Call__c = className;
            failureLog.Request_URL__c = requestURL;
            failureLog.Error_Message__c = errorMessage;
            failureLog.API_Param_ID__c = userFedId;
            insert failureLog;
        }
        catch (Exception e) {
            system.debug('Failed to create API Failure Log: '+e.getMessage()+': ' +  e.getTypeName() + ' -- ' + e.getCause()+'##'+e.getStackTraceString());
        }
    }

        /**
    * @param Method Name: createAPIFailureLogNoProducerReturned
    * @description : Create API Failure Logs for generic callouts
    */
    public API_Failure_Log__c createGenericAPIFailure (String className, String requestURL, String paramId, String errorMessage) {
        try {
            API_Failure_Log__c failureLog = new API_Failure_Log__c();
            failureLog.API_Call__c = className;
            failureLog.Request_URL__c = requestURL;
            failureLog.Error_Message__c = errorMessage;
            failureLog.API_Param_ID__c = paramId;
            return failureLog;
        }
        catch (Exception e) {
            system.debug('Failed to create API Failure Log: '+e.getMessage()+': ' +  e.getTypeName() + ' -- ' + e.getCause()+'##'+e.getStackTraceString());
            return null;
        }
    }

}