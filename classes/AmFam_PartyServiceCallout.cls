public class AmFam_PartyServiceCallout {
    public AmFam_PartyServiceAPIWrapper partyServiceCallOut(String cdhId) {
        API_Header__mdt apiHeader = [SELECT MasterLabel, Header_Value__c FROM API_Header__mdt WHERE MasterLabel = 'AFI-API-Key' LIMIT 1];
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setHeader('AFI-AppName', 'Salesforce');
        request.setHeader('AFI-API-Key', apiHeader.Header_Value__c);
        String param = 'parties/' + cdhId + '/products';
        request.setEndpoint('callout:Party_API/' + param);
        HttpResponse response;
        try {
            response = http.send(request);
            // If the request is successful, parse the JSON response.
            if (response.getStatusCode() == 200) {
                AmFam_PartyServiceAPIWrapper partySvc = (AmFam_PartyServiceAPIWrapper)JSON.deserialize(response.getBody(), AmFam_PartyServiceAPIWrapper.class);
                return partySvc;
            }
        } catch (Exception e) {
            //store exception details on Failure Logs with user details and error details
            AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
            failureLog.createAPIFailureLog('AmFam_QuoteLightningContoller', response, request.getEndpoint(), cdhId, e.getMessage());
            System.debug(e);
       }
       return null;
    }
}