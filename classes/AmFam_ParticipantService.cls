public with sharing class AmFam_ParticipantService {



    //************* update for 1776 ************** */
    public static void grantSameAccessFromAccount (List<Participant__c> newRecords) {
        
        Set<Id> accountIds = new Set<Id>();
        Set<Id> interactionSummaryIds = new Set<Id>();
        Map<Id, Set<Id>> sharesIdbyAccountId = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> participantIdbyInteractionSummaryId = new Map<Id, Set<Id>>();
        List<InteractionSummaryShare> interactionSummaryShareToInsert = new List<InteractionSummaryShare>();


        //************************************************/
        //Retrieve Account Ids and Interaction Summary Ids
        for (Participant__c p : newRecords) {
            accountIds.add(p.Agent__c);
            interactionSummaryIds.add(p.Interaction_Summary__c);
        }



        //***********************************************************************************/
        //Get the Account Shares from the Account related and Map it by AccountId;
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId, AccountId FROM AccountShare WHERE AccountId IN: accountIds];

        for (AccountShare ap : accountShares) {
            Id accountId = ap.AccountId;
            Id userId = ap.UserOrGroupId;

            Set<Id> sharesForAccount = sharesIdbyAccountId.get(accountId);
            if (sharesForAccount == null) {
                sharesForAccount = new Set<Id>();
            }

            sharesForAccount.add(userId);
            sharesIdbyAccountId.put(accountId, sharesForAccount);
        }

        //Get the list of Keys: InteractionSummaryId-ParticipantId from existing Interaction Summary Participants
        Set<String> interactionSummaryKeys = getInteractionSummaryShareKeys(interactionSummaryIds);


        //****************************************************/
        //Build Interaction Summary Participant list to insert
        for (Participant__c p : newRecords) {
            Id accountId = p.Agent__c;
            Id interactionSummaryId = p.Interaction_Summary__c;

            Set<Id> sharesForAccount = sharesIdbyAccountId.get(accountId);

            if (sharesForAccount != null && sharesForAccount.size() > 0) {
                for (Id userId : sharesForAccount) {

                    String key = interactionSummaryId + '-' + userId;

                    //An Interaction Summary may be already associated to a participantId due to a relationship with a different partcipant agent on the same group
                    //Do not add already existing combination [ interactionSummaryId - participantId ]
                    if (!interactionSummaryKeys.contains(key)) {
                        InteractionSummaryShare isp = new InteractionSummaryShare();
                        isp.UserOrGroupId = userId;
                        isp.ParentId = interactionSummaryId;
                        isp.RowCause = 'Manual';
                        isp.AccessLevel = 'Edit';

                        interactionSummaryShareToInsert.add(isp);
                    }
                }
            }
        }


        //************/
        //Commit to DB
        insert interactionSummaryShareToInsert;


    }



    public static void removeAssociationFromAccountShares (List<Participant__c> oldRecords) {


        Set<Id> accountIds = new Set<Id>();
        Set<Id> interactionSummaryIds = new Set<Id>();
        Map<Id, Set<Id>> userIdbyAccountId = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> accountIdByInteractionSummaryId = new Map<Id, Set<Id>>();
        List<InteractionSummaryShare> interactionSummaryShareToDelete = new List<InteractionSummaryShare>();        




        //************************************************/
        //Retrieve Account Ids, Interaction Summary Ids and Build the Map of Accounts indexed by Interactino Summary Id
        for (Participant__c p : oldRecords) {
            Id interactionSummary = p.Interaction_Summary__c;
            Id accountId = p.Agent__c;

            accountIds.add(accountId);
            interactionSummaryIds.add(interactionSummary);

            Set<Id> accountsForInteractionSummary = accountIdByInteractionSummaryId.get(interactionSummary);
            if (accountsForInteractionSummary == null) {
                accountsForInteractionSummary = new Set<Id>();
            }

            accountsForInteractionSummary.add(accountId);
            accountIdByInteractionSummaryId.put(interactionSummary, accountsForInteractionSummary);

        }

        


        //***********************************************************************************************************************/
        //Query remaining Participant records for Interaction Summaries and add the Accounts (Agents) to the list of Accounts IDs
        List<Participant__c> participants = [SELECT Id, Interaction_Summary__c, Agent__c FROM Participant__c WHERE Interaction_Summary__c IN: interactionSummaryIds];

        for (Participant__c p: participants) {
            accountIds.add(p.Agent__c);
        }



        //Get Exclussion Profiles
        List<String> profilesToExclude = new List<String>{'AmFam Agent', 'Agent CSR'};



        //***********************************************************************************/
        //Get the Account Share from the Account related and Map it by AccountId;
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId, AccountId FROM AccountShare WHERE RowCause = 'Manual' AND AccountId IN: accountIds AND UserOrGroup.Profile.Name NOT IN: profilesToExclude];
        
        Set<Id> userIdSet = new Set<Id>();
        for (AccountShare ap : accountShares) {
            Id accountId = ap.AccountId;
            Id userId = ap.UserOrGroupId;

            userIdSet.add(userId);

            Set<Id> usersForAccount = userIdByAccountId.get(accountId);
            if (usersForAccount == null) {
                usersForAccount = new Set<Id>();
            }

            usersForAccount.add(userId);
            userIdbyAccountId.put(accountId, usersForAccount);
        }



        Map<Id, Map<Id, Integer>> usersIndexedBySummapry = new Map<Id, Map<Id, Integer>>();
        for (Participant__c p : participants) {
            Id interactionSummaryId = p.Interaction_Summary__c;
            Id accountId = p.Agent__c;

            Map<Id, Integer> totalParticipantsBySummaryId = usersIndexedBySummapry.get(interactionSummaryId);
            if (totalParticipantsBySummaryId == null) {
                totalParticipantsBySummaryId = new Map<Id, Integer> ();
            }


            Set<Id> usersForAccount = userIdbyAccountId.get(accountId);


            if (usersForAccount != null) {

                for (Id userId : usersForAccount) {

                    Integer totalParticipants = totalParticipantsBySummaryId.containsKey(userId) ? totalParticipantsBySummaryId.get(userId) : 0 ;
                    
                    totalParticipants++;
                    totalParticipantsBySummaryId.put(userId, totalParticipants);

                }
    
            }                    

            usersIndexedBySummapry.put(interactionSummaryId, totalParticipantsBySummaryId);

        }



       //***********************************************************************************/
        //Get the Interaction Summary Shares from the Interaction Summary related and build a Map Interaction Summary Id;
        List<InteractionSummaryShare> interactionSummaryShares = [SELECT Id, UserOrGroupId, ParentId FROM InteractionSummaryShare WHERE ParentId IN: interactionSummaryIds];


        Map<Id, List<InteractionSummaryShare>> interactionSummaryParticipantBySummaryId = new Map<Id, List<InteractionSummaryShare>>();


        for (InteractionSummaryShare isp : interactionSummaryShares) {
            Id interactionSummaryId = isp.ParentId;
            Id participantId = isp.UserOrGroupId;


            Map<Id, Integer> totalParticipantsBySummaryId = usersIndexedBySummapry.get(interactionSummaryId);
            if (totalParticipantsBySummaryId == null) {
                totalParticipantsBySummaryId = new Map<Id, Integer> ();
            }

            List<InteractionSummaryShare> intSumPartForSummaryId = interactionSummaryParticipantBySummaryId.get(interactionSummaryId);
            if (intSumPartForSummaryId == null) {
                intSumPartForSummaryId = new List<InteractionSummaryShare>();
            }

            intSumPartForSummaryId.add(isp);
            interactionSummaryParticipantBySummaryId.put(interactionSummaryId, intSumPartForSummaryId);


        }



        //Retrieve the Interaction Summary Participant keys from Account directly related to the Interactin Summaries
        Set<String> interactionSummaryParticipantIds = getInteractionSummaryShareKeysFromDirectAccounts(interactionSummaryIds);        




        //*********************************************************************** */
        // Build the list of InteractionSummaryParticipant records to be deleted
        for (Participant__c p : oldRecords) {

            Id accountId = p.Agent__c;
            Id interactionSummaryId = p.Interaction_Summary__c;


            Map<Id, Integer> totalParticipantsBySummaryId = usersIndexedBySummapry.get(interactionSummaryId);
            Set<Id> usersForAccount = userIdByAccountId.get(accountId);


            List<InteractionSummaryShare> intSumPartForSummaryId = interactionSummaryParticipantBySummaryId.get(interactionSummaryId);



            if (intSumPartForSummaryId != null && usersForAccount != null) {

                for (Id userId : usersForAccount) {

                    Boolean canDelete = false;

                    //If the total is not null, it means there are other records that may still provide access to the Account Participant Group
                    if (totalParticipantsBySummaryId != null ) {

                        Integer totalParticipants = totalParticipantsBySummaryId.containsKey(userId) ? totalParticipantsBySummaryId.get(userId) : 0 ;

                        //If total is bigger than 0, just decrease it and store it again in memory
                        //If total is 0, the delete can be performed
                        if (totalParticipants > 0) {
                            totalParticipantsBySummaryId.put(userId, --totalParticipants);
                            usersIndexedBySummapry.put(interactionSummaryId, totalParticipantsBySummaryId);
    
                        } else {
                            canDelete = true;
                        }

                    } else  {
                        canDelete = true;
                    }


                    
                    
                    //Add the ISP to the list to be deleted
                    if (canDelete) {

                        for (InteractionSummaryShare isp : intSumPartForSummaryId) {
                            Id ispParticipantId = isp.UserOrGroupId;

                            if (ispParticipantId == userId) {
                                String key = interactionSummaryId + '-' + userId;

                                //If the combined Key is on the list of records from the related account, then don't delete the Interaction Summary Participant
                                if (!interactionSummaryParticipantIds.contains(key)) {
                                    interactionSummaryShareToDelete.add(isp);
                                }
                            }
                        }                        
                    }
                }

                usersIndexedBySummapry.put(interactionSummaryId, totalParticipantsBySummaryId);
            }

        }



        //******************** */
        //commit to Database
        delete interactionSummaryShareToDelete;







    }





    private static Set<String> getInteractionSummaryShareKeys(Set<Id> interactionSumaryIds) {

        Set<String> keys = new Set<String>();

        //Retrieve all the Interaction Summary Participants related to the INteraction Summaries and build the set of Keys: InteractionSummaryId-ParticipantId
        List<InteractionSummaryShare> isps = [SELECT UserOrGroupId, ParentId FROM InteractionSummaryShare WHERE ParentId IN: interactionSumaryIds];

        for (InteractionSummaryShare isp : isps) {
            String key = isp.ParentId + '-' + isp.UserOrGroupId;
            keys.add(key);
        }

        return keys;
    }



    private static Set<String> getInteractionSummaryShareKeysFromDirectAccounts(Set<Id> interactionSumaryIds) {
        Set<String> keys = new Set<String>();

        //Build a Map of interaction Summaries indexed by Account ID
        List<InteractionSummary> interactionSummaryList = [SELECT Id, AccountId FROM InteractionSummary WHERE Id IN: interactionSumaryIds];

        Set<Id> accountIds = new Set<Id>();
        Map<Id, Set<id>> interactionSummaryByAccount = new Map<Id, Set<Id>>();

        for (InteractionSummary is : interactionSummaryList) {
            accountIds.add(is.AccountId);
            Set<Id> interactionSummaries = interactionSummaryByAccount.get(is.AccountId);
            if (interactionSummaries == null)
                interactionSummaries = new Set<Id>();

            interactionSummaries.add(is.Id);
            interactionSummaryByAccount.put(is.AccountId, interactionSummaries);
        }


        //Retrieve the list of Account Participant records and build a Set of keys: InteractionSummaryId-ParticipantId
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId, AccountId FROM AccountShare WHERE AccountId IN: accountIds];

        for (AccountShare ap : accountShares) {
            String accountId = ap.AccountId;
            String userId = ap.UserOrGroupId;

            Set<Id> interactionSummaries = interactionSummaryByAccount.get(accountId);

            if (interactionSummaries != null) {
                for (Id interactionSummaryId : interactionSummaries) {
                    String key = interactionSummaryId + '-' + userId;
                    keys.add(key);
                }
            }
        }


        return keys;

    }



    //************* update for 1776 ************** */






 

/*
    public static void removeAssociationFromAccountParticipantsFromAccount (List<Participant__c> oldRecords) {

        Set<Id> accountIds = new Set<Id>();
        Set<Id> interactionSummaryIds = new Set<Id>();
        Map<Id, Set<Id>> participantIdbyAccountId = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> accountIdByInteractionSummaryId = new Map<Id, Set<Id>>();
        List<InteractionSummaryParticipant> interactionSummaryParticipantToDelete = new List<InteractionSummaryParticipant>();        




        //Retrieve Account Ids, Interaction Summary Ids and Build the Map of Accounts indexed by Interactino Summary Id
        for (Participant__c p : oldRecords) {
            Id interactionSummary = p.Interaction_Summary__c;
            Id accountId = p.Agent__c;

            accountIds.add(accountId);
            interactionSummaryIds.add(interactionSummary);

            Set<Id> accountsForInteractionSummary = accountIdByInteractionSummaryId.get(interactionSummary);
            if (accountsForInteractionSummary == null) {
                accountsForInteractionSummary = new Set<Id>();
            }

            accountsForInteractionSummary.add(accountId);
            accountIdByInteractionSummaryId.put(interactionSummary, accountsForInteractionSummary);

        }

        


        //Query remaining Participant records for Interaction Summaries and add the Accounts (Agents) to the list of Accounts IDs
        List<Participant__c> participants = [SELECT Id, Interaction_Summary__c, Agent__c FROM Participant__c WHERE Interaction_Summary__c IN: interactionSummaryIds];

        for (Participant__c p: participants) {
            accountIds.add(p.Agent__c);
        }





        //Get the Account Participant Groups from the Account related and Map it by AccountId;
        List<AccountParticipant> accountParticipants = [SELECT Id, ParticipantId, AccountId FROM AccountParticipant WHERE AccountId IN: accountIds];

        
        Set<Id> participantIdSet = new Set<Id>();
        for (AccountParticipant ap : accountParticipants) {
            Id accountId = ap.AccountId;
            Id participantId = ap.ParticipantId;

            participantIdSet.add(participantId);

            Set<Id> participantsForAccount = participantIdByAccountId.get(accountId);
            if (participantsForAccount == null) {
                participantsForAccount = new Set<Id>();
            }

            participantsForAccount.add(participantId);
            participantIdByAccountId.put(accountId, participantsForAccount);
        }



        Map<Id, Map<Id, Integer>> participantsIndexedBySummapry = new Map<Id, Map<Id, Integer>>();
        for (Participant__c p : participants) {
            Id interactionSummaryId = p.Interaction_Summary__c;
            Id accountId = p.Agent__c;

            Map<Id, Integer> totalParticipantsBySummaryId = participantsIndexedBySummapry.get(interactionSummaryId);
            if (totalParticipantsBySummaryId == null) {
                totalParticipantsBySummaryId = new Map<Id, Integer> ();
            }


            Set<Id> participantsForAccount = participantIdByAccountId.get(accountId);


            if (participantsForAccount != null) {

                for (Id accountParticipanId : participantsForAccount) {

                    Integer totalParticipants = totalParticipantsBySummaryId.containsKey(accountParticipanId) ? totalParticipantsBySummaryId.get(accountParticipanId) : 0 ;
                    
                    totalParticipants++;
                    totalParticipantsBySummaryId.put(accountParticipanId, totalParticipants);

                }
    
            }                    

            participantsIndexedBySummapry.put(interactionSummaryId, totalParticipantsBySummaryId);

        }



        //Get the Interaction Summary Participant Groups from the Interaction Summary related and build a Map Interaction Summary Id;
        List<InteractionSummaryParticipant> interactionSummaryParticipants = [SELECT Id, ParticipantId, InteractionSummaryId FROM InteractionSummaryParticipant WHERE InteractionSummaryId IN: interactionSummaryIds];


        Map<Id, List<InteractionSummaryParticipant>> interactionSummaryParticipantBySummaryId = new Map<Id, List<InteractionSummaryParticipant>>();


        for (InteractionSummaryParticipant isp : interactionSummaryParticipants) {
            Id interactionSummaryId = isp.InteractionSummaryId;
            Id participantId = isp.ParticipantId;


            Map<Id, Integer> totalParticipantsBySummaryId = participantsIndexedBySummapry.get(interactionSummaryId);
            if (totalParticipantsBySummaryId == null) {
                totalParticipantsBySummaryId = new Map<Id, Integer> ();
            }

            List<InteractionSummaryParticipant> intSumPartForSummaryId = interactionSummaryParticipantBySummaryId.get(interactionSummaryId);
            if (intSumPartForSummaryId == null) {
                intSumPartForSummaryId = new List<InteractionSummaryParticipant>();
            }

            intSumPartForSummaryId.add(isp);
            interactionSummaryParticipantBySummaryId.put(interactionSummaryId, intSumPartForSummaryId);


        }



        //Retrieve the Interaction Summary Participant keys from Account directly related to the Interactin Summaries
        Set<String> interactionSummaryParticipantIds = getInteractionSummaryParticipantKeysFromDirectAccounts(interactionSummaryIds);        




        // Build the list of InteractionSummaryParticipant records to be deleted
        for (Participant__c p : oldRecords) {

            Id accountId = p.Agent__c;
            Id interactionSummaryId = p.Interaction_Summary__c;


            Map<Id, Integer> totalParticipantsBySummaryId = participantsIndexedBySummapry.get(interactionSummaryId);
            Set<Id> participantsForAccount = participantIdByAccountId.get(accountId);


            List<InteractionSummaryParticipant> intSumPartForSummaryId = interactionSummaryParticipantBySummaryId.get(interactionSummaryId);



            if (intSumPartForSummaryId != null) {

                for (Id participantId : participantsForAccount) {

                    Boolean canDelete = false;

                    //If the total is not null, it means there are other records that may still provide access to the Account Participant Group
                    if (totalParticipantsBySummaryId != null ) {

                        Integer totalParticipants = totalParticipantsBySummaryId.containsKey(participantId) ? totalParticipantsBySummaryId.get(participantId) : 0 ;

                        //If total is bigger than 0, just decrease it and store it again in memory
                        //If total is 0, the delete can be performed
                        if (totalParticipants > 0) {
                            totalParticipantsBySummaryId.put(participantId, --totalParticipants);
                            participantsIndexedBySummapry.put(interactionSummaryId, totalParticipantsBySummaryId);
    
                        } else {
                            canDelete = true;
                        }

                    } else  {
                        canDelete = true;
                    }


                    
                    
                    //Add the ISP to the list to be deleted
                    if (canDelete) {

                        for (InteractionSummaryParticipant isp : intSumPartForSummaryId) {
                            Id ispParticipantId = isp.ParticipantId;

                            if (ispParticipantId == participantId) {
                                String key = interactionSummaryId + '-' + participantId;

                                //If the combined Key is on the list of records from the related account, then don't delete the Interaction Summary Participant
                                if (!interactionSummaryParticipantIds.contains(key)) {
                                    interactionSummaryParticipantToDelete.add(isp);
                                }
                            }
                        }                        
                    }
                }

                participantsIndexedBySummapry.put(interactionSummaryId, totalParticipantsBySummaryId);
            }

        }



        //commit to Database
        delete interactionSummaryParticipantToDelete;



    }



    private static Set<String> getInteractionSummaryParticipantKeys(Set<Id> interactionSumaryIds) {

        Set<String> keys = new Set<String>();

        //Retrieve all the Interaction Summary Participants related to the INteraction Summaries and build the set of Keys: InteractionSummaryId-ParticipantId
        List<InteractionSummaryParticipant> isps = [SELECT ParticipantId, InteractionSummaryId FROM InteractionSummaryParticipant WHERE InteractionSummaryId IN: interactionSumaryIds];

        for (InteractionSummaryParticipant isp : isps) {
            String key = isp.InteractionSummaryId + '-' + isp.ParticipantId;
            keys.add(key);
        }

        return keys;
    }




    private static Set<String> getInteractionSummaryParticipantKeysFromDirectAccounts(Set<Id> interactionSumaryIds) {
        Set<String> keys = new Set<String>();

        //Build a Map of interaction Summaries indexed by Account ID
        List<InteractionSummary> interactionSummaryList = [SELECT Id, AccountId FROM InteractionSummary WHERE Id IN: interactionSumaryIds];

        Set<Id> accountIds = new Set<Id>();
        Map<Id, Set<id>> interactionSummaryByAccount = new Map<Id, Set<Id>>();

        for (InteractionSummary is : interactionSummaryList) {
            accountIds.add(is.AccountId);
            Set<Id> interactionSummaries = interactionSummaryByAccount.get(is.AccountId);
            if (interactionSummaries == null)
                interactionSummaries = new Set<Id>();

            interactionSummaries.add(is.Id);
            interactionSummaryByAccount.put(is.AccountId, interactionSummaries);
        }


        //Retrieve the list of Account Participant records and build a Set of keys: InteractionSummaryId-ParticipantId
        List<AccountParticipant> accountParticipants = [SELECT Id, ParticipantId, AccountId FROM AccountParticipant WHERE AccountId IN: accountIds];

        for (AccountParticipant ap : accountParticipants) {
            String accountId = ap.AccountId;
            String participantId = ap.ParticipantId;

            Set<Id> interactionSummaries = interactionSummaryByAccount.get(accountId);

            if (interactionSummaries != null) {
                for (Id interactionSummaryId : interactionSummaries) {
                    String key = interactionSummaryId + '-' + participantId;
                    keys.add(key);
                }
            }
        }


        return keys;

    }


*/



}