public class AmFam_AccountGlobalQuickActionController {
	@AuraEnabled
    public string defaultRecordTypeId {get; set;}
    @AuraEnabled
    public Map<Id, String> AccountRecordTypes {get; set;}
     
    @AuraEnabled       
    public static AmFam_AccountGlobalQuickActionController getRecordTypeValues(){
        AmFam_AccountGlobalQuickActionController obj = new AmFam_AccountGlobalQuickActionController();
        Map<Id, String> recordtypeMap = new Map<Id, String>();
        //Get all record types of Contact object
        List<Schema.RecordTypeInfo> recordTypeInfoList = Account.SObjectType.getDescribe().getRecordTypeInfos();
        for(RecordTypeInfo info: recordTypeInfoList) {
            if(info.isAvailable()) {
                if(info.getName() != 'Master' && info.getName().trim() != ''){
                    recordtypeMap.put(info.getRecordTypeId(), info.getName());
                }
                if(info.isDefaultRecordTypeMapping()){
                    obj.defaultRecordTypeId = info.getRecordTypeId();
                }
            }
        } 
        obj.AccountRecordTypes = recordtypeMap;
        system.debug('obj' + obj);
        return obj;
    }
}