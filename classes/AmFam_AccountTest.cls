@isTest
private class AmFam_AccountTest {


    static testMethod void testAccountOwner(){

        //******************
        //Setup initial data
        wsPartyManage.lpeCallout = false;

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent', '098765');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1', '12345');

        Account agency = insertAgencyAccount('Acme', '12345');

        grantProducerAccess(agent1.Id, '12345', agency.Id);

        System.runAs(admin) {
            Account personAccount = insertPersonAccount('Pepe', 'Trueno', agency.Id);

            System.debug('### agent1: ' + agent1.Id);
            System.debug('### admin: ' + admin.Id);

            //Trigger succesfully execture should have the account created with Agent1 as owner instead of the runas user
            System.assertEquals(agent1.Id, personAccount.OwnerId);
        }
    }

    static testMethod void testHouseholdAccountAndRelationCreation() {
        Id personAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        Account testPA = new Account(FirstName = 'Testing', LastName = 'Account', RecordTypeId = personAccountRecordTypeId);
        test.startTest();
        insert testPA;
        test.stopTest();

        testPA = [SELECT Id, PersonContactId FROM Account WHERE Id =: testPA.Id];

        List<AccountContactRelation> acr = [SELECT Id, AccountId, ContactId FROM AccountContactRelation WHERE ContactId =: testPA.PersonContactId];
        System.assert(acr.size() == 1);
        Account householdAccount = [SELECT Id, Name FROM Account WHERE Id =: acr[0].AccountId];
        System.assert(householdAccount != null);
    }

    static testMethod void testNoHouseholdAccountAndRelationCreation() {
        Id personAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        Account testPA = new Account(FirstName = 'Testing', LastName = 'Account', RecordTypeId = personAccountRecordTypeId, CDHID__c = '12345');
        test.startTest();
        insert testPA;
        test.stopTest();

        testPA = [SELECT Id, PersonContactId FROM Account WHERE Id =: testPA.Id];

        List<AccountContactRelation> acr = [SELECT Id, AccountId, ContactId FROM AccountContactRelation WHERE ContactId =: testPA.PersonContactId];
        System.assert(acr.size() == 0);
    }



     //Method to test AmFam_AccountGlobalActionController class
    static testMethod void AmFam_AccountGlobalActionControllerTest() {
        AmFam_AccountGlobalQuickActionController cont = AmFam_AccountGlobalQuickActionController.getRecordTypeValues() ;
        Id defaultRTId; 
        List<Schema.RecordTypeInfo> recordTypeInfoList = Account.SObjectType.getDescribe().getRecordTypeInfos();
        for(RecordTypeInfo info: recordTypeInfoList) {
            if(info.isDefaultRecordTypeMapping()){
                    defaultRTId = info.getRecordTypeId();
            }
        } 
        system.assertEquals(cont.defaultRecordTypeId, defaultRTId);
    }
    
    //Method to test extendPCIFYToAccount method on AmFam_AccountService class
    static testMethod void testPCIFYOnAccount(){
        
        Id personAccountRecordTypeId =   AmFam_Utility.getPersonAccountRecordType();
        Account testAcc = new Account(FirstName = 'Person', LastName = 'Account',Description ='4917 6100 0000 0000', RecordTypeId = personAccountRecordTypeId);
        test.startTest();
        insert testAcc;
        test.stopTest();
        
        Account personAcc = [Select id, Description from Account where Id =: testAcc.id LIMIT 1];
        System.assert(personAcc.Description.contains('*'));
        
    }
    // Method test for Amfam_AccountService.runAccountTriggerValidations
    static testMethod void Amfam_AccountService_runAccountBeforeInsertTriggerValidations() {
        
        Id personAccountRecordTypeId =   AmFam_Utility.getPersonAccountRecordType();
        Account validAccount = new Account(FirstName = 'Person', LastName = 'Account',CDHID__c ='1234567', RecordTypeId = personAccountRecordTypeId);
        Account duplicateAccount = new Account(FirstName = 'Person', LastName = 'Account',CDHID__c ='1234567', RecordTypeId = personAccountRecordTypeId);
        
        test.startTest();
        insert validAccount;

        try {
            // should fail due to duplicate CDHID of the same RecordType.
            insert duplicateAccount;
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('Exception: Duplicate CDHID'));
        }
       
        test.stopTest();
    }
    
    // Method test for Amfam_AccountService.runAccountTriggerValidations
    static testMethod void Amfam_AccountService_runAccountBeforeUpdateTriggerValidations() {
        
        Id personAccountRecordTypeId =   AmFam_Utility.getPersonAccountRecordType();
        
        Account validAccount = new Account(FirstName = 'Person', LastName = 'Account',CDHID__c ='1234567', RecordTypeId = personAccountRecordTypeId);
        Account duplicateAccount = new Account(FirstName = 'Person1', LastName = 'Account1',CDHID__c ='12347', RecordTypeId = personAccountRecordTypeId);
        
        test.startTest();
        insert validAccount;
        insert duplicateAccount;

        try {
            // should fail due to duplicate CDHID of the same RecordType.
            duplicateAccount.CDHID__c = '1234567';
            update duplicateAccount;
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('Exception: Duplicate CDHID'));
        }
       
        test.stopTest();
    }


    //************************* */
    // Utility Methods
    //************************* */


    private static Account insertAgencyAccount(String accName, String producerIdentifier){
        Id devRecordTypeId =   AmFam_Utility.getAccountAgencyRecordType();
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.ShippingStreet = '123 Main Street';
        objAcc.ShippingCity = 'San Francisco';
        objAcc.ShippingStateCode = 'CA';
        objAcc.ShippingPostalCode = '12345';
        objAcc.ShippingCountryCode = 'US';
        objAcc.BillingStreet = '123 Main Street';
        objAcc.BillingCity = 'San Francisco';
        objAcc.BillingStateCode = 'CA';
        objAcc.BillingPostalCode = '12345';
        objAcc.BillingCountryCode = 'US';
        if (string.isNotEmpty(producerIdentifier)){
            objAcc.Producer_ID__c = producerIdentifier;
        }
        
        insert objAcc;

        System.debug('### Agency account: ' + objAcc);
        return objAcc;
    }

    private static Account insertAgencyAccount(String accName){
    
        return insertAgencyAccount(accName, null);
    }




    private static User insertUser(String profileName, String aliasName, String fisrtName, String lastName, String producerIdentifier) {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name =: profileName];
        User agent = new User (     alias = aliasName, 
                                    email= aliasName + '12456@xyz.com',
                                    emailencodingkey='UTF-8', 
                                    FederationIdentifier= aliasName + '12456',
                                    firstname=fisrtName, 
                                    lastname=fisrtName,
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', 
                                    profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username= aliasName + 'test12456@xyz.com.test',
                                    AmFam_User_ID__c= aliasName + '1234',
                                    Producer_ID__c= producerIdentifier
                                );
        
        insert agent;

        return agent;

    }



    private static Account insertPersonAccount(String firstName, String lastName, String agencyId) {
        
        Id devRecordTypeId = AmFam_Utility.getPersonAccountRecordType();       

        Account objAcc = new Account();
        objAcc.FirstName = 'CHANGEOWNER';
        objAcc.LastName = 'TEST II';
        objAcc.FinServ__IndividualType__c = 'Group';
        objAcc.RecordTypeId = devRecordTypeId;
        if (String.isNotEmpty(agencyId)) {
            objAcc.Agency__c = agencyId;
        }

        System.debug('### account to insert: ' + objAcc);

        insert objAcc;
        
        objAcc = [SELECT Id, PersonContactId, Name, RecordTypeId, OwnerId, Owner.Name, Agency__c FROM Account WHERE Id =: objAcc.id];
        
        System.debug('### new account [' + objAcc.Name + '] with owner: ' + objAcc.Owner.Name );    
        
        return objAcc;
    }    



    private static void grantProducerAccess(String userId, String producerIdentifier, String agencyId) {
        Producer p = new Producer();
        p.AccountId = agencyId;
        p.InternalUserId = userId;
        p.Name = producerIdentifier;

        insert p;


        ProducerAccess__c pa = new ProducerAccess__c();
        pa.Producer_Identifier__c = producerIdentifier;
        pa.RelatedId__c = agencyId;
        pa.Reason__c = 'ENTRY';

        insert pa;

    }




}