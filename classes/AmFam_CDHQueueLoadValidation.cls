/**
*   @description: the apex class is specially developed to handle CDH Queue data load, 
*	to prevent insert/update if the related Account lastmodified date is greater than CDH Queue Enabled Date
*   @author: Anbu Chinnathambi
*   @date: 10/19/2021
*   @group: Apex class
*/

public class AmFam_CDHQueueLoadValidation {
    
    public static void validate(List<ContactPointEmail> contactPointEmailList, List<ContactPointPhone> contactPointPhoneList){
        set<Id> accountIdSet = new set<Id>();
        if(contactPointEmailList != null){
            for(ContactPointEmail cpe : contactPointEmailList){
                accountIdSet.add(cpe.ParentId); 
            }    
        }else if(contactPointPhoneList != null){
            for(ContactPointPhone cpe : contactPointPhoneList){
                accountIdSet.add(cpe.ParentId); 
            }    
        }
        
        Id userId = UserInfo.getUserId();
        CDH_Queue_Validation_Setup__c setup = CDH_Queue_Validation_Setup__c.getInstance(userId);
        if(setup?.CDH_Queue_Enabled_Date__c != null && setup?.Validate__c){
            DateTime dtTime = setup?.CDH_Queue_Enabled_Date__c; 
            String setUpUserId = setup?.User_Id__c; 
            Map<Id, Account> accIdAccountMap = new Map<Id, Account>([select id, LastModifiedDate, LastModifiedById  from Account where id =: accountIdSet]);
            if(contactPointEmailList != null){
                for(ContactPointEmail contactEmail : contactPointEmailList ){
                    Account acct = accIdAccountMap.get(contactEmail.ParentId);
                    if(acct.LastModifiedDate > dtTime && string.valueOf(userId) == setUpUserId && string.valueOf(acct.LastModifiedById) != setUpUserId ){
                        contactEmail.addError('The record has already been updated by another process. Manual data load is not required.');    
                    }  
                }    
            }else{
                for(ContactPointPhone contactPhone : contactPointPhoneList ){
                    Account acct = accIdAccountMap.get(contactPhone.ParentId);
                    if(acct.LastModifiedDate > dtTime && string.valueOf(userId) == setUpUserId && string.valueOf(acct.LastModifiedById) != setUpUserId){
                        contactPhone.addError('The record has already been updated by another process. Manual data load is not required.');    
                    }  
                }  
            }
        }
     }
}