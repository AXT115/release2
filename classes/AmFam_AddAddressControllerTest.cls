@isTest 
public inherited sharing class AmFam_AddAddressControllerTest {
    
    static testMethod void testAddAddress(){
        
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];
        Account agencyAccount = insertAgencyAccount('Agency Account');

        Test.startTest();
        System.runAs(objUser){

            
            AmFam_AddAddressController.saveAddress(agencyAccount.Id, 'Mailing', '123 Main Street', 'Miami', null, 'FL', 'USA'); 

            List<Schema.Address> newAddress = [SELECT Id FROM Address];
            System.assertNotEquals(0, newAddress.size(), 'Address record not created Properly');

            List<Schema.Location> newLocations = [SELECT Id FROM Location];
            System.assertNotEquals(0, newLocations.size(), 'Location record not created Properly');

            List<AssociatedLocation> newAssociatedLocations = [SELECT Id FROM AssociatedLocation];
            System.assertNotEquals(0, newAssociatedLocations.size(), 'Associated Location record not created Properly');

            
        }
        Test.stopTest();
    }


    static testMethod void testAddAddressException(){
        
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];
        Account agencyAccount = insertAgencyAccount('Agency Account');

        Test.startTest();
        System.runAs(objUser){

            try {
                AmFam_AddAddressController.saveAddress(agencyAccount.Id, null, '123 Main Street', 'Miami', null, 'FL', 'USA'); 
            } catch (Exception ex) {

            }

            List<Schema.Address> newAddress = [SELECT Id FROM Address];
            System.assertEquals(0, newAddress.size(), 'Address record created but should not');
            

        }
        Test.stopTest();
        
    }





    static testMethod void testAddressTypes() {

        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];

        Test.startTest();
        System.runAs(objUser){


            Map<String, String> addressTypes = AmFam_AddAddressController.getAddressTypes();
            System.assertNotEquals(0, addressTypes.keySet().size(), 'Address Types not retrieved properly');

        }
        Test.stopTest();

    }



    private static Account insertAgencyAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        return objAcc;
    }


}