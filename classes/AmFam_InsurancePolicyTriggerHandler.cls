public with sharing class AmFam_InsurancePolicyTriggerHandler {
    
    
    public void OnBeforeInsert (List<InsurancePolicy> newRecords) {
        AmFam_InsurancePolicyService.updateInsurancePolicyOwnership(newRecords);
    }
    

    public void OnAfterInsert (List<InsurancePolicy> newRecord) {
            AmFam_InsurancePolicyService.removeConvertedPolicies(newRecord);
            AmFam_InsurancePolicyService.updateInsurancePolicyShares(newRecord);
    }

    
    public void OnAfterUpdate (List<InsurancePolicy> newRecord,
                               List<InsurancePolicy> oldRecord,
                               Map<ID, InsurancePolicy> newRecordMap,
                               Map<ID, InsurancePolicy> oldRecordMap) {

        //associated producer could change so need to perhaps redo shares

        //Do not process policies where the batch flag changed, they will be handled by periodic batch process
        //(or the flag is being reset at the completion of the batch process)
        List<InsurancePolicy> nonBatchPolicies = new List<InsurancePolicy>();
        for(InsurancePolicy policy : newRecord) 
        {
            InsurancePolicy oldPolicy = oldRecordMap.get(policy.Id);
            if (policy.Batch_Processed__c == oldPolicy.Batch_Processed__c)
            {
                nonBatchPolicies.add(policy);
            }
        }

        if (!nonBatchPolicies.isEmpty())
        {
            AmFam_InsurancePolicyService.updateInsurancePolicyShares(newRecord);
        }
    }


    public void OnBeforeUpdate(List<InsurancePolicy> newRecords,
                                List<InsurancePolicy> oldRecord,
                                Map<ID, InsurancePolicy> newRecordMap,
                                Map<ID, InsurancePolicy> oldRecordMap) {

                                    
        AmFam_InsurancePolicyService.updateInsurancePolicyOwnership(newRecords);    

    }
    
    /*
    public void OnBeforeDelete(List<InsurancePolicy> oldRecord,
                              Map<ID, InsurancePolicy> oldRecordMap) {

    }



    public void OnAfterDelete(List<InsurancePolicy> oldRecord,
                              Map<ID, InsurancePolicy> oldRecordMap) {

      

    }
    */



}