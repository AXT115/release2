//not 'with sharing' because we are expecting to show policies outside BoB
public without sharing class AmFam_HouseholdPolicyListController {
    public class ResponseRecord
    {
        @AuraEnabled public String policyId;
        @AuraEnabled public String name;
        @AuraEnabled public String role;
        @AuraEnabled public String policyURL;
        @AuraEnabled public Decimal premiumAmount;
        @AuraEnabled public String nameInsuredId;
        @AuraEnabled public String nameInsuredName;
        @AuraEnabled public String nameInsuredURL;
        @AuraEnabled public String billingAccount;
        @AuraEnabled public String billingAccountURL;
        @AuraEnabled public Boolean isActive;
        @AuraEnabled public String policyType;
        @AuraEnabled public String status;
        @AuraEnabled public String expirationDate;
        @AuraEnabled public String producerId;
        @AuraEnabled public String cancellationDate;
        @AuraEnabled public String producerName;
        @AuraEnabled public String producerAccountName;
        @AuraEnabled public String producerAccountURL;
    }

    //-----
    //returns map containing lists of ResponseRecords. 'policies' key for BOB policies, 'additional' key for NonBOB policies
    @AuraEnabled(cacheable=true)
    public static Map<String, List<ResponseRecord>> getPolicies(Id accountId) 
    {
        //for testing
        //return AmFam_HouseholdPolicyListController.loadDummyReponse();
        Id householdRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IndustriesHousehold').getRecordTypeId();

        List<InsurancePolicy> allPolicies;

        Account acct = [SELECT Id, RecordTypeId FROM Account WHERE Id = :accountId LIMIT 1];

        if (acct.RecordTypeId == householdRecordTypeId)
        {
            allPolicies = AmFam_HouseholdPolicyListController.getPoliciesForHouseholdAccount(accountId);
        }
        else 
        {
            allPolicies = AmFam_HouseholdPolicyListController.getPoliciesForBusinessOrPersonAccount(accountId);            
        }

        User currUser = [Select Id,AmFam_User_ID__c from User where Id = :UserInfo.getUserId()];

        //Now we have all the policies associated
        //Next figure out user BoB
        String endpoint = '/alternatekeys?requestIdentifierType=USER_ID&responseIdentifierType=PRODUCER_ID&sourceSystemIdentifier=AMFAM&requestIdentifierValue='+currUser.AmFam_User_ID__c;

        AmFam_ProducerTypeService prodTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut(endpoint);

        if (prodTypeSvc?.alternateKey?.keyId != null) {
            // Get the Producer Info from the Producer Service
            prodTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/' + prodTypeSvc.alternateKey.keyId + '/booksofbusiness');
        }

        //collect the BoB producer ids
        List<String> bobProdIds = new List<String>();
        if (prodTypeSvc?.bookOfBusiness != null)
        {
            for (AmFam_ProducerTypeService.BookOfBusiness book : prodTypeSvc.bookOfBusiness)
            {
                for (AmFam_ProducerTypeService.ProducerCodes prodCode : book.producerCodes)
                {
                    bobProdIds.add(prodCode.producerCode);
                }
            }
        }
        //divide the Policies to those in and not in users BoB
        List<InsurancePolicy> bobPolicies = new List<InsurancePolicy>();
        List<InsurancePolicy> nonBoBPolicies = new List<InsurancePolicy>();
        for (InsurancePolicy pol : allPolicies)
        {
            if (bobProdIds.contains(pol.Producer.Name)) 
            {
                bobPolicies.add(pol);
            }
            else 
            {
                nonBoBPolicies.add(pol);
            }
        }

        //Lastly massage the lists into the response
        Map<String, List<ResponseRecord>> result = new Map<String, List<ResponseRecord>>();

        result.put('policies', AmFam_HouseholdPolicyListController.policiesToResponseRecords(bobPolicies));
        result.put('additional',AmFam_HouseholdPolicyListController.policiesToResponseRecords(nonBoBPolicies));
        System.debug(result);

        return result;  
    }

    private static List<InsurancePolicy> getPoliciesForHouseholdAccount(Id householdAccountId)
    {
        List<AccountContactRelation> relations = [SELECT Id,AccountId,ContactId
            FROM AccountContactRelation
            WHERE AccountId = :householdAccountId];

        List<Id> memberIds = new List<Id>();
        for (AccountContactRelation relation : relations)
        {
            memberIds.add(relation.ContactId);
        }

        //get household member contacts
        List<Contact> contacts = [SELECT Id,AccountId FROM Contact WHERE Id IN :memberIds];

        List<Id> acctIds = new List<Id>();
        for (Contact contact : contacts)
        {
            acctIds.add(contact.AccountId);
        }

        return AmFam_HouseholdPolicyListController.getPoliciesForAccounts(acctIds);
    }

    private static List<InsurancePolicy> getPoliciesForBusinessOrPersonAccount(Id accountId)
    {
        return AmFam_HouseholdPolicyListController.getPoliciesForAccounts(new List<Id>{accountId});
    }

    
    private static List<InsurancePolicy> getPoliciesForAccounts(List<Id> acctIds)
    {
        List<InsurancePolicyParticipant> ipps = [SELECT Id,IsActiveParticipant,InsurancePolicyId,PrimaryParticipantAccountId,
                                                        RelatedParticipantAccountId,Role
                                                    FROM InsurancePolicyParticipant 
                                                    WHERE PrimaryParticipantAccountId in :acctIds]; 

        Set<Id> policyIds = new Set<Id>();
        for (InsurancePolicyParticipant ipp : ipps)
        {
            policyIds.add(ipp.InsurancePolicyId);
        }

        //as Role is a desired field going to define return object and create map to generate the response so can save roles and then map records
        // to it
        List<InsurancePolicy> policies = [SELECT Id,Name,PremiumAmount,NameInsuredId,NameInsured.Name,IsActive,PolicyType,Status,
                                                ExpirationDate,ProducerId,CancellationDate,CancellationReasonType,CancellationReason,
                                                Producer.Name,Producer.AccountId,Producer.Account.Name, 
                                                Billing_Account__c, Billing_Account_Formula__c,
                                                Source_Expiration_Date__c,Source_Cancellation_Date__c,
                                                Source_System_Status__c
                                            FROM InsurancePolicy WHERE Id IN :policyIds
                                            ORDER BY Name];    
                                            
        return policies;
    }

    private static List<ResponseRecord> policiesToResponseRecords(List<InsurancePolicy> policies)
    {
        List<ResponseRecord> response = new List<ResponseRecord>();
        String dateFormatString = 'MM/dd/YYYY';

        for (InsurancePolicy policy : policies)
        {
            ResponseRecord rr = new ResponseRecord();
            rr.name = policy.Name;
            rr.policyURL = '/'+policy.Id;
            rr.premiumAmount = policy.PremiumAmount;
            rr.nameInsuredId = policy.NameInsuredId;
            rr.nameInsuredName = policy.NameInsured.Name;
            rr.nameInsuredURL = '/'+policy.NameInsuredId;
            rr.isActive = policy.IsActive;
            rr.policyType = policy.PolicyType;
            rr.status = policy.Source_System_Status__c;
            rr.expirationDate = policy.Source_Expiration_Date__c?.format();
            rr.producerId = policy.ProducerId;
            rr.cancellationDate = policy.Source_Cancellation_Date__c?.format();
            rr.producerId = policy.ProducerId;
            rr.producerName = policy.Producer.Name;
            rr.producerAccountName = policy.Producer.Account.Name;
            rr.billingAccount = policy.Billing_Account__c;
            rr.billingAccountURL = policy.Billing_Account_Formula__c;
            
            if (policy.Producer.AccountId != null)
            {
                rr.producerAccountURL = '/'+policy.Producer.AccountId;
            }
            response.add(rr);
        }   

        return response;
    }

    /*
    //method to just throw some data back to use
    public static Map<String, List<ResponseRecord>> loadDummyReponse()
    {
        Map<String, List<ResponseRecord>> result = new Map<String, List<ResponseRecord>>();
        List<ResponseRecord> polList = new List<ResponseRecord>();
        List<ResponseRecord> addList = new List<ResponseRecord>();
        
        ResponseRecord rr = new ResponseRecord();
        rr.isActive = true;
        rr.name='1010';
        rr.nameInsuredId='0010n00001F3VeEAAV';
        rr.nameInsuredName='Cindy Winters';
        rr.nameInsuredURL='/0010U00001AuA3mQAF';
        rr.policyType='Auto';
        rr.policyURL='/0YT0U000000CahMWAS';
        rr.premiumAmount=34.59;
        rr.producerId='0Yx0n0000004CPuCAM';
        rr.producerName='99999';
        rr.status='Inforce';
        rr.producerAccountURL='/0Yx0n0000004CPuCAM';
        rr.producerAccountName='Bloop';
        rr.expirationDate='02/22/2022';
        rr.cancellationDate='11/22/3333';
        rr.billingAccount='1234567';
        rr.billingAccountURL='/lightning/cmp/c__AmFam_ViewInsurancePolicyDetails?c__recordId='+'0DUMMYID'+'&c__billingAccount='+rr.billingAccount;
        polList.add(rr);

        rr = new ResponseRecord();
        rr.isActive=true;
        rr.name='5678'; 
        rr.nameInsuredId='0010n00001F3VeEAAV'; 
        rr.nameInsuredName='Cindy Winters';
        rr.nameInsuredURL='/0010U00001AuA3mQAF';
        rr.policyType='Auto';
        rr.policyURL='/0YT0U000000CahMWAS';
        rr.premiumAmount=123.45;
        rr.producerId='0Yx0n0000004CPuCAM';
        rr.producerName='99999';
        rr.status='Inforce';
        rr.producerAccountURL='/0Yx0n0000004CPuCAM';
        rr.producerAccountName='Bleep';
        rr.expirationDate='11/11/2033';
        rr.cancellationDate='11/22/3333';
        rr.billingAccount='7654321';
        rr.billingAccountURL='/lightning/cmp/c__AmFam_ViewInsurancePolicyDetails?c__recordId='+'0DUMMYID'+'&c__billingAccount='+rr.billingAccount;
        addList.add(rr);

        result.put('policies',polList);
        result.put('additional',addList);
        return result;
    }
    */
}