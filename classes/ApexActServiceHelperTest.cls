@isTest 
private class ApexActServiceHelperTest {

@isTest static void removeHTML () {
    String htmlString = '<p>test<p/>';
    System.assertEquals(' test ', ApexActServiceHelper.removeHTML(htmlString));
}

@isTest static void formatTaskBody(){
    Task task = new Task();
    task.Subject = 'Test Subject';
    task.Description = 'Test Description';
    task.CallDisposition = 'Test Call Result';
    String expectedResult = String.format('Subject: {0} \n Comments: {1} \n Call Result: {2}', 
    new List<String>{task.Subject, task.Description != null ? task.Description : '', task.CallDisposition != null ? task.CallDisposition : ''});
    
    //System.assertEquals(expectedResult,ApexActServiceHelper.formatTaskBody(task));
}

}