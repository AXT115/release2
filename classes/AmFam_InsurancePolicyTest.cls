@isTest
public class AmFam_InsurancePolicyTest {
    
	@testSetup static void setup() {

		Account agencyAccount = insertAgencyAccount('Agency Account');
		Account agentAccount = insertAgentAccount('Agent Account');
		Account personAccount = insertPersonAccount('Test 1', 'Person Account 1', 'Group');


	} 



    static testMethod void testCreateInsurancePolicy() {


        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Id agencyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Id agentRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        


        //******************
        //Setup initial data
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];

        User admin = insertUser('System Administrator', 'sagent', 'User', 'Agent');
        User agent1 = insertUser('AmFam Agent', 'sagent1', 'User1', 'Agent1');

		System.runAs(admin) {

	    	Account personAccount = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE RecordTypeId =: personAccountRTId LIMIT 1] ;
	    	Account agencyAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agencyRTID LIMIT 1] ;
	    	Account agentAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agentRTID LIMIT 1] ;
	    	Contact agentContact = [SELECT Id, Name, AccountId FROM Contact WHERE AccountId =: agentAccount.Id LIMIT 1] ;

            AccountContactRelation acr1 = insertAccountContactRelation(agencyAccount.Id, agentContact.Id);
			AccountContactRelation acr2 = insertAccountContactRelation(agentAccount.Id, personAccount.PersonContactId);

            Producer prod = new Producer();
            prod.Producer_ID__c = '77770';
            prod.Name = '77770';
            prod.InternalUserId = agent1.Id;
            prod.Type = 'Agency';
            insert prod;

	    	Test.startTest();

			InsurancePolicy ip = insertInsurancePolicyWithProducer(personAccount.Id,prod.Id);
			InsurancePolicy ipBad = insertInsurancePolicyWithoutProducer(personAccount.Id);

            System.debug('ip:'+ip.Id);
            System.debug('ipBad:'+ipBad.Id);
	    	Test.stopTest();


            List<InsurancePolicy> policyList = [SELECT Id, OwnerId, Owner.Name FROM InsurancePolicy WHERE Id =: ip.Id];
            for (insurancePolicy obj : policyList) {
                System.assertEquals(agent1.Id, obj.OwnerId);
            }


            List<InsurancePolicyShare> shares = [SELECT Id,ParentId,UserOrGroupId FROM InsurancePolicyShare WHERE ParentId = :ip.Id];
            System.debug(shares);
            System.assert(shares.size() > 0);

            shares = [SELECT Id,ParentId,UserOrGroupId FROM InsurancePolicyShare WHERE ParentId = :ipBad.Id];
            System.debug('badshare:'+shares);

            //only share should be the autocreated one that will get wiped on any share recalc
            System.assert(shares.size() == 1);
		}
	}


    static testMethod void testDeleteInsurancePolicyQuoteAndApplication() {


        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Id ipQuoteRT = Schema.SObjectType.InsurancePolicy.getRecordTypeInfosByDeveloperName().get('Quote').getRecordTypeId();
        Id ipApplicationRT = Schema.SObjectType.InsurancePolicy.getRecordTypeInfosByDeveloperName().get('Application').getRecordTypeId();
    

        //******************
        //Setup initial data
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];


		System.runAs(objUser) {		

	    	Account personAccount = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE RecordTypeId =: personAccountRTId LIMIT 1] ;

            System.debug('### Quotes RT:' + ipQuoteRT);
            System.debug('### Application RT:' + ipApplicationRT);


	    	Test.startTest();

            InsurancePolicy ipQ = insertInsurancePolicy(personAccount.Id, ipQuoteRT, 'IP003QUOTE');
            InsurancePolicy ipA = insertInsurancePolicy(personAccount.Id, ipApplicationRT, 'IP003QUOTE');
			

	    	Test.stopTest();

            List<InsurancePolicy> ipAfter_Quotes = [SELECT Id, Name FROM InsurancePolicy WHERE RecordTypeId =: ipQuoteRT];
            List<InsurancePolicy> ipAfter_Applications = [SELECT Id,Name FROM InsurancePolicy WHERE RecordTypeId =: ipApplicationRT];
            //After processing, the QUOTE version should be deleted (total == 0) and APPLICATION TYPE will remain (total > 0)
            System.assert(ipAfter_Quotes.size() == 0);
            System.assert(ipAfter_Applications.size() > 0);
		}
	}

    static testMethod void testRemovePolicyShares() 
    {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent' LIMIT 1];
        //A 1, A 2, B 1, C 2
        User agencyUser = new User (alias = 'agency1', email='agency1@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='testa1',
                                    firstname='User', lastname='Agency1',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='agency1@xyz.com.test',
                                    AmFam_User_ID__c='agency1'
                                );

        User agent1 = new User (alias = 'agent1', email='agent1@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='testa1',
                                firstname='User', lastname='Agent1',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='agent1@xyz.com.test',
                                AmFam_User_ID__c='agent1'
                            );

        User agent2 = new User (alias = 'agent2', email='agent2@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='testa2',
                                    firstname='User', lastname='Agent2',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='agent2@xyz.com.test',
                                    AmFam_User_ID__c='agent2'
                                );

        User agent3 = new User (alias = 'agent3', email='agent3@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='testa3',
                                firstname='User', lastname='Agent3',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='agent3@xyz.com.test',
                                AmFam_User_ID__c='agent3'
                            );
        List<User> users = new List<User>{agent1,agent2,agent3,agencyUser};
        insert users;

        System.debug('Users:'+agencyUser.Id+' '+agent1.Id+' '+agent2.Id+' '+agent3.Id);
        //create producers
        Account agency = AmFam_TestDataFactory.insertAgencyAccount('TheAgency', '77777');
        
        Account insured = AmFam_TestDataFactory.insertPersonAccount('Insured', 'Insured', 'Individual');

        Producer agencyProd = new Producer();
        agencyProd.AccountId = agency.Id;
        agencyProd.InternalUserId = agencyUser.Id;
        agencyProd.Type = 'Agency';
        agencyProd.Name = '77777';

        Producer prodA1 = new Producer();
        prodA1.AccountId = insured.Id;
        prodA1.InternalUserId = agent1.Id;
        prodA1.Name = '77777';

        Producer prodA2 = new Producer();
        prodA2.AccountId = insured.Id;
        prodA2.InternalUserId = agent1.Id;
        prodA2.Name = '78787';

        Producer prodB1 = new Producer();
        prodB1.AccountId = insured.Id;
        prodB1.InternalUserId = agent2.Id;
        prodB1.Name = '77777';

        Producer prodC2 = new Producer();
        prodC2.AccountId = insured.Id;
        prodC2.InternalUserId = agent3.Id;
        prodC2.Name = '78787';

        insert new List<Producer>{agencyProd,prodA1,prodA2,prodB1,prodC2};

        InsurancePolicy pol = new InsurancePolicy();
        pol.Name = '1234';
        pol.PolicyName = 'Policy1';
        pol.NameInsuredId = insured.Id;
        pol.PolicyType = 'Life';
        pol.ProducerId = agencyProd.Id;
        insert pol;

        Test.startTest();
 
        delete prodA1;
        Test.stopTest();

        List<InsurancePolicyShare> newShares = [SELECT Id,ParentId,Parent.Name,UserOrGroupId,UserOrGroup.Name,RowCause FROM InsurancePolicyShare WHERE RowCause = 'Manual'];
        System.debug(newShares);
        
        for (InsurancePolicyShare share : newShares) 
        {
            System.debug(share.Id+'-'+share.UserOrGroup.Name+'-'+share.Parent.Name+'-'+share.RowCause);
        }

        //should have 1 manual share created
        System.assert(newShares.size() == 1,'Expecting 1 Shares but got '+newShares.size());
    }

    static testMethod void testNewPolicySharesForNewProducer() 
    {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent' LIMIT 1];

        User agencyUser = new User (alias = 'agency1', email='agency1@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='testa1',
                                    firstname='User', lastname='Agency1',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='agency1@xyz.com.test',
                                    AmFam_User_ID__c='agency1'
                                );

        User agent1 = new User (alias = 'agent1', email='agent1@xyz.com',
                                emailencodingkey='UTF-8', FederationIdentifier='testa1',
                                firstname='User', lastname='Agent1',
                                languagelocalekey='en_US',
                                localesidkey='en_US', profileid = p.Id,
                                timezonesidkey='America/Chicago',
                                username='agent1@xyz.com.test',
                                AmFam_User_ID__c='agent1'
                            );
        insert new List<User>{agencyUser,agent1};
        
        Account agency = AmFam_TestDataFactory.insertAgencyAccount('TheAgency', '77777');
        
        Account insured = AmFam_TestDataFactory.insertPersonAccount('Insured', 'Insured', 'Individual');
                        
        Producer agencyProd = new Producer();
        agencyProd.AccountId = agency.Id;
        agencyProd.InternalUserId = agencyUser.Id;
        agencyProd.Type = 'Agency';
        agencyProd.Name = '77777';
        insert agencyProd;

        InsurancePolicy pol = new InsurancePolicy();
        pol.Name = '1234';
        pol.PolicyName = 'Policy1';
        pol.NameInsuredId = insured.Id;
        pol.PolicyType = 'Life';
        pol.ProducerId = agencyProd.Id;
        insert pol;

        Test.startTest();

        Producer newProd = new Producer();
        newProd.InternalUserId = agent1.Id;
        newProd.Type = 'Manual';
        newProd.Name = '77777';
        insert newProd;

        Test.stopTest();

        List<InsurancePolicyShare> newShares = [SELECT Id,ParentId,Parent.Name,UserOrGroupId,UserOrGroup.Name,RowCause FROM InsurancePolicyShare WHERE RowCause = 'Manual'];
        System.debug(newShares);
        
        for (InsurancePolicyShare share : newShares) 
        {
            System.debug(share.Id+'-'+share.UserOrGroup.Name+'-'+share.Parent.Name+'-'+share.RowCause);
        }

        //should have 1 manual share created
        System.assert(newShares.size() == 1,'Expecting 1 Shares but got '+newShares.size());
    }

    private static Producer insertProducer(Account acc, User internalUser, String identifier) {

        Producer producer = new Producer();
        producer.AccountId = acc.Id;
        producer.InternalUserId = internalUser.Id;
        producer.Name = identifier;

        insert producer;

        return producer;
    }


    private static User insertUser(String profileName, String aliasName, String fisrtName, String lastName) {

        Profile p = [SELECT Id, Name FROM Profile WHERE Name =: profileName];
        User agent = new User (     alias = aliasName, 
                                    email= aliasName + '12456@xyz.com',
                                    emailencodingkey='UTF-8', 
                                    FederationIdentifier= aliasName + '12456',
                                    firstname=fisrtName, 
                                    lastname=fisrtName,
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', 
                                    profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username= aliasName + 'test12456@xyz.com.test',
                                    AmFam_User_ID__c= aliasName + '1234'
                                );
        
        insert agent;

        return agent;

    }


	private static InsurancePolicy insertInsurancePolicyWithProducer(String insuredId,Id producerId) {
		InsurancePolicy ip = new InsurancePolicy();
		ip.NameInsuredId = insuredId;
		ip.Name = 'IP004TEST';
        ip.ProducerId = producerId;
		insert ip;
		return ip;
	}

	private static InsurancePolicy insertInsurancePolicyWithoutProducer(String insuredId) {
		//return insertInsurancePolicy(insuredId, null, 'IP004TEST');
		InsurancePolicy ip = new InsurancePolicy();
		ip.NameInsuredId = insuredId;
		ip.Name = 'IP004TEST';
		insert ip;
		return ip;
 
	}

	private static InsurancePolicy insertInsurancePolicy(String insuredId, String recordTypeId, String submissionId) {
		InsurancePolicy ip = new InsurancePolicy();
		ip.NameInsuredId = insuredId;
		ip.Name = submissionId;
        ip.SubmissionID__c = submissionId;
        if (recordTypeId != null) {
            ip.RecordTypeId = recordTypeId;
        }

		insert ip;
		return ip;
	}

    private static Account insertPersonAccount(String firstName, String lastName, String individualType){
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.FirstName = firstName;
        objAcc.LastName = lastName;
        objAcc.FinServ__IndividualType__c = individualType;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        objAcc = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE Id =: objAcc.id];
        return objAcc;
    }
    
        
    private static Account insertAgencyAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.Producer_ID__c = '77770';
        insert objAcc;
        return objAcc;
    }

    
    private static Account insertAgentAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;

        Contact objContact = new Contact();
        objContact.FirstName = accName;
        objContact.LastName = accName;
        objContact.AccountId = objAcc.Id;
        insert objContact;

        return objAcc;
    }


    private static AccountContactRelation insertAccountContactRelation(String accountId, String contactId) {
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = accountId;
        acr.ContactId = contactId;
        insert acr;
        return acr;
    }

}