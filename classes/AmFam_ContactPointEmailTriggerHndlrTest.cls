@IsTest
public with sharing class AmFam_ContactPointEmailTriggerHndlrTest {

    private class SetupObjects
    {
        Id householdId;
        Id person1Id;
        Id person2Id;
    }

    private static SetupObjects setup() {

        SetupObjects so = new SetupObjects();

        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');
        household.Household_Email__c = 'nope@nobody.com';
        update household;
        so.householdId = household.Id;


        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;
        so.person1Id = person1.Id;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');
        so.person2Id = person2.Id;

        List<Contact> contacts = [SELECT Id FROM Contact];

        List<AccountContactRelation> rels = new List<AccountContactRelation>();
        for (Contact contact : contacts)
        {
            AccountContactRelation rel = new AccountContactRelation();
            rel.AccountId = household.Id;
            rel.ContactId = contact.Id;
            rel.Roles = 'Client';
            rel.IsActive = true;
            rels.add(rel);
        }
        
        insert rels;

        return so;
	} 

    public static testmethod void testPositive()
    {
        SetupObjects so = AmFam_ContactPointEmailTriggerHndlrTest.setup();

        String email = 'iamsomebody@yay.com';
        ContactPointEmail cpEmail = new ContactPointEmail();
        cpEmail.EmailAddress = email;
        cpEmail.ParentId = so.person1Id;
        cpEmail.IsPrimary = true;
        insert cpEmail;

        Account hh = [SELECT Household_Email__c FROM Account WHERE Id = :so.householdId LIMIT 1];
        System.assert(hh.Household_Email__c == email);
   }

    public static testmethod void testNegative()
    {
        SetupObjects so = AmFam_ContactPointEmailTriggerHndlrTest.setup();

        String email = 'buzz@kill.com';
        ContactPointEmail cpEmail = new ContactPointEmail();
        cpEmail.EmailAddress = email;
        cpEmail.ParentId = so.person2Id;
        cpEmail.IsPrimary = true;
        insert cpEmail;

        Account hh = [SELECT Household_Email__c FROM Account WHERE Id = :so.householdId LIMIT 1];
        System.assert(hh.Household_Email__c != email);
      }
}