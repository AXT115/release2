public with sharing class AmFam_NewLeadController {
    @AuraEnabled
    public static AmFam_PageLayoutForm.PageLayout getPageLayoutMetadata(String pageLayoutName) {
        return AmFam_PageLayoutForm.getPageLayoutMetadata(pageLayoutName);
    }

    /*
     * Makes a callout to PartySearch to refresh the data on the lead based on CDHID__c or Contact_CDHID__c. 
     * This method will DML update the lead record 
     * 
     * @args leadRecordId
     */
    @AuraEnabled
    public static void refreshLead(String leadRecordId) {
         if(leadRecordId != null) {
            try {
                Lead lead = wsPartySearchHelper.refreshLead(leadRecordId);
                update lead;
            } catch (Exception e) {
                System.debug('Caught Exception in refreshLead ' + e.getMessage());
            }
        }
    }

    @AuraEnabled
    public static LeadWrapper sendLeadToCDH(Id leadId, Boolean code1Complete, Boolean applyPartyMatch, String selectedAddress){
        
        try{
            
            Lead rec = [SELECT Id, FirstName, MiddleName, LastName, Gender__c, Marital_Status__c, Date_of_Birth__c,
                            Drivers_License_Number__c,Driver_s_License_State__c, Drivers_License_Issue_Month__c, Drivers_License_Issue_Year__c,
                            CountryCode, Street, City, StateCode, Company, PostalCode, Email, Email_Usage__c, Agency__r.Producer_Id__c,
                            Phone, Phone_Usage__c, MobilePhone, CDHID__c, Contact_CDHID__c, OwnerId, Party_Level__c, Agency__r.Is_Corporate_Account__c, 
                            Source_System_Key__c, Primary_Language__c, Residence_Business__c, Mailing__c, Residence_Business_2__c, 
                            Residence_Business_3__c, Mailing_2__c, Mailing_3__c, Additional_Phone_1__c, Additional_Phone_2__c, Additional_Phone_3__c, 
                            Additional_Phone_1_Usage__c, Additional_Phone_2_Usage__c, Additional_Phone_3_Usage__c, Additional_Email_1__c, Additional_Email_2__c,
                            Additional_Email_1_Usage__c, Additional_Email_2_Usage__c, Partner__c
                FROM Lead
                WHERE Id =: leadId Limit 1];

            wsPartyManageServiceCallout.PartyManageServiceSOAP partyManage = new wsPartyManageServiceCallout.PartyManageServiceSOAP();

            // Entrant
            wsPartyManageParty.EntrantType entrant = wsPartyManage.constructEntrant(rec, selectedAddress);
            system.debug(entrant);

            // Party
            wsPartyManageParty.PartyType party = wsPartymanage.constructParty(rec);
            party.Entrant = entrant;

            // AddEntrantRequest
            wsPartyManageService.AddPartyRequestType addEntrantRequest = wsPartyManage.constructAddPartyRequest(rec, null, party);
            addEntrantRequest.code1Complete = code1Complete;
            addEntrantRequest.applyPartyMatching = applyPartyMatch;

            // Callout
            wsPartyManageService.AddPartyResultType resp = partyManage.addParty_http(addEntrantRequest);
            if (resp.Party?.partyIdentifier != null && resp.Errors == null) {
                rec = parseAndPersistCDHResponse(rec, resp.Party);
                update rec;
                // Call Producer Association after Add Entrant is completed
                ID jobID = System.enqueueJob(new AmFam_LeadAssociationToProducerQueueable(rec));
            } else {
                
                //we are not sending all the fields to CDH but we need all the fields to be rendered on lightning page
                SObjectType type = leadId.getSObjectType();
                List<String> allFields = new List<String>(type.getDescribe().fields.getMap().keySet());
                String soql = String.format('SELECT {0} FROM {1} WHERE Id = :leadId', 
                                                            new List<String>{ String.join(allFields, ','), ''+type });
                rec = (Lead)Security.stripInaccessible(AccessType.UPDATABLE, Database.query(soql)).getRecords()[0];
                
                // Delete the lead because CDH didn't validate the already created one
                deletelead(leadId);
                rec.id = null;
            }
            LeadWrapper lw = new LeadWrapper(rec, resp.Errors?.Error, resp.Code1Result, resp.PartyMatch, resp.HouseholdMatch);
            return lw;
         }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
    }
    
    @future
    public static void deletelead(Id leadId) {
        Lead l = new Lead(Id = leadId);
        delete l;
    }
    
    @AuraEnabled
    public static Id createLead(Lead leadRec, String selectedAddress) {
        try{
            if(selectedAddress != null){
                Map<String, Object> addressMap;
                for(object data: (List<Object>)JSON.deserializeUntyped(selectedAddress)){
                    addressMap = (Map<String, Object>)data;
                }
                if(addressMap != null){
                    leadRec.Street =  String.valueOf(addressMap.get('addressLine1'));
                    leadRec.City =  String.valueOf(addressMap.get('city'));
                    leadRec.StateCode =  String.valueOf(addressMap.get('stateCode'));
                    leadRec.PostalCode =  String.valueOf(addressMap.get('zip5Code'));
                    leadRec.CountryCode =  'US';
                }
            }
            
            upsert leadRec;
            return leadRec.id;
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Lead updateLeadRecord(Lead leadRec, String selectedAddress, Id leadId) {
        
        leadRec.id = leadId;
        if(selectedAddress != null){
            
            Map<String, Object> addressMap;
            for(object data: (List<Object>)JSON.deserializeUntyped(selectedAddress)){
                addressMap = (Map<String, Object>)data;
            }
            
            if(addressMap != null){
                leadRec.Street =  String.valueOf(addressMap.get('addressLine1'));
                leadRec.City =  String.valueOf(addressMap.get('city'));
                leadRec.StateCode =  String.valueOf(addressMap.get('stateCode'));
                leadRec.PostalCode =  String.valueOf(addressMap.get('zip5Code'));
                leadRec.CountryCode =  'US';
            }
        }
        leadRec.Date_of_Birth__c = leadRec?.Date_of_Birth__c != null ? Date.Valueof(leadRec?.Date_of_Birth__c): null;
        //if(leadId !=null){update leadRec;}
        return leadRec;
    }
    
    @AuraEnabled
    public static LeadWrapper sendLeadEditToCDH(String leadRecord, Id leadId, Boolean code1Complete, Boolean applyPartyMatch, String selectedAddress) {
        try{
            String previousPhone = null;
            String previousPhoneUsage = null;
            String previousEmail = null;
            String previousEmailUsage = null;
            Boolean LeadUpdate = true;
                
            Lead leadToUpdate = new Lead();
            Lead newLeadRec = (Lead)JSON.deserialize(leadRecord, Lead.class);
            LeadWrapper lw = new LeadWrapper(null, null, null, null, null);
                
            Lead oldLeadRec = [SELECT Id, FirstName, MiddleName, LastName, Gender__c, Marital_Status__c, Date_of_Birth__c,
                Drivers_License_Number__c, Driver_s_License_State__c, Drivers_License_Issue_Month__c, Drivers_License_Issue_Year__c,
                CountryCode, Company, Street, City, StateCode, PostalCode, Email, Email_Usage__c, Agency__r.Producer_Id__c,
                Phone, Phone_Usage__c, MobilePhone, CDHID__c, Contact_CDHID__c, Country, State, OwnerId, Party_Level__c, Party_Version__c, 
                Agency__r.Is_Corporate_Account__c, Source_System_Key__c, Primary_Language__c, Residence_Business__c, Mailing__c, Residence_Business_2__c, 
                Residence_Business_3__c, Mailing_2__c, Mailing_3__c, Additional_Phone_1__c, Additional_Phone_2__c, Additional_Phone_3__c, 
                Additional_Phone_1_Usage__c, Additional_Phone_2_Usage__c, Additional_Phone_3_Usage__c, Additional_Email_1__c, Additional_Email_2__c,
                Additional_Email_1_Usage__c, Additional_Email_2_Usage__c, Partner__c
                FROM Lead
                WHERE Id =: leadId Limit 1];
            system.debug('Old Lead Company: '+oldLeadRec.Company);
            system.debug('New Lead Company: '+newLeadRec.Company);
            if (checkIfFieldsChanged(oldLeadRec, newLeadRec, 'Locked_Fields_For_Contact_Party_Level')) {
                Boolean addressFieldsChanged = checkIfFieldsChanged(oldLeadRec, newLeadRec, 'Address_Fields');
                Boolean phoneFieldsChanged = checkIfFieldsChanged(oldLeadRec, newLeadRec, 'Phone_Fields');
                Boolean emailFieldsChanged = checkIfFieldsChanged(oldLeadRec, newLeadRec, 'Email_Fields');
                Boolean dlFieldsChanged = checkIfFieldsChanged(oldLeadRec, newLeadRec, 'Drivers_License_Fields');
                Boolean addtlFieldsChanged = checkIfFieldsChanged(oldLeadRec, newLeadRec, 'Additional_Fields');
                
                Boolean companyChanged = false;
                if(String.isBlank(oldLeadRec?.Company) && String.isNotBlank(newLeadRec?.Company) || String.isNotBlank(oldLeadRec?.Company) && String.isBlank(newLeadRec?.Company)){
                	companyChanged = true;
                    //eventhough address has not changed, we have to post the new usage with address
                    addressFieldsChanged = true;
                }
                    
                if (phoneFieldsChanged) {
                    previousPhone = oldLeadRec.Phone;
                    previousPhoneUsage = oldLeadRec.Phone_Usage__c;
                }
                if (emailFieldsChanged) {
                    previousEmail = oldLeadRec.Email;
                    previousEmailUsage = oldLeadRec.Email_Usage__c;
                }
                  
                if(oldLeadRec.Party_Level__c == 'Entrant' && oldLeadRec.CDHID__c != null && oldLeadRec.Contact_CDHID__c == null) { // Is Entrant
                        wsPartyManageServiceCallout.PartyManageServiceSOAP partyManage = new wsPartyManageServiceCallout.PartyManageServiceSOAP();
                        // Party
                        wsPartyManageParty.PartyType party = wsPartymanage.constructParty(newLeadRec);
                        party.SourceSystemInformation.sourceSystemKey = String.isBlank(oldLeadRec.Source_System_Key__c)?String.valueOf(oldLeadRec.id):oldLeadRec.Source_System_Key__c;  
                        
                    // Construct Entrant
                    wsPartyManageParty.EntrantType entrant = wsPartyManage.constructEntrantForEdit(newLeadRec, addressFieldsChanged, phoneFieldsChanged, emailFieldsChanged, dlFieldsChanged, addtlFieldsChanged, selectedAddress);
                    party.Entrant = entrant;
                    system.debug(entrant);
                    party.partyVersion = oldLeadRec.Party_Version__c;
                    party.partyIdentifier = oldLeadRec.CDHID__c;
                    party.partnerIdentifier = oldLeadRec.Partner__c;
                            
                    // EditPartyRequest
                    wsPartyManageService.EditPartyRequestType editPartyRequest = wsPartyManage.constructEditPartyRequest(newLeadRec, oldLeadRec, party, addressFieldsChanged, phoneFieldsChanged, emailFieldsChanged, dlFieldsChanged, addtlFieldsChanged, previousPhone, previousPhoneUsage, previousEmail, previousEmailUsage, companyChanged);
                    editPartyRequest.code1Complete = code1Complete;
                    editPartyRequest.applyPartyMatching = applyPartyMatch;
                        
                    // Callout
                    wsPartyManageService.EditPartyResultType resp = partyManage.editParty_http(editPartyRequest);
                    System.debug('CDH Response');
                    System.debug(resp);
                    leadToUpdate = newLeadRec;
                        
                    if (resp.Party?.partyIdentifier != null && resp.Errors == null) {
                        leadToUpdate.id = oldLeadRec.Id;
                        leadToUpdate = parseAndPersistCDHResponse(leadToUpdate, resp.Party);
                        // Call Producer Association after Add Entrant is completed
                        ID jobID = System.enqueueJob(new AmFam_LeadAssociationToProducerQueueable(oldLeadRec));
                    }else{
                        LeadUpdate = false;
                    }
                    lw = new LeadWrapper(leadToUpdate, resp.Errors?.Error, resp.Code1Result, resp.PartyMatch, null);
                } else if (oldLeadRec.Party_Level__c == 'Contact'){
                    String msg = 'The lead you are attempting to update matches an existing contact. Please cancel your edit and convert the lead from the lead details screen.';
                    AuraHandledException e = new AuraHandledException(msg);
                    e.setMessage(msg);
                    throw e;
                }
            }
            //system.debug('lw>>' + lw.errors);
            leadToUpdate = newLeadRec;
            leadToUpdate.id = oldLeadRec.Id;
            if(LeadUpdate){update leadToUpdate;}
            return lw;
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public static Boolean checkIfFieldsChanged(Lead oldLead, Lead newLead, String fieldSetName) {
        List<Schema.FieldSetMember> fields = Schema.SObjectType.Lead.fieldSets.getMap().get(fieldSetName).getFields();
        Boolean keyFieldsChanged = false;
        for (Schema.FieldSetMember fsm : fields) {
            if (oldLead.get(fsm.getFieldPath()) != newLead.get(fsm.getFieldPath())) {
                keyFieldsChanged = true;
                break;
            }
        }
        return keyFieldsChanged;
    }

    public class LeadWrapper {
        public LeadWrapper(Lead leadRec, List<wsPartyManageParty.ErrorType> err, List<wsPartyManageParty.Code1ResultType> c1, List<wsPartyManageService.PartyMatchType> pM, List<wsPartyManageParty.HouseholdType> hM) {
            this.l = leadRec;
            this.errors = err;
            this.code1 = c1;
            this.householdMatch = hM;
            //added to distinguish residential and mailing address and delivery indicator to readable comment  
            if(c1 != null){
                this.returnedCode1Addresses = null;
                for(wsPartyManageParty.Code1ResultType code: c1){
                    
                    if(code.Code1Wrapper?.AddressesReturnedFromCode1 != null){
                        for(wsPartyManageParty.AddressType addressType: code.Code1Wrapper.AddressesReturnedFromCode1){
                            
                              if(addressType.deliveryPointValidationMatchIndicator == 'Y') {
                                 addressType.deliveryPointValidationMatchIndicator = 'Yes';
                              }else if(addressType.deliveryPointValidationMatchIndicator == 'N'){
                                 addressType.deliveryPointValidationMatchIndicator = 'Not a valid delivery point';    
                              }else if(addressType.deliveryPointValidationMatchIndicator == 'S'){
                                 addressType.deliveryPointValidationMatchIndicator = 'Unable to confirm validity of secondary number (e.g., apartment, suite, etc.)';    
                              }else if(addressType.deliveryPointValidationMatchIndicator == 'D'){
                                 addressType.deliveryPointValidationMatchIndicator = 'Not a valid delivery point. Missing secondary number (e.g., apartment, suite, etc.)';    
                              }else if(addressType.deliveryPointValidationMatchIndicator == 'M' || addressType.deliveryPointValidationMatchIndicator == 'U'){
                                 addressType.deliveryPointValidationMatchIndicator = 'Unable to confirm delivery point';    
                              }else{
                                  addressType.deliveryPointValidationMatchIndicator = '';
                              }
                            
                              if(addressType.deliveryPointValidationCommercialMailreceivingCode == 'Y') {
                                 addressType.deliveryPointValidationCommercialMailreceivingCode = 'Yes';
                              }else if(addressType.deliveryPointValidationCommercialMailreceivingCode == 'N'){
                                 addressType.deliveryPointValidationCommercialMailreceivingCode = 'No';      
                              }else if(addressType.deliveryPointValidationCommercialMailreceivingCode == 'U'){
                                 addressType.deliveryPointValidationCommercialMailreceivingCode = 'Unconfirmed'; 
                              }else{
                                  addressType.deliveryPointValidationCommercialMailreceivingCode = '';
                              }
                            
                            //combine Address Line 2 to Address Line 1
                            if(addressType.addressLine2 != null){
                                addressType.addressLine1 = addressType.addressLine1 +' '+ addressType.addressLine2;
                            }
                        } 
                     }
                    
                    this.returnedCode1Addresses = code;
                }
            }
            this.partyMatch = null;
            List<PartyMatch> potentialMatches = new List<PartyMatch>();
            List<PartyMatch> exactMatches = new List<PartyMatch>();
            if (pM != null) {
                
                for (wsPartyManageService.PartyMatchType pmt : pM) {
                    if (pmt.Match == 'EXACT') {
                        exactMatches.add(new PartyMatch(pmt));
                    } else{
                        potentialMatches.add(new PartyMatch(pmt));
                    }
                }
            }
            
            if (exactMatches.size() > 0) {
                this.partyMatch = exactMatches;
                this.exactMatches = true;
            } else {
                this.partyMatch = potentialMatches;
                this.exactMatches = false;
            } 
        }
        @AuraEnabled public Lead l {get;set;}
        @AuraEnabled public List<wsPartyManageParty.ErrorType> errors {get;set;}
        @AuraEnabled public List<wsPartyManageParty.Code1ResultType> code1 {get;set;}
        @AuraEnabled public List<PartyMatch> partyMatch {get;set;}
        @AuraEnabled public List<wsPartyManageParty.HouseholdType> householdMatch {get;set;}
        @AuraEnabled public Boolean exactMatches {get;set;}
        @AuraEnabled public wsPartyManageParty.Code1ResultType returnedCode1Addresses {get;set;}
        
    }
    
    public class PartyMatch {
        public PartyMatch(wsPartyManageService.PartyMatchType match) {
            this.matchType = match.Match;
            this.partyLevelCode = match.Party?.PartyLevelCode == 'ENTRANT'? 'LEAD':match.Party?.PartyLevelCode;
            if(match.Party?.Entrant != null){
                this.partyType = 'isLead';
                this.fullName = '';
                if(match.Party.Entrant?.EntrantDemographicInfo?.PersonName?.firstName!=null){
                    this.fullName += match.Party.Entrant?.EntrantDemographicInfo?.PersonName?.firstName;
                }
                if(match.Party.Entrant?.EntrantDemographicInfo?.PersonName?.middleName!=null){
                    this.fullName += ' '+match.Party.Entrant?.EntrantDemographicInfo?.PersonName?.middleName;
                }
                if(match.Party.Entrant?.EntrantDemographicInfo?.PersonName?.lastName!=null){
                    this.fullName += ' '+match.Party.Entrant?.EntrantDemographicInfo?.PersonName?.lastName;
                } 
                if(match.Party.Entrant?.EntrantDemographicInfo?.PersonName?.FamilySuffix!=null){
                    this.fullName += ' '+match.Party.Entrant?.EntrantDemographicInfo?.PersonName?.FamilySuffix;
                }
                if(match.Party.Entrant?.EntrantIdentityInfo?.DriversLicense != null){
                    this.dlicense =  match.Party.Entrant?.EntrantIdentityInfo?.DriversLicense?.licenseNumber;
                }
                // Age Calculation
                if (match.Party.Entrant?.EntrantDemographicInfo?.birthDate != null) {
                    Date today = Date.today();
                    Date bDate = Date.valueOf(match.Party.Entrant?.EntrantDemographicInfo?.birthDate);
                    Integer calculatedAge = today.year() - bdate.year();
                    if (today.dayOfYear() < bDate.dayOfYear()) {
                        calculatedAge = calculatedAge - 1;
                    }
                    this.age = calculatedAge;
                }
                this.companyName = match.Party.Entrant?.EntrantDemographicInfo?.OrganizationName?.organizationName; 
                this.cdhID = match.Party.partyIdentifier;
                this.isProducerAssociated = match.producerAssociationExists;
                this.lender = match.lender;
                if (match.Party?.Entrant?.PartyAddress != null) {
                        for (wsPartyManageParty.PartyAddressType addresses : match.Party.Entrant.PartyAddress) {
                            for (wsPartyManageParty.PartyAddressPurposeAndUsageType addressType : addresses.PartyAddressPurposeAndUsage) {
                                if (addressType.PartyAddressUsage == 'M' && addressType.PartyAddressPurpose == 'PRM') {
                                    this.primaryState = addresses.Address.stateCode;
                                    this.address = addresses.Address.addressLine1 + ' ' + addresses.Address.city + ', ' + addresses.Address.stateCode + ' ' + addresses.Address.zip5Code;
                                }
                            }
                        }
                }
            }
            
            if(match.Party?.Contact !=null){
                if (match.Party?.Contact?.PartyTypeCode == 'P') {
                    this.partyType = 'isPersonAccount';
                    this.fullName = '';
                    if(match.Party.Contact.Person?.PersonDemographicInfo?.Name?.firstName!=null){
                        this.fullName += match.Party.Contact.Person?.PersonDemographicInfo?.Name?.firstName;
                    }
                    if(match.Party.Contact.Person?.PersonDemographicInfo?.Name?.middleName!=null){
                        this.fullName += ' '+match.Party.Contact.Person?.PersonDemographicInfo?.Name?.middleName;
                    }
                    if(match.Party.Contact.Person?.PersonDemographicInfo?.Name?.lastName!=null){
                        this.fullName += ' '+match.Party.Contact.Person?.PersonDemographicInfo?.Name?.lastName;
                    } 
                    if(match.Party.Contact.Person?.PersonDemographicInfo?.Name?.FamilySuffix!=null){
                        this.fullName += ' '+match.Party.Contact.Person?.PersonDemographicInfo?.Name?.FamilySuffix;
                    }
                    if(match.Party.Contact.Person?.PersonIdentityInfo?.DriversLicense != null){
                        this.dlicense =  match.Party.Contact.Person?.PersonIdentityInfo?.DriversLicense?.licenseNumber;
                    }
                    // Age Calculation
                    if (match.Party?.Contact?.Person?.PersonDemographicInfo?.birthDate != null) {
                        Date today = Date.today();
                        Date bDate = Date.valueOf(match.Party.Contact.Person.PersonDemographicInfo.birthDate);
                        Integer calculatedAge = today.year() - bdate.year();
                        if (today.dayOfYear() < bDate.dayOfYear()) {
                            calculatedAge = calculatedAge - 1;
                        }
                        this.age = calculatedAge;
                    }
                    } else if (match.Party.Contact.PartyTypeCode == 'O') {
                        // HANDLE PARTY MATCH FOR BUSINESS ACCOUNT CALLOUT HERE
                        this.partyType = 'isBusinessAccount';
                        this.fullName = match.Party.Contact.Organization?.OrganizationDemographicInfo?.Name?.organizationName;
                        if (match.Party.Contact?.Organization?.OrganizationIdentityInfo != null) {
                            this.taxpayerId = match.Party.Contact?.Organization?.OrganizationIdentityInfo?.TIN?.tinNumber;
                        }
                        this.formOfBusiness = match.Party.Contact.Organization?.OrganizationDemographicInfo?.FormOfBusiness?.FormOfBusinessDescription;
                    }
                    this.cdhID = match.Party.partyIdentifier;
                    this.isProducerAssociated = match.producerAssociationExists;
                    this.lender = match.lender;
                    if (match.Party?.Contact?.PartyAddress != null) {
                        for (wsPartyManageParty.PartyAddressType addresses : match.Party.Contact.PartyAddress) {
                            for (wsPartyManageParty.PartyAddressPurposeAndUsageType addressType : addresses.PartyAddressPurposeAndUsage) {
                                if (addressType.PartyAddressUsage == 'M' && addressType.PartyAddressPurpose == 'PRM') {
                                    this.primaryState = addresses.Address.stateCode;
                                    this.address = addresses.Address.addressLine1 + ' ' + addresses.Address.city + ', ' + addresses.Address.stateCode + ' ' + addresses.Address.zip5Code;
                                }
                            }
                        }
                    }
            }
            
        }

        
        @AuraEnabled public String fullName {get;set;}
        @AuraEnabled public String companyName {get;set;}
        @AuraEnabled public String matchType {get;set;}
        @AuraEnabled public String dlicense {get;set;}
        @AuraEnabled public Integer age {get;set;}
        @AuraEnabled public String primaryState {get;set;}
        @AuraEnabled public String cdhID {get;set;}
        @AuraEnabled public Boolean isProducerAssociated {get;set;}
        @AuraEnabled public String partyType {get;set;}
        @AuraEnabled public String partyLevelCode {get;set;}
        @AuraEnabled public String address {get;set;}
        @AuraEnabled public String formOfBusiness {get;set;}
        @AuraEnabled public String taxpayerID {get;set;}
        @AuraEnabled public Boolean lender {get;set;}
    }
    
    @AuraEnabled
    public static String selectAccountPartyMatch(String selectedRow, Lead leadRecord, Boolean editLead, String partyType) {
        try{
            
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(selectedRow.replace('\"', '"'));
            
            Lead newLead = leadRecord;
            newLead.Party_Level__c = 'Contact';
            newLead.Contact_CDHID__c = (String)m.get('cdhID'); 
            
            newLead.Date_of_Birth__c = newLead?.Date_of_Birth__c != null ? Date.Valueof(newLead?.Date_of_Birth__c): null;
            // Try to refresh the lead record. Do not block the creation of the lead record.
            try {
            newLead = wsPartySearchHelper.refreshLead(newLead);
            } catch (Exception e) {
            System.debug('Caught Exception in refreshLead ' + e.getMessage());
            }
                        
            upsert newLead; 
            return newLead.id;
            
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
        
    } 
    
    @AuraEnabled
    public static String selectLeadPartyMatch(String selectedRow, Lead leadRecord, Boolean editLead) {
        
        try{
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(selectedRow);
            Lead existingLead = AmFam_Utility.getLeadByCDHID((String)m.get('cdhID'));
            Lead newLead = leadRecord;
            newLead.CDHID__c = (String)m.get('cdhID');
            newLead.Party_Level__c = 'Entrant';
            if(existingLead != null){
                newLead.Source_System_Key__c = String.isBlank(existingLead.Source_System_Key__c)?String.valueOf(existingLead.id):existingLead.Source_System_Key__c;  
                newLead.Party_Version__c = existingLead.Party_Version__c;
             }//if a lead find a exact match on edit and matching lead is not found in SF, then to avoid the source key conflict, 'E' has been added
             else if(newLead?.id != null && String.isBlank(newLead?.Source_System_Key__c)){
                newLead.Source_System_Key__c = 'E'+leadRecord?.id;
             }
             
            // Try to refresh the lead record. Do not block the creation of the lead record.
            try {
                newLead = wsPartySearchHelper.refreshLead(newLead);
            } catch (Exception e) {
                System.debug('Caught Exception in refreshLead ' + e.getMessage());
            }
             
            upsert newLead;
            if(newLead != null){
                AmFam_LeadAssociationToProducer.leadAssociationToProducer(newLead.id);
            }
            
            // Redirect to existing SF record
            return newLead.id;
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
        
     public static Lead parseAndPersistCDHResponse(Lead l, wsPartyManageParty.PartyType party) {
        l.Party_Level__c = party.PartyLevelCode;
        l.Party_Version__c = party.partyVersion;
        l.Partner__c = party.partnerIdentifier;
        // Entrant Specific Code
        if (party.PartyLevelCode == 'ENTRANT') {
            l.CDHID__c = party.partyIdentifier;
            if (party.Entrant?.EntrantDemographicInfo != null) {
                l.FirstName = party.Entrant?.EntrantDemographicInfo?.PersonName?.firstName;
                if(party.Entrant?.EntrantDemographicInfo?.PersonName?.middleName != null){
                	l.MiddleName = party.Entrant?.EntrantDemographicInfo?.PersonName?.middleName;    
                }
                l.LastName = party.Entrant?.EntrantDemographicInfo?.PersonName?.lastName;
                l.Company = party.Entrant?.EntrantDemographicInfo?.OrganizationName?.organizationName;
                if (party.Entrant?.EntrantDemographicInfo?.birthDate != null && party.Entrant?.EntrantDemographicInfo?.birthDate != '') {
                    l.Date_of_Birth__c = Date.valueOf(party.Entrant.EntrantDemographicInfo.birthDate);
                }
                l.Gender__c = party.Entrant?.EntrantDemographicInfo?.Gender;
                l.Marital_Status__c = party.Entrant?.EntrantDemographicInfo?.MaritalStatus;
            }
            if (party.Entrant?.EntrantIdentityInfo != null) {
                l.Driver_s_License_State__c = party.Entrant?.EntrantIdentityInfo?.DriversLicense?.licenseState;
                l.Drivers_License_Number__c = party.Entrant?.EntrantIdentityInfo?.DriversLicense?.licenseNumber;
                l.Drivers_License_Issue_Month__c = party.Entrant?.EntrantIdentityInfo?.DriversLicense?.OriginalLicenseDate?.licenseMonthString;
                l.Drivers_License_Issue_Year__c = party.Entrant?.EntrantIdentityInfo?.DriversLicense?.OriginalLicenseDate?.licenseYearString;
            }
            l.Primary_Language__c = Party.Entrant?.EntrantAdditionalInfo?.PrimaryLanguage?.code;
            // Addresses
            if (party?.Entrant?.PartyAddress != null) {
                l = AmFam_Utility.mapAddresses(party?.Entrant?.PartyAddress, l);
            }
            // Phones
            if (party?.Entrant?.PartyPhone != null) {
                l = AmFam_Utility.mapPhones(party?.Entrant?.PartyPhone, l);
            }
            // Emails
            if (party?.Entrant?.PartyEmail != null) {
                l = AmFam_Utility.mapEmails(party?.Entrant?.PartyEmail, l);
            }
        } else { // Contact Specific Code
            l.Contact_CDHID__c = party.partyIdentifier;
            if (party.Contact?.Person?.PersonDemographicInfo != null) {
                l.FirstName = party.Contact?.Person?.PersonDemographicInfo?.Name?.firstName;
                l.MiddleName = party.Contact?.Person?.PersonDemographicInfo?.Name?.middleName;
                l.LastName = party.Contact?.Person?.PersonDemographicInfo?.Name?.lastName;
                if (party.Contact?.Person?.PersonDemographicInfo?.birthDate != null && party.Contact?.Person?.PersonDemographicInfo?.birthDate != '') {
                    l.Date_of_Birth__c = Date.valueOf(party.Contact?.Person?.PersonDemographicInfo?.birthDate);
                }
                l.Gender__c = party.Contact?.Person?.PersonDemographicInfo?.Gender;
                l.Marital_Status__c = party.Contact?.Person?.PersonDemographicInfo?.MaritalStatus;
            }
            if (party.Contact?.Person?.PersonIdentityInfo != null) {
                l.Driver_s_License_State__c = party.Contact?.Person?.PersonIdentityInfo?.DriversLicense?.licenseState;
                l.Drivers_License_Number__c = party.Contact?.Person?.PersonIdentityInfo?.DriversLicense?.licenseNumber;
                l.Drivers_License_Issue_Month__c = party.Contact?.Person?.PersonIdentityInfo?.DriversLicense?.OriginalLicenseDate?.licenseMonthString;
                l.Drivers_License_Issue_Year__c = party.Contact?.Person?.PersonIdentityInfo?.DriversLicense?.OriginalLicenseDate?.licenseYearString;
            }
            l.Company = party.Contact?.Organization?.OrganizationDemographicInfo?.Name?.organizationName;
            l.Primary_Language__c = party.Contact?.PartyAdditionalInfo?.PrimaryLanguage?.code;
            // Addresses
            if (party?.Entrant?.PartyAddress != null) {
                l = AmFam_Utility.mapAddresses(party?.Contact?.PartyAddress, l);
            }
            // Phones
            if (party?.Entrant?.PartyPhone != null) {
                l = AmFam_Utility.mapPhones(party?.Contact?.PartyPhone, l);
            }
            // Emails
            if (party?.Entrant?.PartyEmail != null) {
                l = AmFam_Utility.mapEmails(party?.Contact?.PartyEmail, l);
            }
        }
        return l;
    }
}