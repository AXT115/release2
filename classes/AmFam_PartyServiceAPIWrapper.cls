public class AmFam_PartyServiceAPIWrapper {

	public class Status {
		public String maxMessageLevel;
		public Integer code;
		public String reason;
		public String transactionId;
	}

	public class Products {
		@AuraEnabled public String typeOfProductCode;
		@AuraEnabled public String sourceSystemProductIdentifier;
		@AuraEnabled public String productCode;
		@AuraEnabled public String productTypeDescription;
		@AuraEnabled public String policyTypeCode;
		@AuraEnabled public String sourceSystemName;
		@AuraEnabled public String contractState;
		@AuraEnabled public String servicingAgentProducerId;
		@AuraEnabled public String policyInForceOn;
		@AuraEnabled public List<Risk> risk;
		@AuraEnabled public String policyInForceIndicator;
		@AuraEnabled public String policyExpiresOn;
		@AuraEnabled public String agencyProducerId;
		@AuraEnabled public Boolean hasAccess;


	}

	public Status status;
	public List<Products> products;

	public class Risk {
		@AuraEnabled public String riskId;
		@AuraEnabled public String riskTypeCode2;
		@AuraEnabled public String riskTypeDesc;
		@AuraEnabled public String riskDescription;
	}

	public static AmFam_PartyServiceAPIWrapper parse(String json) {
		return (AmFam_PartyServiceAPIWrapper) System.JSON.deserialize(json, AmFam_PartyServiceAPIWrapper.class);
	}
}