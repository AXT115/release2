/* Class Name   : AmFam_ApiFailureLoggerTest
 * Description  : Test class ApiFailureLogger utility
 */

@isTest
private class AmFam_ApiFailureLoggerTest {
    static testMethod void createAPIFailureLogTest() {

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        String userExtId = 'test123';
        String extraParam = '?idIsExternalId=true&levelOfDetail=EXTENDED%2C%20STAFF%2C%20BOOK+OF+BUSINESS%2C%20MANAGERS';
        request.setEndpoint('callout:Producer_API/' + userExtId + extraParam);
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(AmFam_ProducerServiceTest.json);
        response.setStatusCode(200);
        AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
        failureLog.createAPIFailureLog('AmFam_ProducerServiceHandler', response, request.getEndpoint(), 'test123', 'No Response');
    }
    static testMethod void createAPIFailureLogTest1() {

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        String userExtId = 'test123';
        String extraParam = '?idIsExternalId=true&levelOfDetail=EXTENDED%2C%20STAFF%2C%20BOOK+OF+BUSINESS%2C%20MANAGERS';
        request.setEndpoint('callout:Producer_API/' + userExtId + extraParam);
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setStatusCode(200);
        AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
        failureLog.createAPIFailureLog('AmFam_ProducerServiceHandler', response, request.getEndpoint(), 'test123', 'No Response');
    }
    static testMethod void createAPIFailureLogTest2() {

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        String userExtId = 'test123';
        String extraParam = '?idIsExternalId=true&levelOfDetail=EXTENDED%2C%20STAFF%2C%20BOOK+OF+BUSINESS%2C%20MANAGERS';
        request.setEndpoint('callout:Producer_API/' + userExtId + extraParam);
        HttpResponse response;
        AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
        failureLog.createAPIFailureLog('AmFam_ProducerServiceHandler', response, request.getEndpoint(), 'test123', 'No Response');
    }

    @isTest static void createAPIFailureLogNoProducerReturnedTest() {

        String errorMessage = 'No Accounts found in SF related to producer API response';
        String API_URL = '/v1/producers/?idIsExternalId=true...';
        AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
        failureLog.createAPIFailureLogNoProducerReturned('AmFam_AccountLeadSharingModel', API_URL, 'test123', errorMessage);
    }
}