<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Life Cornerstone</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">https://intssg.amfam.com/saml/v1/ipipeline?initialTab=CaseList&amp;random=0000017a0ce96434-3e808d&amp;retry=1&amp;target=https://intssg.amfam.com/saml/v1/ipipeline</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">https://prdssg.amfam.com/saml/v1/ipipeline?initialTab=CaseList&amp;random=0000017b8c469fb2-2ca5e8&amp;retry=1&amp;target=https://prdssg.amfam.com/saml/v1/ipipeline</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">https://accssg.amfam.com/saml/v1/ipipeline?initialTab=CaseList&amp;random=00000179f7fd456c-3a703e&amp;retry=1&amp;target=https://accssg.amfam.com/saml/v1/ipipeline</value>
    </values>
    <values>
        <field>Source_System__c</field>
        <value xsi:type="xsd:string">Life_Cornerstone</value>
    </values>
</CustomMetadata>
