<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Commercial Classic</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">https://int1p92pc.amfam.com/pc/PolicyCenter.do</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">https://cfrpolicycenter.amfam.com/pc/PolicyCenter.do</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">https://acc1p92pc.amfam.com/pc/PolicyCenter.do</value>
    </values>
    <values>
        <field>Source_System__c</field>
        <value xsi:type="xsd:string">Commercial_Classic</value>
    </values>
</CustomMetadata>
