<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CLPolicyCenter</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">https://int1p92pc.amfam.com/pc/ViewQuote.do?sub=</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">https://cfrpolicycenter.amfam.com/pc/ViewQuote.do?sub=</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">https://acc1p92pc.amfam.com/pc/ViewQuote.do?sub=</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Quote</value>
    </values>
</CustomMetadata>
