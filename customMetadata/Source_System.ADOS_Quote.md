<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ADOS</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">http://sishome-int.application.amfam.com/sis/action/editSisEntity.do?sisEntityToEdit=</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">http://sis.amfam.com/sis/action/editSisEntity.do?sisEntityToEdit=</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">http://sishome-qa.application.amfam.com/sis/action/editSisEntity.do?sisEntityToEdit=</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Quote</value>
    </values>
</CustomMetadata>
