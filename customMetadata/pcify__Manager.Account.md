<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Account</label>
    <protected>false</protected>
    <values>
        <field>pcify__AuditAction__c</field>
        <value xsi:type="xsd:string">Update</value>
    </values>
    <values>
        <field>pcify__BatchEndDate__c</field>
        <value xsi:type="xsd:dateTime">2021-05-25T18:31:00.000Z</value>
    </values>
    <values>
        <field>pcify__BatchFilterField__c</field>
        <value xsi:type="xsd:string">CreatedDate</value>
    </values>
    <values>
        <field>pcify__BatchQueryLimit__c</field>
        <value xsi:type="xsd:double">50000.0</value>
    </values>
    <values>
        <field>pcify__BatchSize__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>pcify__BatchStartDate__c</field>
        <value xsi:type="xsd:dateTime">2021-05-25T18:31:00.000Z</value>
    </values>
    <values>
        <field>pcify__DetectionAction__c</field>
        <value xsi:type="xsd:string">Update</value>
    </values>
    <values>
        <field>pcify__DetectionFieldsPlus__c</field>
        <value xsi:type="xsd:string">JigsawCompanyId,Joint_Venture_Comment__c,Primary_Spoken_Language_Other__c,Pronunciation__pc,Representative__c,Secondary_Spoken_Language_Other__c,Sic,SicDesc,FinServ__ReviewFrequency__c,Representative_Position__c</value>
    </values>
    <values>
        <field>pcify__IsActive__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>pcify__LuhnOnly__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>pcify__MaskFields__c</field>
        <value xsi:type="xsd:string">Name,AccountSource,Description,Type,Agent_District_Code__c,FinServ__CurrentEmployer__pc,FinServ__CustomerID__c,PersonEmailBouncedReason,FEIN__c,Goes_By__pc,Greeting__pc,Important_Notes__c,FinServ__IndividualId__c,Source__c,Other_Comment__c,Party_Version__c</value>
    </values>
    <values>
        <field>pcify__MaskType__c</field>
        <value xsi:type="xsd:string">FullMask</value>
    </values>
    <values>
        <field>pcify__UseOwnTrigger__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
