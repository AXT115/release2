<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PropertyAdvance</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">https://producerengage-ctpint-auto.application.amfam.com/ProducerEngage/dist/html/index-agents.html#/apexquoteentry?</value>
    </values>
    <values>
        <field>Product_Policy_Code__c</field>
        <value xsi:type="xsd:string">Homeowners</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">https://producerengage.amfam.com/ProducerEngage/dist/html/index-agents.html#/apexquoteentry?</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">https://producerengage-ctpqa-auto.application.amfam.com/ProducerEngage/dist/html/index-agents.html#/apexquoteentry?</value>
    </values>
</CustomMetadata>
