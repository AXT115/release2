<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PropertyPlus</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">https://billintctp.amfam.com/BillingAccount/servlet/route;jsessionid=Azk6ebp+kaedzFxsqs1BdkJ3.server203ga?operation=View+Billing+History&amp;internalsso.basic.only=true&amp;billingNumberSearchString=</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">https://billingcenter.amfam.com/BillingAccount/servlet/route;jsessionid=FPduTihaxzbsRtOFYKhHkzpL.server218ga?operation=View+Billing+Summary&amp;billingNumberSearchString=</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">https://billqactp.amfam.com/BillingAccount/servlet/route?operation=View+Billing+History&amp;internalsso.basic.only=true&amp;billingNumberSearchString=</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Billing</value>
    </values>
</CustomMetadata>
