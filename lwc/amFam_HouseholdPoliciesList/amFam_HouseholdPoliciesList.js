import { LightningElement,wire,api } from 'lwc';
import getPolicies from '@salesforce/apex/AmFam_HouseholdPolicyListController.getPolicies';
import { getRecord } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import RECORDTYPEID from '@salesforce/schema/Account.RecordTypeId';

const _FIELDS = [RECORDTYPEID];

const policyColumns = [
    {label: 'Policy #',fieldName: 'policyURL',type: 'url',typeAttributes: {label: { fieldName: 'name' }, 
    target: '_parent'},hideDefaultActions:"true"},
    {label: 'Premium', fieldName: 'premiumAmount', type: 'currency', hideDefaultActions:"true", typeAttributes: { currencyCode: 'USD', step: '0.01'}},
    {label: 'Type', fieldName: 'policyType', type: 'text', hideDefaultActions:"true"},
    {label: 'Status', fieldName: 'status', type: 'text', hideDefaultActions:"true"},
    {label: 'Billing',fieldName: 'billingAccountURL',type: 'url',typeAttributes: {label: { fieldName: 'billingAccount' }, 
        target: '_parent'},hideDefaultActions:"true"},
    {label: 'Expiration', fieldName: 'expirationDate', type: 'text', hideDefaultActions:"true"},
    //Agent
    {label: 'Cancellation', fieldName: 'cancellationDate', type: 'text', hideDefaultActions:"true"},
    {label: 'Producer',fieldName: 'producerAccountURL',type: 'url',typeAttributes: {label: { fieldName: 'producerAccountName' }, 
    target: '_parent'},hideDefaultActions:"true"},
];

const additionalColumns = [
    {label: 'Type', fieldName: 'policyType', type: 'text', hideDefaultActions:"true"},
    {label: 'Status', fieldName: 'status', type: 'text', hideDefaultActions:"true"},
    {label: 'Expiration', fieldName: 'expirationDate', type: 'text', hideDefaultActions:"true"},
    {label: 'Cancellation', fieldName: 'cancellationDate', type: 'text', hideDefaultActions:"true"},
    {label: 'Producer',fieldName: 'producerAccountURL',type: 'url',typeAttributes: {label: { fieldName: 'producerAccountName' }, 
    target: '_parent'},hideDefaultActions:"true"},
];

export default class AmFam_HouseholdPoliciesList extends LightningElement {
    @api recordId;
    policies;
    additionalPolicies;
    objectInfo;
    recordTypeId;

    policyColumns = policyColumns;
    additionalColumns = additionalColumns;

    @wire(getObjectInfo,  { objectApiName: ACCOUNT_OBJECT })
    wiredObjectInfo({data, error})
    {
        if (data) {
            this.objectInfo = data
        } 
        else if (error) 
        {
            console.log('Error on @wire for account obj info - '+error);
        }
    }
    objectInfo;

    @wire(getRecord, { recordId: '$recordId', fields: _FIELDS })
    wiredRecord({ data, error }) {
        if (data) {
            let record = data;
            this.recordTypeId = record.fields.RecordTypeId.value;
        } 
        else if (error) 
        {
            console.log('Error on @wire for record '+this.recordId+' - '+error);
        }
    }

    @wire(getPolicies, { accountId: '$recordId' })
    wirePolicies({ error, data }) 
    {
        if (data)
        {
            //this.policies = data['policies'];
            this.additionalPolicies = data['additional'];

            var sorted = [...data['policies']].sort(function(a, b){
                if (a.name < b.name)
                {
                    return -1;
                } else if (a.name > b.name)
                {
                    return 1;
                }
                return 0;
            });

            this.policies = sorted;
        }
    }

    get showPolicies() 
    {
        var result = false

        if (this.objectInfo != null && this.recordTypeId != null)
        {
            //const rtis = this.objectInfo.data.recordTypeInfos;
            const rtis = this.objectInfo.recordTypeInfos;
            const rtInfo= Object.keys(rtis).find(rti => rtis[rti].name === 'Household');

            if(this.recordTypeId === rtInfo)
            {
                result = true;
            }
        }

        return result;
    }


}