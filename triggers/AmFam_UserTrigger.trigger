/**
*   @description  trigger for User object. Note: this should be the ONLY trigger created for User object.
*  Additional changes to trigger logic should be added/modified with the handler class
*
*   @author Salesforce Services
*   @date 05/13/2020
*   @group Trigger
*/
trigger AmFam_UserTrigger on User (before insert, before update, after insert, after update) {

    //Activate and Deactivate trigger using Custom Metadata
    List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt
                                                WHERE DeveloperName = 'User_Object' LIMIT 1];

    Boolean trigActive = true;
    if (triggerSwitch.size() > 0) {
        trigActive = triggerSwitch[0].isActive__c;
        if (!trigActive) {
            return;
        }
    }

    AmFam_UserTriggerHandler handler = new AmFam_UserTriggerHandler(); 
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            handler.OnBeforeInsert(trigger.New);
        } else {
            handler.OnAfterInsert(trigger.New,trigger.NewMap);
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            handler.OnBeforeUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        } else {
            handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    }
}