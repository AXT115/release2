trigger AmFam_ProducerTrigger on Producer (after insert, before delete, after delete) {

    //********************************************************
    //Activate and Deactivate trigger using Custom Metadata
    //********************************************************

    List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt
                                                WHERE DeveloperName = 'Producer_Object' LIMIT 1];

    Boolean trigActive = true;
    if (triggerSwitch.size() > 0) {
        trigActive = triggerSwitch[0].isActive__c;
        if (!trigActive) {
            return;
        }
    }


    //********************************************************
    //Dispatch Trigger excution 
    //********************************************************

    AmFam_ProducerTriggerHandler handler = new AmFam_ProducerTriggerHandler();

    if (Trigger.isInsert) {
        if (Trigger.isAfter) {
            handler.OnAfterInsert(trigger.New);
        }
    } else if (Trigger.isDelete) {
        if (Trigger.isAfter) 
        {
            handler.OnAfterDelete(trigger.Old,Trigger.OldMap);
        }
        else 
        {
            handler.OnBeforeDelete(trigger.Old,Trigger.OldMap);
        }
     }




}