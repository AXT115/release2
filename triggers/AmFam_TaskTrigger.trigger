trigger AmFam_TaskTrigger on Task (before insert, before update, after insert, after update) {
    AmFam_TaskTriggerHandler handler = new AmFam_TaskTriggerHandler();

    if( Trigger.isInsert ) {
        if(Trigger.isBefore) {
            handler.OnBeforeInsert(trigger.New);
        } else {
            handler.OnAfterInsert(trigger.New);
        }
    }
}