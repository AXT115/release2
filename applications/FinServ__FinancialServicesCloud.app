<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-Account</defaultLandingTab>
    <description>Build client trust with collaboration and productivity tools for advisory teams</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Financial Services Cloud (Classic)</label>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>FinServ__FinancialAccount__c</tabs>
    <tabs>FinServ__AssetsAndLiabilities__c</tabs>
    <tabs>FinServ__FinancialGoal__c</tabs>
    <tabs>FinServ__FinancialHolding__c</tabs>
    <tabs>FinServ__Securities__c</tabs>
    <tabs>FinServ__ReciprocalRole__c</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
</CustomApplication>
